-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 12 juil. 2024 à 10:32
-- Version du serveur : 8.2.0
-- Version de PHP : 8.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

/*
		Pourquoi utiliser l'interclassement utf8mb4_general_ci ?
https://stackoverflow.com/questions/2344118/utf-8-general-bin-unicode
https://stackoverflow.com/questions/30074492/what-is-the-difference-between-utf8mb4-and-utf8-charsets-in-mysql

		Résumé :
		Utf8 est un codage de longueur de variable.
		On utilise utf8mb4 pour stocker tous les types de caractère possible.
		Exemple : Les emojis sont possible d'être stocké via utf8mb4 et non pas le utf8mb3
		Ps : Le tag mb3 ou mb4 c'est la plage de caractère qu'on peut stocker.
				Exemple mb3 : va de 0x000 à 0xFFF.

		Le classement general_ci a été choisis car les opérations sont souvent plus rapide que les autres.
		Pour gagner en précision, on peut utiliser unicode_ci.
 */


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `dunkdatabddv2`
--
-- --------------------------------------------------------

--
-- Structure de la table `coach`
--

DROP TABLE IF EXISTS `coach`;
CREATE TABLE IF NOT EXISTS `coach` (
	`id` int NOT NULL AUTO_INCREMENT,
	`human_id` int NOT NULL,
	`franchise_id` int NOT NULL,
	`title` varchar(50) NOT NULL,
    `in_date` date NULL,
    `out_date` date NULL,
	PRIMARY KEY (`id`),
	KEY `fk_human_id_coach` (`human_id`),
	KEY `fk_franchise_id_coach` (`franchise_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `coach`
--

INSERT INTO `coach` (`id`, `human_id`, `franchise_id`, `title`) VALUES
(1, 107, 4, 'main_coach'),
(2, 109, 5, 'main_coach'),
(3, 33, 1, 'main_coach'),
(4, 110, 6, 'main_coach'),
(5, 55, 2, 'main_coach'),
(6, 74, 3, 'main_coach'),
(7, 18, 1, 'assistant_coach'),
(8, 35, 2, 'assistant_coach'),
(9, 57, 3, 'assistant_coach'),
(10, 76, 4, 'assistant_coach'),
(11, 108, 5, 'assistant_coach'),
(12, 111, 6, 'assistant_coach');

-- --------------------------------------------------------
--
-- Structure de la table `history_team_player_foul`
--

DROP TABLE IF EXISTS `history_team_player_foul`;
CREATE TABLE IF NOT EXISTS `history_team_player_foul` (
	`id` int NOT NULL AUTO_INCREMENT,
	`team_player_id` int NOT NULL,
	`foul_type_id` int NOT NULL,
	`foul_time` varchar(10) NOT NULL,
	PRIMARY KEY (`id`),
	KEY `fk_team_player_id_history_team_player_foul` (`team_player_id`),
	KEY `fk_foul_type_id_history_team_player_foul` (`foul_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `history_team_player_foul`
--

INSERT INTO `history_team_player_foul` (`id`, `foul_type_id`, `team_player_id`, `foul_time`) VALUES
(1, 1, 1, '32:00'),
(2, 1, 2, '45:00'),
(3, 1, 3, '10:00'),
(4, 1, 4, '12:00'),
(5, 1, 5, '50:00'),
(6, 1, 6, '05:00'),
(7, 1, 7, '23:00'),
(8, 1, 8, '47:00'),
(9, 1, 9, '15:00'),
(10, 1, 10, '34:00'),
(11, 2, 1, '11:00'),
(12, 2, 2, '39:00'),
(13, 2, 3, '02:00'),
(14, 2, 4, '27:00'),
(15, 2, 5, '53:00'),
(16, 2, 6, '18:00'),
(17, 2, 7, '35:00'),
(18, 2, 8, '59:00'),
(19, 2, 9, '22:00'),
(20, 2, 10, '14:00'),
(21, 3, 1, '41:00'),
(22, 3, 2, '09:00'),
(23, 3, 3, '18:00'),
(24, 3, 4, '48:00'),
(25, 3, 5, '16:00'),
(26, 3, 6, '24:00'),
(27, 3, 7, '55:00'),
(28, 3, 8, '21:00'),
(29, 3, 9, '32:00'),
(30, 3, 10, '45:00'),
(31, 4, 1, '10:00'),
(32, 4, 2, '12:00'),
(33, 4, 3, '50:00'),
(34, 4, 4, '05:00'),
(35, 4, 5, '23:00'),
(36, 4, 6, '47:00'),
(37, 4, 7, '15:00'),
(38, 4, 8, '34:00'),
(39, 4, 9, '11:00'),
(40, 4, 10, '39:00'),
(41, 5, 1, '02:00'),
(42, 5, 2, '27:00'),
(43, 5, 3, '53:00'),
(44, 5, 4, '18:00'),
(45, 5, 5, '35:00'),
(46, 5, 6, '59:00'),
(47, 5, 7, '22:00'),
(48, 5, 8, '14:00'),
(49, 5, 9, '41:00'),
(50, 5, 10, '09:00'),
(51, 6, 1, '18:00'),
(52, 6, 2, '48:00'),
(53, 6, 3, '16:00'),
(54, 6, 4, '24:00'),
(55, 6, 5, '55:00'),
(56, 6, 6, '21:00'),
(57, 6, 7, '32:00'),
(58, 6, 8, '45:00'),
(59, 6, 9, '10:00'),
(60, 6, 10, '12:00'),
(61, 7, 1, '50:00'),
(62, 7, 2, '05:00'),
(63, 7, 3, '23:00'),
(64, 7, 4, '47:00'),
(65, 7, 5, '15:00'),
(66, 7, 6, '34:00'),
(67, 7, 7, '11:00'),
(68, 7, 8, '39:00'),
(69, 7, 9, '02:00'),
(70, 7, 10, '27:00'),
(71, 8, 1, '53:00'),
(72, 8, 2, '18:00'),
(73, 8, 3, '35:00'),
(74, 8, 4, '59:00'),
(75, 8, 5, '22:00'),
(76, 8, 6, '14:00'),
(77, 8, 7, '41:00'),
(78, 8, 8, '09:00'),
(79, 8, 9, '18:00'),
(80, 8, 10, '48:00'),
(81, 9, 1, '16:00'),
(82, 9, 2, '24:00'),
(83, 9, 3, '55:00'),
(84, 9, 4, '21:00'),
(85, 9, 5, '32:00'),
(86, 9, 6, '45:00'),
(87, 9, 7, '10:00'),
(88, 9, 8, '12:00'),
(89, 9, 9, '50:00'),
(90, 9, 10, '05:00'),
(91, 10, 1, '23:00'),
(92, 10, 2, '47:00'),
(93, 10, 3, '15:00'),
(94, 10, 4, '34:00'),
(95, 10, 5, '11:00'),
(96, 10, 6, '39:00'),
(97, 10, 7, '02:00'),
(98, 10, 8, '27:00'),
(99, 10, 9, '53:00'),
(100, 10, 10, '18:00'),
(101, 11, 1, '35:00'),
(102, 11, 2, '59:00'),
(103, 11, 3, '22:00'),
(104, 11, 4, '14:00'),
(105, 11, 5, '41:00'),
(106, 11, 6, '09:00'),
(107, 11, 7, '18:00'),
(108, 11, 8, '48:00'),
(109, 11, 9, '16:00'),
(110, 11, 10, '24:00'),
(111, 12, 1, '55:00'),
(112, 12, 2, '21:00'),
(113, 12, 3, '32:00'),
(114, 12, 4, '45:00'),
(115, 12, 5, '10:00'),
(116, 12, 6, '12:00'),
(117, 12, 7, '50:00'),
(118, 12, 8, '05:00'),
(119, 12, 9, '23:00'),
(120, 12, 10, '47:00'),
(121, 13, 1, '15:00'),
(122, 13, 2, '34:00'),
(123, 13, 3, '11:00'),
(124, 13, 4, '39:00'),
(125, 13, 5, '02:00'),
(126, 13, 6, '27:00'),
(127, 13, 7, '53:00'),
(128, 13, 8, '18:00'),
(129, 13, 9, '35:00'),
(130, 13, 10, '59:00'),
(131, 14, 1, '22:00'),
(132, 14, 2, '14:00'),
(133, 14, 3, '41:00'),
(134, 14, 4, '09:00'),
(135, 14, 5, '18:00'),
(136, 14, 6, '48:00'),
(137, 14, 7, '16:00'),
(138, 14, 8, '24:00'),
(139, 14, 9, '55:00'),
(140, 14, 10, '21:00'),
(141, 15, 1, '32:00'),
(142, 15, 2, '45:00'),
(143, 15, 3, '10:00'),
(144, 15, 4, '12:00'),
(145, 15, 5, '50:00'),
(146, 15, 6, '05:00'),
(147, 15, 7, '23:00'),
(148, 15, 8, '47:00'),
(149, 15, 9, '15:00'),
(150, 15, 10, '34:00'),
(151, 16, 1, '11:00'),
(152, 16, 2, '39:00'),
(153, 16, 3, '02:00'),
(154, 16, 4, '27:00'),
(155, 16, 5, '53:00'),
(156, 16, 6, '18:00'),
(157, 16, 7, '35:00'),
(158, 16, 8, '59:00'),
(159, 16, 9, '22:00'),
(160, 16, 10, '14:00');

-- --------------------------------------------------------

--
-- Structure de la table `history_substitute`
--

DROP TABLE IF EXISTS `history_substitute`;
CREATE TABLE IF NOT EXISTS `history_substitute` (
	`id` int NOT NULL AUTO_INCREMENT,
	`incoming_team_player_id` int NOT NULL,
	`outgoing_team_player_id` int NOT NULL,
	`substitute_time` varchar(10) NOT NULL,
	PRIMARY KEY (`id`),
	KEY `fk_incoming_team_player_id_history_substitute` (`incoming_team_player_id`),
	KEY `fk_outgoing_team_player_id_history_substitute` (`outgoing_team_player_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `replacement`
--

INSERT INTO `history_substitute` (`id`, `incoming_team_player_id`, `outgoing_team_player_id`, `substitute_time`) VALUES
(1, 1, 11, '15:00'),
(2, 2, 12, '45:00'),
(3, 3, 13, '15:00'),
(4, 4, 14, '20:00'),
(5, 5, 15, '50:00'),
(6, 6, 16, '20:00'),
(7, 7, 17, '15:00'),
(8, 8, 18, '45:00'),
(9, 9, 19, '15:00'),
(10, 10, 20, '20:00');

-- --------------------------------------------------------

--
-- Structure de la table `game_arbitrator`
--

DROP TABLE IF EXISTS `game_arbitrator`;
CREATE TABLE IF NOT EXISTS `game_arbitrator` (
	`id` int NOT NULL AUTO_INCREMENT,
	`game_id` int NOT NULL,
	`arbitrator_id` int NOT NULL,
	`title` varchar(50) NOT NULL,
	PRIMARY KEY (`id`),
	KEY `fk_game_id_game_arbitrator` (`game_id`),
	KEY `fk_arbitrator_id_game_arbitrator` (`arbitrator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `game_arbitrator`
--

INSERT INTO `game_arbitrator` (`id`, `game_id`, `arbitrator_id`, `title`) VALUES
(1, 1, 1, 'holder'),
(2, 2, 2, 'assistant'),
(3, 3, 3, 'assistant');

-- --------------------------------------------------------

--
-- Structure de la table `history_team_player_point`
--

DROP TABLE IF EXISTS `history_team_player_point`;
CREATE TABLE IF NOT EXISTS `history_team_player_point` (
	`id` int NOT NULL AUTO_INCREMENT,
	`team_player_id` int NOT NULL,
	`point_type_id` int NOT NULL,
	`point_time` varchar(10) NOT NULL,
	PRIMARY KEY (`id`),
	KEY `fk_team_player_id_history_team_player_point` (`team_player_id`),
	KEY `fk_point_type_id_history_team_player_point` (`point_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `history_team_player_point`
--

INSERT INTO `history_team_player_point` (`id`, `team_player_id`, `point_type_id`, `point_time`) VALUES
(1, 1, 1, '00:01'),
(2, 1, 2, '00:02'),
(3, 2, 3, '00:03'),
(4, 2, 1, '00:04'),
(5, 3, 2, '00:05'),
(6, 4, 3, '00:06'),
(7, 5, 1, '00:07'),
(8, 6, 2, '00:08');

-- --------------------------------------------------------

--
-- Structure de la table `team_player`
--

DROP TABLE IF EXISTS `team_player`;
CREATE TABLE IF NOT EXISTS `team_player` (
	`id` int NOT NULL AUTO_INCREMENT,
	`team_id` int NOT NULL,
	`player_id` int NOT NULL,
	`post` varchar(20) NOT NULL,
	`title` varchar(20)	NOT NULL,
	PRIMARY KEY (`id`),
	KEY `fk_team_id_team_player` (`team_id`),
	KEY `fk_player_id_team_player` (`player_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Déchargement des données de la table `team_player`
--
-- Post :
-- Meneur => 'leader'
-- Arrière => 'back'
-- Ailier => 'winger'
-- Ailier fort => 'strong_winger'
-- Pivot => 'pivot'
-- Player Title :
-- remplacant => 'substitute'
-- captaine => 'captain'
-- capitaine suppléant => 'alternate_captain'

INSERT INTO `team_player` (`id`, `team_id`, `player_id`, `post`, `title`) VALUES
(1, 1, 1, 'leader', 'captain'),
(2, 1, 2, 'back', 'alternate_captain'),
(3, 1, 3, 'winger', 'player'),
(4, 1, 4, 'strong_winger', 'player'),
(5, 1, 5, 'pivot', 'player'),
(6, 1, 6, 'substitute', 'player'),
(7, 1, 7, 'substitute', 'player'),
(8, 1, 8, 'substitute', 'player'),
(9, 1, 9, 'substitute', 'player'),
(10, 1, 10, 'substitute', 'player'),
(11, 1, 11, 'substitute', 'player'),
(12, 1, 12, 'substitute', 'player'),
(13, 1, 13, 'substitute', 'player'),
(14, 1, 14, 'substitute', 'player'),
(15, 1, 15, 'substitute', 'player'),
(16, 1, 16, 'substitute', 'player'),
(17, 1, 17, 'substitute', 'player'),
(18, 1, 18, 'substitute', 'player'),
(19, 2, 19, 'substitute', 'player'),
(20, 2, 20, 'substitute', 'player'),
(21, 2, 21, 'substitute', 'player'),
(22, 2, 22, 'substitute', 'player'),
(23, 2, 23, 'substitute', 'player');

-- --------------------------------------------------------

--
-- Structure de la table `team`
--

DROP TABLE IF EXISTS `team`;
CREATE TABLE IF NOT EXISTS `team` (
	`id` int NOT NULL AUTO_INCREMENT,
	`game_id` int NOT NULL,
	`franchise_id` int NOT NULL,
	`is_home` boolean NOT NULL DEFAULT 0,
    `score` int NULL,
	PRIMARY KEY (`id`),
	KEY `fk_franchise_id_team` (`franchise_id`),
	KEY `fk_game_id_team` (`game_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `team`
--
INSERT INTO `team` (`id`, `game_id`, `franchise_id`, `is_home`, `score`) VALUES
(1, 1, 1, 1, 120),
(2, 2, 2, 1, 118),
(3, 3, 3, 1, 136),
(4, 1, 4, 0, 95),
(5, 2, 5, 0, 114),
(6, 3, 6, 0, 139);

-- --------------------------------------------------------

--
-- Structure de la table `game`
--

DROP TABLE IF EXISTS `game`;
CREATE TABLE IF NOT EXISTS `game` (
    `id` int NOT NULL AUTO_INCREMENT,
    `begin_date_time` DATETIME NOT NULL,
    `place_id` int NOT NULL,
    `game_time` varchar(10) NULL,
    PRIMARY KEY (`id`),
    KEY `fk_place_id_game` (`place_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `game`
--

INSERT INTO `game` (`id`, `begin_date_time`, `place_id`, `game_time`) VALUES
(1, '2024-06-18 16:00:00', 1, '48:00'),
(2, '2024-06-19 16:00:00', 2, '48:00'),
(3, '2024-06-20 16:00:00', 3, '52:00');

-- --------------------------------------------------------

--
-- Structure de la table `player`
--

DROP TABLE IF EXISTS `player`;
CREATE TABLE IF NOT EXISTS `player` (
	`id` int NOT NULL AUTO_INCREMENT,
	`human_id` int NOT NULL,
	`jersey_number` int NOT NULL,
	`main_post` varchar(50) NOT NULL,
	`franchise_id` int DEFAULT NULL,
    `in_date` DATE NULL,
    `out_date` DATE NULL,
	PRIMARY KEY (`id`),
	KEY `fk_human_id_player` (`human_id`),
	KEY `fk_franchise_id_player` (`franchise_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `player`
--
-- Post :
-- Meneur => 'leader'
-- Arrière => 'back'
-- Ailier => 'winger'
-- Ailier fort => 'strong_winger'
-- Pivot => 'pivot'

INSERT INTO `player` (`id`, `jersey_number`, `human_id`, `main_post`, `franchise_id`) VALUES
(1, 1, 1, 'leader', 1),
(2, 2, 2, 'leader', 1),
(3, 26, 3, 'back', 1),
(4, 0, 4, 'back', 1),
(5, 4, 5, 'back', 1),
(6, 15, 6, 'back', 1),
(7, 21, 7, 'winger', 1),
(8, 8, 8, 'winger', 1),
(9, 6, 9, 'winger', 1),
(10, 12, 10, 'winger', 1),
(11, 3, 11, 'strong_winger', 1),
(12, 32, 12, 'strong_winger', 1),
(13, 14, 13, 'pivot', 1),
(14, 20, 14, 'pivot', 1),
(15, 30, 15, 'leader', 2),
(16, 3, 16, 'leader', 2),
(17, 55, 17, 'back', 2),
(18, 26, 19, 'back', 2),
(19, 26, 20, 'back', 2),
(20, 8, 21, 'back', 2),
(21, 4, 22, 'back', 2),
(22, 11, 23, 'winger', 2),
(23, 22, 24, 'winger', 2),
(24, 0, 25, 'strong_winger', 2),
(25, 16, 26, 'strong_winger', 2),
(26, 23, 27, 'strong_winger', 2),
(27, 20, 28, 'strong_winger', 2),
(28, 32, 29, 'pivot', 2),
(29, 5, 30, 'pivot', 2),
(30, 10, 31, 'leader', 3),
(31, 9, 32, 'leader', 3),
(32, 45, 36, 'back', 3),
(33, 11, 34, 'back', 3),
(34, 22, 37, 'back', 3),
(35, 35, 38, 'winger', 3),
(36, 8, 39, 'back', 3),
(37, 1, 40, 'back', 3),
(38, 21, 41, 'winger', 3),
(39, 32, 42, 'strong_winger', 3),
(40, 3, 43, 'strong_winger', 3),
(41, 20, 44, 'winger', 3),
(42, 4, 45, 'pivot', 3),
(43, 31, 46, 'pivot', 3),
(44, 30, 47, 'pivot', 3),
(45, 13, 98, 'strong_winger', 3),
(46, 1, 116, 'leader', 4),
(47, 25, 99, 'winger', 4),
(48, 7, 103, 'winger', 4),
(49, 4, 104, 'leader', 4),
(50, 14, 115, 'back', 4),
(51, 22, 105, 'winger', 4),
(52, 24, 106, 'winger', 4),
(53, 3, 48, 'strong_winger', 4),
(54, 0, 49, 'strong_winger', 4),
(55, 10, 50, 'strong_winger', 4),
(56, 26, 51, 'pivot', 4),
(57, 23, 52, 'strong_winger', 4),
(58, 54, 100, 'strong_winger', 4),
(59, 32, 114, 'pivot', 4),
(60, 28, 101, 'strong_winger', 4),
(61, 20, 113, 'leader', 5),
(62, 4, 54, 'leader', 5),
(63, 11, 56, 'leader', 5),
(64, 9, 58, 'leader', 5),
(65, 44, 59, 'back', 5),
(66, 7, 53, 'winger', 5),
(67, 9, 60, 'winger', 5),
(68, 30, 82, 'winger', 5),
(69, 50, 62, 'winger', 5),
(70, 13, 63, 'strong_winger', 5),
(71, 27, 64, 'strong_winger', 5),
(72, 12, 65, 'strong_winger', 5),
(73, 0, 66, 'strong_winger', 5),
(74, 42, 67, 'strong_winger', 5),
(75, 8, 68, 'pivot', 5),
(76, 26, 69, 'strong_winger', 5),
(77, 40, 70, 'pivot', 5),
(78, 88, 71, 'pivot', 5),
(79, 0, 72, 'leader', 6),
(80, 23, 73, 'leader', 6),
(81, 30, 75, 'leader', 6),
(82, 22, 77, 'leader', 6),
(83, 3, 78, 'leader', 6),
(84, 10, 79, 'back', 6),
(85, 11, 80, 'winger', 6),
(86, 7, 81, 'winger', 6),
(87, 14, 83, 'winger', 6),
(88, 24, 84, 'winger', 6),
(89, 0, 85, 'winger', 6),
(90, 2, 86, 'winger', 6),
(91, 21, 87, 'strong_winger', 6),
(92, 8, 88, 'strong_winger', 6),
(93, 17, 89, 'winger', 6),
(94, 12, 90, 'pivot', 6),
(95, 4, 91, 'pivot', 6),
(96, 5, 92, 'pivot', 6);



-- --------------------------------------------------------

--
-- Structure de la table `place`
--

DROP TABLE IF EXISTS `place`;
CREATE TABLE IF NOT EXISTS `place` (
	`id` int NOT NULL AUTO_INCREMENT,
	`gym` varchar(50) NOT NULL,
	`address` varchar(100) NOT NULL,
	`city` varchar(50) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `place`
--

INSERT INTO `place` (`id`, `gym`, `address`, `city`) VALUES
(1, 'Crypto.com Arena', '1111 S. Figueroa Street, CA 90015', 'Los Angeles'),
(2, 'Chase Center', '1 Warriors Way, CA 94158', 'San Francisco'),
(3, 'Rocket Mortgage FieldHouse', '1 Center Court, OH 44115', 'Cleveland'),
(4, 'Frost Bank Center', '1 Frost Bank Center Drive, TX 78219,', 'San Antonio'),
(5, 'Td Garden', '100 Legends Way, MA 02114', 'Boston'),
(6, 'Spectrum Center', '333 E Trade Street, NC 28202', 'Charlotte');

-- --------------------------------------------------------

--
-- Structure de la table `post`
--

-- DROP TABLE IF EXISTS `post`;
-- CREATE TABLE IF NOT EXISTS `post` (
--	 `id` int NOT NULL AUTO_INCREMENT,
--	 `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
--	 PRIMARY KEY (`PostId`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `post`
--

-- INSERT INTO `post` (`PostId`, `Name`) VALUES
-- (1, 'leader'),
-- (2, 'back'),
-- (3, 'winger'),
-- (4, 'strong_winger'),
-- (5, 'pivot');

-- Post :
-- Meneur => 'leader'
-- Arrière => 'back'
-- Ailier => 'winger'
-- Ailier fort => 'strong_winger'
-- Pivot => 'pivot'
-- --------------------------------------------------------

--
-- Structure de la table `foul_type`
--

DROP TABLE IF EXISTS `foul_type`;
CREATE TABLE IF NOT EXISTS `foul_type` (
	`id` int NOT NULL AUTO_INCREMENT,
	`name` varchar(100) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `foul_type`
--

INSERT INTO `foul_type` (`id`, `name`) VALUES
(1, 'Faute de Main (Holding)'),
(2, 'Faute de Pousser (Pushing)'),
(3, 'Faute de Bloquer (Blocking)'),
(4, 'Faute de Charge (Charging)'),
(5, "Faute d'Accrochage (Hooking)"),
(6, 'Faute sur le Tireur (Shooting Foul)'),
(7, 'Faute Technique (Technical Foul)'),
(8, 'Faute Technique de Banc (Bench Technical Foul)'),
(9, 'Retard de Jeu (Delay of Game)'),
(10, 'Faute de Simulacre (Flopping)'),
(11, 'Faute Antisportive (Unsportsmanlike Foul)'),
(12, 'Faute Disqualifiante (Disqualifying Foul)'),
(13, "Faute d'Équipe (franchise Fouls)"),
(14, 'Faute des 3 Secondes (Three-Second Violation)'),
(15, 'Double Dribble'),
(16, 'Marcher (Traveling)');

-- --------------------------------------------------------

--
-- Structure de la table `point_type`
--

DROP TABLE IF EXISTS `point_type`;
CREATE TABLE IF NOT EXISTS `point_type` (
	`id` int NOT NULL AUTO_INCREMENT,
	`name` varchar(30) NOT NULL,
	`point` int NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `point_type`
--

INSERT INTO `point_type` (`name`, `point`, `id`) VALUES
('Panier à 2 points', 2, 1),
('Panier à 3 points', 3, 2),
('Lancer Franc à 1 point', 1, 3);

-- --------------------------------------------------------

--
-- Structure de la table `arbitrator`
--

DROP TABLE IF EXISTS `arbitrator`;
CREATE TABLE IF NOT EXISTS `arbitrator` (
	`id` int NOT NULL AUTO_INCREMENT,
	`human_id` int NOT NULL,
	PRIMARY KEY (`id`),
	KEY `fk_human_id_arbitrator` (`human_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `arbitrator`
--

INSERT INTO `arbitrator` (`id`, `human_id`) VALUES
(1, 93),
(2, 96),
(3, 94),
(4, 95),
(5, 97),
(6, 102);

-- --------------------------------------------------------

--
-- Structure de la table `franchise`
--

DROP TABLE IF EXISTS `franchise`;
CREATE TABLE IF NOT EXISTS `franchise` (
	`id` int NOT NULL AUTO_INCREMENT,
	`name` varchar(50) NOT NULL,
	`color` varchar(30) NOT NULL,
	`initials` varchar(10) NOT NULL,
    `isActive` boolean NOT NULL DEFAULT true,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `franchise`
--

INSERT INTO `franchise` (`id`, `name`, `color`, `initials`) VALUES
(1, 'Los Angeles Lakers', 'Violet', 'LAL'),
(2, 'Golden State Warriors', 'Bleu', 'GSW'),
(3, 'Cleveland Cavaliers', 'Rouge', 'CLE'),
(4, 'San Antonio Spurs', 'Noir', 'SAS'),
(5, 'Boston Celtics', 'Vert', 'BOS'),
(6, 'Charlotte Hornets', 'Turquoise', 'CHA');

-- --------------------------------------------------------
--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
	`id` int NOT NULL AUTO_INCREMENT,
	`login` varchar(30) NOT NULL,
	`password` varchar(100) NOT NULL,
	`role` varchar(30) NOT NULL,
	`human_id` int NULL,
	PRIMARY KEY (`id`),
	KEY `fk_human_id_user` (`human_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `user`
--

-- Role :
-- admin
-- coach
-- manager

INSERT INTO `user` (`id`, `login`, `password`, `role`, `human_id`) VALUES
(1, 'root', '$2y$10$f3PHfX9iSmwspuzecIEiae1paJkHad0SSf3hJmkrTzSPBVXy5nisi', 'admin', 1),
(2, 'manager', '$2y$10$TRgmUaSOYhpkuKDjYzKUWec5yAC3NrwU.T3ji3Zl8QlzAletGKEga', 'manager', 2),
(3, 'coach', '$2y$10$9Cn.x2JYm3XOEygXyib/TekpbXEPzhi/GTc08kkzC2B0StYv9o6VS', 'coach', 33),
(4, 'admin', '$2y$10$QC2VxPquXkivtKxoi18oh.fhHHR0lqRdNcXf.rDnqKuEDY0iunsRi', 'admin', null);

-- ---------------------------------------------------------- --------------------------------------------------------

--
-- Structure de la table `human`
--

DROP TABLE IF EXISTS `human`;
CREATE TABLE IF NOT EXISTS `human` (
	`id` int NOT NULL AUTO_INCREMENT,
	`last_name` varchar(30) NOT NULL,
	`first_name` varchar(30) NOT NULL,
	`gender` varchar(20) NOT NULL,
	`nationality` varchar(30) NOT NULL,
	`birth_date` date NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `human`
--

INSERT INTO `human` (`last_name`, `first_name`, `gender`, `nationality`, `birth_date`, `id`) VALUES
('Russell', "D'Angelo", 'man', 'USA', '1996-02-23', 1),
('Vincent', 'Gabe', 'man', 'USA', '1996-06-14', 2),
('Dinwiddie', 'Spencer', 'man', 'USA', '1993-04-06', 3),
('Hood-Schifino', 'Jalen', 'man', 'USA', '2003-01-02', 4),
('Mays', 'Skylar', 'man', 'USA', '1997-09-05', 5),
('Reaves', 'Austin', 'man', 'USA', '1998-05-29', 6),
('Lewis', 'Maxwell', 'man', 'USA', '1990-08-12', 7),
('Hachimura', 'Rui', 'man', 'JPN', '1992-11-19', 8),
('James', 'LeBron', 'man', 'USA', '1989-12-30', 9),
('Prince', 'Taurean', 'man', 'USA', '1991-07-05', 10),
('Davis', 'Anthony', 'man', 'USA', '1990-03-18', 11),
('Wood', 'Christian', 'man', 'USA', '1992-09-22', 12),
('Castleton', 'Colin', 'man', 'USA', '1994-02-09', 13),
('Giles', 'Harry', 'man', 'USA', '1993-10-27', 14),
('Curry', 'Stephen', 'man', 'USA', '1988-03-14', 15),
('Paul', 'Chris', 'man', 'USA', '1986-05-06', 16),
('Podziemski', 'Brandin', 'man', 'USA', '1995-08-03', 17),
('Adams', 'Ron', 'man', 'USA', '1947-11-18', 18),
('Spencer', 'Pat', 'man', 'USA', '1991-11-11', 19),
('Payton II', 'Gary', 'man', 'USA', '1993-06-09', 20),
('Robinson', 'Jerome', 'man', 'USA', '1994-10-04', 21),
('Moody', 'Moses', 'man', 'USA', '1998-02-20', 22),
('Thompson', 'Klay', 'man', 'USA', '1990-07-17', 23),
('Wiggins', 'Andrew', 'man', 'CND', '1995-02-03', 24),
('Kuminga', 'Jonathan', 'man', 'CNG', '1999-04-12', 25),
('Garuba', 'Usman', 'man', 'ESP', '2001-11-08', 26),
('Green', 'Draymond', 'man', 'USA', '1990-03-04', 27),
('Dario', 'Saric', 'man', 'CRT', '1991-05-15', 28),
('Jackson-Davis', 'Trayce', 'man', 'USA', '1996-08-28', 29),
('Looney', 'Kevon', 'man', 'USA', '1993-09-15', 30),
('Garland', 'Darius', 'man', 'USA', '1999-01-31', 31),
('Porter Jr.', 'Craig', 'man', 'USA', '1997-12-25', 32),
('Bickerstaff', 'J. B.', 'man', 'USA', '1979-03-10', 33),
('Jerome', 'Ty', 'man', 'USA', '1996-10-18', 34),
('Buckner', 'Greg', 'man', 'USA', '1944-02-11', 35),
('Mitchell', 'Donovan', 'man', 'USA', '1997-07-30', 36),
('LeVert', 'Caris', 'man', 'USA', '1994-08-27', 37),
('Okoro', 'Isaac', 'man', 'USA', '2000-01-10', 38),
('Smith', 'Zhaire', 'man', 'USA', '1997-02-02', 39),
('Strus', 'Max', 'man', 'USA', '1994-06-21', 40),
('Bates', 'Emoni', 'man', 'USA', '2002-09-28', 41),
('Wade', 'Dean', 'man', 'USA', '1995-12-03', 42),
('Mobley', 'Isaiah', 'man', 'USA', '1997-04-16', 43),
('Niang', 'Georges', 'man', 'USA', '1992-12-08', 44),
('Mobley', 'Evan', 'man', 'USA', '1996-05-07', 45),
('Allen', 'Jarrett', 'man', 'USA', '1998-08-02', 46),
('Jones', 'Damian', 'man', 'USA', '1991-01-23', 47),
('Johnson', 'Keldon', 'man', 'USA', '1997-02-09', 48),
('Gray', 'RaiQuan', 'man', 'USA', '1995-09-14', 49),
('Sochan', 'Jeremy', 'man', 'PLG', '1993-03-22', 50),
('Barlow', 'Dominick', 'man', 'USA', '1990-10-19', 51),
('Collins', 'Zach', 'man', 'USA', '1994-12-07', 52),
('Brown', 'Jaylen', 'man', 'USA', '1998-08-29', 53),
('Holiday', 'Jrue', 'man', 'USA', '1990-06-12', 54),
('Mazzulla', 'Joe', 'man', 'USA', '1988-06-30', 55),
('Pritchard', 'Payton', 'man', 'USA', '1996-04-11', 56),
('Cassell', 'Sam', 'man', 'USA', '1969-11-18', 57),
('White', 'Derrick', 'man', 'USA', '1992-07-26', 58),
('Springer', 'Jaden', 'man', 'USA', '1999-11-21', 59),
('Champagnie', 'Justin', 'man', 'USA', '1996-02-18', 60),
('Mykhaïliouk', 'Sviatoslav', 'man', 'UKR', '1997-01-04', 62),
('Peterson', 'Drew', 'man', 'USA', '1994-12-02', 63),
('Walsh', 'Jordan', 'man', 'USA', '1990-08-07', 64),
('Brissett', 'Oshae', 'man', 'CND', '1991-05-17', 65),
('Tatum', 'Jayson', 'man', 'USA', '1998-03-03', 66),
('Horford', 'Al', 'man', 'RDOM', '1986-06-03', 67),
('Porziņģis', 'Kristaps', 'man', 'LTN', '1995-08-12', 68),
('Tillman', 'Xavier', 'man', 'USA', '1996-10-14', 69),
('Kornet', 'Luke', 'man', 'USA', '1993-07-24', 70),
('Queta', 'Neemias', 'man', 'PTG', '1999-07-07', 71),
('Ball', 'LaMelo', 'man', 'USA', '2001-08-22', 72),
('Mann', 'Tre', 'man', 'USA', '1992-12-18', 73),
('Clifford', 'Steve', 'man', 'USA', '1961-03-17', 74),
('Curry', 'Sett', 'man', 'USA', '1994-01-29', 75),
('Friedman', 'Nick', 'man', 'USA', '1981-04-22', 76),
('Micić', 'Vasilije', 'man', 'SRB', '1994-09-15', 77),
('Smith Jr.', 'Nick', 'man', 'USA', '1993-03-11', 78),
('Bailey', 'Amari', 'man', 'USA', '1995-06-13', 79),
('Martin', 'Cody', 'man', 'USA', '1996-02-02', 80),
('McGowens', 'Bryce', 'man', 'USA', '1998-09-20', 81),
('Hauser', 'Sam', 'man', 'USA', '1995-01-26', 82),
('Black', 'Leaky', 'man', 'USA', '1990-04-05', 83),
('Miller', 'Brandon', 'man', 'USA', '1991-10-07', 84),
('Bridges', 'Miles', 'man', 'USA', '1999-07-02', 85),
('Bertans', 'Davis', 'man', 'LTN', '1993-12-14', 86),
('Thor', 'JT', 'man', 'USA', '1995-02-28', 87),
('Williams', 'Grant', 'man', 'USA', '1994-09-19', 88),
('Pokusevski', 'Aleksej', 'man', 'SRB', '2000-12-26', 89),
('Bolden', 'Marques', 'man', 'IND', '1996-08-08', 90),
('Richards', 'Nick', 'man', 'JAM', '1993-06-06', 91),
('Williams', 'Mark', 'man', 'USA', '1991-03-03', 92),
('Bissang', 'Joseph', 'man', 'FR', '1992-11-01', 93),
('Viator', 'Eddie', 'man', 'FR', '1989-10-20', 94),
('Rosso', 'Yohan', 'man', 'FR', '1990-07-11', 95),
('Mehta', 'Suyash', 'man', 'IND', '1993-04-09', 96),
('Lewis', 'Eric', 'man', 'USA', '1990-11-08', 97),
('Thompson', 'Tristan', 'man', 'CND', '1995-12-08', 98),
('Cissoko', 'Sidy', 'man', 'FR', '1993-05-21', 99),
('Mamukelashvili', 'Sandro', 'man', 'GOG', '1998-12-16', 100),
('Bassey', 'Charles', 'man', 'NRG', '1998-12-16', 101),
('Brothers', 'Tony', 'man', 'USA', '1992-02-17', 102),
('Duke Jr.', 'David', 'man', 'USA', '1993-07-29', 103),
('Graham', 'Devonte', 'man', 'USA', '1990-05-10', 104),
('Branham', 'Malaki', 'man', 'USA', '2003-05-12', 105),
('Vassel', 'Devin', 'man', 'USA', '2000-08-23', 106),
('Ham', 'Darvin', 'man', 'USA', '1973-07-23', 107),
('Dubois', 'J.D', 'man', 'USA', '1990-04-16', 108),
('Kerr', 'Steve', 'man', 'USA', '1965-09-27', 109),
('Popovich', 'Gregg', 'man', 'USA', '1949-01-28', 110),
('Brown', 'Brett', 'man', 'USA', '1961-02-16', 111),
('Quinones', 'Lester', 'man', 'USA', '2000-11-16', 112),
('Davison', 'JD', 'man', 'USA', '2002-10-03', 113),
('Wembanyama', 'Victor', 'man', 'USA', '2004-01-04', 114),
('Wesley', 'Blake', 'man', 'USA', '2003-03-16', 115),
('Bouyea', 'Jamaree', 'man', 'USA', '1999-06-27', 116);

-- ---------------------------------------------------------- --------------------------------------------------------

--
-- Contraintes pour la table `arbitrator`
--
ALTER TABLE `arbitrator`
	ADD CONSTRAINT `fk_human_id_arbitrator` FOREIGN KEY (`human_id`) REFERENCES `human` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `coach`
--
ALTER TABLE `coach`
	ADD CONSTRAINT `fk_human_id_coach` FOREIGN KEY (`human_id`) REFERENCES `human` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	ADD CONSTRAINT `fk_franchise_id_coach` FOREIGN KEY (`franchise_id`) REFERENCES `franchise` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `team`
--
ALTER TABLE `team`
		ADD CONSTRAINT `fk_game_id_team` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
		ADD CONSTRAINT `fk_franchise_id_team` FOREIGN KEY (`franchise_id`) REFERENCES `franchise` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `team_player`
--
ALTER TABLE `team_player`
	ADD CONSTRAINT `fk_team_id_team_player` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	ADD CONSTRAINT `fk_player_id_team_player` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `history_team_player_foul`
--
ALTER TABLE `history_team_player_foul`
	ADD CONSTRAINT `fk_team_player_id_history_team_player_foul` FOREIGN KEY (`team_player_id`) REFERENCES `team_player` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	ADD CONSTRAINT `fk_foul_type_id_history_team_player_foul` FOREIGN KEY (`foul_type_id`) REFERENCES `foul_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
	
--
-- Contraintes pour la table `game`
--
ALTER TABLE `game`
	ADD CONSTRAINT `fk_place_id_game` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `game_arbitrator`
--
ALTER TABLE `game_arbitrator`
	ADD CONSTRAINT `fk_game_id_game_arbitrator` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	ADD CONSTRAINT `fk_arbitrator_id_game_arbitrator` FOREIGN KEY (`arbitrator_id`) REFERENCES `arbitrator` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `history_team_player_point`
--
ALTER TABLE `history_team_player_point`
	ADD CONSTRAINT `fk_team_player_id_history_team_player_point` FOREIGN KEY (`team_player_id`) REFERENCES `team_player` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	ADD CONSTRAINT `fk_point_type_id_history_team_player_point` FOREIGN KEY (`point_type_id`) REFERENCES `point_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `player`
--
ALTER TABLE `player`
	ADD CONSTRAINT `fk_franchise_id_player` FOREIGN KEY (`franchise_id`) REFERENCES `franchise` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	ADD CONSTRAINT `fk_human_id_player` FOREIGN KEY (`human_id`) REFERENCES `human` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `history_substitute`
--
ALTER TABLE `history_substitute`
	ADD CONSTRAINT `fk_incoming_team_player_id_history_substitute` FOREIGN KEY (`incoming_team_player_id`) REFERENCES `team_player` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	ADD CONSTRAINT `fk_outgoing_team_player_id_history_substitute` FOREIGN KEY (`outgoing_team_player_id`) REFERENCES `team_player` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `game`
--
ALTER TABLE `user`
	ADD CONSTRAINT `fk_human_id_user` FOREIGN KEY (`human_id`) REFERENCES `human` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
