-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : lun. 09 sep. 2024 à 23:27
-- Version du serveur : 8.2.0
-- Version de PHP : 8.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `dunkdatabdd2`
--

-- --------------------------------------------------------

--
-- Structure de la table `arbitrator`
--

DROP TABLE IF EXISTS `arbitrator`;
CREATE TABLE IF NOT EXISTS `arbitrator` (
  `id` int NOT NULL AUTO_INCREMENT,
  `human_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_human_id_arbitrator` (`human_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `arbitrator`
--

INSERT INTO `arbitrator` (`id`, `human_id`) VALUES
(1, 93),
(3, 94),
(4, 95),
(2, 96),
(5, 97),
(6, 102);

-- --------------------------------------------------------

--
-- Structure de la table `coach`
--

DROP TABLE IF EXISTS `coach`;
CREATE TABLE IF NOT EXISTS `coach` (
  `id` int NOT NULL AUTO_INCREMENT,
  `human_id` int NOT NULL,
  `franchise_id` int NOT NULL,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `in_date` date DEFAULT NULL,
  `out_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_human_id_coach` (`human_id`),
  KEY `fk_franchise_id_coach` (`franchise_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `coach`
--

INSERT INTO `coach` (`id`, `human_id`, `franchise_id`, `title`, `in_date`, `out_date`) VALUES
(1, 107, 4, 'main_coach', NULL, NULL),
(2, 109, 5, 'main_coach', NULL, NULL),
(3, 33, 1, 'main_coach', NULL, NULL),
(4, 110, 6, 'main_coach', NULL, NULL),
(5, 55, 2, 'main_coach', NULL, NULL),
(6, 74, 3, 'main_coach', NULL, NULL),
(7, 18, 1, 'assistant_coach', NULL, NULL),
(8, 35, 2, 'assistant_coach', NULL, NULL),
(9, 57, 3, 'assistant_coach', NULL, NULL),
(10, 76, 4, 'assistant_coach', NULL, NULL),
(11, 108, 5, 'assistant_coach', NULL, NULL),
(12, 111, 6, 'assistant_coach', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `foul_type`
--

DROP TABLE IF EXISTS `foul_type`;
CREATE TABLE IF NOT EXISTS `foul_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `foul_type`
--

INSERT INTO `foul_type` (`id`, `name`) VALUES
(1, 'Faute de Main (Holding)'),
(2, 'Faute de Pousser (Pushing)'),
(3, 'Faute de Bloquer (Blocking)'),
(4, 'Faute de Charge (Charging)'),
(5, 'Faute d\'Accrochage (Hooking)'),
(6, 'Faute sur le Tireur (Shooting Foul)'),
(7, 'Faute Technique (Technical Foul)'),
(8, 'Faute Technique de Banc (Bench Technical Foul)'),
(9, 'Retard de Jeu (Delay of Game)'),
(10, 'Faute de Simulacre (Flopping)'),
(11, 'Faute Antisportive (Unsportsmanlike Foul)'),
(12, 'Faute Disqualifiante (Disqualifying Foul)'),
(13, 'Faute d\'Équipe (franchise Fouls)'),
(14, 'Faute des 3 Secondes (Three-Second Violation)'),
(15, 'Double Dribble'),
(16, 'Marcher (Traveling)');

-- --------------------------------------------------------

--
-- Structure de la table `franchise`
--

DROP TABLE IF EXISTS `franchise`;
CREATE TABLE IF NOT EXISTS `franchise` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `color` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `initials` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `franchise`
--

INSERT INTO `franchise` (`id`, `name`, `color`, `initials`, `isActive`) VALUES
(1, 'Los Angeles Lakers', 'Violet', 'LAL', 1),
(2, 'Golden State Warriors', 'Bleu', 'GSW', 1),
(3, 'Cleveland Cavaliers', 'Rouge', 'CLE', 1),
(4, 'San Antonio Spurs', 'Noir', 'SAS', 1),
(5, 'Boston Celtics', 'Vert', 'BOS', 1),
(6, 'Charlotte Hornets', 'Turquoise', 'CHA', 1);

-- --------------------------------------------------------

--
-- Structure de la table `game`
--

DROP TABLE IF EXISTS `game`;
CREATE TABLE IF NOT EXISTS `game` (
  `id` int NOT NULL AUTO_INCREMENT,
  `begin_date_time` datetime NOT NULL,
  `place_id` int NOT NULL,
  `game_time` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_place_id_game` (`place_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `game`
--

INSERT INTO `game` (`id`, `begin_date_time`, `place_id`, `game_time`) VALUES
(6, '2024-08-05 16:00:00', 1, '45:39'),
(7, '2024-08-19 14:00:00', 2, NULL),
(8, '2024-09-02 11:30:00', 3, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `game_arbitrator`
--

DROP TABLE IF EXISTS `game_arbitrator`;
CREATE TABLE IF NOT EXISTS `game_arbitrator` (
  `id` int NOT NULL AUTO_INCREMENT,
  `game_id` int NOT NULL,
  `arbitrator_id` int NOT NULL,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_game_id_game_arbitrator` (`game_id`),
  KEY `fk_arbitrator_id_game_arbitrator` (`arbitrator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `game_arbitrator`
--

INSERT INTO `game_arbitrator` (`id`, `game_id`, `arbitrator_id`, `title`) VALUES
(25, 6, 1, 'holder'),
(26, 6, 3, 'assistant'),
(27, 6, 4, 'assistant'),
(40, 8, 4, 'holder'),
(41, 8, 5, 'assistant'),
(42, 8, 6, 'assistant'),
(43, 7, 1, 'holder'),
(44, 7, 4, 'assistant'),
(45, 7, 5, 'assistant');

-- --------------------------------------------------------

--
-- Structure de la table `history_substitute`
--

DROP TABLE IF EXISTS `history_substitute`;
CREATE TABLE IF NOT EXISTS `history_substitute` (
  `id` int NOT NULL AUTO_INCREMENT,
  `incoming_team_player_id` int NOT NULL,
  `outgoing_team_player_id` int NOT NULL,
  `substitute_time` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_incoming_team_player_id_history_substitute` (`incoming_team_player_id`),
  KEY `fk_outgoing_team_player_id_history_substitute` (`outgoing_team_player_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `history_substitute`
--

INSERT INTO `history_substitute` (`id`, `incoming_team_player_id`, `outgoing_team_player_id`, `substitute_time`) VALUES
(16, 86, 24, '17:45'),
(17, 85, 25, '25:55'),
(18, 84, 26, '25:55'),
(19, 83, 27, '30:26'),
(20, 88, 40, '36:00'),
(21, 87, 41, '36:00');

-- --------------------------------------------------------

--
-- Structure de la table `history_team_player_foul`
--

DROP TABLE IF EXISTS `history_team_player_foul`;
CREATE TABLE IF NOT EXISTS `history_team_player_foul` (
  `id` int NOT NULL AUTO_INCREMENT,
  `team_player_id` int NOT NULL,
  `foul_type_id` int NOT NULL,
  `foul_time` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_team_player_id_history_team_player_foul` (`team_player_id`),
  KEY `fk_foul_type_id_history_team_player_foul` (`foul_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `history_team_player_foul`
--

INSERT INTO `history_team_player_foul` (`id`, `team_player_id`, `foul_type_id`, `foul_time`) VALUES
(165, 27, 2, '12:35'),
(166, 81, 10, '18:51'),
(167, 44, 15, '40:30');

-- --------------------------------------------------------

--
-- Structure de la table `history_team_player_point`
--

DROP TABLE IF EXISTS `history_team_player_point`;
CREATE TABLE IF NOT EXISTS `history_team_player_point` (
  `id` int NOT NULL AUTO_INCREMENT,
  `team_player_id` int NOT NULL,
  `point_type_id` int NOT NULL,
  `point_time` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_team_player_id_history_team_player_point` (`team_player_id`),
  KEY `fk_point_type_id_history_team_player_point` (`point_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `history_team_player_point`
--

INSERT INTO `history_team_player_point` (`id`, `team_player_id`, `point_type_id`, `point_time`) VALUES
(60, 24, 1, '02:37'),
(61, 25, 1, '05:28'),
(62, 27, 1, '09:37'),
(63, 28, 1, '10:26'),
(64, 25, 1, '15:26'),
(65, 27, 1, '20:57'),
(66, 24, 1, '27:37'),
(67, 25, 1, '28:26'),
(68, 28, 1, '34:15'),
(69, 83, 1, '40:24'),
(70, 84, 1, '42:10'),
(71, 84, 1, '44:06'),
(72, 26, 2, '07:16'),
(73, 26, 2, '18:00'),
(74, 26, 2, '30:12'),
(75, 27, 2, '32:34'),
(76, 24, 3, '13:11'),
(77, 28, 3, '22:46'),
(78, 81, 3, '36:23'),
(79, 82, 3, '39:36'),
(80, 40, 2, '18:01'),
(81, 41, 1, '20:05'),
(82, 42, 1, '22:12'),
(83, 43, 3, '24:13'),
(84, 44, 3, '26:15'),
(85, 40, 1, '28:37'),
(86, 41, 2, '30:48'),
(87, 42, 1, '32:57'),
(88, 43, 1, '34:52'),
(89, 44, 3, '36:36'),
(90, 87, 1, '38:21'),
(91, 88, 1, '40:30'),
(92, 88, 2, '42:42');

-- --------------------------------------------------------

--
-- Structure de la table `human`
--

DROP TABLE IF EXISTS `human`;
CREATE TABLE IF NOT EXISTS `human` (
  `id` int NOT NULL AUTO_INCREMENT,
  `last_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `first_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `gender` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `nationality` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `birth_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `human`
--

INSERT INTO `human` (`id`, `last_name`, `first_name`, `gender`, `nationality`, `birth_date`) VALUES
(1, 'Russell', 'D\'Angelo', 'man', 'USA', '1996-02-23'),
(2, 'Vincent', 'Gabe', 'man', 'USA', '1996-06-14'),
(3, 'Dinwiddie', 'Spencer', 'man', 'USA', '1993-04-06'),
(4, 'Hood-Schifino', 'Jalen', 'man', 'USA', '2003-01-02'),
(5, 'Mays', 'Skylar', 'man', 'USA', '1997-09-05'),
(6, 'Reaves', 'Austin', 'man', 'USA', '1998-05-29'),
(7, 'Lewis', 'Maxwell', 'man', 'USA', '1990-08-12'),
(8, 'Hachimura', 'Rui', 'man', 'JPN', '1992-11-19'),
(9, 'James', 'LeBron', 'man', 'USA', '1989-12-30'),
(10, 'Prince', 'Taurean', 'man', 'USA', '1991-07-05'),
(11, 'Davis', 'Anthony', 'man', 'USA', '1990-03-18'),
(12, 'Wood', 'Christian', 'man', 'USA', '1992-09-22'),
(13, 'Castleton', 'Colin', 'man', 'USA', '1994-02-09'),
(14, 'Giles', 'Harry', 'man', 'USA', '1993-10-27'),
(15, 'Curry', 'Stephen', 'man', 'USA', '1988-03-14'),
(16, 'Paul', 'Chris', 'man', 'USA', '1986-05-06'),
(17, 'Podziemski', 'Brandin', 'man', 'USA', '1995-08-03'),
(18, 'Adams', 'Ron', 'man', 'USA', '1947-11-18'),
(19, 'Spencer', 'Pat', 'man', 'USA', '1991-11-11'),
(20, 'Payton II', 'Gary', 'man', 'USA', '1993-06-09'),
(21, 'Robinson', 'Jerome', 'man', 'USA', '1994-10-04'),
(22, 'Moody', 'Moses', 'man', 'USA', '1998-02-20'),
(23, 'Thompson', 'Klay', 'man', 'USA', '1990-07-17'),
(24, 'Wiggins', 'Andrew', 'man', 'CND', '1995-02-03'),
(25, 'Kuminga', 'Jonathan', 'man', 'CNG', '1999-04-12'),
(26, 'Garuba', 'Usman', 'man', 'ESP', '2001-11-08'),
(27, 'Green', 'Draymond', 'man', 'USA', '1990-03-04'),
(28, 'Dario', 'Saric', 'man', 'CRT', '1991-05-15'),
(29, 'Jackson-Davis', 'Trayce', 'man', 'USA', '1996-08-28'),
(30, 'Looney', 'Kevon', 'man', 'USA', '1993-09-15'),
(31, 'Garland', 'Darius', 'man', 'USA', '1999-01-31'),
(32, 'Porter Jr.', 'Craig', 'man', 'USA', '1997-12-25'),
(33, 'Bickerstaff', 'J. B.', 'man', 'USA', '1979-03-10'),
(34, 'Jerome', 'Ty', 'man', 'USA', '1996-10-18'),
(35, 'Buckner', 'Greg', 'man', 'USA', '1944-02-11'),
(36, 'Mitchell', 'Donovan', 'man', 'USA', '1997-07-30'),
(37, 'LeVert', 'Caris', 'man', 'USA', '1994-08-27'),
(38, 'Okoro', 'Isaac', 'man', 'USA', '2000-01-10'),
(39, 'Smith', 'Zhaire', 'man', 'USA', '1997-02-02'),
(40, 'Strus', 'Max', 'man', 'USA', '1994-06-21'),
(41, 'Bates', 'Emoni', 'man', 'USA', '2002-09-28'),
(42, 'Wade', 'Dean', 'man', 'USA', '1995-12-03'),
(43, 'Mobley', 'Isaiah', 'man', 'USA', '1997-04-16'),
(44, 'Niang', 'Georges', 'man', 'USA', '1992-12-08'),
(45, 'Mobley', 'Evan', 'man', 'USA', '1996-05-07'),
(46, 'Allen', 'Jarrett', 'man', 'USA', '1998-08-02'),
(47, 'Jones', 'Damian', 'man', 'USA', '1991-01-23'),
(48, 'Johnson', 'Keldon', 'man', 'USA', '1997-02-09'),
(49, 'Gray', 'RaiQuan', 'man', 'USA', '1995-09-14'),
(50, 'Sochan', 'Jeremy', 'man', 'PLG', '1993-03-22'),
(51, 'Barlow', 'Dominick', 'man', 'USA', '1990-10-19'),
(52, 'Collins', 'Zach', 'man', 'USA', '1994-12-07'),
(53, 'Brown', 'Jaylen', 'man', 'USA', '1998-08-29'),
(54, 'Holiday', 'Jrue', 'man', 'USA', '1990-06-12'),
(55, 'Mazzulla', 'Joe', 'man', 'USA', '1988-06-30'),
(56, 'Pritchard', 'Payton', 'man', 'USA', '1996-04-11'),
(57, 'Cassell', 'Sam', 'man', 'USA', '1969-11-18'),
(58, 'White', 'Derrick', 'man', 'USA', '1992-07-26'),
(59, 'Springer', 'Jaden', 'man', 'USA', '1999-11-21'),
(60, 'Champagnie', 'Justin', 'man', 'USA', '1996-02-18'),
(62, 'Mykhaïliouk', 'Sviatoslav', 'man', 'UKR', '1997-01-04'),
(63, 'Peterson', 'Drew', 'man', 'USA', '1994-12-02'),
(64, 'Walsh', 'Jordan', 'man', 'USA', '1990-08-07'),
(65, 'Brissett', 'Oshae', 'man', 'CND', '1991-05-17'),
(66, 'Tatum', 'Jayson', 'man', 'USA', '1998-03-03'),
(67, 'Horford', 'Al', 'man', 'RDOM', '1986-06-03'),
(68, 'Porziņģis', 'Kristaps', 'man', 'LTN', '1995-08-12'),
(69, 'Tillman', 'Xavier', 'man', 'USA', '1996-10-14'),
(70, 'Kornet', 'Luke', 'man', 'USA', '1993-07-24'),
(71, 'Queta', 'Neemias', 'man', 'PTG', '1999-07-07'),
(72, 'Ball', 'LaMelo', 'man', 'USA', '2001-08-22'),
(73, 'Mann', 'Tre', 'man', 'USA', '1992-12-18'),
(74, 'Clifford', 'Steve', 'man', 'USA', '1961-03-17'),
(75, 'Curry', 'Sett', 'man', 'USA', '1994-01-29'),
(76, 'Friedman', 'Nick', 'man', 'USA', '1981-04-22'),
(77, 'Micić', 'Vasilije', 'man', 'SRB', '1994-09-15'),
(78, 'Smith Jr.', 'Nick', 'man', 'USA', '1993-03-11'),
(79, 'Bailey', 'Amari', 'man', 'USA', '1995-06-13'),
(80, 'Martin', 'Cody', 'man', 'USA', '1996-02-02'),
(81, 'McGowens', 'Bryce', 'man', 'USA', '1998-09-20'),
(82, 'Hauser', 'Sam', 'man', 'USA', '1995-01-26'),
(83, 'Black', 'Leaky', 'man', 'USA', '1990-04-05'),
(84, 'Miller', 'Brandon', 'man', 'USA', '1991-10-07'),
(85, 'Bridges', 'Miles', 'man', 'USA', '1999-07-02'),
(86, 'Bertans', 'Davis', 'man', 'LTN', '1993-12-14'),
(87, 'Thor', 'JT', 'man', 'USA', '1995-02-28'),
(88, 'Williams', 'Grant', 'man', 'USA', '1994-09-19'),
(89, 'Pokusevski', 'Aleksej', 'man', 'SRB', '2000-12-26'),
(90, 'Bolden', 'Marques', 'man', 'IND', '1996-08-08'),
(91, 'Richards', 'Nick', 'man', 'JAM', '1993-06-06'),
(92, 'Williams', 'Mark', 'man', 'USA', '1991-03-03'),
(93, 'Bissang', 'Joseph', 'man', 'FR', '1992-11-01'),
(94, 'Viator', 'Eddie', 'man', 'FR', '1989-10-20'),
(95, 'Rosso', 'Yohan', 'man', 'FR', '1990-07-11'),
(96, 'Mehta', 'Suyash', 'man', 'IND', '1993-04-09'),
(97, 'Lewis', 'Eric', 'man', 'USA', '1990-11-08'),
(98, 'Thompson', 'Tristan', 'man', 'CND', '1995-12-08'),
(99, 'Cissoko', 'Sidy', 'man', 'FR', '1993-05-21'),
(100, 'Mamukelashvili', 'Sandro', 'man', 'GOG', '1998-12-16'),
(101, 'Bassey', 'Charles', 'man', 'NRG', '1998-12-16'),
(102, 'Brothers', 'Tony', 'man', 'USA', '1992-02-17'),
(103, 'Duke Jr.', 'David', 'man', 'USA', '1993-07-29'),
(104, 'Graham', 'Devonte', 'man', 'USA', '1990-05-10'),
(105, 'Branham', 'Malaki', 'man', 'USA', '2003-05-12'),
(106, 'Vassel', 'Devin', 'man', 'USA', '2000-08-23'),
(107, 'Ham', 'Darvin', 'man', 'USA', '1973-07-23'),
(108, 'Dubois', 'J.D', 'man', 'USA', '1990-04-16'),
(109, 'Kerr', 'Steve', 'man', 'USA', '1965-09-27'),
(110, 'Popovich', 'Gregg', 'man', 'USA', '1949-01-28'),
(111, 'Brown', 'Brett', 'man', 'USA', '1961-02-16'),
(112, 'Quinones', 'Lester', 'man', 'USA', '2000-11-16'),
(113, 'Davison', 'JD', 'man', 'USA', '2002-10-03'),
(114, 'Wembanyama', 'Victor', 'man', 'USA', '2004-01-04'),
(115, 'Wesley', 'Blake', 'man', 'USA', '2003-03-16'),
(116, 'Bouyea', 'Jamaree', 'man', 'USA', '1999-06-27');

-- --------------------------------------------------------

--
-- Structure de la table `place`
--

DROP TABLE IF EXISTS `place`;
CREATE TABLE IF NOT EXISTS `place` (
  `id` int NOT NULL AUTO_INCREMENT,
  `gym` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `city` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `place`
--

INSERT INTO `place` (`id`, `gym`, `address`, `city`) VALUES
(1, 'Crypto.com Arena', '1111 S. Figueroa Street, CA 90015', 'Los Angeles'),
(2, 'Chase Center', '1 Warriors Way, CA 94158', 'San Francisco'),
(3, 'Rocket Mortgage FieldHouse', '1 Center Court, OH 44115', 'Cleveland'),
(4, 'Frost Bank Center', '1 Frost Bank Center Drive, TX 78219,', 'San Antonio'),
(5, 'Td Garden', '100 Legends Way, MA 02114', 'Boston'),
(6, 'Spectrum Center', '333 E Trade Street, NC 28202', 'Charlotte');

-- --------------------------------------------------------

--
-- Structure de la table `player`
--

DROP TABLE IF EXISTS `player`;
CREATE TABLE IF NOT EXISTS `player` (
  `id` int NOT NULL AUTO_INCREMENT,
  `human_id` int NOT NULL,
  `jersey_number` int NOT NULL,
  `main_post` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `franchise_id` int DEFAULT NULL,
  `in_date` date DEFAULT NULL,
  `out_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_human_id_player` (`human_id`),
  KEY `fk_franchise_id_player` (`franchise_id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `player`
--

INSERT INTO `player` (`id`, `human_id`, `jersey_number`, `main_post`, `franchise_id`, `in_date`, `out_date`) VALUES
(1, 1, 1, 'leader', 1, NULL, NULL),
(2, 2, 2, 'leader', 1, NULL, NULL),
(3, 3, 26, 'back', 1, NULL, NULL),
(4, 4, 0, 'back', 1, NULL, NULL),
(5, 5, 4, 'back', 1, NULL, NULL),
(6, 6, 15, 'back', 1, NULL, NULL),
(7, 7, 21, 'winger', 1, NULL, NULL),
(8, 8, 8, 'winger', 1, NULL, NULL),
(9, 9, 6, 'winger', 1, NULL, NULL),
(10, 10, 12, 'winger', 1, NULL, NULL),
(11, 11, 3, 'strong_winger', 1, NULL, NULL),
(12, 12, 32, 'strong_winger', 1, NULL, NULL),
(13, 13, 14, 'pivot', 1, NULL, NULL),
(14, 14, 20, 'pivot', 1, NULL, NULL),
(15, 15, 30, 'leader', 2, NULL, NULL),
(16, 16, 3, 'leader', 2, NULL, NULL),
(17, 17, 55, 'back', 2, NULL, NULL),
(18, 19, 26, 'back', 2, NULL, NULL),
(19, 20, 26, 'back', 2, NULL, NULL),
(20, 21, 8, 'back', 2, NULL, NULL),
(21, 22, 4, 'back', 2, NULL, NULL),
(22, 23, 11, 'winger', 2, NULL, NULL),
(23, 24, 22, 'winger', 2, NULL, NULL),
(24, 25, 0, 'strong_winger', 2, NULL, NULL),
(25, 26, 16, 'strong_winger', 2, NULL, NULL),
(26, 27, 23, 'strong_winger', 2, NULL, NULL),
(27, 28, 20, 'strong_winger', 2, NULL, NULL),
(28, 29, 32, 'pivot', 2, NULL, NULL),
(29, 30, 5, 'pivot', 2, NULL, NULL),
(30, 31, 10, 'leader', 3, NULL, NULL),
(31, 32, 9, 'leader', 3, NULL, NULL),
(32, 36, 45, 'back', 3, NULL, NULL),
(33, 34, 11, 'back', 3, NULL, NULL),
(34, 37, 22, 'back', 3, NULL, NULL),
(35, 38, 35, 'winger', 3, NULL, NULL),
(36, 39, 8, 'back', 3, NULL, NULL),
(37, 40, 1, 'back', 3, NULL, NULL),
(38, 41, 21, 'winger', 3, NULL, NULL),
(39, 42, 32, 'strong_winger', 3, NULL, NULL),
(40, 43, 3, 'strong_winger', 3, NULL, NULL),
(41, 44, 20, 'winger', 3, NULL, NULL),
(42, 45, 4, 'pivot', 3, NULL, NULL),
(43, 46, 31, 'pivot', 3, NULL, NULL),
(44, 47, 30, 'pivot', 3, NULL, NULL),
(45, 98, 13, 'strong_winger', 3, NULL, NULL),
(46, 116, 1, 'leader', 4, NULL, NULL),
(47, 99, 25, 'winger', 4, NULL, NULL),
(48, 103, 7, 'winger', 4, NULL, NULL),
(49, 104, 4, 'leader', 4, NULL, NULL),
(50, 115, 14, 'back', 4, NULL, NULL),
(51, 105, 22, 'winger', 4, NULL, NULL),
(52, 106, 24, 'winger', 4, NULL, NULL),
(53, 48, 3, 'strong_winger', 4, NULL, NULL),
(54, 49, 0, 'strong_winger', 4, NULL, NULL),
(55, 50, 10, 'strong_winger', 4, NULL, NULL),
(56, 51, 26, 'pivot', 4, NULL, NULL),
(57, 52, 23, 'strong_winger', 4, NULL, NULL),
(58, 100, 54, 'strong_winger', 4, NULL, NULL),
(59, 114, 32, 'pivot', 4, NULL, NULL),
(60, 101, 28, 'strong_winger', 4, NULL, NULL),
(61, 113, 20, 'leader', 5, NULL, NULL),
(62, 54, 4, 'leader', 5, NULL, NULL),
(63, 56, 11, 'leader', 5, NULL, NULL),
(64, 58, 9, 'leader', 5, NULL, NULL),
(65, 59, 44, 'back', 5, NULL, NULL),
(66, 53, 7, 'winger', 5, NULL, NULL),
(67, 60, 9, 'winger', 5, NULL, NULL),
(68, 82, 30, 'winger', 5, NULL, NULL),
(69, 62, 50, 'winger', 5, NULL, NULL),
(70, 63, 13, 'strong_winger', 5, NULL, NULL),
(71, 64, 27, 'strong_winger', 5, NULL, NULL),
(72, 65, 12, 'strong_winger', 5, NULL, NULL),
(73, 66, 0, 'strong_winger', 5, NULL, NULL),
(74, 67, 42, 'strong_winger', 5, NULL, NULL),
(75, 68, 8, 'pivot', 5, NULL, NULL),
(76, 69, 26, 'strong_winger', 5, NULL, NULL),
(77, 70, 40, 'pivot', 5, NULL, NULL),
(78, 71, 88, 'pivot', 5, NULL, NULL),
(79, 72, 0, 'leader', 6, NULL, NULL),
(80, 73, 23, 'leader', 6, NULL, NULL),
(81, 75, 30, 'leader', 6, NULL, NULL),
(82, 77, 22, 'leader', 6, NULL, NULL),
(83, 78, 3, 'leader', 6, NULL, NULL),
(84, 79, 10, 'back', 6, NULL, NULL),
(85, 80, 11, 'winger', 6, NULL, NULL),
(86, 81, 7, 'winger', 6, NULL, NULL),
(87, 83, 14, 'winger', 6, NULL, NULL),
(88, 84, 24, 'winger', 6, NULL, NULL),
(89, 85, 0, 'winger', 6, NULL, NULL),
(90, 86, 2, 'winger', 6, NULL, NULL),
(91, 87, 21, 'strong_winger', 6, NULL, NULL),
(92, 88, 8, 'strong_winger', 6, NULL, NULL),
(93, 89, 17, 'winger', 6, NULL, NULL),
(94, 90, 12, 'pivot', 6, NULL, NULL),
(95, 91, 4, 'pivot', 6, NULL, NULL),
(96, 92, 5, 'pivot', 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `point_type`
--

DROP TABLE IF EXISTS `point_type`;
CREATE TABLE IF NOT EXISTS `point_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `point` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `point_type`
--

INSERT INTO `point_type` (`id`, `name`, `point`) VALUES
(1, 'Panier à 2 points', 2),
(2, 'Panier à 3 points', 3),
(3, 'Lancer Franc à 1 point', 1);

-- --------------------------------------------------------

--
-- Structure de la table `team`
--

DROP TABLE IF EXISTS `team`;
CREATE TABLE IF NOT EXISTS `team` (
  `id` int NOT NULL AUTO_INCREMENT,
  `game_id` int NOT NULL,
  `franchise_id` int NOT NULL,
  `is_home` tinyint(1) NOT NULL DEFAULT '0',
  `score` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_franchise_id_team` (`franchise_id`),
  KEY `fk_game_id_team` (`game_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `team`
--

INSERT INTO `team` (`id`, `game_id`, `franchise_id`, `is_home`, `score`) VALUES
(11, 6, 1, 1, 40),
(12, 6, 2, 0, 26),
(13, 7, 3, 1, NULL),
(14, 7, 4, 0, NULL),
(15, 8, 5, 1, NULL),
(16, 8, 6, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `team_player`
--

DROP TABLE IF EXISTS `team_player`;
CREATE TABLE IF NOT EXISTS `team_player` (
  `id` int NOT NULL AUTO_INCREMENT,
  `team_id` int NOT NULL,
  `player_id` int NOT NULL,
  `post` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_team_id_team_player` (`team_id`),
  KEY `fk_player_id_team_player` (`player_id`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `team_player`
--

INSERT INTO `team_player` (`id`, `team_id`, `player_id`, `post`, `title`) VALUES
(24, 11, 1, 'pivot', 'player'),
(25, 11, 2, 'leader', 'player'),
(26, 11, 3, 'winger', 'player'),
(27, 11, 4, 'strong_winger', 'player'),
(28, 11, 5, 'back', 'player'),
(40, 12, 15, 'pivot', 'player'),
(41, 12, 16, 'leader', 'player'),
(42, 12, 17, 'winger', 'captain'),
(43, 12, 18, 'strong_winger', 'player'),
(44, 12, 19, 'back', 'alternate_captain'),
(81, 11, 6, 'substitute', 'player'),
(82, 11, 7, 'substitute', 'player'),
(83, 11, 8, 'substitute', 'player'),
(84, 11, 9, 'substitute', 'captain'),
(85, 11, 10, 'substitute', 'player'),
(86, 11, 11, 'substitute', 'player'),
(87, 12, 20, 'substitute', 'player'),
(88, 12, 21, 'substitute', 'player'),
(152, 15, 61, 'pivot', 'captain'),
(153, 15, 62, 'leader', 'alternate_captain'),
(154, 15, 63, 'winger', 'player'),
(155, 15, 64, 'strong_winger', 'player'),
(156, 15, 65, 'back', 'player'),
(157, 16, 79, 'pivot', 'captain'),
(158, 16, 80, 'leader', 'alternate_captain'),
(159, 16, 81, 'winger', 'player'),
(160, 16, 82, 'strong_winger', 'player'),
(161, 16, 83, 'back', 'player'),
(162, 15, 66, 'substitute', 'player'),
(163, 15, 67, 'substitute', 'player'),
(164, 15, 68, 'substitute', 'player'),
(165, 16, 84, 'substitute', 'player'),
(166, 16, 85, 'substitute', 'player'),
(167, 13, 30, 'pivot', 'captain'),
(168, 13, 31, 'leader', 'alternate_captain'),
(169, 13, 32, 'winger', 'player'),
(170, 13, 33, 'strong_winger', 'player'),
(171, 13, 34, 'back', 'player'),
(172, 14, 46, 'pivot', 'alternate_captain'),
(173, 14, 47, 'leader', 'player'),
(174, 14, 48, 'winger', 'player'),
(175, 14, 49, 'strong_winger', 'player'),
(176, 14, 50, 'back', 'player'),
(177, 13, 35, 'substitute', 'player'),
(178, 13, 37, 'substitute', 'player'),
(179, 13, 36, 'substitute', 'player'),
(180, 14, 55, 'substitute', 'captain'),
(181, 14, 60, 'substitute', 'player'),
(182, 14, 59, 'substitute', 'player');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `login` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `role` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `human_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_human_id_user` (`human_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `role`, `human_id`) VALUES
(2, 'manager', '$2y$10$TRgmUaSOYhpkuKDjYzKUWec5yAC3NrwU.T3ji3Zl8QlzAletGKEga', 'manager', 2),
(3, 'coach', '$2y$10$9Cn.x2JYm3XOEygXyib/TekpbXEPzhi/GTc08kkzC2B0StYv9o6VS', 'coach', 33),
(4, 'admin', '$2y$10$QC2VxPquXkivtKxoi18oh.fhHHR0lqRdNcXf.rDnqKuEDY0iunsRi', 'admin', NULL),
(5, 'coach2', '$2y$10$nf.o5A5ncwW.B4/Vqj0hQ.YBBsSh8dt.u96Xw8rNIjgWCz.DEMcjC', 'coach', 55),
(6, 'coach3', '$2y$10$Eel7fTX4JzDd2mjBlTOLiO3k0dKiC.5sHvcsnEs2odXbJhz2TkJ36', 'coach', 74),
(7, 'coach4', '$2y$10$GBYVEQcoX0E8/izWxj0AnOLsl.Z09mCEYRzb92UVhHEuauejgk87i', 'coach', 107),
(8, 'coach5', '$2y$10$WfjKAhWwVYfHVpCvnNiX3OXdbOkWVW0.Nt6eSG6AhH05Sp6s7KC5q', 'coach', 109),
(9, 'coach6', '$2y$10$MPSe3gdEfcOVZmgcxc/poOOaIcuo4p.GxDlZ6HeRGE4UUfVQJndoK', 'coach', 110);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `arbitrator`
--
ALTER TABLE `arbitrator`
  ADD CONSTRAINT `fk_human_id_arbitrator` FOREIGN KEY (`human_id`) REFERENCES `human` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `coach`
--
ALTER TABLE `coach`
  ADD CONSTRAINT `fk_franchise_id_coach` FOREIGN KEY (`franchise_id`) REFERENCES `franchise` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_human_id_coach` FOREIGN KEY (`human_id`) REFERENCES `human` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `game`
--
ALTER TABLE `game`
  ADD CONSTRAINT `fk_place_id_game` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `game_arbitrator`
--
ALTER TABLE `game_arbitrator`
  ADD CONSTRAINT `fk_arbitrator_id_game_arbitrator` FOREIGN KEY (`arbitrator_id`) REFERENCES `arbitrator` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_game_id_game_arbitrator` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `history_substitute`
--
ALTER TABLE `history_substitute`
  ADD CONSTRAINT `fk_incoming_team_player_id_history_substitute` FOREIGN KEY (`incoming_team_player_id`) REFERENCES `team_player` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_outgoing_team_player_id_history_substitute` FOREIGN KEY (`outgoing_team_player_id`) REFERENCES `team_player` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `history_team_player_foul`
--
ALTER TABLE `history_team_player_foul`
  ADD CONSTRAINT `fk_foul_type_id_history_team_player_foul` FOREIGN KEY (`foul_type_id`) REFERENCES `foul_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_team_player_id_history_team_player_foul` FOREIGN KEY (`team_player_id`) REFERENCES `team_player` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `history_team_player_point`
--
ALTER TABLE `history_team_player_point`
  ADD CONSTRAINT `fk_point_type_id_history_team_player_point` FOREIGN KEY (`point_type_id`) REFERENCES `point_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_team_player_id_history_team_player_point` FOREIGN KEY (`team_player_id`) REFERENCES `team_player` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `player`
--
ALTER TABLE `player`
  ADD CONSTRAINT `fk_franchise_id_player` FOREIGN KEY (`franchise_id`) REFERENCES `franchise` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_human_id_player` FOREIGN KEY (`human_id`) REFERENCES `human` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `team`
--
ALTER TABLE `team`
  ADD CONSTRAINT `fk_franchise_id_team` FOREIGN KEY (`franchise_id`) REFERENCES `franchise` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_game_id_team` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `team_player`
--
ALTER TABLE `team_player`
  ADD CONSTRAINT `fk_player_id_team_player` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_team_id_team_player` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_human_id_user` FOREIGN KEY (`human_id`) REFERENCES `human` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
