<?php
// Enable error reporting for debugging purposes
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Initialize the routing system
require_once __DIR__ . '/Router.php';

$router = new Router();
$router->route();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="180x180" href="/dunkdata/src/assets/logo/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/dunkdata/src/assets/logo/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/dunkdata/src/assets/logo/favicon/favicon-16x16.png">
    <link rel="manifest" href="/dunkdata/src/assets/logo/favicon/site.webmanifest">
    <title>Dunk Data</title>
    <link rel="stylesheet" href="/dunkdata/src/assets/styles/fonts.css">
    <link rel="stylesheet" href="/dunkdata/src/assets/styles/colors.css">
    <link rel="stylesheet" href="/dunkdata/src/assets/styles/style.css">
    <link rel="stylesheet" href="/dunkdata/src/assets/styles/button.css">
    <link rel="stylesheet" href="/dunkdata/src/assets/styles/form.css">
    <link rel="stylesheet" href="/dunkdata/src/assets/styles/card.css">
    <link rel="stylesheet" href="/dunkdata/src/assets/styles/table.css">
    <link rel="stylesheet" href="/dunkdata/src/views/navbar/style/navbar.css">
</head>
