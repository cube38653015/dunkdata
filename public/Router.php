<?php
include_once __DIR__ . '/../src/controllers/LoginController.php';
include_once __DIR__ . '/../src/controllers/ManagerController.php';
include_once __DIR__ . '/../src/controllers/CoachController.php';
include_once __DIR__ . '/../src/controllers/AdminController.php';
include_once __DIR__ . '/../src/controllers/ApiController.php';

class Router
{

    private array $allUris;
    private array $authorizedUris;
    private string $actualUri;

    private bool $isCoach;
    private bool $isManager;
    private bool $isAdmin;

    public function __construct()
    {
        if (!isset($_SESSION)) {
            session_start();
        }

        // Variable de session de base
        $_SESSION['baseProject'] = __DIR__ . '/..';
        $_SESSION['dirPublic'] = $_SESSION['baseProject'] . '/public';
        $_SESSION['dirReport'] = $_SESSION['dirPublic'] . '/reports';
        $_SESSION['dirGameReport'] = $_SESSION['dirReport'] . '/game_reports';
        $_SESSION['dirSrc'] = $_SESSION['baseProject'] . '/src';
        $_SESSION['dirViews'] = $_SESSION['dirSrc'] . '/views';
        $_SESSION['baseUri'] = '/dunkdata/public/index.php';
        $_SESSION['baseApiUri'] = '/public/index.php/api';

        // Role de l'utilisateur
        $this->isCoach = isset($_SESSION['role']) && $_SESSION['role'] === 'coach';
        $this->isManager = isset($_SESSION['role']) && $_SESSION['role'] === 'manager';
        $this->isAdmin = isset($_SESSION['role']) && $_SESSION['role'] === 'admin';

        // Uri actuel
        $this->actualUri = $_SERVER['REQUEST_URI'];

        // Uri d'acceuil
        $homeUri = $_SESSION['baseUri'];
        $apiUri = $_SESSION['baseApiUri'];

        // Mise en session de toutes les urls connues
        $_SESSION['homeUri'] = str_contains($homeUri, 'index.php') ? $homeUri : $homeUri . '/';
        $_SESSION['aboutUri'] = $homeUri . '/about';
        $_SESSION['loginUri'] = $homeUri . '/login';
        $_SESSION['logoutUri'] = $homeUri . '/logout';
        $_SESSION['loginSuccessUri'] = $homeUri . '/loginSuccess';
        $_SESSION['checkAccessUri'] = $homeUri . '/checkAccess';

        // URI COACH
        $_SESSION['coachHomeUri'] = $homeUri . '/coach/home';
        $_SESSION['coachShowNewTeamPlayerFormUri'] = $homeUri . '/coach/newTeamPlayerForm';
        $_SESSION['coachCreateTeamPlayerUri'] = $homeUri . '/coach/createTeamPlayer';

        // URI MANAGER
        $_SESSION['managerHomeUri'] = $homeUri . '/manager/home';
        $_SESSION['managerShowNewGameFormUri'] = $homeUri . '/manager/newGameForm';
        $_SESSION['managerCreateGameUri'] = $homeUri . '/manager/createGame';
        $_SESSION['managerGamesInProgressUri'] = $homeUri . '/manager/gamesInProgress';
        $_SESSION['managerHistoryGamesUri'] = $homeUri . '/manager/historyGames';
        $_SESSION['managerShowUpdateGameFormUri'] = $homeUri . '/manager/updateGameForm';
        $_SESSION['managerUpdateGameUri'] = $homeUri . '/manager/updateHistoryGame';
        $_SESSION['managerShowUpdateHistoryGameFormUri'] = $homeUri . '/manager/updateHistoryGameForm';
        $_SESSION['managerDeleteGameInProgressUri'] = $homeUri . '/manager/deleteGameInProgress';
        $_SESSION['managerDeleteGameClosedUri'] = $homeUri . '/manager/deleteGameClosed';
        $_SESSION['managerDownloadGameReportUri'] = $homeUri . '/manager/downloadGameReport';

        // URI ADMIN
        $_SESSION['adminHomeUri'] = $homeUri . '/admin/home';
        $_SESSION['adminHomeUsersUri'] = $homeUri . '/admin/users/homeUsers';
        $_SESSION['adminShowNewUserFormUri'] = $homeUri . '/admin/users/showNewUserForm';
        $_SESSION['adminCreateUserUri'] = $homeUri . '/admin/users/createUser';
        $_SESSION['adminShowUpdateUserFormUri'] = $homeUri . '/admin/users/showUpdateUserForm';
        $_SESSION['adminUpdateUserFormUri'] = $homeUri . '/admin/users/updateUserForm';
        $_SESSION['adminUpdateUserUri'] = $homeUri . '/admin/users/updateUser';
        $_SESSION['adminDeleteUserUri'] = $homeUri . '/admin/users/deleteUser';
        $_SESSION['adminHomeFranchisesUri'] = $homeUri . '/admin/franchises/homeFranchises';
        $_SESSION['adminShowNewFranchiseFormUri'] = $homeUri . '/admin/franchises/showNewFranchiseForm';
        $_SESSION['adminCreateFranchiseUri'] = $homeUri . '/admin/franchises/createFranchise';
        $_SESSION['adminShowUpdateFranchiseFormUri'] = $homeUri . '/admin/franchises/showUpdateFranchiseForm';
        $_SESSION['adminUpdateFranchiseUri'] = $homeUri . '/admin/franchises/updateFranchise';
        $_SESSION['adminHomeFranchisePlayersUri'] = $homeUri . '/admin/franchises/homeFranchisePlayers';
        $_SESSION['adminShowUpdatePlayerFormUri'] = $homeUri . '/admin/franchises/showUpdatePlayerForm';
        $_SESSION['adminUpdatePlayerUri'] = $homeUri . '/admin/franchises/updatePlayer';
        $_SESSION['adminDeletePlayerUri'] = $homeUri . '/admin/franchises/deletePlayer';
        $_SESSION['adminUpdateFranchisePlayersUri'] = $homeUri . '/admin/franchises/updateFranchisePlayers';
        $_SESSION['adminHomeFranchiseCoachesUri'] = $homeUri . '/admin/franchises/homeFranchiseCoaches';
        $_SESSION['adminShowUpdateCoachFormUri'] = $homeUri . '/admin/franchises/showUpdateCoachForm';
        $_SESSION['adminUpdateCoachUri'] = $homeUri . '/admin/franchises/updateCoach';
        $_SESSION['adminDeleteCoachUri'] = $homeUri . '/admin/franchises/deleteCoach';
        $_SESSION['adminUpdateFranchiseCoachesUri'] = $homeUri . '/admin/franchises/updateFranchiseCoaches';
        $_SESSION['adminDeleteFranchiseUri'] = $homeUri . '/admin/franchises/deleteFranchise';
        $_SESSION['adminHomeArbitratorsUri'] = $homeUri . '/admin/arbitrators/homeArbitrators';
        $_SESSION['adminShowNewArbitratorFormUri'] = $homeUri . '/admin/arbitrators/showNewArbitratorForm';
        $_SESSION['adminCreateArbitratorUri'] = $homeUri . '/admin/arbitrators/createArbitrator';
        $_SESSION['adminShowUpdateArbitratorFormUri'] = $homeUri . '/admin/arbitrators/showUpdateArbitratorForm';
        $_SESSION['adminUpdateArbitratorUri'] = $homeUri . '/admin/arbitrators/updateArbitrator';
        $_SESSION['adminDeleteArbitratorUri'] = $homeUri . '/admin/arbitrators/deleteArbitrator';
        $_SESSION['adminHomeGamesUri'] = $homeUri . '/admin/games/homeGames';
        $_SESSION['adminShowNewGameFormUri'] = $homeUri . '/admin/games/showNewGameForm';
        $_SESSION['adminCreateGameUri'] = $homeUri . '/admin/games/createGame';
        $_SESSION['adminGamesInProgressUri'] = $homeUri . '/admin/games/gamesInProgress';
        $_SESSION['adminShowUpdateGameInProgressFormUri'] = $homeUri . '/admin/games/updateGameInProgressForm';
        $_SESSION['adminUpdateGameInProgressUri'] = $homeUri . '/admin/games/updateGameInProgress';
        $_SESSION['adminDeleteGameInProgressUri'] = $homeUri . '/admin/games/deleteGameInProgress';
        $_SESSION['adminUpdateGameUri'] = $homeUri . '/admin/games/updateHistoryGame';
        $_SESSION['adminHistoryGamesUri'] = $homeUri . '/admin/games/historyGames';
        $_SESSION['adminShowUpdateHistoryGameFormUri'] = $homeUri . '/admin/games/updateHistoryGameForm';
        $_SESSION['adminDeleteGameClosedUri'] = $homeUri . '/admin/games/deleteGameClosed';
        $_SESSION['adminDownloadGameReportUri'] = $homeUri . '/admin/games/downloadGameReport';

        // API
        $_SESSION['apiGetHistoryGamesUri'] = $apiUri . '/historyGames';
        $_SESSION['apiGetGamesInProgressUri'] = $apiUri . '/inProgressGames';


        // Liste complète de toutes les uri utilisées sur le site
        $this->allUris = [
            $_SESSION['homeUri'],
            $_SESSION['aboutUri'],
            $_SESSION['loginUri'],
            $_SESSION['logoutUri'],
            $_SESSION['loginSuccessUri'],
            $_SESSION['checkAccessUri'],
            $_SESSION['coachHomeUri'],
            $_SESSION['coachShowNewTeamPlayerFormUri'],
            $_SESSION['coachCreateTeamPlayerUri'],
            $_SESSION['managerHomeUri'],
            $_SESSION['managerShowNewGameFormUri'],
            $_SESSION['managerCreateGameUri'],
            $_SESSION['managerGamesInProgressUri'],
            $_SESSION['managerHistoryGamesUri'],
            $_SESSION['managerShowUpdateHistoryGameFormUri'],
            $_SESSION['managerShowUpdateGameFormUri'],
            $_SESSION['managerUpdateGameUri'],
            $_SESSION['managerDeleteGameInProgressUri'],
            $_SESSION['managerDeleteGameClosedUri'],
            $_SESSION['managerDownloadGameReportUri'],
            $_SESSION['adminHomeUri'],
            $_SESSION['adminHomeUsersUri'],
            $_SESSION['adminShowNewUserFormUri'],
            $_SESSION['adminCreateUserUri'],
            $_SESSION['adminShowUpdateUserFormUri'],
            $_SESSION['adminUpdateUserFormUri'],
            $_SESSION['adminUpdateUserUri'],
            $_SESSION['adminDeleteUserUri'],
            $_SESSION['adminHomeFranchisesUri'],
            $_SESSION['adminShowNewFranchiseFormUri'],
            $_SESSION['adminCreateFranchiseUri'],
            $_SESSION['adminShowUpdateFranchiseFormUri'],
            $_SESSION['adminUpdateFranchiseUri'],
            $_SESSION['adminHomeFranchisePlayersUri'],
            $_SESSION['adminShowUpdatePlayerFormUri'],
            $_SESSION['adminUpdatePlayerUri'],
            $_SESSION['adminDeletePlayerUri'],
            $_SESSION['adminUpdateFranchisePlayersUri'],
            $_SESSION['adminHomeFranchiseCoachesUri'],
            $_SESSION['adminShowUpdateCoachFormUri'],
            $_SESSION['adminUpdateCoachUri'],
            $_SESSION['adminDeleteCoachUri'],
            $_SESSION['adminUpdateFranchiseCoachesUri'],
            $_SESSION['adminDeleteFranchiseUri'],
            $_SESSION['adminHomeArbitratorsUri'],
            $_SESSION['adminShowNewArbitratorFormUri'],
            $_SESSION['adminCreateArbitratorUri'],
            $_SESSION['adminShowUpdateArbitratorFormUri'],
            $_SESSION['adminUpdateArbitratorUri'],
            $_SESSION['adminDeleteArbitratorUri'],
            $_SESSION['adminHomeGamesUri'],
            $_SESSION['adminShowNewGameFormUri'],
            $_SESSION['adminCreateGameUri'],
            $_SESSION['adminGamesInProgressUri'],
            $_SESSION['adminShowUpdateGameInProgressFormUri'],
            $_SESSION['adminUpdateGameInProgressUri'],
            $_SESSION['adminDeleteGameInProgressUri'],
            $_SESSION['adminUpdateGameUri'],
            $_SESSION['adminHistoryGamesUri'],
            $_SESSION['adminShowUpdateHistoryGameFormUri'],
            $_SESSION['adminDeleteGameClosedUri'],
            $_SESSION['adminDownloadGameReportUri'],
            $_SESSION['apiGetHistoryGamesUri'],
            $_SESSION['apiGetGamesInProgressUri']
        ];

        // Liste des uri de base authorizés à utiliser lorsqu'on est déconnecté (ou connecté)
        $this->authorizedUris = [
            $_SESSION['homeUri'],
            $_SESSION['aboutUri'],
            $_SESSION['loginUri'],
            $_SESSION['logoutUri'],
            $_SESSION['checkAccessUri'],

            $_SESSION['apiGetHistoryGamesUri'],
            $_SESSION['apiGetGamesInProgressUri']
        ];

        if ($this->isCoach) {
            $this->authorizedUris = array_merge($this->authorizedUris,
                [
                    $_SESSION['loginSuccessUri'],
                    $_SESSION['coachHomeUri'],
                    $_SESSION['coachShowNewTeamPlayerFormUri'],
                    $_SESSION['coachCreateTeamPlayerUri'],
                ]
            );
        } else if ($this->isManager) {
            $this->authorizedUris = array_merge($this->authorizedUris,
                [
                    $_SESSION['loginSuccessUri'],
                    $_SESSION['managerHomeUri'],
                    $_SESSION['managerShowNewGameFormUri'],
                    $_SESSION['managerCreateGameUri'],
                    $_SESSION['managerGamesInProgressUri'],
                    $_SESSION['managerHistoryGamesUri'],
                    $_SESSION['managerShowUpdateHistoryGameFormUri'],
                    $_SESSION['managerShowUpdateGameFormUri'],
                    $_SESSION['managerUpdateGameUri'],
                    $_SESSION['managerDeleteGameInProgressUri'],
                    $_SESSION['managerDeleteGameClosedUri'],
                    $_SESSION['managerDownloadGameReportUri']
                ]
            );
        } else if ($this->isAdmin) {
            $this->authorizedUris = array_merge($this->authorizedUris,
                [
                    $_SESSION['loginSuccessUri'],
                    $_SESSION['adminHomeUri'],
                    $_SESSION['adminHomeUsersUri'],
                    $_SESSION['adminShowNewUserFormUri'],
                    $_SESSION['adminCreateUserUri'],
                    $_SESSION['adminShowUpdateUserFormUri'],
                    $_SESSION['adminUpdateUserFormUri'],
                    $_SESSION['adminUpdateUserUri'],
                    $_SESSION['adminDeleteUserUri'],
                    $_SESSION['adminHomeFranchisesUri'],
                    $_SESSION['adminShowNewFranchiseFormUri'],
                    $_SESSION['adminCreateFranchiseUri'],
                    $_SESSION['adminShowUpdateFranchiseFormUri'],
                    $_SESSION['adminUpdateFranchiseUri'],
                    $_SESSION['adminHomeFranchisePlayersUri'],
                    $_SESSION['adminShowUpdatePlayerFormUri'],
                    $_SESSION['adminUpdatePlayerUri'],
                    $_SESSION['adminDeletePlayerUri'],
                    $_SESSION['adminUpdateFranchisePlayersUri'],
                    $_SESSION['adminHomeFranchiseCoachesUri'],
                    $_SESSION['adminShowUpdateCoachFormUri'],
                    $_SESSION['adminUpdateCoachUri'],
                    $_SESSION['adminDeleteCoachUri'],
                    $_SESSION['adminUpdateFranchiseCoachesUri'],
                    $_SESSION['adminDeleteFranchiseUri'],
                    $_SESSION['adminHomeArbitratorsUri'],
                    $_SESSION['adminShowNewArbitratorFormUri'],
                    $_SESSION['adminCreateArbitratorUri'],
                    $_SESSION['adminShowUpdateArbitratorFormUri'],
                    $_SESSION['adminUpdateArbitratorUri'],
                    $_SESSION['adminDeleteArbitratorUri'],
                    $_SESSION['adminHomeGamesUri'],
                    $_SESSION['adminShowNewGameFormUri'],
                    $_SESSION['adminCreateGameUri'],
                    $_SESSION['adminGamesInProgressUri'],
                    $_SESSION['adminShowUpdateGameInProgressFormUri'],
                    $_SESSION['adminUpdateGameInProgressUri'],
                    $_SESSION['adminDeleteGameInProgressUri'],
                    $_SESSION['adminUpdateGameUri'],
                    $_SESSION['adminHistoryGamesUri'],
                    $_SESSION['adminShowUpdateHistoryGameFormUri'],
                    $_SESSION['adminDeleteGameClosedUri'],
                    $_SESSION['adminDownloadGameReportUri']
                ]
            );
        }
    }

    public function route(): void
    {
        if (!in_array($this->actualUri, $this->allUris)) {
            http_response_code(404);
            echo 'Page not found';
            return;
        }

        if (!in_array($this->actualUri, $this->authorizedUris)) {
            http_response_code(400);
            echo 'Page not authorized';
            return;
        }

        if ($this->baseRoute()) {
            return;
        }

        if ($this->isCoach) {
            $this->coachRoute();
        } else if ($this->isManager) {
            $this->managerRoute();
        } else if ($this->isAdmin) {
            $this->adminRoute();
        }
    }

    private function managerRoute(): void
    {
        $managerController = new ManagerController();

        switch ($this->actualUri) {
            case $_SESSION['loginSuccessUri']:
                $managerController->showHome();
                header('Location: ' . $_SESSION['managerHomeUri']);
                break;
            case $_SESSION['managerHomeUri']:
                include $_SESSION['dirViews'] . '/manager/home.php';
                break;
            case $_SESSION['managerShowNewGameFormUri']:
                $managerController->newGameForm();
                break;
            case $_SESSION['managerCreateGameUri']:
                $managerController->createGame($_POST);
                break;
            case $_SESSION['managerGamesInProgressUri']:
                $managerController->gamesInProgress();
                break;
            case $_SESSION['managerHistoryGamesUri']:
                $managerController->historyGames();
                break;
            case $_SESSION['managerShowUpdateGameFormUri']:
                $managerController->closingGameForm($_POST['gameId']);
                break;
            case $_SESSION['managerShowUpdateHistoryGameFormUri']:
                $managerController->historyGameForm($_POST['gameId']);
                break;
            case $_SESSION['managerUpdateGameUri']:
                $managerController->updateOrClosingGame($_POST);
                break;
            case $_SESSION['managerDeleteGameInProgressUri']:
                $managerController->deleteGameInProgress($_POST);
                break;
            case $_SESSION['managerDeleteGameClosedUri']:
                $managerController->deleteGameClosed($_POST);
                break;
            case $_SESSION['managerDownloadGameReportUri']:
                $managerController->downloadGameReport($_POST);
                break;
            default:
                echo 'Page not authorized';
        }
    }

    private function coachRoute(): void
    {
        $coachController = new CoachController();

        switch ($this->actualUri) {
            case $_SESSION['loginSuccessUri']:
                $coachController->showHome();
                break;
            case $_SESSION['coachHomeUri']:
                $coachController->showGames();
                break;
            case $_SESSION['coachShowNewTeamPlayerFormUri']:
                $_SESSION['teamId'] = $_POST['teamId'];
                $coachController->newTeamPlayerForm($_POST['teamId']);
                break;
            case $_SESSION['coachCreateTeamPlayerUri']:
                $coachController->createTeamPlayer($_POST);
                break;
            default:
                echo 'Page not authorized';
        }
    }

    private function adminRoute(): void
    {
        $adminController = new AdminController();

        switch ($this->actualUri) {
            case $_SESSION['loginSuccessUri']:
                $adminController->showHome();
                break;
            case $_SESSION['adminHomeUri']:
                include $_SESSION['dirViews'] . '/admin/home.php';
                break;
            case $_SESSION['adminHomeUsersUri']:
                $adminController->homeUsers();
                break;
            case $_SESSION['adminShowNewUserFormUri']:
                $adminController->showNewUserForm();
                break;
            case $_SESSION['adminCreateUserUri']:
                $adminController->createUser($_POST);
                break;
            case $_SESSION['adminShowUpdateUserFormUri']:
                $_SESSION['userId'] = $_POST['userId'];
                $adminController->updateUserForm($_SESSION['userId']);
                break;
            case $_SESSION['adminUpdateUserUri']:
                $adminController->updateUser($_POST);
                break;
            case $_SESSION['adminDeleteUserUri']:
                $adminController->deleteUser($_POST);
                break;
            case $_SESSION['adminHomeFranchisesUri']:
                $adminController->homeFranchises();
                break;
            case $_SESSION['adminShowNewFranchiseFormUri']:
                $adminController->showNewFranchiseForm();
                break;
            case $_SESSION['adminCreateFranchiseUri']:
                $adminController->createFranchise($_POST);
                break;
            case $_SESSION['adminShowUpdateFranchiseFormUri']:
                $_SESSION['franchiseId'] = $_POST['franchiseId'];
                $adminController->showUpdateFranchiseForm($_SESSION['franchiseId']);
                break;
            case $_SESSION['adminUpdateFranchiseUri']:
                $adminController->updateFranchise($_POST);
                break;
            case $_SESSION['adminHomeFranchisePlayersUri']:
                $franchiseId = $_POST['franchiseId'] ?? $_SESSION['franchiseId'];
                $_SESSION['franchiseId'] = $franchiseId;
                $adminController->homeFranchisePlayers($franchiseId);
                break;
            case $_SESSION['adminShowUpdatePlayerFormUri']:
                $_SESSION['playerId'] = $_POST['playerId'];
                $adminController->showUpdatePlayerForm($_SESSION['playerId']);
                break;
            case $_SESSION['adminUpdatePlayerUri']:
                $adminController->updatePlayer($_POST);
                break;
            case $_SESSION['adminDeletePlayerUri']:
                $adminController->deletePlayer($_POST);
                break;
            case $_SESSION['adminUpdateFranchisePlayersUri']:
                $adminController->updateFranchisePlayers($_POST);
                break;
            case $_SESSION['adminHomeFranchiseCoachesUri']:
                $franchiseId = $_POST['franchiseId'] ?? $_SESSION['franchiseId'];
                $_SESSION['franchiseId'] = $franchiseId;
                $adminController->homeFranchiseCoaches($franchiseId);
                break;
            case $_SESSION['adminShowUpdateCoachFormUri']:
                $_SESSION['coachId'] = $_POST['coachId'];
                $adminController->showUpdateCoachForm($_SESSION['coachId']);
                break;
            case $_SESSION['adminUpdateCoachUri']:
                $adminController->updateCoach($_POST);
                break;
            case $_SESSION['adminDeleteCoachUri']:
                $_SESSION['coachId'] = $_POST['coachId'];
                $adminController->deleteCoach($_SESSION['coachId']);
                break;
            case $_SESSION['adminUpdateFranchiseCoachesUri']:
                $adminController->updateFranchiseCoaches($_POST);
                break;
            case $_SESSION['adminDeleteFranchiseUri']:
                $_SESSION['franchiseId'] = $_POST['franchiseId'];
                $adminController->deleteFranchise($_SESSION['franchiseId']);
                break;
            case $_SESSION['adminHomeArbitratorsUri']:
                $adminController->homeArbitrators();
                break;
            case $_SESSION['adminShowNewArbitratorFormUri']:
                $adminController->showNewArbitratorForm();
                break;
            case $_SESSION['adminCreateArbitratorUri']:
                $adminController->createArbitrator($_POST);
                break;
            case $_SESSION['adminShowUpdateArbitratorFormUri']:
                $_SESSION['arbitratorId'] = $_POST['arbitratorId'];
                $adminController->showUpdateArbitratorForm($_SESSION['arbitratorId']);
                break;
            case $_SESSION['adminUpdateArbitratorUri']:
                $adminController->updateArbitrator($_POST);
                break;
            case $_SESSION['adminDeleteArbitratorUri']:
                $adminController->deleteArbitrator($_POST);
                break;
            case $_SESSION['adminHomeGamesUri']:
                $adminController->homeGames();
                break;
            case $_SESSION['adminShowNewGameFormUri']:
                $adminController->showNewGameForm();
                break;
            case $_SESSION['adminCreateGameUri']:
                $adminController->createGame($_POST);
                break;
            case $_SESSION['adminGamesInProgressUri']:
                $adminController->gamesInProgress();
                break;
            case $_SESSION['adminShowUpdateGameInProgressFormUri']:
                $adminController->updateShowGameInProgressForm($_POST['gameId']);
                break;
            case $_SESSION['adminUpdateGameInProgressUri']:
                $adminController->updateGameInProgress($_POST);
                break;
            case $_SESSION['adminDeleteGameInProgressUri']:
                $adminController->deleteGameInProgress($_POST);
                break;
            case $_SESSION['adminUpdateGameUri']:
                $adminController->updateOrClosingGame($_POST);
                break;
            case $_SESSION['adminHistoryGamesUri']:
                $adminController->historyGames();
                break;
            case $_SESSION['adminShowUpdateHistoryGameFormUri']:
                $adminController->showUpdateHistoryGameForm($_POST['gameId']);
                break;
            case $_SESSION['adminDeleteGameClosedUri']:
                $adminController->deleteGameClosed($_POST);
                break;
            case $_SESSION['adminDownloadGameReportUri']:
                $adminController->downloadGameReport($_POST);
                break;
            default:
                echo 'Page not authorized';
        }
    }

    private function baseRoute(): bool
    {
        switch ($this->actualUri) {
            case $_SESSION['homeUri']:
                include $_SESSION['dirViews'] . '/home/home.php';
                break;
            case $_SESSION['aboutUri']:
                include $_SESSION['dirViews'] . '/about/about.php';
                break;
            case $_SESSION['checkAccessUri']:
                $controller = new LoginController();
                $controller->checkUser($_POST);
                break;
            case $_SESSION['loginUri']:
                $controller = new LoginController();
                $controller->showLoginPage();
                break;
            case $_SESSION['logoutUri']:
                $controller = new LoginController();
                $controller->logout();
                break;
            case $_SESSION['apiGetHistoryGamesUri']:
                $apiController = new ApiController();
                $apiController->getHistoryGames();
                break;
            case $_SESSION['apiGetGamesInProgressUri']:
                $apiController = new ApiController();
                $apiController->getGamesInProgress();
                break;
            default:
                return false;
        }
        return true;
    }
}
