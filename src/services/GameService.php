<?php
include_once __DIR__ . '/../../config/connexion-bdd.php';
include_once __DIR__ . '/../models/Game.php';
include_once __DIR__ . '/../models/Arbitrator.php';
include_once __DIR__ . '/../models/DetailedGame.php';
include_once __DIR__ . '/../repositories/GameRepository.php';
include_once __DIR__ . '/../repositories/ArbitratorRepository.php';
include_once __DIR__ . '/../repositories/TeamRepository.php';
include_once __DIR__ . '/HelperService.php';
include_once __DIR__ . '/TeamService.php';
include_once __DIR__ . '/../repositories/HistorySubstituteRepository.php';
include_once __DIR__ . '/../repositories/HistoryTeamPlayerPointsRepository.php';
include_once __DIR__ . '/../repositories/HistoryTeamPlayerFoulsRepository.php';
include_once __DIR__ . '/../repositories/TeamPlayerRepository.php';
include_once __DIR__ . '/../repositories/GameArbitratorRepository.php';
include_once __DIR__ . '/FoulService.php';
include_once __DIR__ . '/PointService.php';
include_once __DIR__ . '/PdfService.php';

class GameService
{
    private HistoryTeamPlayerPointsRepository $historyTeamPlayerPointsRepository;
    private HistoryTeamPlayerFoulsRepository $historyTeamPlayerFoulsRepository;
    private HistorySubstituteRepository $historySubstituteRepository;
    private GameArbitratorRepository $gameArbitratorRepository;
    private ArbitratorRepository $arbitratorRepository;
    private TeamPlayerRepository $teamPlayerRepository;
    private GameRepository $gameRepository;
    private TeamRepository $teamRepository;

    private ArbitratorService $arbitratorService;
    private PlayerService $playerService;
    private TeamService $teamService;
    private FoulService $foulService;
    private PointService $pointService;

    public function __construct()
    {
        $this->historySubstituteRepository = new HistorySubstituteRepository();
        $this->historyTeamPlayerPointsRepository = new HistoryTeamPlayerPointsRepository();
        $this->historyTeamPlayerFoulsRepository = new HistoryTeamPlayerFoulsRepository();
        $this->gameArbitratorRepository = new GameArbitratorRepository();
        $this->arbitratorRepository = new ArbitratorRepository();
        $this->teamPlayerRepository = new TeamPlayerRepository();
        $this->gameRepository = new GameRepository();
        $this->teamRepository = new TeamRepository();

        $this->arbitratorService = new ArbitratorService();
        $this->playerService = new PlayerService();
        $this->teamService = new TeamService();
        $this->foulService = new FoulService();
        $this->pointService = new PointService();
    }

    public function getAllGame() : array
    {
        return $this->gameRepository->getAll();
    }

    public function getGamesInProgress(): array
    {
        return $this->gameRepository->getGamesInProgress();
    }

    public function getGamesByFranchise($franchiseId): array
    {
        return $this->gameRepository->getGamesByFranchise($franchiseId);
    }

    public function getGame($gameId): Game
    {
        return $this->gameRepository->get($gameId);
    }

    public function getHistoryGames(): array
    {
        return $this->gameRepository->getHistoryGames();
    }

    public function create(array $data): bool
    {
        $data = HelperService::CleanArrayFromHtlm($data);

        $beginDateTime = $data['beginDateTime'];
        $placeId = (int)$data['placeId'];
        $game = $this->gameRepository->create($beginDateTime, $placeId);

        $arbitratorHolderId = (int)$data['arbitratorHolderId'];
        $this->arbitratorRepository->createForGame($game->getId(), $arbitratorHolderId, Arbitrator::TITLE_HOLDER);

        $arbitratorAssistant1Id = (int)$data['arbitratorAssistant1Id'];
        $this->arbitratorRepository->createForGame($game->getId(), $arbitratorAssistant1Id, Arbitrator::TITLE_ASSISTANT);

        $arbitratorAssistant2Id = (int)$data['arbitratorAssistant2Id'];
        $this->arbitratorRepository->createForGame($game->getId(), $arbitratorAssistant2Id, Arbitrator::TITLE_ASSISTANT);

        $homeFranchiseId = (int)$data['homeFranchiseId'];
        $this->teamRepository->create($game->getId(), $homeFranchiseId, true);

        $awayFranchiseId = (int)$data['awayFranchiseId'];
        $this->teamRepository->create($game->getId(), $awayFranchiseId);

        return true;
    }

    public function update(array $data): bool
    {
        $data = HelperService::CleanArrayFromHtlm($data);

        $gameId = $data['gameId'];
        $beginDateTime = $data['beginDateTime'];
        $placeId = (int)$data['placeId'];
        $gameTime = NULL;
        if (isset($data['gameTime']) && $data['gameTime'] !== '') {
            $gameTime = (int)$data['gameTime'];
        }

        return $this->gameRepository->update($gameId, $beginDateTime, $placeId, $gameTime);
    }

    public function delete(int $gameId): bool
    {
        $id = (int)HelperService::CleanVariableFromHtlm($gameId);

        if ($this->gameRepository->get($id)) {
            $this->historyTeamPlayerPointsRepository->deleteByGame($id);
            $this->historyTeamPlayerFoulsRepository->deleteByGame($id);
            $this->historySubstituteRepository->deleteByGame($id);
            $this->teamPlayerRepository->deleteByGame($id);
            $this->teamRepository->deleteByGame($id);
            $this->gameArbitratorRepository->deleteByGame($id);
            $this->gameRepository->delete($id);
        }
        return false;
    }

//    public function getListOngoingGamesByFranchise($franchiseId): array
//    {
//        return $this->gameRepository->getOngoingGamesByFranchise($franchiseId);
//    }

    public function getDetailedGame(int $gameId): DetailedGame
    {
        $gameId = HelperService::CleanVariableFromHtlm($gameId);
        $game = $this->getGame($gameId);
        $teams = $this->teamService->getHomeTeamAndAwayTeamByGame($gameId);

        $arbitrators = $this->arbitratorService->getAllByGame($gameId);

        $franchiseHomeId = $teams['home']->getFranchise()->getId();
        $franchiseAwayId = $teams['away']->getFranchise()->getId();

        $homeTeamPlayers = $this->playerService->getTeamPlayersByGameByFranchise($gameId, $franchiseHomeId);
        $awayTeamPlayers = $this->playerService->getTeamPlayersByGameByFranchise($gameId, $franchiseAwayId);

        $historyHomeSubstitutes = $this->getHistorySubstitutesByGameByFranchise($gameId, $franchiseHomeId);
        $historyAwaySubstitutes = $this->getHistorySubstitutesByGameByFranchise($gameId, $franchiseAwayId);

        $historyHomeTeamPlayerPoints = $this->getHistoryTeamPlayerPointsByGameByFranchise($gameId, $franchiseHomeId);
        $historyAwayTeamPlayerPoints = $this->getHistoryTeamPlayerPointsByGameByFranchise($gameId, $franchiseAwayId);

        $historyHomeTeamPlayerFouls = $this->getHistoryTeamPlayerFoulsByGameByFranchise($gameId, $franchiseHomeId);
        $historyAwayTeamPlayerFouls = $this->getHistoryTeamPlayerFoulsByGameByFranchise($gameId, $franchiseAwayId);

        $homeDetailedTeam = new DetailedTeam($teams['home'], $homeTeamPlayers, $historyHomeTeamPlayerFouls, $historyHomeSubstitutes, $historyHomeTeamPlayerPoints);
        $awayDetailedTeam = new DetailedTeam($teams['away'], $awayTeamPlayers, $historyAwayTeamPlayerFouls, $historyAwaySubstitutes, $historyAwayTeamPlayerPoints);

        return new DetailedGame($game, $homeDetailedTeam, $awayDetailedTeam, $arbitrators);
    }

    public function getDetailedGamesInProgress(): array
    {
        $games = $this->getGamesInProgress();
        $detailedGames = [];
        foreach($games as $game) {
            $detailedGames[] = $this->getDetailedGame($game->getId());
        }
        return $detailedGames;
    }

    public function getDetailedGamesInProgressByFranchise(int $franchiseId): array
    {
        $franchiseId = HelperService::CleanVariableFromHtlm($franchiseId);
        $games = $this->gameRepository->getGamesInProgressByFranchise($franchiseId);
        $detailedGames = [];
        foreach($games as $game) {
            $detailedGames[] = $this->getDetailedGame($game->getId());
        }
        return $detailedGames;
    }

    public function getHistorySubstitutesByGameByFranchise(int $gameId, int $franchiseId): array
    {
        $gameId = HelperService::CleanVariableFromHtlm($gameId);
        $franchiseId = HelperService::CleanVariableFromHtlm($franchiseId);
        return $this->historySubstituteRepository->getAllByGameByFranchise($gameId, $franchiseId);
    }

    public function getHistoryTeamPlayerPointsByGameByFranchise(int $gameId, int $franchiseId): array
    {
        $gameId = HelperService::CleanVariableFromHtlm($gameId);
        $franchiseId = HelperService::CleanVariableFromHtlm($franchiseId);
        return $this->historyTeamPlayerPointsRepository->getAllByGameByFranchise($gameId, $franchiseId);
    }

    public function getHistoryTeamPlayerFoulsByGameByFranchise(int $gameId, int $franchiseId): array
    {
        $gameId = HelperService::CleanVariableFromHtlm($gameId);
        $franchiseId = HelperService::CleanVariableFromHtlm($franchiseId);
        return $this->historyTeamPlayerFoulsRepository->getAllByGameByFranchise($gameId, $franchiseId);
    }

    public function updateHistoryTeamPlayerPoint(array $pointTypesByTeamPLayerAtGameTimes, int $gameId): void
    {
        $pointTypesByTeamPLayerAtGameTimes = HelperService::CleanArrayFromHtlm($pointTypesByTeamPLayerAtGameTimes);
        $this->historyTeamPlayerPointsRepository->deleteByGame($gameId);
        foreach($pointTypesByTeamPLayerAtGameTimes as $pointTypesByTeamPLayerAtGameTime) {
            $teamPlayerId = $pointTypesByTeamPLayerAtGameTime['teamPlayerId'];
            $pointTypeId = $pointTypesByTeamPLayerAtGameTime['pointTypeId'];
            $pointTime = $pointTypesByTeamPLayerAtGameTime['pointTime'];
            $this->historyTeamPlayerPointsRepository->create($teamPlayerId, $pointTypeId, $pointTime);
        }
    }

    public function updateHistoryTeamPlayerFoul(array $foulTypesByTeamPlayerAtGameTimes, int $gameId): void
    {
        $foulTypesByTeamPlayerAtGameTimes = HelperService::CleanArrayFromHtlm($foulTypesByTeamPlayerAtGameTimes);
        $this->historyTeamPlayerFoulsRepository->deleteByGame($gameId);
        foreach($foulTypesByTeamPlayerAtGameTimes as $foulTypesByTeamPlayerAtGameTime) {
            $teamPlayerId = $foulTypesByTeamPlayerAtGameTime['teamPlayerId'];
            $foulTypeId = $foulTypesByTeamPlayerAtGameTime['foulTypeId'];
            $foulTime = $foulTypesByTeamPlayerAtGameTime['foulTime'];
            $this->historyTeamPlayerFoulsRepository->create($teamPlayerId, $foulTypeId, $foulTime);
        }
    }

    public function updateHistorySubstitute(array $substitutesTeamPlayerAtGameTimes, int $gameId): void
    {
        $substitutesTeamPlayerAtGameTimes = HelperService::CleanArrayFromHtlm($substitutesTeamPlayerAtGameTimes);
        $this->historySubstituteRepository->deleteByGame($gameId);
        foreach($substitutesTeamPlayerAtGameTimes as $substitutesTeamPlayerAtGameTime) {
            $incomingPlayerId = $substitutesTeamPlayerAtGameTime['incomingTeamPlayerId'];
            $outgoingPlayerId = $substitutesTeamPlayerAtGameTime['outgoingTeamPlayerId'];
            $substituteTime = $substitutesTeamPlayerAtGameTime['substituteTime'];
            $this->historySubstituteRepository->create($incomingPlayerId, $outgoingPlayerId, $substituteTime);
        }
    }

    public function updateScore(int $gameId, int $franchiseId, int $score): void
    {
        $this->teamRepository->updateScore($gameId, $franchiseId, $score);
    }

    public function closeGame(array $data): bool
    {
        $data = HelperService::CleanArrayFromHtlm($data);
        $gameMinuteTime = $data['gameMinuteTime'];
        $gameSecondTime = $data['gameSecondTime'];
        if ($gameMinuteTime === '' && $gameSecondTime === '') {
            return true;
        }
        $gameTime = $gameMinuteTime . ":" . $gameSecondTime;
        $gameId = $data['gameId'];
        return $this->gameRepository->updateGameTime($gameId, $gameTime);
    }

    /**
     * Met à jours ou cloture les données du jeu
     * @param array $data
     * @return void
     */
    public function updateOrClosingGame(array $data): void
    {
        $data = HelperService::CleanArrayFromHtlm($data);
        // Points
        $pointTypesByTeamPLayerAtGameTimes = [];

        if(array_key_exists('teamPlayerPointsScoredIds', $data)) {
            $pointTypeByTeamPlayerAtGameTime = [
                'teamPlayerId' => 0,
                'pointTypeId' => 0,
                'pointTime' => ''
            ];

            $teamPlayerPointsScoredIds = $data['teamPlayerPointsScoredIds'];
            $pointTypeIds = $data['pointTypeIds'];
            $pointMinuteTimes = $data['pointMinuteTimes'];
            $pointSecondTimes = $data['pointSecondTimes'];

            foreach ($teamPlayerPointsScoredIds as $key => $teamPlayerPointsScoredId) {
                $pointTypeByTeamPlayerAtGameTime['teamPlayerId'] = $teamPlayerPointsScoredId;
                $pointTypeByTeamPlayerAtGameTime['pointTypeId'] = $pointTypeIds[$key];
                $pointTypeByTeamPlayerAtGameTime['pointTime'] = $pointMinuteTimes[$key] . ':' . $pointSecondTimes[$key];
                $pointTypesByTeamPLayerAtGameTimes[] = $pointTypeByTeamPlayerAtGameTime;
            }
        }

        // Fouls
        $foulTypesByTeamPlayerAtGameTimes = [];

        if(array_key_exists('foulTeamPlayerIds', $data)) {
            $foulTypesByTeamPlayerAtGameTime = [
                'teamPlayerId' => 0,
                'foulTypeId' => 0,
                'foulTime' => ''
            ];
            $teamPlayerFoulsIds = $data['foulTeamPlayerIds'];
            $foulTypeIds = $data['foulTypeIds'];
            $foulMinuteTimes = $data['foulMinuteTimes'];
            $foulSecondTimes = $data['foulSecondTimes'];

            foreach ($teamPlayerFoulsIds as $key => $teamPlayerFoulsId) {
                $foulTypesByTeamPlayerAtGameTime['teamPlayerId'] = $teamPlayerFoulsId;
                $foulTypesByTeamPlayerAtGameTime['foulTypeId'] = $foulTypeIds[$key];
                $foulTypesByTeamPlayerAtGameTime['foulTime'] = $foulMinuteTimes[$key] . ':' . $foulSecondTimes[$key];
                $foulTypesByTeamPlayerAtGameTimes[] = $foulTypesByTeamPlayerAtGameTime;
            }
        }

        // Substitutes
        $substitutesTeamPlayerAtGameTimes = [];

        if(array_key_exists('incomingTeamPlayerIds', $data)) {
            $substituteTeamPlayerAtGameTimes = [
                'incomingTeamPlayerId' => 0,
                'outgoingTeamPlayerId' => 0,
                'substituteTime' => ''
            ];
            $incomingTeamPlayerIds = $data['incomingTeamPlayerIds'];
            $outgoingTeamPlayerIds = $data['outgoingTeamPlayerIds'];
            $substituteMinuteTimes = $data['substituteMinuteTimes'];
            $substituteSecondTimes = $data['substituteSecondTimes'];

            foreach ($incomingTeamPlayerIds as $key => $incomingTeamPlayerId) {
                $substituteTeamPlayerAtGameTimes['incomingTeamPlayerId'] = $incomingTeamPlayerId;
                $substituteTeamPlayerAtGameTimes['outgoingTeamPlayerId'] = $outgoingTeamPlayerIds[$key];
                $substituteTeamPlayerAtGameTimes['substituteTime'] = $substituteMinuteTimes[$key] . ':' . $substituteSecondTimes[$key];
                $substitutesTeamPlayerAtGameTimes[] = $substituteTeamPlayerAtGameTimes;
            }
        }

        $gameId = $data['gameId'];

        if (array_key_exists('scoreHome', $data)) {
            $scoreHome = $data['scoreHome'];
            $franchiseHomeId = $data['franchiseHomeId'];
            $this->updateScore($gameId, $franchiseHomeId, $scoreHome);
        }

        if (array_key_exists('scoreAway', $data)) {
            $scoreAway = $data['scoreAway'];
            $franchiseAwayId = $data['franchiseAwayId'];
            $this->updateScore($gameId, $franchiseAwayId, $scoreAway);
        }

        $this->updateHistoryTeamPlayerPoint($pointTypesByTeamPLayerAtGameTimes, $gameId);
        $this->updateHistoryTeamPlayerFoul($foulTypesByTeamPlayerAtGameTimes, $gameId);
        $this->updateHistorySubstitute($substitutesTeamPlayerAtGameTimes, $gameId);

        $this->closeGame($data);
    }

    public function downloadGameReport(int $gameId): void
    {
        $detailGame = $this->getDetailedGame($gameId);
        PdfService::createGameReport($detailGame);

        $fullPath = $_SESSION['dirGameReport']."/".$gameId.".pdf";

        if (is_readable($fullPath)) {
            $fsize = filesize($fullPath);
            header("Content-type: application/pdf");
            // 'attachment' is to force a download
            header("Content-Disposition: attachment; filename=Feuille_de_match_du_" . $detailGame->getGame()->getBeginDateTime()->format('d-m-Y_H-i') . ".pdf");
            header("Content-length: $fsize");
            // Open files directly
            header("Cache-control: private");
            readfile($fullPath);
        }

    }

    public function renderGameForm(int $gameId, $viewPath): void
    {
        $foulTypes = $this->foulService->getFoulTypes();
        $pointTypes = $this->pointService->getPointTypes();
        $detailedGame = $this->getDetailedGame($gameId);

        $homeDetailedTeam = $detailedGame->getHomeDetailedTeam();
        $awayDetailedTeam = $detailedGame->getAwayDetailedTeam();

        $franchiseHomeId = $homeDetailedTeam->getTeam()->getFranchise()->getId();
        $franchiseAwayId = $awayDetailedTeam->getTeam()->getFranchise()->getId();

        $homeTeamPlayers = $homeDetailedTeam->getTeamPlayers();
        $historyHomeSubstitutes = $homeDetailedTeam->getHistorySubstitutes();
        $historyHomeTeamPlayerPoints = $homeDetailedTeam->getHistoryTeamPlayerPoints();
        $historyHomeTeamPlayerFouls = $homeDetailedTeam->getHistoryTeamPlayerFouls();

        $awayTeamPlayers = $awayDetailedTeam->getTeamPlayers();
        $historyAwaySubstitutes = $awayDetailedTeam->getHistorySubstitutes();
        $historyAwayTeamPlayerPoints = $awayDetailedTeam->getHistoryTeamPlayerPoints();
        $historyAwayTeamPlayerFouls = $awayDetailedTeam->getHistoryTeamPlayerFouls();

        include $_SESSION['dirViews'] . $viewPath;
    }

    public function getDetailedHistoryGames(): array
    {
        $games = $this->getHistoryGames();
        $detailedGames = [];
        foreach ($games as $game) {
            $detailedGame = $this->getDetailedGame($game->getId());
            $detailedGames[] = $detailedGame;
        }
        return $detailedGames;
    }
}