<?php
include_once __DIR__ . '/../../lib/tfpdf/tfpdf.php';

class GameReport extends tFPDF {

    public function __construct() {
        parent::__construct();
        // Add a Unicode font (uses UTF-8)
        $this->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
        $this->AddFont('DejaVuB','','DejaVuSansCondensed-Bold.ttf',true);
    }

    function Header(): void
    {
        $this->SetFont('DejaVuB', '', 14);
        $this->Cell(0, 10, 'Match de Basketball - Résumé', 0, 1, 'C');
        $this->Ln(10);
    }

    function Footer(): void
    {
        $this->SetY(-15);
        $this->SetFont('DejaVu', '', 8);
        $this->Cell(0, 10, 'Page '.$this->PageNo().'/{nb}', 0, 0, 'C');
    }

    function GameInfos($date, $address, $time, $gameMinuteTime, $gameSecondTime, $homeTeam, $awayTeam, $homeScore, $awayScore): void
    {
        $this->SetFont('DejaVu', '', 12);
        $this->Cell(0, 10, "Date du match : $date", 0, 1);
        $this->Cell(0, 10, "Adresse du lieu : $address", 0, 1);
        $this->Cell(0, 10, "Heure du début du match : $time", 0, 1);
        $this->Cell(0, 10, "Temps de jeu : $gameMinuteTime min $gameSecondTime sec", 0, 1);
        $this->Cell(0, 10, "Equipes : $homeTeam vs $awayTeam", 0, 1);
        $this->Cell(0, 10, "Score: $homeScore VS $awayScore", 0, 1);
        $this->Ln(10);
    }

    function TeamInfos($team, $captain, $viceCaptain, $players, $substitutes, $points, $sanctions, $substitutions): void
    {
        $this->AddPage();
        $this->SetFont('DejaVuB', '', 14);
        $this->Cell(0, 10, "Équipe : $team", 0, 1, 'C');
        $this->Ln(10);

        $this->SetFont('DejaVu', '', 12);
        $this->Cell(0, 10, "Capitaine : $captain", 0, 1);
        $this->Cell(0, 10, "Capitaine Suppléant : $viceCaptain", 0, 1);
        $this->Ln(5);

        // Titulaires
        $this->SetFont('DejaVuB', '', 12);
        $this->Cell(0, 10, "Joueurs titulaires :", 0, 1);
        $this->SetFont('DejaVu', '', 12);
        foreach ($players as $player) {
            $this->Cell(0, 10, "- {$player['position']} : {$player['name']} ", 0, 1);
        }
        $this->Ln(5);

        // Remplaçants
        $this->SetFont('DejaVuB', '', 12);
        $this->Cell(0, 10, "Joueurs remplaçants :", 0, 1);
        $this->SetFont('DejaVu', '', 12);
        foreach ($substitutes as $sub) {
            $this->Cell(0, 10, "- $sub", 0, 1);
        }
        $this->Ln(10);

        // Points
        $this->SetFont('DejaVuB', '', 12);
        $this->Cell(0, 10, "Points marqués :", 0, 1);
        $this->SetFont('DejaVu', '', 12);
        $this->CreateTable(['Temps', 'Joueur', 'Type', 'Points'], $points);

        // Sanctions
        $this->SetFont('DejaVuB', '', 12);
        $this->Cell(0, 10, "Sanctions :", 0, 1);
        $this->SetFont('DejaVu', '', 12);
        $this->CreateTable(['Temps', 'Joueur', 'Sanction'], $sanctions);

        // Remplacements
        $this->SetFont('DejaVuB', '', 12);
        $this->Cell(0, 10, "Remplacements :", 0, 1);
        $this->SetFont('DejaVu', '', 12);
        $this->CreateTable(['Temps', 'Sortant', 'Entrant'], $substitutions);
    }

    function CreateTable($header, $data): void
    {
        $w = array(40, 60, 50, 30);

        // En-tête
        for($i = 0; $i < count($header); $i++) {
            $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C');
        }
        $this->Ln();

        // Données
        foreach ($data as $row) {
            $i = 0;
            foreach ($row as $col) {
                $this->Cell($w[$i], 6, $col, 1);
                $i++;
            }
            $this->Ln();
        }
        $this->Ln(10);
    }

    function Referees($mainReferee, $assistantRefs): void
    {
        $this->AddPage();
        $this->SetFont('DejaVuB', '', 14);
        $this->Cell(0, 10, "Arbitres", 0, 1, 'C');
        $this->Ln(10);

        $this->SetFont('DejaVu', '', 12);
        $this->Cell(0, 10, "Arbitre Principal : $mainReferee", 0, 1);
        foreach ($assistantRefs as $ref) {
            $this->Cell(0, 10, "Arbitre Assistant : $ref", 0, 1);
        }
        $this->Ln(10);
    }
}