<?php
include_once __DIR__ . '/../repositories/FoulTypeRepository.php';

class FoulService
{
    private FoulTypeRepository $foulTypeRepository;

    public function __construct()
    {
        $this->foulTypeRepository = new FoulTypeRepository();
    }

    public function getFoulTypes(): array
    {
        return $this->foulTypeRepository->getFoulTypes();
    }
}