<?php
include_once __DIR__ . '/../models/Player.php';
include_once __DIR__ . '/../repositories/PlayerRepository.php';
include_once __DIR__ . '/../repositories/TeamPlayerRepository.php';

class PlayerService
{
    private PlayerRepository $playerRepository;
    private TeamPlayerRepository $teamPlayerRepository;


    public function __construct()
    {
        $this->playerRepository = new PlayerRepository();
        $this->teamPlayerRepository = new TeamPlayerRepository();
    }

    public function getPlayer($playerId): Player
    {
        return $this->playerRepository->getPlayer($playerId);
    }

    public function getActualPlayersByFranchise($franchiseId): array
    {
        $players = $this->playerRepository->getPlayersByFranchise($franchiseId);
        $actualPlayers = [];
        foreach($players as $player) {
            if ($player->getOutDate() === NULL) {
                $actualPlayers[] = $player;
            }
        }
        return $actualPlayers;
    }

    public function getInAndOutPlayersByFranchise($franchiseId): array
    {
        return $this->playerRepository->getPlayersByFranchise($franchiseId);
    }

    public function getPlayersByGame(int $gameId): array
    {
        return $this->playerRepository->getPlayersByGame($gameId);
    }

    public function getTeamPlayersByGameByFranchise(int $gameId, int $franchiseId): array
    {
        return $this->teamPlayerRepository->getTeamPlayersByGameByFranchise($gameId, $franchiseId);
    }

    public function getInAndOutPlayersByGame(int $gameId): array
    {
        return $this->playerRepository->getPlayersByGame($gameId);
    }

    public function getTeamPlayersByTeam(int $teamId): array
    {
        return $this->teamPlayerRepository->getTeamPlayersByTeamId($teamId);
    }

    public function getOtherPlayers($franchiseId): array
    {
        return $this->playerRepository->getOtherPlayers($franchiseId);
    }

    public function update($data): bool
    {
        $playerId = $data['playerId'];
        $humanId = $this->getPlayer($playerId)->getHuman()->getId();
        $firstName = $data['firstName'];
        $lastName = $data['lastName'];
        $gender = $data['gender'];
        $nationality = $data['nationality'];
        $birthDate = $data['birthDate'];
        $jerseyNumber = $data['jerseyNumber'];
        $mainPost = $data['mainPost'];
        return $this->playerRepository->update($playerId, $humanId, $firstName, $lastName, $gender, $nationality, $birthDate, $jerseyNumber, $mainPost);
    }

    public function delete($data): bool
    {
        $playerId = $data['playerId'];
        return $this->playerRepository->delete($playerId);
    }

    public function updateFranchisePlayers($data): bool
    {
        return $this->playerRepository->updateFranchisePlayers($data);
    }
}