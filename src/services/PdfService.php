<?php

include_once __DIR__ . '/tfpdf_templates/GameReport.php';
include_once __DIR__ . '/GameService.php';

class PdfService
{
    public static function createGameReport(DetailedGame $detailedGame): void
    {
        // Création d'un nouveau PDF
        $pdf = new GameReport();
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $homeDetailedTeam = $detailedGame->getHomeDetailedTeam();
        $awayDetailedTeam = $detailedGame->getAwayDetailedTeam();
        
        // Informations générales du match
        $gameBeginDate = $detailedGame->getGame()->getBeginDateTime();
        $gameAddress = $detailedGame->getGame()->getPlace()->getAddress();
        $homeTeam = $homeDetailedTeam->getTeam()->getFranchise()->getName();
        $awayTeam = $awayDetailedTeam->getTeam()->getFranchise()->getName();
        $homeScore = $detailedGame->getHomeDetailedTeam()->getTeam()->getScore();
        $awayScore = $detailedGame->getAwayDetailedTeam()->getTeam()->getScore();
        $gameMinuteTime = $detailedGame->getGame()->getGameMinuteTime();
        $gameSecondTime = $detailedGame->getGame()->getGameSecondTime();

        $pdf->GameInfos(
            $gameBeginDate->format('d-m-Y'),
            $gameAddress, 
            $gameBeginDate->format('H:i'),
            $gameMinuteTime,
            $gameSecondTime,
            $homeTeam,
            $awayTeam,
            $homeScore, 
            $awayScore
        );
        
        // Informations de l'équipe domicile
        $homeTeamPlayers = [];
        $homeSusbtitutePlayers = [];
        $homeCaptain = "";
        $homeAlternateCaptain = "";
        foreach($homeDetailedTeam->getTeamPlayers() as $teamPlayer) {
            $playerName = $teamPlayer->getPlayer()->getHuman()->getlastName() . ' ' . $teamPlayer->getPlayer()->getHuman()->getFirstName();
            
            if ($teamPlayer->getPost() === TeamPlayer::POST_SUBSTITUTE) {
                $homeSusbtitutePlayers[] = $playerName;
            } else {
                $homeTeamPlayers[] = [
                    'name' => $playerName,
                    'position' => $teamPlayer->getPostTrad()
                ];
            }

            if ($teamPlayer->getTitle() === TeamPlayer::TITLE_CAPTAIN) {
                $homeCaptain = $playerName;
            } else if ($teamPlayer->getTitle() === TeamPlayer::TITLE_ALTERNATE_CAPTAIN) {
                $homeAlternateCaptain = $playerName;
            }
        }
        
        $homeHistoryTeamPlayerPoints = [];
        foreach($homeDetailedTeam->getHistoryTeamPlayerPoints() as $historyTeamPlayerPoint) {
            $playerName = $historyTeamPlayerPoint->getTeamPlayer()->getPlayer()->getHuman()->getlastName() . ' ' . $historyTeamPlayerPoint->getTeamPlayer()->getPlayer()->getHuman()->getFirstName();
            $time = $historyTeamPlayerPoint->getPointTime();
            $pointType = $historyTeamPlayerPoint->getPointType()->getName();
            $point = $historyTeamPlayerPoint->getPointType()->getPoint();

            $homeHistoryTeamPlayerPoints[] = [
                'time' => $time,
                'player' => $playerName,
                'type' => $pointType,
                'points' => $point,
            ];
        }
        
        $homeHistoryTeamPlayerFouls = [];
        foreach($homeDetailedTeam->getHistoryTeamPlayerFouls() as $historyTeamPlayerFoul) {
            $playerName = $historyTeamPlayerFoul->getTeamPlayer()->getPlayer()->getHuman()->getlastName() . ' ' . $historyTeamPlayerFoul->getTeamPlayer()->getPlayer()->getHuman()->getFirstName();
            $time = $historyTeamPlayerFoul->getFoulTime();
            $foul = $historyTeamPlayerFoul->getFoulType()->getName();

            $homeHistoryTeamPlayerFouls[] = [
                'time' => $time,
                'player' => $playerName,
                'foul' => $foul,
            ];
        }

        $homeHistorySubstitutes = [];
        foreach($homeDetailedTeam->getHistorySubstitutes() as $historySubstitute) {
            $time = $historySubstitute->getSubstituteTime();
            $outPlayerName = $historySubstitute->getOutgoingTeamPlayer()->getPlayer()->getHuman()->getlastName() . ' ' . $historySubstitute->getOutgoingTeamPlayer()->getPlayer()->getHuman()->getFirstName();
            $inPlayerName = $historySubstitute->getIncomingTeamPlayer()->getPlayer()->getHuman()->getlastName() . ' ' . $historySubstitute->getIncomingTeamPlayer()->getPlayer()->getHuman()->getFirstName();

            $homeHistorySubstitutes[] = [
                'time' => $time,
                'out' => $outPlayerName,
                'in' => $inPlayerName,
            ];
        }
        
        $pdf->TeamInfos($homeDetailedTeam->getTeam()->getFranchise()->getName(), $homeCaptain, $homeAlternateCaptain, $homeTeamPlayers, $homeSusbtitutePlayers, $homeHistoryTeamPlayerPoints, $homeHistoryTeamPlayerFouls, $homeHistorySubstitutes);

        // Informations de l'équipe extérieure
        $awayTeamPlayers = [];
        $awaySusbtitutePlayers = [];
        $awayCaptain = "";
        $awayAlternateCaptain = "";
        foreach($awayDetailedTeam->getTeamPlayers() as $teamPlayer) {
            $playerName = $teamPlayer->getPlayer()->getHuman()->getlastName() . ' ' . $teamPlayer->getPlayer()->getHuman()->getFirstName();
            
            if ($teamPlayer->getPost() === TeamPlayer::POST_SUBSTITUTE) {
                $awaySusbtitutePlayers[] = $playerName;
            } else {
                $awayTeamPlayers[] = [
                    'name' => $playerName,
                    'position' => $teamPlayer->getPostTrad()
                ];
            }

            if ($teamPlayer->getTitle() === TeamPlayer::TITLE_CAPTAIN) {
                $awayCaptain = $playerName;
            } else if ($teamPlayer->getTitle() === TeamPlayer::TITLE_ALTERNATE_CAPTAIN) {
                $awayAlternateCaptain = $playerName;
            }
        }
        
        $awayHistoryTeamPlayerPoints = [];
        foreach($awayDetailedTeam->getHistoryTeamPlayerPoints() as $historyTeamPlayerPoint) {
            $playerName = $historyTeamPlayerPoint->getTeamPlayer()->getPlayer()->getHuman()->getlastName() . ' ' . $historyTeamPlayerPoint->getTeamPlayer()->getPlayer()->getHuman()->getFirstName();
            $time = $historyTeamPlayerPoint->getPointTime();
            $pointType = $historyTeamPlayerPoint->getPointType()->getName();
            $point = $historyTeamPlayerPoint->getPointType()->getPoint();

            $awayHistoryTeamPlayerPoints[] = [
                'time' => $time,
                'player' => $playerName,
                'type' => $pointType,
                'points' => $point,
            ];
        }
        
        $awayHistoryTeamPlayerFouls = [];
        foreach($awayDetailedTeam->getHistoryTeamPlayerFouls() as $historyTeamPlayerFoul) {
            $playerName = $historyTeamPlayerFoul->getTeamPlayer()->getPlayer()->getHuman()->getlastName() . ' ' . $historyTeamPlayerFoul->getTeamPlayer()->getPlayer()->getHuman()->getFirstName();
            $time = $historyTeamPlayerFoul->getFoulTime();
            $foul = $historyTeamPlayerFoul->getFoulType()->getName();

            $awayHistoryTeamPlayerFouls[] = [
                'time' => $time,
                'player' => $playerName,
                'foul' => $foul,
            ];
        }

        $awayHistorySubstitutes = [];
        foreach($awayDetailedTeam->getHistorySubstitutes() as $historySubstitute) {
            $time = $historySubstitute->getSubstituteTime();
            $outPlayerName = $historySubstitute->getOutgoingTeamPlayer()->getPlayer()->getHuman()->getlastName() . ' ' . $historySubstitute->getOutgoingTeamPlayer()->getPlayer()->getHuman()->getFirstName();
            $inPlayerName = $historySubstitute->getIncomingTeamPlayer()->getPlayer()->getHuman()->getlastName() . ' ' . $historySubstitute->getIncomingTeamPlayer()->getPlayer()->getHuman()->getFirstName();

            $awayHistorySubstitutes[] = [
                'time' => $time,
                'out' => $outPlayerName,
                'in' => $inPlayerName,
            ];
        }
        $pdf->TeamInfos($awayDetailedTeam->getTeam()->getFranchise()->getName(), $awayCaptain, $awayAlternateCaptain, $awayTeamPlayers, $awaySusbtitutePlayers, $awayHistoryTeamPlayerPoints, $awayHistoryTeamPlayerFouls, $awayHistorySubstitutes);
        
        // Arbitres
        $holderArbitrator = $detailedGame->getHolderArbitrator();
        $holderArbitratorName = $holderArbitrator->getHuman()->getlastName() . ' ' . $holderArbitrator->getHuman()->getFirstName();
        $assistantArbitrator1 = $detailedGame->getAssistantArbitrator1();
        $assistantArbitratorName1 = $assistantArbitrator1->getHuman()->getlastName() . ' ' . $holderArbitrator->getHuman()->getFirstName();
        $assistantArbitrator2 = $detailedGame->getAssistantArbitrator2();
        $assistantArbitratorName2 = $assistantArbitrator2->getHuman()->getlastName() . ' ' . $holderArbitrator->getHuman()->getFirstName();

        $pdf->Referees($holderArbitratorName, [$assistantArbitratorName1, $assistantArbitratorName2]);

        $savePath = $_SESSION['dirGameReport'];
        $reportName = $detailedGame->getGame()->getId().'.pdf';

        if (!is_dir($savePath)) {
            mkdir($savePath, 0777, true);
        }

        // Sauvegarde du PDF
        $pdf->Output($savePath . '/' . $reportName, 'F');
    }
}