<?php
include_once __DIR__ . '/../../config/connexion-bdd.php';
include_once __DIR__ . '/../models/Franchise.php';
include_once __DIR__ . '/../repositories/FranchiseRepository.php';

class FranchiseService
{
    private FranchiseRepository $franchiseRepository;

    public function __construct()
    {
        $this->franchiseRepository = new FranchiseRepository();
    }

    public function getAllFranchises() : array
    {
        return $this->franchiseRepository->getAll();
    }

    public function getAllActiveFranchises() : array
    {
        return $this->franchiseRepository->getAllActive();
    }

    public function getFranchise($franchiseId): Franchise
    {
        return $this->franchiseRepository->get($franchiseId);
    }

    public function create(array $data): null|Franchise
    {
        return $this->franchiseRepository->create($data);
    }

    public function update($data): bool
    {
        $franchiseId = $data['franchiseId'];
        $name = $data['name'];
        $color = $data['color'];
        $initials = $data['initials'];
        $isActive = $data['isActive'];
        return $this->franchiseRepository->update($franchiseId, $name, $color, $initials, $isActive);
    }

    public function deleteFranchise($franchiseId): bool
    {
        return $this->franchiseRepository->delete($franchiseId);
    }
}
