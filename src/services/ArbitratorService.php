<?php
include_once __DIR__ . '/../models/Arbitrator.php';
include_once __DIR__ . '/../repositories/ArbitratorRepository.php';

class ArbitratorService
{
    private ArbitratorRepository $arbitratorRepository;


    public function __construct()
    {
        $this->arbitratorRepository = new ArbitratorRepository();
    }

    public function getAllArbitrators() : array
    {
        return $this->arbitratorRepository->getAll();
    }

    public function get($arbitratorId) : Arbitrator
    {
        return $this->arbitratorRepository->get($arbitratorId);
    }

    public function getAllByGame($gameId) : array
    {
        $arbitrators = $this->arbitratorRepository->getAllByGame($gameId);
        $assistants = [];
        $holder = null;
        foreach ($arbitrators as $arbitrator) {
            if ($arbitrator->getTitle() === Arbitrator::TITLE_HOLDER) {
                $holder = $arbitrator;
            } else {
                $assistants[] = $arbitrator;
            }
        }

        return [
            Arbitrator::TITLE_HOLDER      => $holder,
            Arbitrator::TITLE_ASSISTANT_1 => count($assistants) >= 1 ? $assistants[0] : null,
            Arbitrator::TITLE_ASSISTANT_2 => count($assistants) >= 2 ? $assistants[1] : null
        ];
    }

    public function create(array $data): bool
    {
        $data = HelperService::CleanArrayFromHtlm($data);
        return $this->arbitratorRepository->create($data);
    }

    public function update(array $data) : null|Arbitrator
    {
        return $this->arbitratorRepository->update($data);
    }

    public function createForGame($gameId, $arbitratorId, $title) : bool
    {
        return $this->arbitratorRepository->createForGame($gameId, $arbitratorId, $title);
    }

    public function deleteByGame(int $gameId) : bool
    {
        return $this->arbitratorRepository->deleteByGame($gameId);
    }

    public function delete(array $data): bool
    {
        $data = HelperService::CleanArrayFromHtlm($data);
        $arbitratorId = $data['arbitratorId'];
        return $this->arbitratorRepository->deleteByArbitratorId($arbitratorId);
    }
}