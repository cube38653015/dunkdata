<?php
include_once __DIR__ . '/../../config/connexion-bdd.php';
include_once __DIR__ . '/../models/Place.php';
include_once __DIR__ . '/../repositories/PlaceRepository.php';

class PlaceService
{
    private PlaceRepository $placeRepository;

    public function __construct()
    {
        $this->placeRepository = new PlaceRepository();
    }

    public function getAllPlaces() : array
    {
        return $this->placeRepository->getAll();
    }

    public function getPlace($placeId): Place
    {
        return $this->placeRepository->get($placeId);
    }

    public function create(array $data): null|Place
    {
        return $this->placeRepository->create($data);
    }
}