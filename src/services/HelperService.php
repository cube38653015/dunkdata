<?php


class HelperService {

    public static function CleanVariableFromHtlm(string $var): string
    {
        return htmlspecialchars(strip_tags($var));
    }

    public static function CleanArrayFromHtlm(array $vars): array
    {
        $newVars = [];
        foreach($vars as $key => $var) {
            if (is_array($var)) {
                $newVars[$key] = self::CleanArrayFromHtlm($var);
            } else {
                $newVars[$key] = self::CleanVariableFromHtlm($var);
            }

        }
        return $newVars;
    }
}