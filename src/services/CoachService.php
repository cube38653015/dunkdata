<?php
include_once __DIR__ . '/../repositories/CoachRepository.php';

class CoachService
{
    private CoachRepository $coachRepository;

    public function __construct()
    {
        $this->coachRepository = new CoachRepository();
    }

    public function get($coachId) : null|Coach
    {
        return $this->coachRepository->getCoach($coachId);
    }

    public function getCoachByHumanId(int $humanId): null|Coach
    {
        return $this->coachRepository->getCoachByHumanId($humanId);
    }

    public function getAll(): array
    {
        return $this->coachRepository->getAll();
    }

    public function getCoachesByFranchise(int $franchiseId): array
    {
        $coaches = $this->coachRepository->getCoachByFranchiseId($franchiseId);
        $mainCoach = null;
        $assistantCoach = null;
        foreach($coaches as $coach) {
            if($coach->getTitle() === Coach::TITLE_MAIN_COACH) {
                $mainCoach = $coach;
            } else {
                $assistantCoach = $coach;
            }
        }
        return [
            Coach::TITLE_MAIN_COACH => $mainCoach,
            Coach::TITLE_ASSISTANT_COACH => $assistantCoach,
        ];
    }

    public function getActualCoachesByFranchise(int $franchiseId): array
    {
        $coaches = $this->coachRepository->getCoachByFranchiseId($franchiseId);
        $actualMainCoaches = [];
        $actualAssistantCoaches = [];
        foreach($coaches as $coach) {
            if($coach->getTitle() === Coach::TITLE_MAIN_COACH && $coach->getOutDate() === NULL) {
                $actualMainCoaches[] = $coach;
            } else if($coach->getTitle() === Coach::TITLE_ASSISTANT_COACH && $coach->getOutDate() === NULL) {
                $actualAssistantCoaches[] = $coach;
            }
        }
        return [
            'actualMainCoaches' => $actualMainCoaches,
            'actualAssistantCoaches' => $actualAssistantCoaches
        ];
    }

    public function getOtherCoaches($franchiseId): array
    {
        return $this->coachRepository->getOtherCoaches($franchiseId);
    }

    public function update($data): bool
    {
        $coachId = $data['coachId'];
        $humanId = $this->get($coachId)->getHuman()->getId();
        $firstName = $data['firstName'];
        $lastName = $data['lastName'];
        $gender = $data['gender'];
        $nationality = $data['nationality'];
        $birthDate = $data['birthDate'];
        $title = $data['title'];
        return $this->coachRepository->update($coachId, $humanId, $firstName, $lastName, $gender, $nationality, $birthDate, $title);
    }

    public function delete($coachId): bool
    {
        return $this->coachRepository->delete($coachId);
    }

    public function updateFranchiseCoaches($data): bool
    {
        return $this->coachRepository->updateFranchiseCoaches($data);
    }
}