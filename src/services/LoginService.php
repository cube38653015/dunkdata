<?php

include_once __DIR__ . '/../repositories/UserRepository.php';
include_once __DIR__ . '/../services/CoachService.php';

class LoginService
{
    private UserRepository $userRepository;
    private CoachService $coachService;


    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->coachService = new CoachService();
    }

    public function checkUser(string $login, string $password): bool
    {
        $user = $this->userRepository->getByLogin($login);
        if ($user && password_verify($password, $user->getPassword())) {
            session_start();
            $_SESSION['role'] = $user->getRole();
            switch($user->getRole()) {
                case 'coach':
                    $coach = $this->coachService->getCoachByHumanId($user->getHuman()->getId());
                    $_SESSION['coach'] = $coach;
                    $_SESSION['franchise'] = $coach->getFranchise();
                    break;
                case 'admin':
                case 'manager':
                    break;
            }
            return true;
        }

        return false;

    }
}