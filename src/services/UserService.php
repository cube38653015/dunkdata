<?php
include_once __DIR__ . '/../models/User.php';
include_once __DIR__ . '/../repositories/UserRepository.php';
include_once __DIR__ . '/../services/CoachService.php';

class UserService
{
    private UserRepository $userRepository;
    private CoachService $coachService;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->coachService = new CoachService();
    }

    public function getAllUser() : array
    {
        return $this->userRepository->getAll();
    }

    public function get($userId): User
    {
        return $this->userRepository->get($userId);
    }

    public function create(array $data): bool
    {
        $data = HelperService::CleanArrayFromHtlm($data);
        $login = $data['login'];
        $password = $data['password'];
        $role = $data['role'];

        if ($role === User::ROLE_COACH) {
            $coachId = $data['coachId'];
            $coach = $this->coachService->get($coachId);
            $humanId = $coach->getHuman()->getId();
            if ($humanId > 0) {
                return $this->userRepository->create($login, $password, $role, $humanId);
            }
            return false;
        }

        return $this->userRepository->create($login, $password, $role);
    }

    public function update(array $data): bool
    {
        $data = HelperService::CleanArrayFromHtlm($data);
        $id = $data['userId'];
        $login = $data['login'];
        $role = $data['role'];
        $coachId = null;
        $humanId = null;

        $user = $this->get($id);

        if (!empty($data['password'])) {
            $password = password_hash($data['password'], PASSWORD_DEFAULT);
        } else {
            $password = $user->getPassword();
        }

        if ($role === User::ROLE_COACH) {
            $coachId = $data['coachId'] ?? null;
            $coach = $this->coachService->get($coachId);
            $humanId = $coach->getHuman()->getId();
        }

        return $this->userRepository->update($id, $login, $password, $role, $coachId, $humanId);
    }

    public function delete(array $data): bool
    {
        $data = HelperService::CleanArrayFromHtlm($data);
        $userId = $data['userId'];
        return $this->userRepository->deleteByUserId($userId);
    }
}