<?php
include_once __DIR__ . '/../repositories/PointTypeRepository.php';

class PointService
{
    private PointTypeRepository $pointTypeRepository;

    public function __construct() {
        $this->pointTypeRepository = new PointTypeRepository();
    }

    public function getPointTypes(): array
    {
        return $this->pointTypeRepository->getPointTypes();
    }
}