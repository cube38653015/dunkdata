<?php
include_once __DIR__ . '/../services/CoachService.php';

class AdminService
{
    private CoachService $coachService;

    public function __construct()
    {
        $this->coachService = new CoachService();
    }

    public function getCoachesByFranchise(): array
    {
        $coaches = $this->coachService->getAll();
        $coachesByFranchise = [];
        foreach($coaches as $coach) {
            $coachesByFranchise[$coach->getFranchise()->getId()][] = $coach;
        }
        return $coachesByFranchise;
    }
}