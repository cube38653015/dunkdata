<?php
include_once __DIR__ . '/../repositories/TeamRepository.php';

class TeamService
{
    private TeamRepository $teamRepository;

    public function __construct()
    {
        $this->teamRepository = new TeamRepository();
    }

    /**
     * @param int $gameId
     * @return array
     */
    public function getHomeTeamAndAwayTeamByGame(int $gameId): array
    {
        $teams = $this->teamRepository->getTeamsByGame($gameId);
        $homeTeam = null;
        $awayTeam = null;
        foreach($teams as $team) {
            if ($team->getIsHome()) {
                $homeTeam = $team;
            } else {
                $awayTeam = $team;
            }
        }

        return [
            'home' => $homeTeam,
            'away' => $awayTeam
        ];
    }
}