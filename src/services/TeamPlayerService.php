<?php

include_once __DIR__ . '/../repositories/TeamPlayerRepository.php';

class TeamPlayerService
{
    private TeamPlayerRepository $teamPlayerRepository;

    public function __construct()
    {
        $this->teamPlayerRepository = new TeamPlayerRepository();
    }

    public function createTeamPlayer(array $data): bool
    {
        $data = HelperService::CleanArrayFromHtlm($data);

        $teamId = $_SESSION['teamId'];

        $captainPlayerId = $data[TeamPlayer::TITLE_CAPTAIN];
        $alternateCaptainId = $data[TeamPlayer::TITLE_ALTERNATE_CAPTAIN];

        $teamPlayerPivot = [
            'teamId' => $teamId,
            'playerId' => $data[Player::POST_PIVOT],
            'title' => $captainPlayerId === $data[Player::POST_PIVOT] ? TeamPlayer::TITLE_CAPTAIN : ($alternateCaptainId === $data[Player::POST_PIVOT] ? TeamPlayer::TITLE_ALTERNATE_CAPTAIN : TeamPlayer::TITLE_PLAYER),
            'post' => Player::POST_PIVOT
        ];

        $teamPlayerLeader = [
            'teamId' => $teamId,
            'playerId' => $data[Player::POST_LEADER],
            'title' => $captainPlayerId === $data[Player::POST_LEADER] ? TeamPlayer::TITLE_CAPTAIN : ($alternateCaptainId === $data[Player::POST_LEADER] ? TeamPlayer::TITLE_ALTERNATE_CAPTAIN : TeamPlayer::TITLE_PLAYER),
            'post' => Player::POST_LEADER
        ];

        $teamPlayerWinger = [
            'teamId' => $teamId,
            'playerId' => $data[Player::POST_WINGER],
            'title' => $captainPlayerId === $data[Player::POST_WINGER] ? TeamPlayer::TITLE_CAPTAIN : ($alternateCaptainId === $data[Player::POST_WINGER] ? TeamPlayer::TITLE_ALTERNATE_CAPTAIN : TeamPlayer::TITLE_PLAYER),
            'post' => Player::POST_WINGER
        ];

        $teamPlayerStrongWinger = [
            'teamId' => $teamId,
            'playerId' => $data[Player::POST_STRONG_WINGER],
            'title' => $captainPlayerId === $data[Player::POST_STRONG_WINGER] ? TeamPlayer::TITLE_CAPTAIN : ($alternateCaptainId === $data[Player::POST_STRONG_WINGER] ? TeamPlayer::TITLE_ALTERNATE_CAPTAIN : TeamPlayer::TITLE_PLAYER),
            'post' => Player::POST_STRONG_WINGER
        ];

        $teamPlayerBack = [
            'teamId' => $teamId,
            'playerId' => $data[Player::POST_BACK],
            'title' => $captainPlayerId === $data[Player::POST_BACK] ? TeamPlayer::TITLE_CAPTAIN : ($alternateCaptainId === $data[Player::POST_BACK] ? TeamPlayer::TITLE_ALTERNATE_CAPTAIN : TeamPlayer::TITLE_PLAYER),
            'post' => Player::POST_BACK
        ];

        $teamPlayers = [
            $teamPlayerPivot,
            $teamPlayerLeader,
            $teamPlayerWinger,
            $teamPlayerStrongWinger,
            $teamPlayerBack
        ];

        if (array_key_exists('substitutes', $data)) {
            foreach($data['substitutes'] as $substitute) {
                $teamPlayerSubstitute = [
                    'teamId' => $teamId,
                    'playerId' => $substitute,
                    'title' => $captainPlayerId === $substitute ? TeamPlayer::TITLE_CAPTAIN : ($alternateCaptainId === $substitute ? TeamPlayer::TITLE_CAPTAIN : TeamPlayer::TITLE_PLAYER),
                    'post' => 'substitute'
                ];
                $teamPlayers[] = $teamPlayerSubstitute;
            }
        }

        foreach($teamPlayers as $teamPlayer) {
            if (!$this->teamPlayerRepository->create($teamPlayer['teamId'], $teamPlayer['playerId'], $teamPlayer['post'], $teamPlayer['title'])) {
                return false;
            }
        }
        return true;
    }

    public function deleteAllTeamPlayerByTeam(int $teamId): bool
    {
        $teamId = HelperService::CleanVariableFromHtlm($teamId);
        return $this->teamPlayerRepository->deleteAllTeamPlayerByTeamId($teamId);
    }

    public function getDetailedTeamPlayer(array $teamPlayers): array
    {
        $detailedTeamPlayers = [];
        $detailedTeamPlayers[TeamPlayer::POST_SUBSTITUTE] = [];

        foreach($teamPlayers as $teamPlayer) {
            if ($teamPlayer->getTitle() === TeamPlayer::TITLE_CAPTAIN) {
                $detailedTeamPlayers[TeamPlayer::TITLE_CAPTAIN] = $teamPlayer;
            } else if ($teamPlayer->getTitle() === TeamPlayer::TITLE_ALTERNATE_CAPTAIN) {
                $detailedTeamPlayers[TeamPlayer::TITLE_ALTERNATE_CAPTAIN] = $teamPlayer;
            }

            if ($teamPlayer->getPost() === Player::POST_PIVOT) {
                $detailedTeamPlayers[Player::POST_PIVOT] = $teamPlayer;
            } else if ($teamPlayer->getPost() === Player::POST_LEADER) {
                $detailedTeamPlayers[Player::POST_LEADER] = $teamPlayer;
            } else if ($teamPlayer->getPost() === Player::POST_WINGER) {
                $detailedTeamPlayers[Player::POST_WINGER] = $teamPlayer;
            } else if ($teamPlayer->getPost() === Player::POST_STRONG_WINGER) {
                $detailedTeamPlayers[Player::POST_STRONG_WINGER] = $teamPlayer;
            } else if ($teamPlayer->getPost() === Player::POST_BACK) {
                $detailedTeamPlayers[Player::POST_BACK] = $teamPlayer;
            } else {
                $detailedTeamPlayers[TeamPlayer::POST_SUBSTITUTE][$teamPlayer->getPlayer()->getId()] = $teamPlayer;
            }
        }
        return $detailedTeamPlayers;
    }

    public function updateTeamPlayers($data): bool
    {
        $data = HelperService::CleanArrayFromHtlm($data);

        $homeTeamId = $data['homeTeamId'];
        $homeCaptainPlayerId = $data['captainHome'];
        $homeAlternateCaptainId = $data['alternateCaptainHome'];

        $homeTeamPlayerPivot = [
            'teamId' => $homeTeamId,
            'playerId' => $data['pivotHome'],
            'title' => $homeCaptainPlayerId === $data['pivotHome'] ? TeamPlayer::TITLE_CAPTAIN : ($homeAlternateCaptainId === $data['pivotHome'] ? TeamPlayer::TITLE_ALTERNATE_CAPTAIN : TeamPlayer::TITLE_PLAYER),
            'post' => Player::POST_PIVOT
        ];

        $homeTeamPlayerLeader = [
            'teamId' => $homeTeamId,
            'playerId' => $data['leaderHome'],
            'title' => $homeCaptainPlayerId === $data['leaderHome'] ? TeamPlayer::TITLE_CAPTAIN : ($homeAlternateCaptainId === $data['leaderHome'] ? TeamPlayer::TITLE_ALTERNATE_CAPTAIN : TeamPlayer::TITLE_PLAYER),
            'post' => Player::POST_LEADER
        ];

        $homeTeamPlayerWinger = [
            'teamId' => $homeTeamId,
            'playerId' => $data['wingerHome'],
            'title' => $homeCaptainPlayerId === $data['wingerHome'] ? TeamPlayer::TITLE_CAPTAIN : ($homeAlternateCaptainId === $data['wingerHome'] ? TeamPlayer::TITLE_ALTERNATE_CAPTAIN : TeamPlayer::TITLE_PLAYER),
            'post' => Player::POST_WINGER
        ];

        $homeTeamPlayerStrongWinger = [
            'teamId' => $homeTeamId,
            'playerId' => $data['strongWingerHome'],
            'title' => $homeCaptainPlayerId === $data['strongWingerHome'] ? TeamPlayer::TITLE_CAPTAIN : ($homeAlternateCaptainId === $data['strongWingerHome'] ? TeamPlayer::TITLE_ALTERNATE_CAPTAIN : TeamPlayer::TITLE_PLAYER),
            'post' => Player::POST_STRONG_WINGER
        ];

        $homeTeamPlayerBack = [
            'teamId' => $homeTeamId,
            'playerId' => $data['backHome'],
            'title' => $homeCaptainPlayerId === $data['backHome'] ? TeamPlayer::TITLE_CAPTAIN : ($homeAlternateCaptainId === $data['backHome'] ? TeamPlayer::TITLE_ALTERNATE_CAPTAIN : TeamPlayer::TITLE_PLAYER),
            'post' => Player::POST_BACK
        ];

        $awayTeamId = $data['awayTeamId'];
        $awayCaptainPlayerId = $data['captainAway'];
        $awayAlternateCaptainId = $data['alternateCaptainAway'];

        $awayTeamPlayerPivot = [
            'teamId' => $awayTeamId,
            'playerId' => $data['pivotAway'],
            'title' => $awayCaptainPlayerId === $data['pivotAway'] ? TeamPlayer::TITLE_CAPTAIN : ($awayAlternateCaptainId === $data['pivotAway'] ? TeamPlayer::TITLE_ALTERNATE_CAPTAIN : TeamPlayer::TITLE_PLAYER),
            'post' => Player::POST_PIVOT
        ];

        $awayTeamPlayerLeader = [
            'teamId' => $awayTeamId,
            'playerId' => $data['leaderAway'],
            'title' => $awayCaptainPlayerId === $data['leaderAway'] ? TeamPlayer::TITLE_CAPTAIN : ($awayAlternateCaptainId === $data['leaderAway'] ? TeamPlayer::TITLE_ALTERNATE_CAPTAIN : TeamPlayer::TITLE_PLAYER),
            'post' => Player::POST_LEADER
        ];

        $awayTeamPlayerWinger = [
            'teamId' => $awayTeamId,
            'playerId' => $data['wingerAway'],
            'title' => $awayCaptainPlayerId === $data['wingerAway'] ? TeamPlayer::TITLE_CAPTAIN : ($awayAlternateCaptainId === $data['wingerAway'] ? TeamPlayer::TITLE_ALTERNATE_CAPTAIN : TeamPlayer::TITLE_PLAYER),
            'post' => Player::POST_WINGER
        ];

        $awayTeamPlayerStrongWinger = [
            'teamId' => $awayTeamId,
            'playerId' => $data['strongWingerAway'],
            'title' => $awayCaptainPlayerId === $data['strongWingerAway'] ? TeamPlayer::TITLE_CAPTAIN : ($awayAlternateCaptainId === $data['strongWingerAway'] ? TeamPlayer::TITLE_ALTERNATE_CAPTAIN : TeamPlayer::TITLE_PLAYER),
            'post' => Player::POST_STRONG_WINGER
        ];

        $awayTeamPlayerBack = [
            'teamId' => $awayTeamId,
            'playerId' => $data['backAway'],
            'title' => $awayCaptainPlayerId === $data['backAway'] ? TeamPlayer::TITLE_CAPTAIN : ($awayAlternateCaptainId === $data['backAway'] ? TeamPlayer::TITLE_ALTERNATE_CAPTAIN : TeamPlayer::TITLE_PLAYER),
            'post' => Player::POST_BACK
        ];

        $teamPlayers = [
            $homeTeamPlayerPivot,
            $homeTeamPlayerLeader,
            $homeTeamPlayerWinger,
            $homeTeamPlayerStrongWinger,
            $homeTeamPlayerBack,
            $awayTeamPlayerPivot,
            $awayTeamPlayerLeader,
            $awayTeamPlayerWinger,
            $awayTeamPlayerStrongWinger,
            $awayTeamPlayerBack
        ];

        $this->teamPlayerRepository->deleteByGame($data['gameId']);

        foreach($teamPlayers as $teamPlayer) {
            $this->teamPlayerRepository->create($teamPlayer['teamId'], $teamPlayer['playerId'], $teamPlayer['post'], $teamPlayer['title']);
        }

        $substitutes = [];

        if (array_key_exists('homeSubstitutes', $data)) {
            foreach($data['homeSubstitutes'] as $substitute) {
                $teamPlayerSubstitute = [
                    'teamId' => $homeTeamId,
                    'playerId' => $substitute,
                    'title' => $homeCaptainPlayerId === $substitute ? TeamPlayer::TITLE_CAPTAIN : ($homeAlternateCaptainId === $substitute ? TeamPlayer::TITLE_CAPTAIN : TeamPlayer::TITLE_PLAYER),
                    'post' => TEAMPLAYER::POST_SUBSTITUTE
                ];
                $substitutes[] = $teamPlayerSubstitute;
            }
        }

        if (array_key_exists('awaySubstitutes', $data)) {
            foreach($data['awaySubstitutes'] as $substitute) {
                $teamPlayerSubstitute = [
                    'teamId' => $awayTeamId,
                    'playerId' => $substitute,
                    'title' => $awayCaptainPlayerId === $substitute ? TeamPlayer::TITLE_CAPTAIN : ($awayAlternateCaptainId === $substitute ? TeamPlayer::TITLE_CAPTAIN : TeamPlayer::TITLE_PLAYER),
                    'post' => TEAMPLAYER::POST_SUBSTITUTE
                ];
                $substitutes[] = $teamPlayerSubstitute;
            }
        }

        foreach($substitutes as $substitute) {
            if (!$this->teamPlayerRepository->create($substitute['teamId'], $substitute['playerId'], $substitute['post'], $substitute['title'])) {
                return false;
            }
        }

        return true;
    }





}
