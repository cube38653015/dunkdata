<?php
include_once __DIR__ . '/../repositories/BaseRepository.php';
include_once __DIR__ . '/../models/FoulType.php';
include_once __DIR__ . '/../models/HistoryTeamPlayerFoul.php';

class HistoryTeamPlayerFoulsRepository extends BaseRepository
{

    public function getAllByGameByFranchise(int $gameId, int $franchiseId): array
    {
        $query = "SELECT 
                htpf.id,
                htpf.foul_time,
                htpf.foul_type_id,
                ft.name as foul_type_name,
                htpf.team_player_id,
                tp.player_id,
                tp.team_id,
                tp.post as team_player_post,
                tp.title as team_player_title,
                p.human_id, 
                h.last_name, h.first_name, h.gender, h.nationality, h.birth_date, 
                p.jersey_number,
                p.main_post,
                p.in_date,
                p.out_date,
                p.franchise_id,
                f.name, f.color, f.initials, f.isActive,
                t.is_home,
                t.game_id,
                g.begin_date_time,
                g.game_time,
                g.place_id,
                place.gym, place.address, place.city
             FROM " . self::TABLE_HISTORY_TEAM_PLAYER_FOUL . " htpf
             INNER JOIN " . self::TABLE_TEAM_PLAYER . " tp ON tp.id = htpf.team_player_id
             INNER JOIN " . self::TABLE_FOUL_TYPE . " ft ON ft.id = htpf.foul_type_id
             INNER JOIN " . self::TABLE_PLAYER . " p ON p.id = tp.player_id
             INNER JOIN " . self::TABLE_HUMAN . " h ON p.human_id = h.id
             INNER JOIN " . self::TABLE_FRANCHISE . " f ON p.franchise_id = f.id
             INNER JOIN " . self::TABLE_TEAM . " t ON t.id = tp.team_id
             INNER JOIN " . self::TABLE_GAME . " g ON g.id = t.game_id
             INNER JOIN " . self::TABLE_PLACE . " place ON place.id = g.place_id
             WHERE g.id = ? AND f.id = ?
             ORDER BY htpf.foul_time ASC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$gameId, $franchiseId]);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $historyTeamPlayerFouls = [];
        foreach ($rows as $row) {
            $historyTeamPlayerFouls[] = $this->mappingHistoryTeamPlayerFouls($row);
        }
        return $historyTeamPlayerFouls;
    }

    public function deleteByGame(int $gameId): bool
    {
        $query = "DELETE htpf.* FROM " . self::TABLE_HISTORY_TEAM_PLAYER_FOUL . " htpf
                  INNER JOIN " . self::TABLE_TEAM_PLAYER . " tp ON tp.id = htpf.team_player_id
                  INNER JOIN " . self::TABLE_TEAM . " t ON t.id = tp.team_id
                  INNER JOIN " . self::TABLE_GAME . " g ON g.id = t.game_id
                  WHERE g.id = ?";
        $stmt = $this->conn->prepare($query);

        if($stmt->execute([$gameId])) {
            return true;
        }
        return false;
    }

    public function create(int $teamPlayerId, int $foulTypeId, string $foulTime): bool
    {
        $query = "INSERT INTO " . self::TABLE_HISTORY_TEAM_PLAYER_FOUL . " 
                (team_player_id, foul_type_id, foul_time)
                VALUES 
                (:teamPlayerId, :foulTypeId, :foulTime)";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":teamPlayerId", $teamPlayerId);
        $stmt->bindParam(":foulTypeId", $foulTypeId);
        $stmt->bindParam(":foulTime", $foulTime);

        return $stmt->execute();
    }

    private function mappingHistoryTeamPlayerFouls($row) : HistoryTeamPlayerFoul
    {

        $human = new Human(
            $row['human_id'],
            $row['last_name'],
            $row['first_name'],
            $row['gender'],
            $row['nationality'],
            $row['birth_date']
        );

        $franchise = new Franchise (
            $row['franchise_id'],
            $row['name'],
            $row['color'],
            $row['initials'],
            $row['isActive']
        );

        $player = new Player(
            $row['player_id'],
            $human,
            $row['jersey_number'],
            $row['main_post'],
            $franchise,
            $row['in_date'],
            $row['out_date']
        );

        $place = new Place(
            $row['place_id'],
            $row['gym'],
            $row['address'],
            $row['city']
        );

        // Specify the format of the input string
        $format = 'Y-m-d H:i:s';
        // Convert the string to a DateTime object
        $beginDateTime = DateTime::createFromFormat($format, $row['begin_date_time']);

        $game = new Game(
            $row['game_id'],
            $beginDateTime,
            $place,
            $row['game_time']
        );

        $team = new Team(
            $row['team_id'],
            $game,
            $franchise,
            $row['is_home']
        );

        $teamPlayer = new TeamPlayer(
            $row['team_player_id'],
            $team,
            $player,
            $row['team_player_post'],
            $row['team_player_title']
        );

        $foulType = new FoulType(
            $row['foul_type_id'],
            $row['foul_type_name']
        );

        return new HistoryTeamPlayerFoul(
            $row['id'],
            $teamPlayer,
            $foulType,
            $row['foul_time']
        );
    }
}
