<?php
include_once __DIR__ . '/../models/Coach.php';
include_once __DIR__ . '/../repositories/BaseRepository.php';

class CoachRepository extends BaseRepository
{

    public function getCoach(int $coachId) : null|Coach
    {
        $query = "SELECT c.id, c.human_id, h.last_name, h.first_name, h.gender, h.nationality, h.birth_date, c.franchise_id, c.title, f.name, f.color, f.initials, f.isActive,
                         c.in_date, c.out_date
                  FROM " . self::TABLE_COACH . " c
                  INNER JOIN human h ON c.human_id = h.id
                  INNER JOIN franchise f ON c.franchise_id = f.id
                  WHERE c.id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$coachId]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $this->mappingCoach($row);
    }

    public function getCoachByHumanId(int $humanId): null|Coach
    {
        $query = "SELECT c.id, c.title, c.human_id, h.last_name, h.first_name, h.gender, h.nationality, h.birth_date, c.franchise_id, f.name, f.color, f.initials, f.isActive,
                         c.in_date, c.out_date
                  FROM " . self::TABLE_COACH . " c
                  INNER JOIN human h ON c.human_id = h.id
                  INNER JOIN franchise f ON c.franchise_id = f.id
                  WHERE c.human_id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$humanId]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $this->mappingCoach($row);
    }

    public function getAll(): array
    {
        $query = "SELECT c.id, c.human_id, h.last_name, h.first_name, h.gender, h.nationality, h.birth_date, c.franchise_id, f.name, f.color, f.initials, f.isActive, c.title,
                         c.in_date, c.out_date
                  FROM " . self::TABLE_COACH . " c
                  INNER JOIN human h ON c.human_id = h.id
                  INNER JOIN franchise f ON c.franchise_id = f.id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $coaches = [];
        foreach ($rows as $row) {
            $user = $this->mappingCoach($row);
            if ($user !== null) {
                $coaches[] = $user;
            }
        }
        return $coaches;
    }

    public function getCoachByFranchiseId(int $franchiseId): array
    {
        $query = "SELECT c.id, c.human_id, h.last_name, h.first_name, h.gender, h.nationality, h.birth_date, c.franchise_id, f.name, f.color, f.initials, f.isActive, c.title,
                         c.in_date, c.out_date
                  FROM " . self::TABLE_COACH . " c
                  INNER JOIN human h ON c.human_id = h.id
                  INNER JOIN franchise f ON c.franchise_id = f.id
                  WHERE c.franchise_id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$franchiseId]);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $coaches = [];
        foreach ($rows as $row) {
            $user = $this->mappingCoach($row);
            if ($user !== null) {
                $coaches[] = $user;
            }
        }
        return $coaches;
    }

    public function getOtherCoaches($franchiseId) : array
    {
        $query = "SELECT 
                c.id, 
                c.human_id as human_id, 
                h.last_name as last_name, 
                h.first_name as first_name, 
                h.gender as gender, 
                h.nationality as nationality, 
                h.birth_date as birth_date,
                c.title as title,
                c.franchise_id as franchise_id,
                f.name as name, 
                f.color as color, 
                f.initials as initials,
                f.isActive as isActive,
                c.in_date, c.out_date
             FROM " . self::TABLE_COACH . " c
             INNER JOIN " . self::TABLE_HUMAN . " h ON c.human_id = h.id
             INNER JOIN " . self::TABLE_FRANCHISE . " f ON c.franchise_id = f.id
             WHERE (c.franchise_id != ? OR c.out_date IS NOT NULL)
             GROUP BY c.human_id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$franchiseId]);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $coaches = [];
        foreach ($rows as $row) {
            $coaches[] = $this->mappingCoach($row);
        }
        return $coaches;
    }

    public function update($coachId, $humanId, $firstName, $lastName, $gender, $nationality, $birthDate, $title) : bool
    {
        $queryHuman = "
            UPDATE " . self::TABLE_HUMAN . " 
            SET 
                first_name = :firstName, 
                last_name = :lastName, 
                gender = :gender, 
                nationality = :nationality, 
                birth_date = :birthDate 
            WHERE id = :humanId";
        $stmtHuman = $this->conn->prepare($queryHuman);
        $stmtHuman->bindParam(":firstName", $firstName);
        $stmtHuman->bindParam(":lastName", $lastName);
        $stmtHuman->bindParam(":gender", $gender);
        $stmtHuman->bindParam(":nationality", $nationality);
        $stmtHuman->bindParam(":birthDate", $birthDate);
        $stmtHuman->bindParam(":humanId", $humanId);
        $stmtHuman->execute();

        $queryCoach = "
            UPDATE " . self::TABLE_COACH . " 
            SET 
                title = :title
            WHERE id = :coachId";
        $stmtCoach = $this->conn->prepare($queryCoach);
        $stmtCoach->bindParam(":title", $title);
        $stmtCoach->bindParam(":coachId", $coachId);

        return $stmtCoach->execute();
    }

    public function delete($coachId): bool
    {
        $query = "
        UPDATE " . self::TABLE_COACH . "
        SET out_date = :out_date
        WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $currentDate = date('Y-m-d');
        $stmt->bindParam(':out_date', $currentDate);
        $stmt->bindParam(':id', $coachId);

        return $stmt->execute();
    }

    public function updateFranchiseCoaches($data): bool
    {
        $franchiseId = $data['franchiseId'];
        $todayDate = date('Y-m-d');

        if (isset($data['coachLastName'])) {
            foreach ($data['coachLastName'] as $index => $firstName) {
                $lastName = $data['coachFirstName'][$index] ?? null;
                $gender = $data['coachGender'][$index] ?? null;
                $nationality = $data['coachNationality'][$index] ?? null;
                $birthDate = $data['coachBirthDate'][$index] ?? null;
                $title = $data['coachTitle'][$index] ?? null;

                $queryHuman = "INSERT INTO " . self::TABLE_HUMAN . " 
            (first_name, last_name, gender, nationality, birth_date) 
            VALUES 
            (:first_name, :last_name, :gender, :nationality, :birth_date)";
                $stmtHuman = $this->conn->prepare($queryHuman);
                $stmtHuman->execute([
                    ':first_name' => $firstName,
                    ':last_name' => $lastName,
                    ':gender' => $gender,
                    ':nationality' => $nationality,
                    ':birth_date' => $birthDate
                ]);

                $humanId = $this->conn->lastInsertId();

                $queryCoach = "INSERT INTO " . self::TABLE_COACH . " 
            (human_id, title, franchise_id, in_date) 
            VALUES 
            (:human_id, :title, :franchise_id, :in_date)";
                $stmtPlayer = $this->conn->prepare($queryCoach);
                $stmtPlayer->execute([
                    ':human_id' => $humanId,
                    ':title' => $title,
                    ':franchise_id' => $franchiseId,
                    ':in_date' => $todayDate
                ]);
            }
        }

        if (isset($data['otherCoach'])) {
            foreach ($data['otherCoach'] as $coachId) {
                $queryOutDate = "UPDATE " . self::TABLE_COACH . " 
            SET out_date = :out_date 
            WHERE id = :coach_id AND out_date IS NULL";
                $stmtOutDate = $this->conn->prepare($queryOutDate);
                $stmtOutDate->execute([
                    ':out_date' => $todayDate,
                    ':coach_id' => $coachId
                ]);

                $queryFetch = "SELECT human_id, title FROM " . self::TABLE_COACH . " 
            WHERE id = :coach_id LIMIT 1";
                $stmtFetch = $this->conn->prepare($queryFetch);
                $stmtFetch->execute([':coach_id' => $coachId]);
                $playerData = $stmtFetch->fetch(PDO::FETCH_ASSOC);

                if ($playerData) {
                    $queryCoach = "INSERT INTO " . self::TABLE_COACH . " 
                (human_id, title, franchise_id, in_date) 
                VALUES 
                (:human_id, :title, :franchise_id, :in_date)";
                    $stmtPlayer = $this->conn->prepare($queryCoach);
                    $stmtPlayer->execute([
                        ':human_id' => $playerData['human_id'],
                        ':title' => $playerData['title'],
                        ':franchise_id' => $franchiseId,
                        ':in_date' => $todayDate
                    ]);
                }
            }
        }

        return true;
    }

    private function mappingCoach($row): null|Coach
    {
        $human = new Human(
            $row['human_id'],
            $row['last_name'],
            $row['first_name'],
            $row['gender'],
            $row['nationality'],
            $row['birth_date']
        );

        $franchise = new Franchise(
            $row['franchise_id'],
            $row['name'],
            $row['color'],
            $row['initials'],
            $row['isActive']
        );

        $title = '';
        if (isset($row['title'])) {
            $title = $row['title'];
        }

        return new Coach($row['id'], $human, $franchise, $title, $row['in_date'], $row['out_date']);
    }
}