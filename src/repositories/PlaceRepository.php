<?php
include_once __DIR__ . '/../models/Place.php';
include_once __DIR__ . '/../repositories/BaseRepository.php';

class PlaceRepository extends BaseRepository
{

    public function getAll() : array
    {
        $query = "SELECT id, gym, address, city FROM " . self::TABLE_PLACE;
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $places = [];
        foreach ($rows as $row) {
            $place = $this->mappingPlace($row);
            if ($place !== null) {
                $places[] = $place;
            }
        }
        return $places;
    }

    public function get($placeId) : null|Place
    {
        $query = "SELECT id, gym, address, city FROM " . self::TABLE_PLACE . " WHERE id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$placeId]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $this->mappingPlace($row);
    }

    public function create($data): null|Place
    {
        $query = "INSERT INTO " . self::TABLE_PLACE . " SET gym=:gym, address=:address, city=:city";
        $stmt = $this->conn->prepare($query);

        $gym = htmlspecialchars(strip_tags($data['gym']));
        $address = htmlspecialchars(strip_tags($data['address']));
        $city = htmlspecialchars(strip_tags($data['city']));

        $stmt->bindParam(":gym", $gym);
        $stmt->bindParam(":address", $address);
        $stmt->bindParam(":city", $city);

        if ($stmt->execute()) {
            $id = $this->conn->lastInsertId();
            return $this->get($id);
        }
        return null;
    }

    private function mappingPlace($row) : null|Place
    {
        if (!empty($row)) {
            return new Place($row['id'], $row['gym'], $row['address'], $row['city']);
        }
        return null;
    }
}
