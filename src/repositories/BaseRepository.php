<?php

class BaseRepository
{
    protected const TABLE_PLAYER = "player";
    protected const TABLE_HUMAN = "human";
    protected const TABLE_FRANCHISE = "franchise";
    protected const TABLE_GAME = "game";
    protected const TABLE_HISTORY_TEAM_PLAYER_FOUL = "history_team_player_foul";
    protected const TABLE_FOUL_TYPE = "foul_type";
    protected const TABLE_PLACE = "place";
    protected const TABLE_TEAM = "team";
    protected const TABLE_ARBITRATOR = "arbitrator";
    protected const TABLE_GAME_ARBITRATOR = 'game_arbitrator';
    protected const TABLE_TEAM_PLAYER = 'team_player';
    protected const TABLE_USER = 'user';
    protected const TABLE_COACH = "coach";
    protected const TABLE_POINT_TYPE = "point_type";
    protected const TABLE_HISTORY_SUBSTITUTE = "history_substitute";
    protected const TABLE_HISTORY_TEAM_PLAYER_POINT = "history_team_player_point";

    protected ?PDO $conn;

    public function __construct()
    {
        $database = new Database();
        $this->conn = $database->getConnection();
    }

}