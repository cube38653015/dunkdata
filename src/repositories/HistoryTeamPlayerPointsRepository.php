<?php

include_once __DIR__ . '/../models/HistoryTeamPlayerPoint.php';

class historyTeamPlayerPointsRepository extends BaseRepository
{
    public function getAllByGameByFranchise(int $gameId, int $franchiseId): array
    {
        $query = "SELECT 
                htpp.id,
                htpp.team_player_id,
                htpp.point_type_id,
                pt.name as point_type_name,
                pt.point as point_type_point,
                htpp.point_time,
                tp.player_id,
                tp.team_id,
                tp.post,
                tp.title,
                p.human_id,
                h.last_name, h.first_name, h.gender, h.nationality, h.birth_date,
                p.jersey_number,
                p.main_post,
                p.in_date,
                p.out_date,
                p.franchise_id,
                f.name, f.color, f.initials, f.isActive,
                t.is_home,
                t.game_id,
                g.begin_date_time,
                g.game_time,
                g.place_id,
                place.gym, place.address, place.city
                FROM " . self::TABLE_HISTORY_TEAM_PLAYER_POINT . " htpp
                INNER JOIN " . self::TABLE_TEAM_PLAYER . " tp ON tp.id = htpp.team_player_id
                INNER JOIN " . self::TABLE_POINT_TYPE . " pt ON pt.id = htpp.point_type_id
                INNER JOIN " . self::TABLE_PLAYER . " p ON p.id = tp.player_id
                INNER JOIN " . self::TABLE_HUMAN . " h ON p.human_id = h.id
                INNER JOIN " . self::TABLE_FRANCHISE . " f ON p.franchise_id = f.id
                INNER JOIN " . self::TABLE_TEAM . " t ON t.id = tp.team_id
                INNER JOIN " . self::TABLE_GAME . " g ON g.id = t.game_id
                INNER JOIN " . self::TABLE_PLACE . " place ON place.id = g.place_id
                WHERE g.id = ? AND f.id = ?
                ORDER BY htpp.point_time ASC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$gameId, $franchiseId]);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $historyTeamPlayerPoints = [];
        foreach ($rows as $row) {
            $historyTeamPlayerPoints[] = $this->mappingHistoryTeamPlayerPoints($row);
        }
        return $historyTeamPlayerPoints;
    }

    public function deleteByGame(int $gameId): bool
    {
        $query = "DELETE htpp.* FROM " . self::TABLE_HISTORY_TEAM_PLAYER_POINT . " htpp
                  INNER JOIN " . self::TABLE_TEAM_PLAYER . " tp ON tp.id = htpp.team_player_id
                  INNER JOIN " . self::TABLE_TEAM . " t ON t.id = tp.team_id
                  INNER JOIN " . self::TABLE_GAME . " g ON g.id = t.game_id
                  WHERE g.id = ?";
        $stmt = $this->conn->prepare($query);

        if($stmt->execute([$gameId])) {
            return true;
        }
        return false;
    }

    public function create(int $teamPlayerId, int $pointTypeId, string $pointTime): bool
    {
        $query = "INSERT INTO " . self::TABLE_HISTORY_TEAM_PLAYER_POINT . " 
                (team_player_id, point_type_id, point_time)
                VALUES 
                (:teamPlayerId, :pointTypeId, :pointTime)";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":teamPlayerId", $teamPlayerId);
        $stmt->bindParam(":pointTypeId", $pointTypeId);
        $stmt->bindParam(":pointTime", $pointTime);

        return $stmt->execute();
    }

    private function mappingHistoryTeamPlayerPoints($row) : HistoryTeamPlayerPoint
    {
        $human = new Human(
            $row['human_id'],
            $row['last_name'],
            $row['first_name'],
            $row['gender'],
            $row['nationality'],
            $row['birth_date']
        );

        $franchise = new Franchise (
            $row['franchise_id'],
            $row['name'],
            $row['color'],
            $row['initials'],
            $row['isActive']
        );

        $player = new Player(
            $row['player_id'],
            $human,
            $row['jersey_number'],
            $row['main_post'],
            $franchise,
            $row['in_date'],
            $row['out_date']
        );

        $place = new Place(
            $row['place_id'],
            $row['gym'],
            $row['address'],
            $row['city']
        );

        // Specify the format of the input string
        $format = 'Y-m-d H:i:s';
        // Convert the string to a DateTime object
        $beginDateTime = DateTime::createFromFormat($format, $row['begin_date_time']);

        $game = new Game(
            $row['game_id'],
            $beginDateTime,
            $place,
            $row['game_time']
        );

        $team = new Team(
            $row['team_id'],
            $game,
            $franchise,
            $row['is_home']
        );

        $teamPlayer = new TeamPlayer(
            $row['team_player_id'],
            $team,
            $player,
            $row['post'],
            $row['title']
        );

        $pointType = new PointType(
            $row['point_type_id'],
            $row['point_type_name'],
            $row['point_type_point']
        );

        return new HistoryTeamPlayerPoint(
            $row['id'],
            $teamPlayer,
            $pointType,
            $row['point_time']
        );
    }
    
}
