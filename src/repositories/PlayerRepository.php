<?php
include_once __DIR__ . '/../models/Player.php';
include_once __DIR__ . '/../models/Human.php';
include_once __DIR__ . '/../models/Franchise.php';
include_once __DIR__ . '/../repositories/BaseRepository.php';

class PlayerRepository extends BaseRepository
{
    public function getPlayer($playerId) : null|Player
    {
        $query = "SELECT 
                p.id, 
                p.human_id as human_id, 
                h.last_name as last_name, 
                h.first_name as first_name, 
                h.gender as gender, 
                h.nationality as nationality, 
                h.birth_date as birth_date,
                p.jersey_number as jersey_number,
                p.main_post as main_post,
                p.franchise_id as franchise_id,
                f.name as name, 
                f.color as color, 
                f.initials as initials,
                f.isActive as isActive,
                p.in_date, p.out_date
             FROM " . self::TABLE_PLAYER . " p
             INNER JOIN " . self::TABLE_HUMAN . " h ON p.human_id = h.id
             INNER JOIN " . self::TABLE_FRANCHISE . " f ON p.franchise_id = f.id
             WHERE p.id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$playerId]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $this->mappingPlayer($row);
    }

    public function getPlayersByFranchise($franchiseId) : array
    {
        $query = "SELECT 
                p.id, 
                p.human_id, 
                h.last_name, h.first_name, h.gender, h.nationality, h.birth_date, 
                p.jersey_number,
                p.main_post,
                p.franchise_id,
                f.name, f.color, f.initials, f.isActive,
                p.in_date, p.out_date
             FROM " . self::TABLE_PLAYER . " p
             INNER JOIN " . self::TABLE_HUMAN . " h ON p.human_id = h.id
             INNER JOIN " . self::TABLE_FRANCHISE . " f ON p.franchise_id = f.id
             WHERE p.franchise_id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$franchiseId]);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $players = [];
        foreach ($rows as $row) {
            $players[] = $this->mappingPlayer($row);
        }
        return $players;
    }

    public function getPlayersByGame(int $gameId) : array
    {
        $query = "SELECT 
                p.id, 
                p.human_id as human_id, 
                h.last_name as last_name, 
                h.first_name as first_name, 
                h.gender as gender, 
                h.nationality as nationality, 
                h.birth_date as birth_date,
                p.jersey_number as jersey_number,
                p.main_post as main_post,
                p.franchise_id as franchise_id,
                f.name as name, 
                f.color as color, 
                f.initials as initials,
                f.isActive as isActive,
                p.in_date, p.out_date
             FROM " . self::TABLE_PLAYER . " p
             INNER JOIN " . self::TABLE_HUMAN . " h ON p.human_id = h.id
             INNER JOIN " . self::TABLE_TEAM_PLAYER . " tp ON tp.player_id = p.id
             INNER JOIN " . self::TABLE_TEAM . " t ON t.id = tp.team_id
             INNER JOIN " . self::TABLE_GAME . " g ON g.id = t.game_id AND g.id = ?
             INNER JOIN " . self::TABLE_FRANCHISE . " f ON t.franchise_id = f.id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$gameId]);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $players = [];
        foreach ($rows as $row) {
            $players[] = $this->mappingPlayer($row);
        }
        return $players;
    }

    public function getOtherPlayers($franchiseId) : array
    {
        $query = "SELECT 
                p.id, 
                p.human_id as human_id, 
                h.last_name as last_name, 
                h.first_name as first_name, 
                h.gender as gender, 
                h.nationality as nationality, 
                h.birth_date as birth_date,
                p.jersey_number as jersey_number,
                p.main_post as main_post,
                p.franchise_id as franchise_id,
                f.name as name, 
                f.color as color, 
                f.initials as initials,
                f.isActive as isActive,
                p.in_date, p.out_date
             FROM " . self::TABLE_PLAYER . " p
             INNER JOIN " . self::TABLE_HUMAN . " h ON p.human_id = h.id
             INNER JOIN " . self::TABLE_FRANCHISE . " f ON p.franchise_id = f.id
             WHERE (p.franchise_id != ? OR p.out_date IS NOT NULL)
             GROUP BY p.human_id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$franchiseId]);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $players = [];
        foreach ($rows as $row) {
            $players[] = $this->mappingPlayer($row);
        }
        return $players;
    }

    public function update($playerId, $humanId, $firstName, $lastName, $gender, $nationality, $birthDate, $jerseyNumber, $mainPost) : bool
    {
        $queryHuman = "
            UPDATE " . self::TABLE_HUMAN . " 
            SET 
                first_name = :firstName, 
                last_name = :lastName, 
                gender = :gender, 
                nationality = :nationality, 
                birth_date = :birthDate 
            WHERE id = :humanId";
        $stmtHuman = $this->conn->prepare($queryHuman);
        $stmtHuman->bindParam(":firstName", $firstName);
        $stmtHuman->bindParam(":lastName", $lastName);
        $stmtHuman->bindParam(":gender", $gender);
        $stmtHuman->bindParam(":nationality", $nationality);
        $stmtHuman->bindParam(":birthDate", $birthDate);
        $stmtHuman->bindParam(":humanId", $humanId);
        $stmtHuman->execute();

        $queryPlayer = "
            UPDATE " . self::TABLE_PLAYER . " 
            SET 
                jersey_number = :jerseyNumber, 
                main_post = :mainPost 
            WHERE id = :playerId";
        $stmtPlayer = $this->conn->prepare($queryPlayer);
        $stmtPlayer->bindParam(":jerseyNumber", $jerseyNumber);
        $stmtPlayer->bindParam(":mainPost", $mainPost);
        $stmtPlayer->bindParam(":playerId", $playerId);

        return $stmtPlayer->execute();
    }

    public function delete($playerId): bool
    {
        $query = "
        UPDATE " . self::TABLE_PLAYER . "
        SET out_date = :out_date
        WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $currentDate = date('Y-m-d');
        $stmt->bindParam(':out_date', $currentDate);
        $stmt->bindParam(':id', $playerId);

        return $stmt->execute();
    }

    public function updateFranchisePlayers($data): bool
    {
        $franchiseId = $data['franchiseId'];
        $todayDate = date('Y-m-d');

        if (isset($data['playerFirstName'])) {
            foreach ($data['playerFirstName'] as $index => $firstName) {
                $lastName = $data['playerLastName'][$index] ?? null;
                $gender = $data['playerGender'][$index] ?? null;
                $nationality = $data['playerNationality'][$index] ?? null;
                $birthDate = $data['playerBirthDate'][$index] ?? null;
                $jerseyNumber = $data['playerJerseyNumber'][$index] ?? null;
                $mainPost = $data['playerMainPost'][$index] ?? null;

                $queryHuman = "INSERT INTO " . self::TABLE_HUMAN . " 
            (first_name, last_name, gender, nationality, birth_date) 
            VALUES 
            (:first_name, :last_name, :gender, :nationality, :birth_date)";
                $stmtHuman = $this->conn->prepare($queryHuman);
                $stmtHuman->execute([
                    ':first_name' => $firstName,
                    ':last_name' => $lastName,
                    ':gender' => $gender,
                    ':nationality' => $nationality,
                    ':birth_date' => $birthDate
                ]);

                $humanId = $this->conn->lastInsertId();

                $queryPlayer = "INSERT INTO " . self::TABLE_PLAYER . " 
            (human_id, jersey_number, main_post, franchise_id, in_date) 
            VALUES 
            (:human_id, :jersey_number, :main_post, :franchise_id, :in_date)";
                $stmtPlayer = $this->conn->prepare($queryPlayer);
                $stmtPlayer->execute([
                    ':human_id' => $humanId,
                    ':jersey_number' => $jerseyNumber,
                    ':main_post' => $mainPost,
                    ':franchise_id' => $franchiseId,
                    ':in_date' => $todayDate
                ]);
            }
        }

        if (isset($data['otherPlayer'])) {
            foreach ($data['otherPlayer'] as $playerId) {
                $queryOutDate = "UPDATE " . self::TABLE_PLAYER . " 
            SET out_date = :out_date 
            WHERE id = :player_id AND out_date IS NULL";
                $stmtOutDate = $this->conn->prepare($queryOutDate);
                $stmtOutDate->execute([
                    ':out_date' => $todayDate,
                    ':player_id' => $playerId
                ]);

                $queryFetch = "SELECT human_id, jersey_number, main_post FROM " . self::TABLE_PLAYER . " 
            WHERE id = :player_id LIMIT 1";
                $stmtFetch = $this->conn->prepare($queryFetch);
                $stmtFetch->execute([':player_id' => $playerId]);
                $playerData = $stmtFetch->fetch(PDO::FETCH_ASSOC);

                if ($playerData) {
                    $queryPlayer = "INSERT INTO " . self::TABLE_PLAYER . " 
                (human_id, jersey_number, main_post, franchise_id, in_date) 
                VALUES 
                (:human_id, :jersey_number, :main_post, :franchise_id, :in_date)";
                    $stmtPlayer = $this->conn->prepare($queryPlayer);
                    $stmtPlayer->execute([
                        ':human_id' => $playerData['human_id'],
                        ':jersey_number' => $playerData['jersey_number'],
                        ':main_post' => $playerData['main_post'],
                        ':franchise_id' => $franchiseId,
                        ':in_date' => $todayDate
                    ]);
                }
            }
        }

        return true;
    }

    private function mappingPlayer($row) : Player
    {
        $human = new Human(
            $row['human_id'],
            $row['last_name'],
            $row['first_name'],
            $row['gender'],
            $row['nationality'],
            $row['birth_date']
        );

        $franchise = new Franchise (
            $row['franchise_id'],
            $row['name'],
            $row['color'],
            $row['initials'],
            $row['isActive']
        );

        return new Player(
            $row['id'],
            $human,
            $row['jersey_number'],
            $row['main_post'],
            $franchise,
            $row['in_date'],
            $row['out_date']
        );
    }
}