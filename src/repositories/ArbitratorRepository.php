<?php
include_once __DIR__ . '/../repositories/BaseRepository.php';

class ArbitratorRepository extends BaseRepository
{

    public function getAll(): array
    {
        $query = "SELECT a.id, a.human_id, h.last_name, h.first_name, h.gender, h.nationality, h.birth_date
                  FROM " . self::TABLE_ARBITRATOR . " a
                  INNER JOIN human h ON a.human_id = h.id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $arbitrators = [];
        foreach ($rows as $row) {
            $arbitrator = $this->mappingArbitrator($row);
            if ($arbitrator !== null) {
                $arbitrators[] = $arbitrator;
            }
        }
        return $arbitrators;
    }

    public function get($arbitratorId): null|Arbitrator
    {
        $query = "SELECT a.id, a.human_id, h.last_name, h.first_name, h.gender, h.nationality, h.birth_date
                  FROM " . self::TABLE_ARBITRATOR . " a
                  INNER JOIN human h ON a.human_id = h.id
                  WHERE a.id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$arbitratorId]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $this->mappingArbitrator($row);
    }

    public function getAllByGame($gameId): array
    {
        $query = "SELECT a.id, a.human_id, h.last_name, h.first_name, h.gender, h.nationality, h.birth_date, ga.title
                  FROM arbitrator a
                  INNER JOIN human h ON a.human_id = h.id
                  INNER JOIN game_arbitrator ga ON ga.arbitrator_id = a.id
                  WHERE ga.game_id = $gameId";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $arbitrators = [];
        foreach ($rows as $row) {
            $arbitrator = $this->mappingArbitrator($row);
            if ($arbitrator !== null) {
                $arbitrators[] = $arbitrator;
            }
        }
        return $arbitrators;
    }

    public function createForGame($gameId, $arbitratorId, $title): bool
    {
        $query = "INSERT INTO " . self::TABLE_GAME_ARBITRATOR . " SET game_id=:gameId, arbitrator_id=:arbitratorId, title=:title";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":gameId", $gameId);
        $stmt->bindParam(":arbitratorId", $arbitratorId);
        $stmt->bindParam(":title", $title);

        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    public function create(array $data): bool
    {
        $queryHuman = "INSERT INTO " . self::TABLE_HUMAN . " (last_name, first_name, gender, nationality, birth_date)
        VALUES (:lastName, :firstName, :gender, :nationality, :birthDate);
        ";
        $stmtHuman = $this->conn->prepare($queryHuman);
        $stmtHuman->bindParam(':lastName', $data['lastName']);
        $stmtHuman->bindParam(':firstName', $data['firstName']);
        $stmtHuman->bindParam(':gender', $data['gender']);
        $stmtHuman->bindParam(':nationality', $data['nationality']);
        $stmtHuman->bindParam(':birthDate', $data['birthDate']);

        if (!$stmtHuman->execute()) {
            $this->conn->rollBack();
            return false;
        }

        $humanId = $this->conn->lastInsertId();

        $queryArbitrator = "INSERT INTO " . self::TABLE_ARBITRATOR . " (human_id) VALUES (:humanId);";
        $stmtArbitrator = $this->conn->prepare($queryArbitrator);
        $stmtArbitrator->bindParam(':humanId', $humanId);

        if (!$stmtArbitrator->execute()) {
            $this->conn->rollBack();
            return false;
        }

        return true;
    }

    public function update(array $data): null|Arbitrator
    {
        $query = "UPDATE " . self::TABLE_HUMAN . " SET last_name = :arbitratorLastName, first_name = :arbitratorFirstName, gender = :arbitratorGender, nationality = :arbitratorNationality, birth_date = :arbitratorBirthDate 
        WHERE id = (
        SELECT human_id
        FROM " . self::TABLE_ARBITRATOR . "
        WHERE id = :arbitratorId
        );";
        $stmt = $this->conn->prepare($query);

        $id = intval(htmlspecialchars(strip_tags($data['arbitratorId'])));
        $lastName = htmlspecialchars(strip_tags($data['arbitratorLastName']));
        $firstName = htmlspecialchars(strip_tags($data['arbitratorFirstName']));
        $gender = htmlspecialchars(strip_tags($data['arbitratorGender']));
        $nationality = htmlspecialchars(strip_tags($data['arbitratorNationality']));
        $birthDate = htmlspecialchars(strip_tags($data['arbitratorBirthDate']));

        $stmt->bindParam(":arbitratorLastName", $lastName);
        $stmt->bindParam(":arbitratorFirstName", $firstName);
        $stmt->bindParam(":arbitratorGender", $gender);
        $stmt->bindParam(":arbitratorNationality", $nationality);
        $stmt->bindParam(":arbitratorBirthDate", $birthDate);
        $stmt->bindParam(":arbitratorId", $id);

        if ($stmt->execute()) {
            return $this->get($id);
        }
        return null;
    }

    public function deleteByGame(int $gameId): bool
    {
        $query = "DELETE FROM " . self::TABLE_GAME_ARBITRATOR . " WHERE game_id = ?";
        $stmt = $this->conn->prepare($query);
        return $stmt->execute([$gameId]);
    }


    public function deleteByArbitratorId(int $id): bool
    {
        $query = "DELETE FROM " . self::TABLE_ARBITRATOR . " WHERE id = ?";
        $stmt = $this->conn->prepare($query);

        if($stmt->execute([$id])) {
            return true;
        }
        return false;
    }

    private function mappingArbitrator($row): null|Arbitrator
    {
        $human = new Human(
            $row['human_id'],
            $row['last_name'],
            $row['first_name'],
            $row['gender'],
            $row['nationality'],
            $row['birth_date']
        );

        $title = '';
        if (isset($row['title'])) {
            $title = $row['title'];
        }

        return new Arbitrator($row['id'], $human, $title);
    }

}