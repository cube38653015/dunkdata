<?php

include_once __DIR__ . '/../models/HistorySubstitute.php';

class HistorySubstituteRepository extends BaseRepository
{

    public function getAllByGameByFranchise(int $gameId, int $franchiseId): array
    {
        $query = "SELECT 
                hs.id,
                hs.incoming_team_player_id,
                hs.outgoing_team_player_id,
                hs.substitute_time,
                
                -- incoming_team_player_id
                tpi.id as i_team_player_id,
                tpi.player_id as i_player_id,
                tpi.post as i_post,
                tpi.title as i_title, 
                pi.human_id as i_human_id, 
                hi.last_name as i_last_name, 
                hi.first_name as i_first_name, 
                hi.gender as i_gender,
                hi.nationality as i_nationality, 
                hi.birth_date as i_birth_date, 
                pi.jersey_number as i_jersey_number,
                pi.main_post as i_main_post,
                pi.in_date as i_in_date,
                pi.out_date as i_out_date,
                
                -- outgoing_team_player_id
                tpo.id as o_team_player_id,
                tpo.player_id as o_player_id,
                tpo.post as o_post,
                tpo.title as o_title,
                po.human_id as o_human_id, 
                ho.last_name as o_last_name,
                ho.first_name as o_first_name, 
                ho.gender as o_gender, 
                ho.nationality as o_nationality,
                ho.birth_date as o_birth_date, 
                po.jersey_number as o_jersey_number,
                po.main_post as o_main_post,
                po.in_date as o_in_date,
                po.out_date as o_out_date,
                
                g.begin_date_time,
                g.game_time,
                g.place_id,
                g.id as game_id,
                f.name,
                f.color,
                f.initials,
                f.isActive,
                t.id as team_id,
                t.is_home,
                f.id as franchise_id,
                place.gym, place.address, place.city
             FROM ". self::TABLE_HISTORY_SUBSTITUTE . " hs
             INNER JOIN " . self::TABLE_TEAM_PLAYER . " tpi ON tpi.id = hs.incoming_team_player_id
             INNER JOIN " . self::TABLE_PLAYER . " pi ON pi.id = tpi.player_id
             INNER JOIN " . self::TABLE_HUMAN . " hi ON pi.human_id = hi.id
             INNER JOIN " . self::TABLE_FRANCHISE . " f ON pi.franchise_id = f.id
             INNER JOIN " . self::TABLE_TEAM . " t ON t.id = tpi.team_id
             INNER JOIN " . self::TABLE_GAME . " g ON g.id = t.game_id
             INNER JOIN " . self::TABLE_TEAM_PLAYER . " tpo ON tpo.id = hs.outgoing_team_player_id
             INNER JOIN " . self::TABLE_PLAYER . " po ON po.id = tpo.player_id
             INNER JOIN " . self::TABLE_HUMAN . " ho ON po.human_id = ho.id
             INNER JOIN " . self::TABLE_PLACE . " place ON place.id = g.place_id
             WHERE g.id = ? AND f.id = ?
             ORDER BY hs.substitute_time ASC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$gameId, $franchiseId]);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $historySubstitutes = [];
        foreach ($rows as $row) {
            $historySubstitutes[] = $this->mappingHistorySubstitute($row);
        }
        return $historySubstitutes;
    }

    public function deleteByGame(int $gameId): bool
    {
        $query = "DELETE hs.* FROM " . self::TABLE_HISTORY_SUBSTITUTE . " hs
                  INNER JOIN " . self::TABLE_TEAM_PLAYER . " tp ON tp.id = hs.incoming_team_player_id
                  INNER JOIN " . self::TABLE_TEAM . " t ON t.id = tp.team_id
                  INNER JOIN " . self::TABLE_GAME . " g ON g.id = t.game_id
                  WHERE g.id = ?";
        $stmt = $this->conn->prepare($query);

        if($stmt->execute([$gameId])) {
            return true;
        }
        return false;
    }

    public function create(int $incomingPlayerId, int $outgoingPlayerId, string $substituteTime): bool
    {
        $query = "INSERT INTO " . self::TABLE_HISTORY_SUBSTITUTE . " 
                (incoming_team_player_id, outgoing_team_player_id, substitute_time)
                VALUES 
                (:incomingPlayerId, :outgoingPlayerId, :substituteTime)";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":incomingPlayerId", $incomingPlayerId);
        $stmt->bindParam(":outgoingPlayerId", $outgoingPlayerId);
        $stmt->bindParam(":substituteTime", $substituteTime);

        return $stmt->execute();
    }


    private function mappingHistorySubstitute($row) : HistorySubstitute
    {

        $place = new Place(
            $row['place_id'],
            $row['gym'],
            $row['address'],
            $row['city']
        );

        // Specify the format of the input string
        $format = 'Y-m-d H:i:s';
        // Convert the string to a DateTime object
        $beginDateTime = DateTime::createFromFormat($format, $row['begin_date_time']);

        $game = new Game(
            $row['game_id'],
            $beginDateTime,
            $place,
            $row['game_time']
        );

        $franchise = new Franchise (
            $row['franchise_id'],
            $row['name'],
            $row['color'],
            $row['initials'],
            $row['isActive']
        );

        $team = new Team(
            $row['team_id'],
            $game,
            $franchise,
            $row['is_home']
        );

        $i_human = new Human(
            $row['i_human_id'],
            $row['i_last_name'],
            $row['i_first_name'],
            $row['i_gender'],
            $row['i_nationality'],
            $row['i_birth_date']
        );

        $i_player = new Player(
            $row['i_player_id'],
            $i_human,
            $row['i_jersey_number'],
            $row['i_main_post'],
            $franchise,
            $row['i_in_date'],
            $row['i_out_date']
        );

        $i_team_player = new TeamPlayer(
            $row['i_team_player_id'],
            $team,
            $i_player,
            $row['i_post'],
            $row['i_title']
        );

        $o_human = new Human(
            $row['o_human_id'],
            $row['o_last_name'],
            $row['o_first_name'],
            $row['o_gender'],
            $row['o_nationality'],
            $row['o_birth_date']
        );

        $o_player = new Player(
            $row['o_player_id'],
            $o_human,
            $row['o_jersey_number'],
            $row['o_main_post'],
            $franchise,
            $row['o_in_date'],
            $row['o_out_date']
        );

        $o_team_player = new TeamPlayer(
            $row['o_team_player_id'],
            $team,
            $o_player,
            $row['o_post'],
            $row['o_title']
        );

        return new HistorySubstitute(
            $row['id'],
            $i_team_player,
            $o_team_player,
            $row['substitute_time']
        );
    }
}
