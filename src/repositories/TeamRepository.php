<?php
include_once __DIR__ . '/../models/Team.php';
include_once __DIR__ . '/../repositories/BaseRepository.php';

class TeamRepository extends BaseRepository
{

    public function get($teamId) : Team
    {
        $query = "SELECT 
                t.id,
                t.game_id,
                t.franchise_id,
                t.is_home,
                t.score,
                g.begin_date_time, 
                g.place_id, 
                p.gym, p.address, p.city, 
                f.name as f_name, f.color as f_color, f.initials as f_initials, f.isActive as f_isActive,
                g.game_time
             FROM " . self::TABLE_TEAM . " t
             INNER JOIN " . self::TABLE_GAME . " g ON g.id = t.game_id
             INNER JOIN " . self::TABLE_FRANCHISE . " f ON f.id = t.franchise_id
             INNER JOIN " . self::TABLE_PLACE . " p ON p.id = g.place_id 
             WHERE t.id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$teamId]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $this->mappingTeam($row);
    }

    public function getTeamsByGame(int $gameId): array
    {
        $query = "SELECT 
                t.id,
                t.game_id,
                t.franchise_id,
                t.is_home,
                t.score,
                g.begin_date_time, 
                g.place_id, 
                p.gym, p.address, p.city, 
                f.name as f_name, f.color as f_color, f.initials as f_initials, f.isActive as f_isActive,
                g.game_time
             FROM " . self::TABLE_TEAM . " t
             INNER JOIN " . self::TABLE_GAME . " g ON g.id = t.game_id
             INNER JOIN " . self::TABLE_FRANCHISE . " f ON f.id = t.franchise_id
             INNER JOIN " . self::TABLE_PLACE . " p ON p.id = g.place_id 
             WHERE g.id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$gameId]);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $teams = [];
        foreach ($rows as $row) {
            $teams[] = $this->mappingTeam($row);
        }
        return $teams;
    }

    public function create(int $gameId, int $franchiseId, bool $isHome = false): ?Team
    {
        $query = "INSERT INTO " . self::TABLE_TEAM . " 
            SET game_id=:gameId,
                franchise_id=:franchiseId,
                is_home=:isHome";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":gameId", $gameId);
        $stmt->bindParam(":franchiseId", $franchiseId);
        $stmt->bindParam(":isHome", $isHome);

        if ($stmt->execute()) {
            $teamId = $this->conn->lastInsertId();
            return $this->get($teamId);
        }

        return null;
    }

    public function updateScore($gameId, $franchiseId, $score): void
    {
        $query = "UPDATE " . self::TABLE_TEAM . " 
            SET 
                score = :score
            WHERE game_id = :gameId AND franchise_id = :franchiseId";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":gameId", $gameId);
        $stmt->bindParam(":franchiseId", $franchiseId);
        $stmt->bindParam(":score", $score);
        $stmt->execute();
    }

    public function deleteByGame(int $gameId): bool
    {
        $query = "DELETE t.* FROM " . self::TABLE_TEAM . " t
                  INNER JOIN " . self::TABLE_GAME . " g ON g.id = t.game_id
                  WHERE g.id = ?";
        $stmt = $this->conn->prepare($query);

        if($stmt->execute([$gameId])) {
            return true;
        }
        return false;
    }

    private function mappingTeam($row) : Team
    {
        $place = new Place(
            $row['place_id'],
            $row['gym'],
            $row['address'],
            $row['city']
        );

        // Specify the format of the input string
        $format = 'Y-m-d H:i:s';
        // Convert the string to a DateTime object
        $beginDateTime = DateTime::createFromFormat($format, $row['begin_date_time']);

        $game = new Game(
            $row['game_id'],
            $beginDateTime,
            $place,
            $row['game_time']
        );

        $franchise = new Franchise(
            $row['franchise_id'],
            $row['f_name'],
            $row['f_color'],
            $row['f_initials'],
            $row['f_isActive']
        );

        return new Team(
            $row['id'],
            $game,
            $franchise,
            $row['is_home'],
            $row['score']
        );
    }
}