<?php
include_once __DIR__ . '/../models/Game.php';
include_once __DIR__ . '/../repositories/TeamRepository.php';
include_once __DIR__ . '/../repositories/BaseRepository.php';

class GameRepository extends BaseRepository
{

    public function get($gameId) : Game
    {
        $query = "SELECT 
                g.id, 
                g.begin_date_time, 
                g.place_id, 
                p.gym, p.address, p.city,
                g.game_time
             FROM " . self::TABLE_GAME . " g
             INNER JOIN " . self::TABLE_PLACE . " p ON p.id = g.place_id 
             WHERE g.id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$gameId]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $this->mappingGame($row);
    }

    public function getAll() : array
    {
        $query = "SELECT 
                g.id, 
                g.begin_date_time, 
                g.place_id, p.gym, p.address, p.city,
                g.game_time
             FROM " . self::TABLE_GAME . " g
             INNER JOIN " . self::TABLE_PLACE . " p ON p.id = g.place_id  
             ORDER BY begin_date_time ASC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $games = [];
        foreach ($rows as $row) {
            $games[] = $this->mappingGame($row);
        }
        return $games;
    }

    public function getGamesInProgress(): array
    {
        $query = "SELECT 
                g.id, 
                g.begin_date_time, 
                g.place_id, p.gym, p.address, p.city,
                g.game_time
             FROM " . self::TABLE_GAME . " g
             INNER JOIN " . self::TABLE_PLACE . " p ON p.id = g.place_id
             WHERE g.game_time is NULL
             ORDER BY begin_date_time ASC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $games = [];
        foreach ($rows as $row) {
            $games[] = $this->mappingGame($row);
        }
        return $games;
    }

    public function getGamesByFranchise($franchiseId): array
    {
        $query = "SELECT 
                g.id, 
                g.begin_date_time, 
                g.place_id, p.gym, p.address, p.city,
                g.game_time
             FROM " . self::TABLE_GAME . " g
             INNER JOIN " . self::TABLE_TEAM . " t ON g.id = t.game_id
             INNER JOIN " . self::TABLE_PLACE . " p ON p.id = g.place_id
             WHERE g.game_time is NULL AND t.franchise_id = :franchiseId
             ORDER BY begin_date_time ASC";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":franchiseId", $franchiseId);

        if ($stmt->execute()) {
            $games = [];
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach ($rows as $row) {
                $games[] = $this->mappingGame($row);
            }
            return $games;
        }
        return [];
    }

    public function getHistoryGames(): array
    {
        $query = "SELECT 
                g.id, 
                g.begin_date_time, 
                g.place_id, p.gym, p.address, p.city,
                g.game_time
             FROM " . self::TABLE_GAME . " g
             INNER JOIN " . self::TABLE_PLACE . " p ON p.id = g.place_id
             WHERE g.game_time is NOT NULL
             ORDER BY g.begin_date_time ASC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $games = [];
        foreach ($rows as $row) {
            $games[] = $this->mappingGame($row);
        }
        return $games;
    }

    public function getGamesInProgressByFranchise($franchiseId) : array
    {
        $query = "SELECT 
                DISTINCT g.id, 
                g.begin_date_time,
                g.place_id, 
                p.gym, p.address, p.city,
                g.game_time
             FROM " . self::TABLE_GAME . " g
             INNER JOIN " . self::TABLE_PLACE . " p ON p.id = g.place_id 
             INNER JOIN " . self::TABLE_TEAM . " t ON t.game_id = g.id AND t.franchise_id = :franchiseId
             WHERE begin_date_time IS NOT NULL
             AND game_time IS NULL
             ORDER BY g.begin_date_time ASC";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":franchiseId", $franchiseId);

        if ($stmt->execute()) {
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $games = [];
            foreach ($rows as $row) {
                $games[] = $this->mappingGame($row);
            }
            return $games;
        }
        return [];
    }

    public function create($beginDateTime, $placeId): null|Game
    {
        $query = "INSERT INTO " . self::TABLE_GAME . " 
            SET begin_date_time=:beginDateTime,
                place_id=:placeId";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":beginDateTime", $beginDateTime);
        $stmt->bindParam(":placeId", $placeId);

        if ($stmt->execute()) {
            $gameId = $this->conn->lastInsertId();
            return $this->get($gameId);
        }

        return null;
    }

    public function update(int $gameId, string $beginDateTime, int $placeId, ?int $gameTime): bool
    {
        $query = "UPDATE " . self::TABLE_GAME . " 
            SET 
                begin_date_time = :beginDateTime, 
                place_id = :placeId, 
                game_time = :gameTime
            WHERE id = :gameId";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":beginDateTime", $beginDateTime);
        $stmt->bindParam(":placeId", $placeId);
        $stmt->bindParam(":gameTime", $gameTime);
        $stmt->bindParam(":gameId", $gameId);

        return $stmt->execute();
    }

    public function delete(int $id): bool
    {
        $query = "DELETE FROM " . self::TABLE_GAME . " WHERE id = ?";
        $stmt = $this->conn->prepare($query);

        if($stmt->execute([$id])) {
            return true;
        }
        return false;
    }

    public function updateGameTime(int $gameId, string $gameTime): bool
    {
        $query = "UPDATE " . self::TABLE_GAME . " 
            SET 
                game_time = :gameTime
            WHERE id = :gameId";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":gameId", $gameId);
        $stmt->bindParam(":gameTime", $gameTime);

        return $stmt->execute();
    }

    private function mappingGame($row) : Game
    {
        $place = new Place(
            $row['place_id'],
            $row['gym'],
            $row['address'],
            $row['city']
        );

        // Specify the format of the input string
        $format = 'Y-m-d H:i:s';
        // Convert the string to a DateTime object
        $beginDateTime = DateTime::createFromFormat($format, $row['begin_date_time']);

        return new Game(
            $row['id'],
            $beginDateTime,
            $place,
            $row['game_time']
        );
    }
}
