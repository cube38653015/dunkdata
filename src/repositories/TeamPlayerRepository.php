<?php
include_once __DIR__ . '/../repositories/BaseRepository.php';

class TeamPlayerRepository extends BaseRepository
{
    public function create(int $teamId, int $playerId, string $post, string $title) : bool
    {
        $query = "INSERT INTO " . self::TABLE_TEAM_PLAYER . " 
                  (team_id, player_id, post, title) 
                  VALUES 
                  (:teamId, :playerId, :post, :title)";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":teamId", $teamId);
        $stmt->bindParam(":playerId", $playerId);
        $stmt->bindParam(":post", $post);
        $stmt->bindParam(":title", $title);

        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    public function getTeamPlayersByTeamId(int $teamId): array
    {
        $query = "SELECT 
                tp.id as team_player_id,
                tp.team_id,
                tp.player_id,
                tp.post,
                tp.title,
                p.human_id, 
                h.last_name, h.first_name, h.gender, h.nationality, h.birth_date, 
                p.jersey_number,
                p.main_post,
                p.in_date,
                p.out_date,
                p.franchise_id,
                f.name, f.color, f.initials, f.isActive,
                t.is_home,
                t.game_id,
                g.begin_date_time,
                g.game_time,
                g.place_id,
                place.gym, place.address, place.city
             FROM " . self::TABLE_TEAM_PLAYER . " tp
             INNER JOIN " . self::TABLE_PLAYER . " p ON p.id = tp.player_id
             INNER JOIN " . self::TABLE_HUMAN . " h ON p.human_id = h.id
             INNER JOIN " . self::TABLE_FRANCHISE . " f ON p.franchise_id = f.id
             INNER JOIN " . self::TABLE_TEAM . " t ON t.id = tp.team_id
             INNER JOIN " . self::TABLE_GAME . " g ON g.id = t.game_id
             INNER JOIN " . self::TABLE_PLACE . " place ON place.id = g.place_id
             WHERE tp.team_id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$teamId]);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $teamPlayers = [];
        foreach ($rows as $row) {
            $teamPlayers[] = $this->mappingTeamPlayer($row);
        }
        return $teamPlayers;
    }

    public function getTeamPlayersByGameByFranchise(int $gameId, int $franchiseId): array {
        $query = "SELECT 
                tp.id as team_player_id,
                tp.team_id,
                tp.player_id,
                tp.post,
                tp.title,
                p.human_id, 
                h.last_name, h.first_name, h.gender, h.nationality, h.birth_date, 
                p.jersey_number,
                p.main_post,
                p.in_date,
                p.out_date,
                p.franchise_id,
                f.name, f.color, f.initials, f.isActive,
                t.is_home,
                t.game_id,
                g.begin_date_time,
                g.game_time,
                g.place_id,
                place.gym, place.address, place.city
             FROM " . self::TABLE_TEAM_PLAYER . " tp
             INNER JOIN " . self::TABLE_PLAYER . " p ON p.id = tp.player_id
             INNER JOIN " . self::TABLE_HUMAN . " h ON p.human_id = h.id
             INNER JOIN " . self::TABLE_FRANCHISE . " f ON p.franchise_id = f.id
             INNER JOIN " . self::TABLE_TEAM . " t ON t.id = tp.team_id
             INNER JOIN " . self::TABLE_GAME . " g ON g.id = t.game_id
             INNER JOIN " . self::TABLE_PLACE . " place ON place.id = g.place_id
             WHERE g.id = ? AND f.id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$gameId, $franchiseId]);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $teamPlayers = [];
        foreach ($rows as $row) {
            $teamPlayers[] = $this->mappingTeamPlayer($row);
        }
        return $teamPlayers;
    }

    public function deleteAllTeamPlayerByTeamId(int $teamId): bool
    {
        $query = "DELETE FROM " . self::TABLE_TEAM_PLAYER . " WHERE team_id = ?";
        $stmt = $this->conn->prepare($query);

        if($stmt->execute([$teamId])) {
            return true;
        }
        return false;
    }

    public function deleteByGame(int $gameId): bool
    {
        $query = "DELETE tp.* FROM " . self::TABLE_TEAM_PLAYER . " tp
                  INNER JOIN " . self::TABLE_TEAM . " t ON t.id = tp.team_id
                  INNER JOIN " . self::TABLE_GAME . " g ON g.id = t.game_id
                  WHERE g.id = ?";
        $stmt = $this->conn->prepare($query);

        if($stmt->execute([$gameId])) {
            return true;
        }
        return false;
    }

    public function deleteByGameByPost(int $gameId, string $post): bool
    {
        $query = "DELETE tp.* FROM " . self::TABLE_TEAM_PLAYER . " tp
                  INNER JOIN " . self::TABLE_TEAM . " t ON t.id = tp.team_id
                  INNER JOIN " . self::TABLE_GAME . " g ON g.id = t.game_id
                  WHERE g.id = :gameId
                  AND tp.post = :post";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":gameId", $gameId);
        $stmt->bindParam(":post", $post);

        if($stmt->execute()) {
            return true;
        }
        return false;
    }

    private function mappingTeamPlayer($row) : TeamPlayer
    {

        $human = new Human(
            $row['human_id'],
            $row['last_name'],
            $row['first_name'],
            $row['gender'],
            $row['nationality'],
            $row['birth_date']
        );

        $franchise = new Franchise (
            $row['franchise_id'],
            $row['name'],
            $row['color'],
            $row['initials'],
            $row['isActive']
        );

        $player = new Player(
            $row['player_id'],
            $human,
            $row['jersey_number'],
            $row['main_post'],
            $franchise,
            $row['in_date'],
            $row['out_date']
        );

        $place = new Place(
            $row['place_id'],
            $row['gym'],
            $row['address'],
            $row['city']
        );

        // Specify the format of the input string
        $format = 'Y-m-d H:i:s';
        // Convert the string to a DateTime object
        $beginDateTime = DateTime::createFromFormat($format, $row['begin_date_time']);

        $game = new Game(
            $row['game_id'],
            $beginDateTime,
            $place,
            $row['game_time']
        );


        $team = new Team(
            $row['team_id'],
            $game,
            $franchise,
            $row['is_home']
        );

        return new TeamPlayer(
            $row['team_player_id'],
            $team,
            $player,
            $row['post'],
            $row['title']
        );
    }
}
