<?php
include_once __DIR__ . '/../repositories/BaseRepository.php';

class GameArbitratorRepository extends BaseRepository

{
    public function deleteByGame(int $gameId): bool
    {
        $query = "DELETE ga.* FROM " . self::TABLE_GAME_ARBITRATOR . " ga
                  INNER JOIN " . self::TABLE_GAME . " g ON g.id = ga.game_id
                  WHERE g.id = ?";
        $stmt = $this->conn->prepare($query);

        if($stmt->execute([$gameId])) {
            return true;
        }
        return false;
    }
}
