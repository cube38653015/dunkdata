<?php
include_once __DIR__ . '/../models/Franchise.php';
include_once __DIR__ . '/../repositories/BaseRepository.php';

class FranchiseRepository extends BaseRepository
{

    public function getAll() : array
    {
        $query = "SELECT id, name, color, initials, isActive FROM " . self::TABLE_FRANCHISE;
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $franchises = [];
        foreach ($rows as $row) {
            $franchise = $this->mappingFranchise($row);
            if ($franchise !== null) {
                $franchises[] = $franchise;
            }
        }
        return $franchises;
    }

    public function get($franchiseId) : null|Franchise
    {
        $query = "SELECT id, name, color, initials, isActive FROM " . self::TABLE_FRANCHISE . " WHERE id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$franchiseId]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $this->mappingFranchise($row);
    }

    public function create($data): null|Franchise
    {
        $query = "INSERT INTO " . self::TABLE_FRANCHISE . " SET name=:name, color=:color, initials=:initials, isActive=1";
        $stmt = $this->conn->prepare($query);

        $name = htmlspecialchars(strip_tags($data['name']));
        $color = htmlspecialchars(strip_tags($data['color']));
        $initials = htmlspecialchars(strip_tags($data['initials']));

        $stmt->bindParam(":name", $name);
        $stmt->bindParam(":color", $color);
        $stmt->bindParam(":initials", $initials);

        if ($stmt->execute()) {
            $id = $this->conn->lastInsertId();
            return $this->get($id);
        }
        return null;
    }

    public function update(int $franchiseId, string $name, string $color, string $initials, bool $isActive): bool
    {
        $intIsActive = (int)$isActive;
        $query = "UPDATE " . self::TABLE_FRANCHISE . " SET name=:name, color=:color, initials=:initials, isActive=$intIsActive
                  WHERE id = :franchiseId";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":franchiseId", $franchiseId);
        $stmt->bindParam(":name", $name);
        $stmt->bindParam(":color", $color);
        $stmt->bindParam(":initials", $initials);

        return $stmt->execute();
    }

    public function delete($franchiseId): bool
    {
        $todayDate = date('Y-m-d');

        $queryFranchise = "
        UPDATE " . self::TABLE_FRANCHISE . "
        SET isActive = false
        WHERE id = ?";
        $stmtFranchise = $this->conn->prepare($queryFranchise);
        $stmtFranchise->execute([$franchiseId]);

        $queryPlayer = "
        UPDATE " . self::TABLE_PLAYER . "
        SET out_date = :todayDate
        WHERE franchise_id = :franchiseId AND out_date IS NULL";
        $stmtPlayer = $this->conn->prepare($queryPlayer);
        $stmtPlayer->execute([
            ':todayDate' => $todayDate,
            ':franchiseId' => $franchiseId
        ]);

        $queryCoach = "
        UPDATE " . self::TABLE_COACH . "
        SET out_date = :todayDate
        WHERE franchise_id = :franchiseId AND out_date IS NULL";
        $stmtCoach = $this->conn->prepare($queryCoach);
        $stmtCoach->execute([
            ':todayDate' => $todayDate,
            ':franchiseId' => $franchiseId
        ]);

        return $stmtFranchise->execute();
    }

    public function getAllActive() : array
    {
        $query = "SELECT id, name, color, initials, isActive FROM " . self::TABLE_FRANCHISE . " WHERE isActive = 1";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $franchises = [];
        foreach ($rows as $row) {
            $franchise = $this->mappingFranchise($row);
            if ($franchise !== null) {
                $franchises[] = $franchise;
            }
        }
        return $franchises;
    }

    private function mappingFranchise($row) : null|Franchise
    {
        if (!empty($row)) {
            return new Franchise($row['id'], $row['name'], $row['color'], $row['initials'], $row['isActive']);
        }
        return null;
    }
}
