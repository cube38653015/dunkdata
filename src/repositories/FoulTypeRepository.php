<?php
include_once __DIR__ . '/../repositories/BaseRepository.php';
include_once __DIR__ . '/../models/FoulType.php';

class FoulTypeRepository extends BaseRepository
{
    public function getFoulTypes(): array
    {
        $query = "SELECT 
                ft.id, 
                ft.name
             FROM " . self::TABLE_FOUL_TYPE . " ft";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $games = [];
        foreach ($rows as $row) {
            $games[] = $this->mappingFoulType($row);
        }
        return $games;
    }

    private function mappingFoulType($row) : FoulType
    {
        return new FoulType(
            $row['id'],
            $row['name']
        );
    }
}