<?php
include_once __DIR__ . '/../repositories/BaseRepository.php';
include_once __DIR__ . '/../models/PointType.php';

class PointTypeRepository extends BaseRepository
{
    public function getPointTypes(): array
    {
        $query = "SELECT 
                pt.id, 
                pt.name,
                pt.point
             FROM " . self::TABLE_POINT_TYPE . " pt";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $games = [];
        foreach ($rows as $row) {
            $games[] = $this->mappingPointType($row);
        }
        return $games;
    }

    private function mappingPointType($row) : PointType
    {
        return new PointType(
            $row['id'],
            $row['name'],
            $row['point']
        );
    }
}