<?php
include_once __DIR__ . '/../models/User.php';
include_once __DIR__ . '/../repositories/BaseRepository.php';

class UserRepository extends BaseRepository
{
    public function getAll() : array
    {
        // LEFT JOIN here in order to recover users who do not have a linked human (admin or manager)
        $query = "SELECT 
                    u.id, 
                    u.login, 
                    u.password, 
                    u.role, u.human_id,
                    h.first_name,
                    h.last_name,
                    h.gender,
                    h.nationality,
                    h.birth_date
                  FROM " . self::TABLE_USER . " u
                  LEFT JOIN " . self::TABLE_HUMAN . " h ON h.id = u.human_id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $users = [];
        foreach ($rows as $row) {
            $user = $this->mappingUser($row);
            if ($user !== null) {
                $users[] = $user;
            }
        }
        return $users;
    }

    public function get($userId) : null|User
    {
        $query = "
        SELECT 
        u.id, 
        u.login, 
        u.password, 
        u.role, 
        h.last_name, 
        h.first_name, 
        h.gender, 
        h.nationality, 
        h.birth_date,
        c.id as coach_id,
        c.franchise_id
        FROM " . self::TABLE_USER . " u
        LEFT JOIN " . self::TABLE_HUMAN . " h ON u.human_id = h.id
        LEFT JOIN " . self::TABLE_COACH . " c ON u.human_id = c.human_id
        WHERE u.id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$userId]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $this->mappingUser($row);
    }

    public function getByLogin(string $login) : null|User
    {
        $query = "SELECT u.id, u.login, u.password, u.role, u.human_id,
        h.last_name, h.first_name, h.gender, h.nationality, h.birth_date
        FROM " . self::TABLE_USER . " u
        LEFT JOIN  " . self::TABLE_HUMAN . " h ON u.human_id = h.id
        WHERE u.login = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$login]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $this->mappingUser($row);
    }

    public function create($login, $password, $role, $humanId = null): bool
    {
        $query = "INSERT INTO " . self::TABLE_USER . " 
                (login, password, role, human_id)
                VALUES 
                (:login, :password, :role, :humanId)";
        $stmt = $this->conn->prepare($query);

        $password = password_hash($password, PASSWORD_DEFAULT);

        $stmt->bindParam(":login", $login);
        $stmt->bindParam(":password", $password);
        $stmt->bindParam(":role", $role);
        $stmt->bindParam(":humanId", $humanId);

        return $stmt->execute();
    }

    public function update($id, $login, $password, $role, $coachId, $humanId): bool
    {
        $query = "UPDATE " . self::TABLE_USER . " SET login = :login, password = :password, role = :role WHERE id = :id";
        $stmt = $this->conn->prepare($query);


        $stmt->bindParam(":login", $login);
        $stmt->bindParam(":password", $password);
        $stmt->bindParam(":role", $role);
        $stmt->bindParam(":id", $id);

        $success = $stmt->execute();

        if ($coachId !== null) {
            $queryCoach = "
            UPDATE " . self::TABLE_USER . " 
            SET human_id = :human_id 
            WHERE id = :id";

            $stmtCoach = $this->conn->prepare($queryCoach);
            $stmtCoach->bindParam(':id', $id);
            $stmtCoach->bindParam(':human_id', $humanId);

            $success = $stmtCoach->execute() && $success;
        }

        return $success;
    }

    public function deleteByUserId(int $id): bool
    {
        $query = "DELETE FROM " . self::TABLE_USER . " WHERE id = ?";
        $stmt = $this->conn->prepare($query);

        if($stmt->execute([$id])) {
            return true;
        }
        return false;
    }

    private function mappingUser($row) : ?User
    {
        $human = null;
        if (isset($row['human_id']) && $row['human_id'] > 0) {
            $human = new Human(
                $row['human_id'],
                $row['last_name'],
                $row['first_name'],
                $row['gender'],
                $row['nationality'],
                $row['birth_date']
            );
        }

        $user = new User(
            $row['id'],
            $row['login'],
            $row['password'],
            $row['role'],
            $human
        );

        if (isset($row['coach_id'])) {
            $user->setCoachId($row['coach_id']);
        }

        if (isset($row['franchise_id'])) {
            $user->setFranchiseId($row['franchise_id']);
        }

        return $user;
    }


}
