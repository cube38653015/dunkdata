<?php

class Place implements JsonSerializable
{
    private int $id;
    private string $gym;
    private string $address;
    private string $city;

    public function __construct(int $placeId, string $gym, string $address, string $city) {
        $this->id = $placeId;
        $this->gym = $gym;
        $this->address = $address;
        $this->city = $city;
    }

    // Getters
    public function getId(): int {
        return $this->id;
    }

    public function getGym(): string {
        return $this->gym;
    }

    public function getAddress(): string {
        return $this->address;
    }

    public function getCity(): string {
        return $this->city;
    }

    // Setters
    public function setId(int $id): void {
        $this->id = $id;
    }

    public function setGym(string $gym): void {
        $this->gym = $gym;
    }

    public function setAddress(string $address): void {
        $this->address = $address;
    }

    public function setCity(string $city): void {
        $this->city = $city;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'gym' => $this->getGym(),
            'address' => $this->getAddress(),
            'city' => $this->getCity()
        ];
    }
}