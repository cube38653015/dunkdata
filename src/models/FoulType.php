<?php

class FoulType implements JsonSerializable
{
    private int $id;
    private string $name;

    public function __construct($foulTypeId, $name)
    {
        $this->id = $foulTypeId;
        $this->name = $name;
    }

    // Getters
    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): string {
        return $this->name;
    }

    // Setters
    public function setId(?int $id): void {
        $this->id = $id;
    }

    public function setName(string $name): void {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName()
        ];
    }
}