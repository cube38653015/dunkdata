<?php
include_once __DIR__ . '/../models/Human.php';
include_once __DIR__ . '/../models/Franchise.php';

class Player implements JsonSerializable
{
    public const POST_PIVOT = "pivot";
    public const POST_LEADER = "leader";
    public const POST_WINGER = "winger";
    public const POST_STRONG_WINGER = "strong_winger";
    public const POST_BACK = "back";

    private int $id;
    private Human $human;
    private int $jerseyNumber;
    private string $mainPost;
    private Franchise $franchise;
    private ?string $inDate;
    private ?string $outDate;

    public function __construct(int $playerId, Human $human, int $jerseyNumber, string $mainPost, Franchise $franchise, ?string $inDate, ?string $outDate = null)
    {
        $this->id = $playerId;
        $this->human = $human;
        $this->jerseyNumber = $jerseyNumber;
        $this->mainPost = $mainPost;
        $this->franchise = $franchise;
        $this->inDate = $inDate;
        $this->outDate = $outDate;
    }

    // Getters
    public function getId(): int {
        return $this->id;
    }

    public function getHuman(): Human {
        return $this->human;
    }

    public function getJerseyNumber(): int {
        return $this->jerseyNumber;
    }

    public function getMainPost(): string {
        return $this->mainPost;
    }

    public function getFranchise(): Franchise {
        return $this->franchise;
    }

    public function getInDate(): ?string
    {
        return $this->inDate;
    }

    public function getOutDate(): ?string
    {
        return $this->outDate;
    }

    public function getTranslatedMainPost(): string
    {
        switch ($this->mainPost) {
            case self::POST_PIVOT:
                return "Pivot";
            case self::POST_LEADER:
                return "Meneur";
            case self::POST_WINGER:
                return "Ailier";
            case self::POST_STRONG_WINGER:
                return "Ailier fort";
            case self::POST_BACK:
                return "Arrière";
            default:
                return $this->mainPost;
        }
    }

    // Setters
    public function setId(int $id): void {
        $this->id = $id;
    }

    public function setHuman(Human $human): void {
        $this->human = $human;
    }

    public function setJerseyNumber(int $jerseyNumber): void {
        $this->jerseyNumber = $jerseyNumber;
    }

    public function setMainPost(string $mainPost): void {
        $this->mainPost = $mainPost;
    }

    public function setFranchise(Franchise $franchise): void {
        $this->franchise = $franchise;
    }

    public function setInDate(?string $inDate): void {
        $this->inDate = $inDate;
    }

    public function setOutDate(?string $outDate): void {
        $this->outDate = $outDate;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'human' => $this->getHuman(),
            'jerseyNumber' => $this->getJerseyNumber(),
            'mainPost' => $this->getMainPost(),
            'franchise' => $this->getFranchise(),
            'inDate' => $this->getInDate(),
            'outDate' => $this->getOutDate()
        ];
    }
}