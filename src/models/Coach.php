<?php
include_once __DIR__ . '/../models/Human.php';
include_once __DIR__ . '/../models/Franchise.php';

class Coach  implements JsonSerializable
{
    public const TITLE_MAIN_COACH = "main_coach";
    public const TITLE_ASSISTANT_COACH = "assistant_coach";

    private int $id;
    private Human $human;
    private Franchise $franchise;
    private string $title;
    private ?string $inDate;
    private ?string $outDate;

    public function __construct(int $coachId, Human $human, Franchise $franchise, string $title, ?string $inDate, ?string $outDate) {
        $this->id = $coachId;
        $this->human = $human;
        $this->franchise = $franchise;
        $this->title = $title;
        $this->inDate = $inDate;
        $this->outDate = $outDate;
    }

    // Getters
    public function getId(): int {
        return $this->id;
    }

    public function getHuman(): Human {
        return $this->human;
    }

    public function getFranchise(): Franchise {
        return $this->franchise;
    }

    public function getTitle(): string {
        return $this->title;
    }

    public function getInDate(): ?string
    {
        return $this->inDate;
    }

    public function getOutDate(): ?string
    {
        return $this->outDate;
    }

    public function getTitleTrad(): string {
        return match ($this->title) {
            Coach::TITLE_MAIN_COACH => 'Coach principal',
            Coach::TITLE_ASSISTANT_COACH => 'Coach assistant',
            default => '',
        };
    }

    // Setters
    public function setId(int $id): void {
        $this->id = $id;
    }

    public function setHuman(Human $human): void {
        $this->human = $human;
    }

    public function setFranchise(Franchise $franchise): void {
        $this->franchise = $franchise;
    }

    public function setTitle(string $title): void {
        $this->title = $title;
    }

    public function setInDate(?string $inDate): void {
        $this->inDate = $inDate;
    }

    public function setOutDate(?string $outDate): void {
        $this->outDate = $outDate;
    }

    public function jsonSerialize(): array {
        return [
            'id' => $this->getId(),
            'human' => $this->getHuman(),
            'franchise' => $this->getFranchise(),
            'title' => $this->getTitle(),
            'titleTrad' => $this->getTitleTrad()
        ];
    }
}