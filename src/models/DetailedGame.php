<?php
include_once __DIR__ . '/Game.php';
include_once __DIR__ . '/Place.php';
include_once __DIR__ . '/Arbitrator.php';
include_once __DIR__ . '/DetailedTeam.php';

class DetailedGame implements JsonSerializable
{
    private Game $game;
    private ?Arbitrator $holderArbitrator;
    private ?Arbitrator $assistantArbitrator1;
    private ?Arbitrator $assistantArbitrator2;

    private DetailedTeam $homeDetailedTeam;
    private DetailedTeam $awayDetailedTeam;

    public function __construct(Game $game, DetailedTeam $homeDetailedTeam, DetailedTeam $awayDetailedTeam, array $arbitrators)
    {
        $this->game = $game;
        $this->homeDetailedTeam = $homeDetailedTeam;
        $this->awayDetailedTeam = $awayDetailedTeam;
        $this->setArbitrators($arbitrators);
    }

    // Getters
    public function getGame(): Game
    {
        return $this->game;
    }

    public function getHomeDetailedTeam(): DetailedTeam
    {
        return $this->homeDetailedTeam;
    }

    public function getAwayDetailedTeam(): DetailedTeam
    {
        return $this->awayDetailedTeam;
    }

    public function getHolderArbitrator(): ?Arbitrator
    {
        return $this->holderArbitrator;
    }

    public function getAssistantArbitrator1(): ?Arbitrator
    {
        return $this->assistantArbitrator1;
    }

    public function getAssistantArbitrator2(): ?Arbitrator
    {
        return $this->assistantArbitrator2;
    }

    // Setters
    public function setGame(Game $game): void
    {
        $this->game = $game;
    }

    public function setHomeDetailedTeam(DetailedTeam $homeDetailedTeam): void
    {
        $this->homeDetailedTeam = $homeDetailedTeam;
    }

    public function setAwayDetailedTeam(DetailedTeam $awayDetailedTeam): void
    {
        $this->awayDetailedTeam = $awayDetailedTeam;
    }

    public function setArbitrators(array $arbitrators): void
    {
        $this->holderArbitrator = $arbitrators[Arbitrator::TITLE_HOLDER];
        $this->assistantArbitrator1 = $arbitrators[Arbitrator::TITLE_ASSISTANT_1];
        $this->assistantArbitrator2 = $arbitrators[Arbitrator::TITLE_ASSISTANT_2];
    }

    public function getTeamByFranchise($franchiseId): DetailedTeam
    {
        return $this->homeDetailedTeam->getTeam()->getFranchise()->getId() === $franchiseId ? $this->homeDetailedTeam : $this->awayDetailedTeam;
    }

    public function getOpposingTeam($franchiseId): DetailedTeam
    {
        return $this->homeDetailedTeam->getTeam()->getFranchise()->getId() === $franchiseId ? $this->awayDetailedTeam : $this->homeDetailedTeam;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'game' => $this->getGame(),
            'holderArbitrator' => $this->getHolderArbitrator(),
            'AssitantArbitrator1' => $this->getAssistantArbitrator1(),
            'AssitantArbitrator2' => $this->getAssistantArbitrator2(),
            '$homeDetailedTeam' => $this->getHomeDetailedTeam(),
            '$awayDetailedTeam' => $this->getAwayDetailedTeam()
        ];
    }
}