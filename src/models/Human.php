<?php

class Human implements JsonSerializable {
    public const GENDER_MAN = 'man';
    public const GENDER_WOMAN = 'woman';

    private int $id;
    private string $lastName;
    private string $firstName;
    private string $gender;
    private string $nationality;
    private string $birthDate;

    public function __construct(int $id, string $lastName, string $firstName, string $gender, string $nationality, string $birthDate) {
        $this->id = $id;
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->gender = $gender;
        $this->nationality = $nationality;
        $this->birthDate = $birthDate;
    }

    // Getters
    public function getId(): int {
        return $this->id;
    }

    public function getLastName(): string {
        return $this->lastName;
    }

    public function getFirstName(): string {
        return $this->firstName;
    }

    public function getGender(): string {
        return $this->gender;
    }

    public function getNationality(): string {
        return $this->nationality;
    }

    public function getBirthDate(): string {
        return $this->birthDate;
    }

    // Setters
    public function setId(int $id): void {
        $this->id = $id;
    }

    public function setLastName(string $lastName): void {
        $this->lastName = $lastName;
    }

    public function setFirstName(string $firstName): void {
        $this->firstName = $firstName;
    }

    public function setGender(string $gender): void {
        $this->gender = $gender;
    }

    public function setNationality(string $nationality): void {
        $this->nationality = $nationality;
    }

    public function setBirthDate(string $birthDate): void {
        $this->birthDate = $birthDate;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'firstName' => $this->getFirstName(),
            'lastName' => $this->getLastName(),
            'gender' => $this->getGender(),
            'nationality' => $this->getNationality(),
            'birthDate' => $this->getBirthDate()
        ];
    }
}