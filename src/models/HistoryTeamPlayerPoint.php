<?php

class HistoryTeamPlayerPoint implements JsonSerializable
{
    private int $id;
    private TeamPlayer $teamPlayer;
    private PointType $pointType;
    private string $pointTime;

    public function __construct($historyTeamPlayerPointId, TeamPlayer $teamPlayer, PointType $pointType, string $point_time)
    {
        $this->id = $historyTeamPlayerPointId;
        $this->teamPlayer = $teamPlayer;
        $this->pointType = $pointType;
        $this->pointTime = $point_time;
    }

    // Getters
    public function getId(): ?int {
        return $this->id;
    }

    public function getTeamPlayer(): TeamPlayer {
        return $this->teamPlayer;
    }

    public function getPointType(): PointType {
        return $this->pointType;
    }

    public function getPointTime(): ?string {
        return $this->pointTime;
    }

    public function getPointMinuteTime(): ?string {
        $timeSplited = explode(":", $this->pointTime);
        return $timeSplited[0] ?? null;
    }

    public function getPointSecondTime(): ?string {
        $timeSplited = explode(":", $this->pointTime);
        return $timeSplited[1] ?? null;
    }

    // Setters
    public function setId(?int $id): void {
        $this->id = $id;
    }

    public function setTeamPlayer(TeamPlayer $teamPlayer): void {
        $this->teamPlayer = $teamPlayer;
    }

    public function setPointType(PointType $pointType): void {
        $this->pointType = $pointType;
    }

    public function setPointTime(?string $pointTime): void {
        $this->pointTime = $pointTime;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'teamPlayer' => $this->getTeamPlayer(),
            'poitnType' => $this->getPointType(),
            'pointTime' => $this->getPointTime()
        ];
    }
}