<?php

class PointType implements JsonSerializable
{
    private int $id;
    private string $name;
    private string $point;

    public function __construct($pointTypeId, $name, $point)
    {
        $this->id = $pointTypeId;
        $this->name = $name;
        $this->point = $point;
    }

    // Getters
    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getPoint(): string {
        return $this->point;
    }

    // Setters
    public function setId(?int $id): void {
        $this->id = $id;
    }

    public function setName(string $name): void {
        $this->name = $name;
    }

    public function setPoint(string $point): void {
        $this->point = $point;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'point' => $this->getPoint()
        ];
    }
}