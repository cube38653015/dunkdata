<?php

include_once __DIR__ . '/../models/Game.php';
include_once __DIR__ . '/../models/Franchise.php';

class Team implements JsonSerializable
{
    private int $id;
    private Game $game;
    private Franchise $franchise;
    private ?int $score;
    private bool $isHome;

    public function __construct(int $id, Game $game, Franchise $franchise, bool $isHome = false, ?int $score = null)
    {
        $this->id = $id;
        $this->game = $game;
        $this->franchise = $franchise;
        $this->score = $score;
        $this->isHome = $isHome;
    }

    // Getters
    public function getId(): int {
        return $this->id;
    }

    public function getGame(): Game {
        return $this->game;
    }

    public function getFranchise(): Franchise {
        return $this->franchise;
    }

    public function getScore(): ?int {
        return $this->score;
    }

    public function getIsHome(): bool {
        return $this->isHome;
    }

    // Setters
    public function setId(int $id): void {
        $this->id = $id;
    }

    public function setGame(Game $game): void {
        $this->game = $game;
    }

    public function setFranchise(Franchise $franchise): void {
        $this->franchise = $franchise;
    }

    public function setScore(?int $score): void {
        $this->score = $score;
    }

    public function setIsHome(bool $isHome): void {
        $this->isHome = $isHome;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'human' => $this->getGame(),
            'franchise' => $this->getFranchise(),
            'score' => $this->getScore(),
            'isHome' => $this->getIsHome()
        ];
    }
}
