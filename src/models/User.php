<?php

class User {
    public const ROLE_ADMIN = 'admin';
    public const ROLE_MANAGER = 'manager';
    public const ROLE_COACH = 'coach';

    // ID unique de l'utilisateur
    private int $id;

    // Nom d'utilisateur ou identifiant de connexion
    private string $login;

    // Mot de passe de l'utilisateur, stocké sous forme hachée
    private string $password;

    // Rôle de l'utilisateur, par exemple 'admin', 'coach', 'manager'
    private string $role;

    // Instance de la classe Human associée à l'utilisateur, ou null si non applicable
    private ?Human $human;

    // ID du coach associé à l'utilisateur, ou null si non applicable
    private ?int $coachId = null;

    // ID de la franchise associée à l'utilisateur, ou null si non applicable
    private ?int $franchiseId = null;

    // Constructeur de la classe User
    public function __construct(int $id, string $login, string $password, string $role, ?Human $human) {
        $this->id = $id;
        $this->login = $login;
        $this->password = $password;
        $this->role = $role;
        $this->human = $human;
    }

    // Retourne l'ID de l'utilisateur
    public function getId(): int {
        return $this->id;
    }

    // Retourne le nom d'utilisateur (login)
    public function getLogin(): string {
        return $this->login;
    }

    // Retourne le mot de passe de l'utilisateur
    public function getPassword(): string {
        return $this->password;
    }

    // Retourne le rôle de l'utilisateur
    public function getRole(): string {
        return $this->role;
    }

    // Retourne l'instance de Human associée à l'utilisateur, ou null si non applicable
    public function getHuman(): ?Human {
        return $this->human;
    }

    // Retourne l'ID du coach associé à l'utilisateur, ou null si non applicable
    public function getCoachId(): ?int {
        return $this->coachId;
    }

    // Retourne l'ID de la franchise associée à l'utilisateur, ou null si non applicable
    public function getFranchiseId(): ?int {
        return $this->franchiseId;
    }

    // Définit l'ID de l'utilisateur
    public function setId(int $id): void {
        $this->id = $id;
    }

    // Définit le nom d'utilisateur (login)
    public function setLogin(string $login): void {
        $this->login = $login;
    }

    // Définit le mot de passe de l'utilisateur
    public function setPassword(string $password): void {
        $this->password = $password;
    }

    // Définit le rôle de l'utilisateur
    public function setRole(string $role): void {
        $this->role = $role;
    }

    // Définit l'instance de Human associée à l'utilisateur, ou null si non applicable
    public function setHuman(?Human $human): void {
        $this->human = $human;
    }

    // Définit l'ID du coach associé à l'utilisateur, ou null si non applicable
    public function setCoachId(?int $coachId): void {
        $this->coachId = $coachId;
    }

    // Définit l'ID de la franchise associée à l'utilisateur, ou null si non applicable
    public function setFranchiseId(?int $franchiseId): void {
        $this->franchiseId = $franchiseId;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'login' => $this->getLogin(),
            'password' => $this->getPassword(),
            'role' => $this->getRole(),
            'human' => $this->getHuman(),
            'coachId' => $this->getCoachId(),
            'franchiseId' => $this->getFranchiseId()
        ];
    }
}