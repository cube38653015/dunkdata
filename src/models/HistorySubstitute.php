<?php

class HistorySubstitute implements JsonSerializable
{
    private int $id;
    private TeamPlayer $incomingTeamPlayer;
    private TeamPlayer $outgoingTeamPlayer;
    private string $substituteTime;

    public function __construct(int $id, TeamPlayer $incomingTeamPlayer, TeamPlayer $outgoingTeamPlayer, string $substituteTime)
    {
        $this->id = $id;
        $this->incomingTeamPlayer = $incomingTeamPlayer;
        $this->outgoingTeamPlayer = $outgoingTeamPlayer;
        $this->substituteTime = $substituteTime;
    }

    // Getters
    public function getId(): int
    {
        return $this->id;
    }

    public function getIncomingTeamPlayer(): TeamPlayer
    {
        return $this->incomingTeamPlayer;
    }

    public function getOutgoingTeamPlayer(): TeamPlayer
    {
        return $this->outgoingTeamPlayer;
    }

    public function getSubstituteTime(): string
    {
        return $this->substituteTime;
    }

    public function getSubstituteMinuteTime(): ?string {
        $timeSplited = explode(":", $this->substituteTime);
        return $timeSplited[0] ?? null;
    }

    public function getSubstituteSecondTime(): ?string {
        $timeSplited = explode(":", $this->substituteTime);
        return $timeSplited[1] ?? null;
    }

    // Setters
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function setIncomingTeamPlayer(TeamPlayer $incomingTeamPlayer): void
    {
        $this->incomingTeamPlayer = $incomingTeamPlayer;
    }

    public function setOutgoingTeamPlayer(TeamPlayer $outgoingTeamPlayer): void
    {
        $this->outgoingTeamPlayer = $outgoingTeamPlayer;
    }

    public function setSubstituteTime(string $substituteTime): void
    {
        $this->substituteTime = $substituteTime;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'incomingTeamPlayers' => $this->getIncomingTeamPlayer(),
            'outgoingTeamPlayers' => $this->getOutgoingTeamPlayer(),
            'substittuteTime' => $this->getSubstituteTime()
        ];
    }
}
