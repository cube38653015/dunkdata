<?php
include_once __DIR__ . '/../models/Place.php';
include_once __DIR__ . '/../models/TeamPlayer.php';

class Game implements JsonSerializable
{
    private int $id;
    private DateTime $beginDateTime;
    private Place $place;
    private ?string $gameTime;

    public function __construct(int $gameId, DateTime $beginDateTime, Place $place, ?string $gameTime)
    {
        $this->id = $gameId;
        $this->beginDateTime = $beginDateTime;
        $this->place = $place;
        $this->gameTime = $gameTime;
    }

    // Getters
    public function getId(): int {
        return $this->id;
    }

    public function getBeginDateTime(): DateTime {
        return $this->beginDateTime;
    }

    public function getPlace(): Place {
        return $this->place;
    }

    public function getGameTime(): ?string {
        return $this->gameTime;
    }

    public function getGameMinuteTime(): string {
        if ($this->gameTime !== null) {
            $timeSplited = explode(":", $this->gameTime);
            return $timeSplited[0] ?? "";
        } else return "";
    }

    public function getGameSecondTime(): string {
        if ($this->gameTime !== null) {
            $timeSplited = explode(":", $this->gameTime);
            return $timeSplited[1] ?? "";
        } else return "";
    }

    public function isClosed(): bool {
        return empty($this->gameTime);
    }

    // Setters
    public function setId(int $id): void {
        $this->id = $id;
    }

    public function setBeginDateTime(DateTime $beginDateTime): void {
        $this->beginDateTime = $beginDateTime;
    }

    public function setPlace(Place $place): void {
        $this->place = $place;
    }

    public function setGameTime(?string $gameTime): void {
        $this->gameTime = $gameTime;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'beginDateTime' => $this->getBeginDateTime(),
            'place' => $this->getPlace(),
            'gameTime' => $this->getGameTime()
        ];
    }
}
