<?php

class Arbitrator implements JsonSerializable {
    public const TITLE_HOLDER = 'holder';
    public const TITLE_ASSISTANT = 'assistant';
    public const TITLE_ASSISTANT_1 = 'assistant1';
    public const TITLE_ASSISTANT_2 = 'assistant2';

    private int $id;
    private Human $human;
    private string $title;

    public function __construct(int $arbitratorId, Human $human, string $title)
    {
        $this->id = $arbitratorId;
        $this->human = $human;
        $this->title = $title;
    }

    // Getters
    public function getId(): int
    {
        return $this->id;
    }

    public function getHuman(): Human
    {
        return $this->human;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    // Setters
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function setHuman(Human $human): void
    {
        $this->human = $human;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'human' => $this->getHuman(),
            'title' => $this->getTitle()
        ];
    }
}
