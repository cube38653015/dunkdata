<?php
include_once __DIR__ . '/../models/Team.php';

class DetailedTeam implements JsonSerializable
{
    private Team $team;
    private array $teamPlayers;
    private array $historyTeamPlayerFouls;
    private array $historySubstitutes;
    private array $historyTeamPlayerPoints;

    public function __construct(Team $team, array $teamPlayers, array $historyTeamPlayerFouls, array $historySubstitutes, array $historyTeamPlayerPoints)
    {
        $this->team = $team;
        $this->teamPlayers = $teamPlayers;
        $this->historyTeamPlayerFouls = $historyTeamPlayerFouls;
        $this->historySubstitutes = $historySubstitutes;
        $this->historyTeamPlayerPoints = $historyTeamPlayerPoints;        
    }

    // Getters
    public function getTeam(): Team
    {
        return $this->team;
    }

    public function getTeamPlayers(): array
    {
        return $this->teamPlayers;
    }

    public function getHistoryTeamPlayerFouls(): array
    {
        return $this->historyTeamPlayerFouls;
    }

    public function getHistorySubstitutes(): array
    {
        return $this->historySubstitutes;
    }

    public function getHistoryTeamPlayerPoints(): array
    {
        return $this->historyTeamPlayerPoints;
    }

    // Setters
    public function setTeam(Team $team): void
    {
        $this->team = $team;
    }

    public function setTeamPlayers(array $teamPlayers): void
    {
        $this->teamPlayers = $teamPlayers;
    }

    public function setHistoryTeamPlayerFouls(array $historyTeamPlayerFouls): void
    {
        $this->historyTeamPlayerFouls = $historyTeamPlayerFouls;
    }

    public function setHistorySubstitutes(array $historySubstitutes): void
    {
        $this->historySubstitutes = $historySubstitutes;
    }

    public function setHistoryTeamPlayerPoints(array $historyTeamPlayerPoints): void
    {
        $this->historyTeamPlayerPoints = $historyTeamPlayerPoints;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'team' => $this->getTeam(),
            'teamPlayers' => $this->getTeamPlayers(),
            'historyTeamPlayerFouls' => $this->getHistoryTeamPlayerFouls(),
            'historySubstitutes' => $this->getHistorySubstitutes(),
            'historyTeamPlayerPoints' => $this->getHistoryTeamPlayerPoints()
        ];
    }
}