<?php

class Franchise implements JsonSerializable
{
    private ?int $id;
    private string $name;
    private string $color;
    private string $initials;
    private bool $isActive;

    public function __construct(?int $id, string $name, string $color, string $initials, bool $isActive) {
        $this->id = $id;
        $this->name = $name;
        $this->color = $color;
        $this->initials = $initials;
        $this->isActive = $isActive;
    }

    // Getters
    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getColor(): string {
        return $this->color;
    }

    public function getInitials(): string {
        return $this->initials;
    }

    public function getActivity(): bool
    {
        return $this->isActive;
    }

    // Setters
    public function setId(?int $id): void {
        $this->id = $id;
    }

    public function setName(string $name): void {
        $this->name = $name;
    }

    public function setColor(string $color): void {
        $this->color = $color;
    }

    public function setInitials(string $initials): void {
        $this->initials = $initials;
    }

    public function Compare(Franchise $franchise): bool
    {
        if ($franchise->getName() === $this->name && $franchise->getColor() === $this->color && $franchise->getInitials() === $this->initials && $franchise->getActivity() == $this->isActive) {
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'color' => $this->getColor(),
            'initials' => $this->getInitials()
        ];
    }
}