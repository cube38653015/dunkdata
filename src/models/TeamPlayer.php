<?php

include_once __DIR__ . '/../models/Player.php';

class TeamPlayer implements JsonSerializable
{
    public const TITLE_CAPTAIN = "captain";
    public const TITLE_ALTERNATE_CAPTAIN = "alternate_captain";
    public const TITLE_PLAYER = "player";
    public const POST_SUBSTITUTE = "substitute";

    private int $id;
    private Team $team;
    private Player $player;
    private string $post;
    private string $title;

    public function __construct(int $id, Team $team, Player $player, string $post, string $title)
    {
        $this->id = $id;
        $this->team = $team;
        $this->player = $player;
        $this->post = $post;
        $this->title = $title;
    }

    // Getters
    public function getId(): int {
        return $this->id;
    }

    public function getTeam(): Team {
        return $this->team;
    }

    public function getPlayer(): Player {
        return $this->player;
    }

    public function getPost(): string {
        return $this->post;
    }

    public function getPostTrad(): string {
        return match ($this->post) {
            Player::POST_LEADER => 'Meneur',
            Player::POST_PIVOT => 'Pivot',
            Player::POST_WINGER => 'Ailier',
            Player::POST_STRONG_WINGER => 'Ailier fort',
            Player::POST_BACK => 'Arrière',
            default => '',
        };
    }

    public function getTitle(): string {
        return $this->title;
    }

    // Setters
    public function setId(int $id): void {
        $this->id = $id;
    }

    public function setTeam(Team $team): void {
        $this->team = $team;
    }

    public function setPlayer(Player $player): void {
        $this->player = $player;
    }

    public function setPost(string $post): void {
        $this->post = $post;
    }

    public function setTitle(string $title): void {
        $this->title = $title;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'team' => $this->getTeam(),
            'player' => $this->getPlayer(),
            'post' => $this->getPost(),
            'title' => $this->getTitle()
        ];
    }
}