<?php

class HistoryTeamPlayerFoul implements JsonSerializable
{
    private int $id;
    private TeamPlayer $teamPlayer;
    private FoulType $foulType;
    private string $foulTime;

    public function __construct($historyTeamPlayerFoulId, TeamPlayer $teamPlayer, FoulType $foulType, string $foul_time)
    {
        $this->id = $historyTeamPlayerFoulId;
        $this->teamPlayer = $teamPlayer;
        $this->foulType = $foulType;
        $this->foulTime = $foul_time;
    }

    // Getters
    public function getId(): ?int {
        return $this->id;
    }

    public function getTeamPlayer(): TeamPlayer {
        return $this->teamPlayer;
    }

    public function getFoulType(): FoulType {
        return $this->foulType;
    }

    public function getFoulTime(): ?string {
        return $this->foulTime;
    }

    public function getFoulMinuteTime(): ?string {
        $timeSplited = explode(":", $this->foulTime);
        return $timeSplited[0] ?? null;
    }

    public function getFoulSecondTime(): ?string {
        $timeSplited = explode(":", $this->foulTime);
        return $timeSplited[1] ?? null;
    }

    // Setters
    public function setId(?int $id): void {
        $this->id = $id;
    }

    public function setTeamPlayer(TeamPlayer $teamPlayer): void {
        $this->teamPlayer = $teamPlayer;
    }

    public function setFoulType(FoulType $foulType): void {
        $this->foulType = $foulType;
    }

    public function setFoulTime(?string $foulTime): void {
        $this->foulTime = $foulTime;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'teamPlayer' => $this->getTeamPlayer(),
            'foulType' => $this->getFoulType(),
            'foulTime' => $this->getFoulTime()
        ];
    }
}