<?php

/**
 * @var Arbitrator $holderArbitrator
 * @var Arbitrator $assitantArbitrator1
 * @var Arbitrator $assitantArbitrator2
 * @var array $players
 * @var Player $player
 * @var array $pointTypes
 * @var PointType $pointType
 * @var array $foulTypes
 * @var FoulType $foulType
 * @var array $historyHomeSubstitutes
 * @var HistorySubstitute $historyHomeSubstitute
 * @var array $historyAwaySubstitutes
 * @var HistorySubstitute $historyAwaySubstitute
 * @var DetailedGame $detailedGame
 * @var Game $game
 * @var array $homeTeamPlayers
 * @var TeamPlayer $homeTeamPlayer
 * @var array $awayTeamPlayers
 * @var TeamPlayer $awayTeamPlayer
 * @var array $historyHomeTeamPlayerPoints
 * @var HistoryTeamPlayerPoint $historyHomeTeamPlayerPoint
 * @var array $historyAwayTeamPlayerPoints
 * @var HistoryTeamPlayerPoint $historyAwayTeamPlayerPoint
 * @var array $historyHomeTeamPlayerFouls
 * @var HistoryTeamPlayerFoul $historyHomeTeamPlayerFoul
 * @var array $historyAwayTeamPlayerFouls
 * @var HistoryTeamPlayerFoul $historyAwayTeamPlayerFoul
 */

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Clôture du match</title>
    <link rel="stylesheet" href="/dunkdata/src/views/manager/styles/manager.css">
    <link rel="stylesheet" href="/dunkdata/src/views/manager/styles/gameForm.css">
</head>
<body>
<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/managerNavbar.php'; ?>
</div>
<div class="content">
    <h1>Clôture du match</h1>
    <div>
        <form class="closing-game-content" method="post" action="<?= $_SESSION['managerUpdateGameUri'] ?>">
            <input type="hidden" name="gameId" value="<?= $detailedGame->getGame()->getId() ?>">

            <div class="match-date-place-container">

                <!-- HOURS -->
                <div class="date-container">
                    <span class="date-label">Date et Heure du début</span>
                    <span><?= $detailedGame->getGame()->getBeginDateTime()->format('d/m/Y H:i') ?></span>
                </div>

                <!-- PLACE -->
                <div class="place-container">
                    <span class="place-label">Lieu</span>
                    <span><?= $detailedGame->getGame()->getPlace()->getGym() ?></span>
                    <span><?= $detailedGame->getGame()->getPlace()->getCity() ?></span>
                </div>

                <!-- TIME -->
                <div class="time-container">
                    <span class="time-label">Temps de jeu</span>
                    <span>
                        <input class="time-input min-size" type="number" name="gameMinuteTime" min="0"
                               value="<?= $detailedGame->getGame()->getGameMinuteTime() ?>">
                            :
                        <input class="time-input min-size" type="number" name="gameSecondTime" min="0" max="59"
                               value="<?= $detailedGame->getGame()->getGameSecondTime() ?>">
                </span>
                </div>
            </div>
            <div class="final-score-arbitrators-container">

                <!-- FINAL SCORE -->
                <div class="final-score-container card score-card">
                    <h2 class="card-title ">Score final</h2>
                    <div class="card-content">
                        <div class="card-home-franchise-logo">
                            <img class="franchise-logo"
                                 src="/../dunkdata/src/assets/logo/franchise_logos/<?= $detailedGame->getHomeDetailedTeam()->getTeam()->getFranchise()->getName() ?>.png"
                                 alt="Dunk Data Logo">
                        </div>
                        <div class="card-home-franchise-score">
                            <input type="hidden" name="franchiseHomeId"
                                   value="<?= $detailedGame->getHomeDetailedTeam()->getTeam()->getFranchise()->getId() ?>">
                            <input type="text" name="scoreHome" value="<?= $detailedGame->getHomeDetailedTeam()->getTeam()->getScore() ?>"
                                   required><br>
                        </div>
                        <div class="card-away-franchise-logo">
                            <img class="franchise-logo"
                                 src="/../dunkdata/src/assets/logo/franchise_logos/<?= $detailedGame->getAwayDetailedTeam()->getTeam()->getFranchise()->getName() ?>.png"
                                 alt="Dunk Data Logo">
                        </div>
                        <div class="card-away-franchise-score">
                            <input type="hidden" name="franchiseAwayId"
                                   value="<?= $detailedGame->getAwayDetailedTeam()->getTeam()->getFranchise()->getId() ?>">
                            <input type="text" name="scoreAway" value="<?= $detailedGame->getAwayDetailedTeam()->getTeam()->getScore() ?>"
                                   required><br>
                        </div>
                    </div>
                </div>

                <!-- ARBITRATORS -->
                <div class="arbitrators-container">
                    <span class="arbitrator-title">Arbitre principal</span>
                    <span class="arbitrator-name"><?= $detailedGame->getHolderArbitrator()->getHuman()->getFirstName() . ' ' . $detailedGame->getHolderArbitrator()->getHuman()->getLastName() ?></span>
                    <span class="arbitrator-title">Arbitre assistant 1</span>
                    <span class="arbitrator-name"><?= $detailedGame->getAssistantArbitrator1()->getHuman()->getFirstName() . ' ' . $detailedGame->getAssistantArbitrator1()->getHuman()->getLastName() ?></span>
                    <span class="arbitrator-title">Arbitre assistant 2</span>
                    <span class="arbitrator-name"><?= $detailedGame->getAssistantArbitrator2()->getHuman()->getFirstName() . ' ' . $detailedGame->getAssistantArbitrator2()->getHuman()->getLastName() ?></span>
                </div>
            </div>

            <!-- HOME TEAM -->
            <div class="card detail-card">
                <h2 class="card-title">Equipe Domicile</h2>
                <div class="card-content">

                    <!-- HOME POINTS -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Liste des points marqués</h3>
                        <div class="detail-card-content-part-content">
                            <div class="point-container">
                                <div class="point-title-container">
                                    <div>Temps de jeu (minutes:secondes)</div>
                                    <div>Nom du joueur</div>
                                    <div>Type de points</div>
                                    <div>Action</div>
                                </div>
                                <div class="point-list-container" id="home-points-list">
                                    <?php if (!empty($historyHomeTeamPlayerPoints)): ?>
                                        <?php foreach ($historyHomeTeamPlayerPoints as $historyHomeTeamPlayerPoint): ?>
                                            <section>
                                                <div>
                                                    <input class="time-input min-size" type="number" name="pointMinuteTimes[]"
                                                           value="<?= $historyHomeTeamPlayerPoint->getPointMinuteTime() ?>" required>
                                                    :
                                                    <input class="time-input min-size" type="number" name="pointSecondTimes[]"
                                                           value="<?= $historyHomeTeamPlayerPoint->getPointSecondTime() ?>" required>
                                                </div>
                                                <div class="select-container">
                                                    <select name="teamPlayerPointsScoredIds[]" required>
                                                        <?php foreach ($homeTeamPlayers as $homeTeamPlayer): ?>
                                                            <option value="<?= $homeTeamPlayer->getId() ?>" <?= $historyHomeTeamPlayerPoint->getTeamPlayer()->getId() === $homeTeamPlayer->getId() ? 'selected' : '' ?>>
                                                                <?= $homeTeamPlayer->getPlayer()->getHuman()->getFirstName() . ' ' . $homeTeamPlayer->getPlayer()->getHuman()->getLastName() ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="select-container">
                                                    <select name="pointTypeIds[]" required>
                                                        <?php foreach ($pointTypes as $pointType): ?>
                                                            <option value="<?= $pointType->getId() ?>" <?= $historyHomeTeamPlayerPoint->getPointType()->getId() === $pointType->getId() ? 'selected' : '' ?>>
                                                                <?= $pointType->getName() ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="remove-button" id="remove-line">
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                         stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                              d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                                    </svg>
                                                </div>
                                            </section>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <!-- Here will be the rest of list of points -->
                                </div>
                            </div>
                            <div class="coach-add-substitute-button" id="add-point-home">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                                     class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                    <span>Ajouter un point</span>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <!-- HOME FOULS -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Liste des joueurs sanctionnés</h3>
                        <div class="detail-card-content-part-content">
                            <div class="fouls-container">
                                <div class="fouls-title-container">
                                    <div>Temps de jeu (minutes:secondes)</div>
                                    <div>Nom du joueur</div>
                                    <div>Nom de la sanction</div>
                                    <div>Action</div>
                                </div>
                                <div class="fouls-list-container" id="home-fouls-list">
                                    <?php if (!empty($historyHomeTeamPlayerFouls)): ?>
                                        <?php foreach ($historyHomeTeamPlayerFouls as $historyHomeTeamPlayerFoul): ?>
                                            <section>
                                                <div>
                                                    <input class="time-input min-size" type="number" name="foulMinuteTimes[]" value="<?= $historyHomeTeamPlayerFoul->getFoulMinuteTime() ?>" required>
                                                    :
                                                    <input class="time-input min-size" type="number" name="foulSecondTimes[]" value="<?= $historyHomeTeamPlayerFoul->getFoulSecondTime() ?>" required>
                                                </div>
                                                <div class="select-container">
                                                    <select name="foulTeamPlayerIds[]" required>
                                                        <?php foreach ($homeTeamPlayers as $homeTeamPlayer): ?>
                                                            <option value="<?= $homeTeamPlayer->getId() ?>" <?= $historyHomeTeamPlayerFoul->getTeamPlayer()->getId() === $homeTeamPlayer->getId() ? 'selected' : '' ?>>
                                                                <?= $homeTeamPlayer->getPlayer()->getHuman()->getFirstName() . ' ' . $homeTeamPlayer->getPlayer()->getHuman()->getLastName() ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="select-container">
                                                    <select name="foulTypeIds[]" required>
                                                        <?php foreach ($foulTypes as $foulType): ?>
                                                            <option value="<?= $foulType->getId() ?>" <?=$historyHomeTeamPlayerFoul->getFoulType()->getId() === $foulType->getId() ? 'selected' : '' ?>>
                                                                <?= $foulType->getName() ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="remove-button" id="remove-line">
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                         stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                              d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                                    </svg>
                                                </div>
                                            </section>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <!-- Here will be the list of fouls -->
                                </div>
                            </div>
                            <div class="coach-add-substitute-button" id="add-foul-home">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                                     class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                </svg>
                                <span>Ajouter une sanction</span>
                            </div>
                        </div>
                    </div>

                    <!-- HOME SUBSTITUTES -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Liste des joueurs remplacements</h3>
                        <div class="detail-card-content-part-content">
                            <div class="substitutes-container">
                                <div class="substitutes-title-container">
                                    <div>Temps de jeu (minutes:secondes)</div>
                                    <div>Nom du joueur entrant</div>
                                    <div>Nom du joueur sortant</div>
                                    <div>Action</div>
                                </div>
                                <div class="substitutes-list-container" id="home-substitutes-list">
                                    <?php foreach ($historyHomeSubstitutes as $historyHomeSubstitute): ?>
                                        <section>
                                            <div>
                                                <input class="time-input min-size" type="number" name="substituteMinuteTimes[]"
                                                       value="<?= $historyHomeSubstitute->getSubstituteMinuteTime() ?>"
                                                       required>
                                                :
                                                <input class="time-input min-size" type="number" name="substituteSecondTimes[]"
                                                       value="<?= $historyHomeSubstitute->getSubstituteSecondTime() ?>"
                                                       required>
                                            </div>
                                            <div class="select-container">
                                                <select name="incomingTeamPlayerIds[]" required>
                                                    <?php foreach ($homeTeamPlayers as $homeTeamPlayer): ?>
                                                        <option value="<?= $homeTeamPlayer->getId() ?>"><?= $homeTeamPlayer->getPlayer()->getHuman()->getFirstName() . ' ' . $homeTeamPlayer->getPlayer()->getHuman()->getLastName() ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="select-container">
                                                <select name="outgoingTeamPlayerIds[]" required>
                                                    <?php foreach ($homeTeamPlayers as $homeTeamPlayer): ?>
                                                        <option value="<?= $homeTeamPlayer->getId() ?>"><?= $homeTeamPlayer->getPlayer()->getHuman()->getFirstName() . ' ' . $homeTeamPlayer->getPlayer()->getHuman()->getLastName() ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="remove-button" id="remove-line">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                     stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                          d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                                </svg>
                                            </div>
                                        </section>
                                    <?php endforeach; ?>
                                    <!-- Here will be the list of substitutes -->
                                </div>
                            </div>
                            <div class="coach-add-substitute-button" id="add-substitute-home">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                                     class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                </svg>
                                <span>Ajouter un remplacement</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- AWAY TEAM -->
            <div class="card detail-card">
                <h2 class="card-title">Equipe Extérieure</h2>
                <div class="card-content">

                    <!-- AWAY POINTS -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Liste des points marqués</h3>
                        <div class="detail-card-content-part-content">
                            <div class="point-container">
                                <div class="point-title-container">
                                    <div>Temps de jeu (minutes:secondes)</div>
                                    <div>Nom du joueur</div>
                                    <div>Type de points</div>
                                    <div>Action</div>
                                </div>
                                <div class="point-list-container" id="away-points-list">
                                    <?php if (!empty($historyAwayTeamPlayerPoints)): ?>
                                        <?php foreach ($historyAwayTeamPlayerPoints as $historyAwayTeamPlayerPoint): ?>
                                            <section>
                                                <div>
                                                    <input class="time-input min-size" type="number" name="pointMinuteTimes[]"
                                                           value="<?= $historyAwayTeamPlayerPoint->getPointMinuteTime() ?>" required>
                                                    :
                                                    <input class="time-input min-size" type="number" name="pointSecondTimes[]"
                                                           value="<?= $historyAwayTeamPlayerPoint->getPointSecondTime() ?>" required>
                                                </div>
                                                <div class="select-container">
                                                    <select name="teamPlayerPointsScoredIds[]" required>
                                                        <?php foreach ($awayTeamPlayers as $awayTeamPlayer): ?>
                                                            <option value="<?= $awayTeamPlayer->getId() ?>" <?= $historyAwayTeamPlayerPoint->getTeamPlayer()->getId() === $awayTeamPlayer->getId() ? 'selected' : '' ?>>
                                                                <?= $awayTeamPlayer->getPlayer()->getHuman()->getFirstName() . ' ' . $awayTeamPlayer->getPlayer()->getHuman()->getLastName() ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="select-container">
                                                    <select name="pointTypeIds[]" required>
                                                        <?php foreach ($pointTypes as $pointType): ?>
                                                            <option value="<?= $pointType->getId() ?>" <?= $historyAwayTeamPlayerPoint->getPointType()->getId() === $pointType->getId() ? 'selected' : '' ?>>
                                                                <?= $pointType->getName() ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="remove-button" id="remove-line">
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                         stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                              d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                                    </svg>
                                                </div>
                                            </section>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <!-- Here will be the rest of list of points -->
                                </div>
                            </div>
                            <div class="coach-add-substitute-button" id="add-point-away">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                                     class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                    <span>Ajouter un point</span>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <!-- AWAY FOULS -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Liste des joueurs sanctionnés</h3>
                        <div class="detail-card-content-part-content">
                            <div class="fouls-container">
                                <div class="fouls-title-container">
                                    <div>Temps de jeu (minutes:secondes)</div>
                                    <div>Nom du joueur</div>
                                    <div>Nom de la sanction</div>
                                    <div>Action</div>
                                </div>
                                <div class="fouls-list-container" id="away-fouls-list">
                                    <?php if (!empty($historyAwayTeamPlayerFouls)): ?>
                                        <?php foreach ($historyAwayTeamPlayerFouls as $historyAwayTeamPlayerFoul): ?>
                                            <section>
                                                <div>
                                                    <input class="time-input min-size" type="number" name="foulMinuteTimes[]" value="<?= $historyAwayTeamPlayerFoul->getFoulMinuteTime() ?>" required>
                                                    :
                                                    <input class="time-input min-size" type="number" name="foulSecondTimes[]" value="<?= $historyAwayTeamPlayerFoul->getFoulSecondTime() ?>" required>
                                                </div>
                                                <div class="select-container">
                                                    <select name="foulTeamPlayerIds[]" required>
                                                        <?php foreach ($awayTeamPlayers as $awayTeamPlayer): ?>
                                                            <option value="<?= $awayTeamPlayer->getId() ?>" <?= $historyAwayTeamPlayerFoul->getTeamPlayer()->getId() === $awayTeamPlayer->getId() ? 'selected' : '' ?>>
                                                                <?= $awayTeamPlayer->getPlayer()->getHuman()->getFirstName() . ' ' . $awayTeamPlayer->getPlayer()->getHuman()->getLastName() ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="select-container">
                                                    <select name="foulTypeIds[]" required>
                                                        <?php foreach ($foulTypes as $foulType): ?>
                                                            <option value="<?= $foulType->getId() ?>" <?=$historyAwayTeamPlayerFoul->getFoulType()->getId() === $foulType->getId() ? 'selected' : '' ?>>
                                                                <?= $foulType->getName() ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="remove-button" id="remove-line">
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                         stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                              d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                                    </svg>
                                                </div>
                                            </section>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <!-- Here will be the list of fouls -->
                                </div>
                            </div>
                            <div class="coach-add-substitute-button" id="add-foul-away">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                                     class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                </svg>
                                <span>Ajouter une sanction</span>
                            </div>
                        </div>
                    </div>

                    <!-- AWAY SUBSTITUTES -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Liste des joueurs remplacements</h3>
                        <div class="detail-card-content-part-content">
                            <div class="substitutes-container">
                                <div class="substitutes-title-container">
                                    <div>Temps de jeu (minutes:secondes)</div>
                                    <div>Nom du joueur entrant</div>
                                    <div>Nom du joueur sortant</div>
                                    <div>Action</div>
                                </div>
                                <div class="substitutes-list-container" id="away-substitutes-list">
                                    <?php foreach ($historyAwaySubstitutes as $historyAwaySubstitute): ?>
                                        <section>
                                            <div>
                                                <input class="time-input min-size" type="number" name="substituteMinuteTimes[]"
                                                       value="<?= $historyAwaySubstitute->getSubstituteMinuteTime() ?>"
                                                       required>
                                                :
                                                <input class="time-input min-size" type="number" name="substituteSecondTimes[]"
                                                       value="<?= $historyAwaySubstitute->getSubstituteSecondTime() ?>"
                                                       required>
                                            </div>
                                            <div class="select-container">
                                                <select name="incomingTeamPlayerIds[]" required>
                                                    <?php foreach ($awayTeamPlayers as $awayTeamPlayer): ?>
                                                        <option value="<?= $awayTeamPlayer->getId() ?>"><?= $awayTeamPlayer->getPlayer()->getHuman()->getFirstName() . ' ' . $awayTeamPlayer->getPlayer()->getHuman()->getLastName() ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="select-container">
                                                <select name="outgoingTeamPlayerIds[]" required>
                                                    <?php foreach ($awayTeamPlayers as $awayTeamPlayer): ?>
                                                        <option value="<?= $awayTeamPlayer->getId() ?>"><?= $awayTeamPlayer->getPlayer()->getHuman()->getFirstName() . ' ' . $awayTeamPlayer->getPlayer()->getHuman()->getLastName() ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="remove-button" id="remove-line">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                     stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                          d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                                </svg>
                                            </div>
                                        </section>
                                    <?php endforeach; ?>
                                    <!-- Here will be the list of substitutes -->
                                </div>
                            </div>
                            <div class="coach-add-substitute-button" id="add-substitute-away">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                                     class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                </svg>
                                <span>Ajouter un remplacement</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- FORM BUTTONS -->
            <div class="form-button-container">
                <a class="custom-button blue-night small" href="<?= $_SESSION['managerGamesInProgressUri'] ?>">Annuler</a>
                <button class="custom-button orange small" type="submit">Sauvegarder</button>
            </div>
        </form>
    </div>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            // Fonction pour attacher l'événement de suppression
            function attachRemoveEvent(button) {
                button.addEventListener('click', function () {
                    var entry = this.closest('section');
                    entry.parentNode.removeChild(entry);
                });
            }

            // Ajout des événements de suppression aux boutons existants
            document.querySelectorAll('#remove-line').forEach(function (button) {
                attachRemoveEvent(button);
            });

            // Fonction pour ajouter une nouvelle ligne de points pour l'équipe à domicile
            function addHomePointEntry() {
                var table = document.querySelector('#home-points-list');
                var newRow = document.createElement('section');
                newRow.innerHTML = `
                    <div>
                        <input class="time-input min-size" type="number" name="pointMinuteTimes[]" value="" required>
                        :
                        <input class="time-input min-size" type="number" name="pointSecondTimes[]" value="" required>
                    </div>
                    <div class="select-container">
                        <select name="teamPlayerPointsScoredIds[]" required>
                            <?php foreach ($homeTeamPlayers as $homeTeamPlayer): ?>
                                <option value="<?= $homeTeamPlayer->getId() ?>"><?= $homeTeamPlayer->getPlayer()->getHuman()->getFirstName() . ' ' . $homeTeamPlayer->getPlayer()->getHuman()->getLastName() ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="select-container">
                        <select name="pointTypeIds[]" required>
                            <?php foreach ($pointTypes as $pointType): ?>
                                <option value="<?= $pointType->getId() ?>"><?= $pointType->getName() ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="remove-button" id="remove-line">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                        </svg>
                    </div>
            `;
                table.appendChild(newRow);
                attachRemoveEvent(newRow.querySelector('#remove-line'));
            }

            // Fonction pour ajouter une nouvelle ligne de points pour l'équipe extérieure
            function addAwayPointEntry() {
                var table = document.querySelector('#away-points-list');
                var newRow = document.createElement('section');
                newRow.innerHTML = `
                <div>
                     <input class="time-input min-size" type="number" name="pointMinuteTimes[]" value="" required>
                     :
                     <input class="time-input min-size" type="number" name="pointSecondTimes[]" value="" required>
                </div>
                <div class="select-container">
                    <select name="teamPlayerPointsScoredIds[]" required>
                        <?php foreach ($awayTeamPlayers as $awayTeamPlayer): ?>
                        <option value="<?= $awayTeamPlayer->getId() ?>"><?= $awayTeamPlayer->getPlayer()->getHuman()->getFirstName() . ' ' . $awayTeamPlayer->getPlayer()->getHuman()->getLastName() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="select-container">
                    <select name="pointTypeIds[]" required>
                        <?php foreach ($pointTypes as $pointType): ?>
                        <option value="<?= $pointType->getId() ?>"><?= $pointType->getName() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="remove-button" id="remove-line">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                    </svg>
                </div>
            `;
                table.appendChild(newRow);
                attachRemoveEvent(newRow.querySelector('#remove-line'));
            }

            // Fonction pour ajouter une nouvelle ligne de sanctions pour l'équipe à domicile
            function addHomeFoulEntry() {
                var table = document.querySelector('#home-fouls-list');
                var newRow = document.createElement('section');
                newRow.innerHTML = `
                <div>
                     <input class="time-input min-size" type="number" name="foulMinuteTimes[]" value="" required>
                     :
                     <input class="time-input min-size" type="number" name="foulSecondTimes[]" value="" required>
                </div>
                <div class="select-container">
                    <select name="foulTeamPlayerIds[]" required>
                        <?php foreach ($homeTeamPlayers as $homeTeamPlayer): ?>
                        <option value="<?= $homeTeamPlayer->getId() ?>"><?= $homeTeamPlayer->getPlayer()->getHuman()->getFirstName() . ' ' . $homeTeamPlayer->getPlayer()->getHuman()->getLastName() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="select-container">
                    <select name="foulTypeIds[]" required>
                        <?php foreach ($foulTypes as $foulType): ?>
                        <option value="<?= $foulType->getId() ?>"><?= $foulType->getName() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="remove-button" id="remove-line">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                    </svg>
                </div>
            `;
                table.appendChild(newRow);
                attachRemoveEvent(newRow.querySelector('#remove-line'));
            }

            // Fonction pour ajouter une nouvelle ligne de sanctions pour l'équipe extérieure
            function addAwayFoulEntry() {
                var table = document.querySelector('#away-fouls-list');
                var newRow = document.createElement('section');
                newRow.innerHTML = `
                <div>
                     <input class="time-input min-size" type="number" name="foulMinuteTimes[]" value="" required>
                     :
                     <input class="time-input min-size" type="number" name="foulSecondTimes[]" value="" required>
                </div>
                <div class="select-container">
                    <select name="foulTeamPlayerIds[]" required>
                        <?php foreach ($awayTeamPlayers as $awayTeamPlayer): ?>
                        <option value="<?= $awayTeamPlayer->getId() ?>"><?= $awayTeamPlayer->getPlayer()->getHuman()->getFirstName() . ' ' . $awayTeamPlayer->getPlayer()->getHuman()->getLastName() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="select-container">
                    <select name="foulTypeIds[]" required>
                        <?php foreach ($foulTypes as $foulType): ?>
                        <option value="<?= $foulType->getId() ?>"><?= $foulType->getName() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="remove-button" id="remove-line">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                    </svg>
                </div>
            `;
                table.appendChild(newRow);
                attachRemoveEvent(newRow.querySelector('#remove-line'));
            }

            // Fonction pour ajouter une nouvelle ligne de remplacement pour l'équipe à domicile
            function addHomeSubstituteEntry() {
                var table = document.querySelector('#home-substitutes-list');
                var newRow = document.createElement('section');
                newRow.innerHTML = `
                <div>
                     <input class="time-input min-size" type="number" name="substituteMinuteTimes[]" value="" required>
                     :
                     <input class="time-input min-size" type="number" name="substituteSecondTimes[]" value="" required>
                </div>
                <div class="select-container">
                    <select name="incomingTeamPlayerIds[]" required>
                        <?php foreach ($homeTeamPlayers as $homeTeamPlayer): ?>
                        <option value="<?= $homeTeamPlayer->getId() ?>"><?= $homeTeamPlayer->getPlayer()->getHuman()->getFirstName() . ' ' . $homeTeamPlayer->getPlayer()->getHuman()->getLastName() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="select-container">
                    <select name="outgoingTeamPlayerIds[]" required>
                        <?php foreach ($homeTeamPlayers as $homeTeamPlayer): ?>
                        <option value="<?= $homeTeamPlayer->getId() ?>"><?= $homeTeamPlayer->getPlayer()->getHuman()->getFirstName() . ' ' . $homeTeamPlayer->getPlayer()->getHuman()->getLastName() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="remove-button" id="remove-line">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                    </svg>
                </div>
            `;
                table.appendChild(newRow);
                attachRemoveEvent(newRow.querySelector('#remove-line'));
            }

            // Fonction pour ajouter une nouvelle ligne de remplacement pour l'équipe extérieure
            function addAwaySubstituteEntry() {
                var table = document.querySelector('#away-substitutes-list');
                var newRow = document.createElement('section');
                newRow.innerHTML = `
                <div>
                     <input class="time-input min-size" type="number" name="substituteMinuteTimes[]" value="" required>
                     :
                     <input class="time-input min-size" type="number" name="substituteSecondTimes[]" value="" required>
                </div>
                <div class="select-container">
                    <select name="incomingTeamPlayerIds[]" required>
                        <?php foreach ($awayTeamPlayers as $awayTeamPlayer): ?>
                        <option value="<?= $awayTeamPlayer->getId() ?>"><?= $awayTeamPlayer->getPlayer()->getHuman()->getFirstName() . ' ' . $awayTeamPlayer->getPlayer()->getHuman()->getLastName() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="select-container">
                    <select name="outgoingTeamPlayerIds[]" required>
                        <?php foreach ($awayTeamPlayers as $awayTeamPlayer): ?>
                        <option value="<?= $awayTeamPlayer->getId() ?>"><?= $awayTeamPlayer->getPlayer()->getHuman()->getFirstName() . ' ' . $awayTeamPlayer->getPlayer()->getHuman()->getLastName() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="remove-button" id="remove-line">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                    </svg>
                </div>
            `;
                table.appendChild(newRow);
                attachRemoveEvent(newRow.querySelector('#remove-line'));
            }

            // Attacher les événements de clic pour ajouter des nouvelles entrées
            document.getElementById('add-point-home').addEventListener('click', addHomePointEntry);
            document.getElementById('add-point-away').addEventListener('click', addAwayPointEntry);
            document.getElementById('add-foul-home').addEventListener('click', addHomeFoulEntry);
            document.getElementById('add-foul-away').addEventListener('click', addAwayFoulEntry);
            document.getElementById('add-substitute-home').addEventListener('click', addHomeSubstituteEntry);
            document.getElementById('add-substitute-away').addEventListener('click', addAwaySubstituteEntry);
        });
    </script>
</div>
</body>
</html>
