<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Home Manager</title>
    <link rel="stylesheet" href="/dunkdata/src/views/manager/styles/manager.css">
</head>
<body>
<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/managerNavbar.php'; ?>
</div>
<div class="content">
    <div class="main-menu">
        <a class="custom-button orange medium" href="<?=$_SESSION['managerShowNewGameFormUri']?>">Créer un match</a>
        <a class="custom-button orange medium" href="<?=$_SESSION['managerGamesInProgressUri']?>">Matchs en cours</a>
        <a class="custom-button orange medium" href="<?=$_SESSION['managerHistoryGamesUri']?>">Historique des matchs</a>
    </div>
</div>
</body>
</html>
