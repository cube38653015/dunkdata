<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="/dunkdata/src/views/home/styles/home.css">
</head>
<body>
<div class="content">
    <div class="vertical-background flex-fill">
        <div class="radial-background flex-fill"></div>
        <div class="part-container">
            <span class="punchline">Où chaque point compte...</span>
            <div class="left-part">
                <span class="welcome-title">Bienvenue sur</span>
                <span class="app-title">Dunk Data</span>
            </div>
            <img class="logo" src="/../dunkdata/src/assets/logo/dunkdata-NoBG.png" alt="Dunk Data Logo">
            <div class="home-buttons">
                <a class="custom-button blue-night medium" href="<?= $_SESSION['loginUri'] ?>">
                    <span>
                        Connexion
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>
</body>
</html>
