<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Création d'un nouvel utilisateur</title>
</head>
<body>
<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/adminNavbar.php'; ?>
</div>
<div class="content">
    <h1>Création d'un nouvel utilisateur</h1>
    <div>
        <form method="post" action="index.php">
            <input type="hidden" name="action" value="create">
            <label>Login:</label>
            <input type="text" name="login" required><br>
            <label>Password:</label>
            <input type="text" name="password" required><br>
            <label>Email:</label>
            <input type="text" name="email" required><br>
            <label>Role:</label>
            <input type="text" name="role" required><br>
            <button type="submit" name="createUser">Valider</button>
        </form>
    </div>
</div>
</body>
</html>
