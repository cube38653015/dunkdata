<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login - Dunk Data</title>
    <link rel="stylesheet" href="/dunkdata/src/views/login/login.css">
</head>
<body>
<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/navbar.php'; ?>
</div>
<div class="content">
    <h2>LOGIN</h2>
    <div class="login-content">
        <p>Rentrez votre login et mot de passe pour vous identifier</p>
        <form id="login-form" action="<?= $_SESSION['checkAccessUri'] ?>" method="post">
            <input type="hidden" name="action" value="check">
            <div class="form-group">
                <input type="text" name="login" placeholder="Login" required>
            </div>
            <div class="form-group">
                <input type="password" id="password-input" name="password" placeholder="Password" required>
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="password-toggle" width="24" height="24">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M3.98 8.223A10.477 10.477 0 0 0 1.934 12C3.226 16.338 7.244 19.5 12 19.5c.993 0 1.953-.138 2.863-.395M6.228 6.228A10.451 10.451 0 0 1 12 4.5c4.756 0 8.773 3.162 10.065 7.498a10.522 10.522 0 0 1-4.293 5.774M6.228 6.228 3 3m3.228 3.228 3.65 3.65m7.894 7.894L21 21m-3.228-3.228-3.65-3.65m0 0a3 3 0 1 0-4.243-4.243m4.242 4.242L9.88 9.88" />
                </svg>
            </div>
            <button class="custom-button orange small" type="submit">Login</button>
        </form>
        <p id="error-message" class="error-message">Identifiants incorrects. Veuillez réessayer.</p>
    </div>
</div>
<script>
    document.querySelector('.password-toggle').addEventListener('click', function () {
        const passwordInput = document.getElementById('password-input');
        if (passwordInput.type === 'password') {
            passwordInput.type = 'text';
        } else {
            passwordInput.type = 'password';
        }
    });
</script>
</body>
</html>