document.addEventListener('DOMContentLoaded', function() {
    const togglePassword = document.querySelector('.password-toggle');
    const passwordField = document.querySelector('input[type="password"]');
    const loginForm = document.getElementById('login-form');
    const errorMessage = document.getElementById('error-message');

    togglePassword.addEventListener('click', function() {
        const type = passwordField.getAttribute('type') === 'password' ? 'text' : 'password';
        passwordField.setAttribute('type', type);
        this.textContent = type === 'password' ? '👁️' : '🏀';
    });

    // loginForm.addEventListener('submit', function(event) {
    //     event.preventDefault();
    //
    //     const login = loginForm.login.value;
    //     const password = loginForm.password.value;
    //
    //
    //     if (login === "user@example.com" && password === "password") {
    //         alert('Connexion réussie !');
    //     } else {
    //         errorMessage.style.display = 'block';
    //     }
    // });
});
