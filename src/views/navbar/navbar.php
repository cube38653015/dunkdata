<div class="nav-menu">
    <a class="nav-menu-item" href="<?= $_SESSION['baseUri'] ?>">
        <img class="logo" src="/../dunkdata/src/assets/logo/dunkdata-NoBG.png" alt="Dunk Data Logo">
    </a>
    <a class="nav-menu-item" href="<?= $_SESSION['baseUri'] ?>">
        <span>
            Accueil
        </span>
    </a>
</div>
<div class="nav-menu">
    <?php if (!isset($_SESSION['role'])): ?>
        <a class="nav-menu-item" href="<?= $_SESSION['loginUri'] ?>">
            <span class="nav-menu-item-text">Login</span>
            <span class="nav-menu-item-icon">
                <svg class="w-6 h-6 text-gray-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                     fill="none"
                     viewBox="0 0 24 24">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                          d="M16 12H4m12 0-4 4m4-4-4-4m3-4h2a3 3 0 0 1 3 3v10a3 3 0 0 1-3 3h-2"/>
                </svg>
            </span>
        </a>
    <?php else: ?>
        <a class="nav-menu-item" href="<?= $_SESSION['logoutUri'] ?>" >
            <span class="nav-menu-item-text">Logout</span>
            <span class="nav-menu-item-icon">
                <svg class="w-6 h-6 text-gray-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                     fill="none" viewBox="0 0 24 24">
                  <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M20 12H8m12 0-4 4m4-4-4-4M9 4H7a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h2"/>
                </svg>
            </span>
        </a>
    <?php endif; ?>
</div>
