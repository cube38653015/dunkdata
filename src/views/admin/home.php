<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Accueil</title>
</head>
<body>
<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/adminNavbar.php'; ?>
</div>
<div class="content">
    <div class="main-menu">
        <a class="custom-button orange medium"  href="<?= $_SESSION['adminHomeArbitratorsUri'] ?>">Gestion des arbitres</a></button>
        <a class="custom-button orange medium"  href="<?= $_SESSION['adminHomeGamesUri'] ?>">Gestion des matchs</a></button>
        <a class="custom-button orange medium"  href="<?= $_SESSION['adminHomeUsersUri'] ?>">Gestion des utilisateurs</a></button>
        <a class="custom-button orange medium"  href="<?= $_SESSION['adminHomeFranchisesUri'] ?>">Gestion des franchises</a></button>
    </div>
</div>
</body>
</html>
