<?php
/**
 * @var array $arbitrators
 * @var Arbitrator $arbitrator
 * @var array $places
 * @var Place $place
 * @var array $franchises
 * @var Franchise $franchise
 */
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Create Game</title>
    <link rel="stylesheet" href="/dunkdata/src/views/admin/styles/admin.css">
</head>
<body>
<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/adminNavbar.php'; ?>
</div>
<div class="content">
    <h1>Créer un nouveau match</h1>
    <div>
        <form class="new-match-form" method="post" action="<?= $_SESSION['adminCreateGameUri'] ?>">

            <input type="hidden" name="action" value="create">

            <div class="form-input start-date role-container">
                <h4>Date du match</h4>
                <input type="datetime-local" name="beginDateTime" required>
            </div>

            <div class="form-input place role-container">
                <h4>Lieu</h4>
                <div class="select-container">
                    <select name="placeId" required>
                        <?php foreach ($places as $place): ?>
                            <option value="<?= $place->getId() ?>">
                                <?= $place->getGym() ?>
                            </option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>

            <div class="teams-container card vs-card">
                <h2 class="card-title">Equipes</h2>
                <div class="card-content">
                    <div class="form-input">
                        <label>Equipe locale</label>
                        <div class="select-container">
                            <select name="homeFranchiseId" required>
                                <?php foreach ($franchises as $franchise): ?>
                                    <option value="<?= $franchise->getId() ?>">
                                        <?= $franchise->getName() ?>
                                    </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-input">
                        <label>Equipe extérieure</label>
                        <div class="select-container">
                            <select name="awayFranchiseId" required>
                                <?php foreach ($franchises as $franchise): ?>
                                    <option value="<?= $franchise->getId() ?>">
                                        <?= $franchise->getName() ?>
                                    </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="arbitrators-container card vs-card">
                <h2 class="card-title">Arbitres</h2>
                <div class="card-content">
                    <div class="form-input role-container">
                        <h4>Arbitre principal</h4>
                        <div class="select-container">
                            <select name="arbitratorHolderId" required>
                                <?php foreach ($arbitrators as $arbitrator): ?>
                                    <option value="<?= $arbitrator->getId() ?>">
                                        <?= $arbitrator->getHuman()->getFirstName() . ' ' . $arbitrator->getHuman()->getLastName(); ?>
                                    </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="arbitrator-assistants-container">
                        <div class="form-input role-container">
                            <h4>Arbitre assistant 1</h4>
                            <div class="select-container">
                                <select name="arbitratorAssistant1Id" required>
                                    <?php foreach ($arbitrators as $arbitrator): ?>
                                        <option value="<?= $arbitrator->getId() ?>">
                                            <?= $arbitrator->getHuman()->getFirstName() . ' ' . $arbitrator->getHuman()->getLastName(); ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-input">
                            <h4>Arbitre assistant 2</h4>
                            <div class="select-container">
                                <select name="arbitratorAssistant2Id" required>
                                    <?php foreach ($arbitrators as $arbitrator): ?>
                                        <option value="<?= $arbitrator->getId() ?>">
                                            <?= $arbitrator->getHuman()->getFirstName() . ' ' . $arbitrator->getHuman()->getLastName(); ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-button-container">
                <a class="custom-button blue-night small" href="<?= $_SESSION['adminHomeGamesUri'] ?>">Annuler</a>
                <button class="custom-button orange small" type="submit" name="create">Create</button>
            </div>
        </form>
    </div>
</div>
</body>
</html>
