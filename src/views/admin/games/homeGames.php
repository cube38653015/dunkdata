<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home Matchs</title>
    <link rel="stylesheet" href="">
</head>
<body>
<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/adminNavbar.php'; ?>
</div>
<div class="content">
    <h1>Liste des matchs</h1>
    <div class="main-menu">
        <a class="custom-button orange medium"  href="<?=$_SESSION['adminShowNewGameFormUri']?>">Créer un match</a></button>
        <a class="custom-button orange medium"  href="<?=$_SESSION['adminGamesInProgressUri']?>">Matchs en cours</a></button>
        <a class="custom-button orange medium"  href="<?=$_SESSION['adminHistoryGamesUri']?>">Historique des matchs</a></button>
    </div>
</div>
</body>
</html>
