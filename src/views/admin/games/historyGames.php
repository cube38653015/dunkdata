<?php

/**
 * @var array $detailedGames
 * @var DetailedGame $detailedGame
 */

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Historique des matchs</title>
    <link rel="stylesheet" href="/dunkdata/src/views/admin/styles/admin.css">
</head>
<body>
<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/adminNavbar.php'; ?>
</div>
<div class="content">
    <h1>Historique des matchs</h1>
    <?php if (empty($detailedGames)): ?>
        <div class="no-data-container">
                <span class="no-data-icon">
                                    <svg class="w-6 h-6 text-gray-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                                         width="24" height="24"
                                         fill="none" viewBox="0 0 24 24">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                          d="m15 9-6 6m0-6 6 6m6-3a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                </svg>
                </span>
            <span class="no-data-message">Il n'y a pas de matchs terminés</span>
        </div>
    <?php endif; ?>
    <?php if (!empty($detailedGames)): ?>
        <div class="card-grid">
            <?php foreach ($detailedGames as $detailedGame): ?>
                <div class="card score-card clickable">
                    <div class="card-date-container">
                        <span><?= $detailedGame->getGame()->getBeginDateTime()->format('d-m-Y') ?></span>
                        <span><?= $detailedGame->getGame()->getBeginDateTime()->format('H:i') ?></span>
                    </div>
                    <div class="card-location-container">
                        <span class="card-location-gym"><?= $detailedGame->getGame()->getPlace()->getGym() ?></span>
                        <span class="card-location-address"><?= $detailedGame->getGame()->getPlace()->getAddress() ?></span>
                        <span class="card-location-city"><?= $detailedGame->getGame()->getPlace()->getCity() ?></span>
                    </div>
                    <div class="card-content">
                        <div class="card-home-franchise-logo">
                            <img class="franchise-logo"
                                 src="/../dunkdata/src/assets/logo/franchise_logos/<?= $detailedGame->getHomeDetailedTeam()->getTeam()->getFranchise()->getName() ?>.png"
                                 alt="Dunk Data Logo">
                        </div>
                        <div class="card-home-franchise-score">
                            <input disabled type="hidden" name="franchiseHomeId"
                                   value="<?= $detailedGame->getHomeDetailedTeam()->getTeam()->getFranchise()->getId() ?>">
                            <input disabled type="text" name="scoreHome" value="<?= $detailedGame->getHomeDetailedTeam()->getTeam()->getScore() ?>"
                                   required><br>
                        </div>
                        <div class="card-away-franchise-logo">
                            <img class="franchise-logo"
                                 src="/../dunkdata/src/assets/logo/franchise_logos/<?= $detailedGame->getAwayDetailedTeam()->getTeam()->getFranchise()->getName() ?>.png"
                                 alt="Dunk Data Logo">
                        </div>
                        <div class="card-away-franchise-score">
                            <input disabled type="hidden" name="franchiseAwayId"
                                   value="<?= $detailedGame->getAwayDetailedTeam()->getTeam()->getFranchise()->getId() ?>">
                            <input disabled type="text" name="scoreAway" value="<?= $detailedGame->getAwayDetailedTeam()->getTeam()->getScore() ?>"
                                   required><br>
                        </div>
                    </div>
                    <div class="card-actions three-actions">
                        <form method="post" action="<?= $_SESSION['adminShowUpdateHistoryGameFormUri'] ?>" style="display:inline;">
                            <input type="hidden" name="action" value="edit">
                            <input type="hidden" name="gameId" value="<?= $detailedGame->getGame()->getId() ?>">
                            <button class="main-button card-action-button one-of-three-action" type="submit" name="edit">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" width="24" height="24">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L6.832 19.82a4.5 4.5 0 0 1-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 0 1 1.13-1.897L16.863 4.487Zm0 0L19.5 7.125" />
                                </svg>

                            </button>
                        </form>
                        <form method="post" action="<?= $_SESSION['adminDownloadGameReportUri'] ?>" style="display:inline;">
                            <input type="hidden" name="gameId" value="<?= $detailedGame->getGame()->getId() ?>">
                            <button class="secondary-button card-action-button two-of-three-action" type="submit" name="download">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" width="24" height="24">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 14.25v-2.625a3.375 3.375 0 0 0-3.375-3.375h-1.5A1.125 1.125 0 0 1 13.5 7.125v-1.5a3.375 3.375 0 0 0-3.375-3.375H8.25m.75 12 3 3m0 0 3-3m-3 3v-6m-1.5-9H5.625c-.621 0-1.125.504-1.125 1.125v17.25c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125V11.25a9 9 0 0 0-9-9Z" />
                                </svg>
                            </button>
                        </form>
                        <form method="post" action="<?= $_SESSION['adminDeleteGameClosedUri'] ?>" style="display:inline;">
                            <input type="hidden" name="action" value="delete">
                            <input type="hidden" name="gameId" value="<?= $detailedGame->getGame()->getId() ?>">
                            <button class="danger-button card-action-button three-of-three-action" type="submit" name="download">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                                     width="24" height="24">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0"/>
                                </svg>
                            </button>
                        </form>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>
</body>
</html>
