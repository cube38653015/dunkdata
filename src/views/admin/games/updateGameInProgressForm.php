<?php
/**
 * @var array $arbitrators
 * @var Arbitrator $arbitrator
 * @var array $places
 * @var Place $place
 * @var array $franchises
 * @var Franchise $franchise
 * @var DetailedGame $detailedGame
 * @var array $gameArbitrators
 * @var array $homePlayers
 * @var array $awayPlayers
 * @var array $detailedHomeTeamPlayers
 * @var array $detailedAwayTeamPlayers
 */
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Modification d'un match en cours</title>
    <link rel="stylesheet" href="/dunkdata/src/views/admin/styles/admin.css">
    <link rel="stylesheet" href="/dunkdata/src/views/manager/styles/gameForm.css">
</head>
<body>
<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/adminNavbar.php'; ?>
</div>
<div class="content">
    <h1>Modification d'un match en cours</h1>
    <div>
        <form class="update-match-form" method="post" action="<?= $_SESSION['adminUpdateGameInProgressUri'] ?>">
            <input type="hidden" name="gameId" value="<?= $detailedGame->getGame()->getId() ?>">
            <input type="hidden" name="homeTeamId" value="<?= $detailedGame->getHomeDetailedTeam()->getTeam()->getId() ?>">
            <input type="hidden" name="awayTeamId" value="<?= $detailedGame->getAwayDetailedTeam()->getTeam()->getId() ?>">
            <input type="hidden" name="action" value="create">

            <!-- MATCH PLACE AND DATE-->
            <div class="match-date-place-container">
                <div class="form-input start-date role-container">
                    <h4>Date du match</h4>
                    <input type="datetime-local" name="beginDateTime" value="<?=$detailedGame->getGame()->getBeginDateTime()->format('Y-m-d H:i:s')?>" required>
                </div>

                <div class="form-input place role-container">
                    <h4>Lieu</h4>
                    <div class="select-container">
                        <select name="placeId" required>
                            <?php foreach ($places as $place): ?>
                                <option <?= $place->getId() === $detailedGame->getGame()->getPlace()->getId() ? "selected" : "" ?>
                                        value="<?= $place->getId() ?>"><?= $place->getGym() ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
            </div>

            <!-- HOME TEAM CARD-->
            <div class="card detail-card">
                <div class="card-title">
                    <h2><?= $detailedGame->getHomeDetailedTeam()->getTeam()->getFranchise()->getName(); ?></h2>
                </div>
                <div class="card-content">

                    <!-- PLAYERS -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Titulaires</h3>
                        <div class="detail-card-content-part-content coach-team-players-container">

                            <div class="role-container">
                                <h4>Pivot </h4>
                                <div class="select-container">
                                    <select name="pivotHome" required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($homePlayers as $homePlayer): ?>
                                            <option <?= array_key_exists(Player::POST_PIVOT, $detailedHomeTeamPlayers) && $detailedHomeTeamPlayers[Player::POST_PIVOT]->getPlayer()->getId() === $homePlayer->getId() ? "selected" : "" ?>
                                                    value="<?= $homePlayer->getId() ?>">
                                                <?php echo $homePlayer->getHuman()->getFirstName() . ' ' . $homePlayer->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>


                            <div class="role-container">
                                <h4>Meneur </h4>
                                <div class="select-container">
                                    <select name="leaderHome" required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($homePlayers as $homePlayer): ?>
                                            <option <?= array_key_exists(Player::POST_LEADER, $detailedHomeTeamPlayers) && $detailedHomeTeamPlayers[Player::POST_LEADER]->getPlayer()->getId() === $homePlayer->getId() ? "selected" : "" ?>
                                                    value="<?= $homePlayer->getId() ?>">
                                                <?php echo $homePlayer->getHuman()->getFirstName() . ' ' . $homePlayer->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="role-container">
                                <h4>Ailier </h4>
                                <div class="select-container">
                                    <select name="wingerHome" required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($homePlayers as $homePlayer): ?>
                                            <option <?= array_key_exists(Player::POST_WINGER, $detailedHomeTeamPlayers) && $detailedHomeTeamPlayers[Player::POST_WINGER]->getPlayer()->getId() === $homePlayer->getId() ? "selected" : "" ?>
                                                    value="<?= $homePlayer->getId() ?>">
                                                <?php echo $homePlayer->getHuman()->getFirstName() . ' ' . $homePlayer->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="role-container">
                                <h4>Ailier fort </h4>
                                <div class="select-container">
                                    <select name="strongWingerHome" required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($homePlayers as $homePlayer): ?>
                                            <option <?= array_key_exists(Player::POST_STRONG_WINGER, $detailedHomeTeamPlayers) && $detailedHomeTeamPlayers[Player::POST_STRONG_WINGER]->getPlayer()->getId() === $homePlayer->getId() ? "selected" : "" ?>
                                                    value="<?= $homePlayer->getId() ?>">
                                                <?php echo $homePlayer->getHuman()->getFirstName() . ' ' . $homePlayer->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="role-container">
                                <h4>Arrière </h4>
                                <div class="select-container">
                                    <select name="backHome" required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($homePlayers as $homePlayer): ?>
                                            <option <?= array_key_exists(Player::POST_BACK, $detailedHomeTeamPlayers) && $detailedHomeTeamPlayers[Player::POST_BACK]->getPlayer()->getId() === $homePlayer->getId() ? "selected" : "" ?>
                                                    value="<?= $homePlayer->getId() ?>">
                                                <?php echo $homePlayer->getHuman()->getFirstName() . ' ' . $homePlayer->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- CAPTAINS -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Capitaines</h3>
                        <div class="detail-card-content-part-content coach-team-captains-container">

                            <div class="role-container">
                                <h4>Capitaine </h4>
                                <div class="select-container">
                                    <select id="captainHome" name="captainHome" required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($homePlayers as $homePlayer): ?>
                                            <option <?= array_key_exists(TeamPlayer::TITLE_CAPTAIN, $detailedHomeTeamPlayers) && $detailedHomeTeamPlayers[TeamPlayer::TITLE_CAPTAIN]->getPlayer()->getId() === $homePlayer->getId() ? "selected" : "" ?>
                                                    value="<?= $homePlayer->getId() ?>">
                                                <?php echo $homePlayer->getHuman()->getFirstName() . ' ' . $homePlayer->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="role-container">
                                <h4>Capitaine Suppléant </h4>
                                <div class="select-container">
                                    <select id="alternateCaptainHome" name="alternateCaptainHome"
                                            required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($homePlayers as $homePlayer): ?>
                                            <option <?= array_key_exists(TeamPlayer::TITLE_ALTERNATE_CAPTAIN, $detailedHomeTeamPlayers) && $detailedHomeTeamPlayers[TeamPlayer::TITLE_ALTERNATE_CAPTAIN]->getPlayer()->getId() === $homePlayer->getId() ? "selected" : "" ?>
                                                    value="<?= $homePlayer->getId() ?>">
                                                <?php echo $homePlayer->getHuman()->getFirstName() . ' ' . $homePlayer->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- SUBSTITUTES -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Remplaçants</h3>
                        <div class="detail-card-content-part-content">
                            <div class="coach-team-substitutes-container" id="substitutes-container-home">
                                <?php foreach ($detailedHomeTeamPlayers[TeamPlayer::POST_SUBSTITUTE] as $playerId => $substitute): ?>
                                    <div class="substitute-entry-home">
                                        <div class="select-container">
                                            <select name="homeSubstitutes[]" required>
                                                <option value="" disabled selected>Sélectionner un joueur</option>
                                                <?php foreach ($homePlayers as $homePlayer): ?>
                                                    <option <?= $playerId === $homePlayer->getId() ? "selected" : "" ?>
                                                            value="<?= $homePlayer->getId() ?>">
                                                        <?php echo $homePlayer->getHuman()->getFirstName() . ' ' . $homePlayer->getHuman()->getLastName(); ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="remove-button remove-substitute-home">
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                 stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                      d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                            </svg>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="coach-add-substitute-button" id="add-substitute-home">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                                     class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                </svg>
                                <span>Ajouter un remplacement</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- AWAY TEAM CARD-->
            <div class="card detail-card">
                <div class="card-title">
                    <h2><?= $detailedGame->getAwayDetailedTeam()->getTeam()->getFranchise()->getName(); ?></h2>
                </div>
                <div class="card-content">

                    <!-- PLAYERS -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Titulaires</h3>
                        <div class="detail-card-content-part-content coach-team-players-container">

                            <div class="role-container">
                                <h4>Pivot </h4>
                                <div class="select-container">
                                    <select name="pivotAway" required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($awayPlayers as $awayPlayer): ?>
                                            <option <?= array_key_exists(Player::POST_PIVOT, $detailedAwayTeamPlayers) && $detailedAwayTeamPlayers[Player::POST_PIVOT]->getPlayer()->getId() === $awayPlayer->getId() ? "selected" : "" ?>
                                                    value="<?= $awayPlayer->getId() ?>">
                                                <?php echo $awayPlayer->getHuman()->getFirstName() . ' ' . $awayPlayer->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>


                            <div class="role-container">
                                <h4>Meneur </h4>
                                <div class="select-container">
                                    <select name="leaderAway" required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($awayPlayers as $awayPlayer): ?>
                                            <option <?= array_key_exists(Player::POST_LEADER, $detailedAwayTeamPlayers) && $detailedAwayTeamPlayers[Player::POST_LEADER]->getPlayer()->getId() === $awayPlayer->getId() ? "selected" : "" ?>
                                                    value="<?= $awayPlayer->getId() ?>">
                                                <?php echo $awayPlayer->getHuman()->getFirstName() . ' ' . $awayPlayer->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="role-container">
                                <h4>Ailier </h4>
                                <div class="select-container">
                                    <select name="wingerAway" required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($awayPlayers as $awayPlayer): ?>
                                            <option <?= array_key_exists(Player::POST_WINGER, $detailedAwayTeamPlayers) && $detailedAwayTeamPlayers[Player::POST_WINGER]->getPlayer()->getId() === $awayPlayer->getId() ? "selected" : "" ?>
                                                    value="<?= $awayPlayer->getId() ?>">
                                                <?php echo $awayPlayer->getHuman()->getFirstName() . ' ' . $awayPlayer->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="role-container">
                                <h4>Ailier fort </h4>
                                <div class="select-container">
                                    <select name="strongWingerAway" required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($awayPlayers as $awayPlayer): ?>
                                            <option <?= array_key_exists(Player::POST_STRONG_WINGER, $detailedAwayTeamPlayers) && $detailedAwayTeamPlayers[Player::POST_STRONG_WINGER]->getPlayer()->getId() === $awayPlayer->getId() ? "selected" : "" ?>
                                                    value="<?= $awayPlayer->getId() ?>">
                                                <?php echo $awayPlayer->getHuman()->getFirstName() . ' ' . $awayPlayer->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="role-container">
                                <h4>Arrière </h4>
                                <div class="select-container">
                                    <select name="backAway" required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($awayPlayers as $awayPlayer): ?>
                                            <option <?= array_key_exists(Player::POST_BACK, $detailedAwayTeamPlayers) && $detailedAwayTeamPlayers[Player::POST_BACK]->getPlayer()->getId() === $awayPlayer->getId() ? "selected" : "" ?>
                                                    value="<?= $awayPlayer->getId() ?>">
                                                <?php echo $awayPlayer->getHuman()->getFirstName() . ' ' . $awayPlayer->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- CAPTAINS -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Capitaines</h3>
                        <div class="detail-card-content-part-content coach-team-captains-container">

                            <div class="role-container">
                                <h4>Capitaine </h4>
                                <div class="select-container">
                                    <select id="captainAway" name="captainAway" required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($awayPlayers as $awayPlayer): ?>
                                            <option <?= array_key_exists(TeamPlayer::TITLE_CAPTAIN, $detailedAwayTeamPlayers) && $detailedAwayTeamPlayers[TeamPlayer::TITLE_CAPTAIN]->getPlayer()->getId() === $awayPlayer->getId() ? "selected" : "" ?>
                                                    value="<?= $awayPlayer->getId() ?>">
                                                <?php echo $awayPlayer->getHuman()->getFirstName() . ' ' . $awayPlayer->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="role-container">
                                <h4>Capitaine Suppléant </h4>
                                <div class="select-container">
                                    <select id="alternateCaptainAway" name="alternateCaptainAway"
                                            required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($awayPlayers as $awayPlayer): ?>
                                            <option <?= array_key_exists(TeamPlayer::TITLE_ALTERNATE_CAPTAIN, $detailedAwayTeamPlayers) && $detailedAwayTeamPlayers[TeamPlayer::TITLE_ALTERNATE_CAPTAIN]->getPlayer()->getId() === $awayPlayer->getId() ? "selected" : "" ?>
                                                    value="<?= $awayPlayer->getId() ?>">
                                                <?php echo $awayPlayer->getHuman()->getFirstName() . ' ' . $awayPlayer->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- SUBSTITUTES -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Remplaçants</h3>
                        <div class="detail-card-content-part-content">
                            <div class="coach-team-substitutes-container" id="substitutes-container-away">
                                <?php foreach ($detailedAwayTeamPlayers[TeamPlayer::POST_SUBSTITUTE] as $playerId => $substitute): ?>
                                    <div class="substitute-entry-away">
                                        <div class="select-container">
                                            <select name="awaySubstitutes[]" required>
                                                <option value="" disabled selected>Sélectionner un joueur</option>
                                                <?php foreach ($awayPlayers as $awayPlayer): ?>
                                                    <option <?= $playerId === $awayPlayer->getId() ? "selected" : "" ?>
                                                            value="<?= $awayPlayer->getId() ?>">
                                                        <?php echo $awayPlayer->getHuman()->getFirstName() . ' ' . $awayPlayer->getHuman()->getLastName(); ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="remove-button remove-substitute-away">
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                 stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                      d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                            </svg>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="coach-add-substitute-button" id="add-substitute-away">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                                     class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                </svg>
                                <span>Ajouter un remplacement</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ARBITRATORS CARD-->
            <div class="arbitrators-container card vs-card">
                <h2 class="card-title">Arbitres</h2>
                <div class="card-content">
                    <div class="form-input role-container">
                        <h4>Arbitre principal</h4>
                        <div class="select-container">
                            <select name="arbitratorHolderId" required>
                                <?php foreach ($arbitrators as $arbitrator): ?>
                                    <option <?= $arbitrator->getId() === $gameArbitrators[Arbitrator::TITLE_HOLDER]->getId() ? "selected" : "" ?>
                                            value="<?= $arbitrator->getId() ?>">
                                        <?= $arbitrator->getHuman()->getFirstName() . ' ' . $arbitrator->getHuman()->getLastName(); ?>
                                    </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="arbitrator-assistants-container">
                        <div class="form-input role-container">
                            <h4>Arbitre assistant 1</h4>
                            <div class="select-container">
                                <select name="arbitratorAssistant1Id" required>
                                    <?php foreach ($arbitrators as $arbitrator): ?>
                                        <option <?= $arbitrator->getId() === $gameArbitrators[Arbitrator::TITLE_ASSISTANT_1]->getId() ? "selected" : "" ?>
                                                value="<?= $arbitrator->getId() ?>">
                                            <?= $arbitrator->getHuman()->getFirstName() . ' ' . $arbitrator->getHuman()->getLastName(); ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-input role-container">
                            <h4>Arbitre assistant 2</h4>
                            <div class="select-container">
                                <select name="arbitratorAssistant2Id" required>
                                    <?php foreach ($arbitrators as $arbitrator): ?>
                                        <option <?= $arbitrator->getId() === $gameArbitrators[Arbitrator::TITLE_ASSISTANT_2]->getId() ? "selected" : "" ?>
                                                value="<?= $arbitrator->getId() ?>">
                                            <?= $arbitrator->getHuman()->getFirstName() . ' ' . $arbitrator->getHuman()->getLastName(); ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- FORM BUTTONS -->
            <div class="form-button-container">
                <a class="custom-button blue-night small" href="<?= $_SESSION['adminGamesInProgressUri'] ?>">Annuler</a>
                <button class="custom-button orange small" type="submit" name="createTeamPlayer">Sauvegarder</button>
            </div>
        </form>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {

        //  ----------------------------------------------------------------------------------------------------------------------
        //  -------------------------JS HOME TEAM-----------------------------------------
        //  ----------------------------------------------------------------------------------------------------------------------
        let homePlayerOptions = `<option value="" disabled selected>Sélectionner un joueur</option>`; // Option vide par défaut
        homePlayerOptions += `<?php
        foreach ($homePlayers as $player) {
            echo '<option value="' . $player->getId() . '">' . $player->getHuman()->getFirstName() . ' ' . $player->getHuman()->getLastName() . '</option>';
        }
        ?>`;

        let homeCurrentCaptain = "<?= isset($detailedHomeTeamPlayers[TeamPlayer::TITLE_CAPTAIN]) ? $detailedHomeTeamPlayers[TeamPlayer::TITLE_CAPTAIN]->getPlayer()->getId() : '' ?>";
        let homeCurrentAlternateCaptain = "<?= isset($detailedHomeTeamPlayers[TeamPlayer::TITLE_ALTERNATE_CAPTAIN]) ? $detailedHomeTeamPlayers[TeamPlayer::TITLE_ALTERNATE_CAPTAIN]->getPlayer()->getId() : '' ?>";

        // Fonction pour obtenir les joueurs sélectionnés dans les positions principales et les remplaçants
        function getHomeSelectedMainAndSubstitutePlayers() {
            const selectedPlayers = [];

            // Inclure les joueurs des positions principales
            document.querySelectorAll('select[name^="pivotHome"], select[name^="leaderHome"], select[name^="wingerHome"], select[name^="strongWingerHome"], select[name^="backHome"]').forEach(select => {
                if (select.value) {
                    selectedPlayers.push({
                        id: select.value,
                        text: select.options[select.selectedIndex].text
                    });
                }
            });

            // Inclure les joueurs remplaçants
            document.querySelectorAll('select[name="homeSubstitutes[]"]').forEach(select => {
                if (select.value) {
                    selectedPlayers.push({
                        id: select.value,
                        text: select.options[select.selectedIndex].text
                    });
                }
            });

            return selectedPlayers;
        }

        // Fonction pour mettre à jour les options disponibles dans les selects de Capitaine et Capitaine Suppléant de l'équipe Domicile
        function updateHomeCaptainOptions() {
            const selectedPlayers = getHomeSelectedMainAndSubstitutePlayers();
            const captainSelect = document.getElementById('captainHome');
            const alternateCaptainSelect = document.getElementById('alternateCaptainHome');

            // Réinitialiser les options avec l'option par défaut désactivée
            captainSelect.innerHTML = '<option value="" disabled selected>Sélectionner un joueur</option>';
            alternateCaptainSelect.innerHTML = '<option value="" disabled selected>Sélectionner un joueur</option>';

            selectedPlayers.forEach(player => {
                const isSelectedCaptain = player.id === homeCurrentCaptain;
                const isSelectedAlternateCaptain = player.id === homeCurrentAlternateCaptain;

                // Ajouter les options au Capitaine
                captainSelect.innerHTML += `<option value="${player.id}" ${isSelectedCaptain ? 'selected' : ''} ${isSelectedAlternateCaptain ? 'disabled' : ''}>${player.text}</option>`;

                // Ajouter les options au Capitaine Suppléant
                alternateCaptainSelect.innerHTML += `<option value="${player.id}" ${isSelectedAlternateCaptain ? 'selected' : ''} ${isSelectedCaptain ? 'disabled' : ''}>${player.text}</option>`;
            });
        }

        // Fonction pour mettre à jour les sélections de Capitaine et Capitaine Suppléant de l'équipe domicile
        function updateHomeCaptainSelection() {
            const captainSelect = document.getElementById('captainHome');
            const alternateCaptainSelect = document.getElementById('alternateCaptainHome');

            // Désactiver l'option de l'autre select
            captainSelect.querySelectorAll('option').forEach(option => {
                option.disabled = option.value && option.value === alternateCaptainSelect.value;
            });

            alternateCaptainSelect.querySelectorAll('option').forEach(option => {
                option.disabled = option.value && option.value === captainSelect.value;
            });

            // Forcer la désactivation de l'option par défaut après la mise à jour
            captainSelect.querySelector('option[value=""]').disabled = true;
            alternateCaptainSelect.querySelector('option[value=""]').disabled = true;
        }

        // Fonction pour obtenir les joueurs sélectionnés (pour désactivation)
        function $getHomeSelectedPlayers() {
            const selectedPlayers = new Set();
            document.querySelectorAll('select[name^="homeSubstitutes"], select[name^="pivotHome"], select[name^="leaderHome"], select[name^="wingerHome"], select[name^="strongWingerHome"], select[name^="backHome"]').forEach(select => {
                if (select.value) {
                    selectedPlayers.add(select.value);
                }
            });
            return selectedPlayers;
        }

        // Fonction pour désactiver les joueurs déjà sélectionnés dans d'autres sélections
        function updateHomeSelectOptions() {
            const selectedPlayers = $getHomeSelectedPlayers();
            document.querySelectorAll('select[name^="homeSubstitutes"], select[name^="pivotHome"], select[name^="leaderHome"], select[name^="wingerHome"], select[name^="strongWingerHome"], select[name^="backHome"]').forEach(select => {
                const currentSelection = select.value;
                const options = select.querySelectorAll('option');
                options.forEach(option => {
                    option.disabled = selectedPlayers.has(option.value) && option.value !== currentSelection;
                });

                // Forcer la désactivation de l'option par défaut après chaque mise à jour
                select.querySelector('option[value=""]').disabled = true;
            });

            updateHomeCaptainOptions(); // Mettre à jour les options des capitaines
            updateHomeCaptainSelection(); // Mettre à jour la désactivation entre les deux sélecteurs de Capitaine
        }

        // Fonction pour attacher l'événement de suppression
        function attachHomeRemoveEvent(button) {
            button.addEventListener('click', function () {
                let entry = this.closest('.substitute-entry-home');
                entry.parentNode.removeChild(entry);
                updateHomeSelectOptions(); // Mettre à jour les options après suppression
            });
        }

        // Attacher l'événement de suppression aux boutons existants au chargement de la page
        document.querySelectorAll('.remove-substitute-home').forEach(function (button) {
            attachHomeRemoveEvent(button);
        });

        // Fonction pour ajouter un nouveau substitut avec mise à jour des options
        function addHomeSubstitute() {
            var container = document.getElementById('substitutes-container-home');
            var newDiv = document.createElement('div');
            newDiv.classList.add('substitute-entry-home');

            var newSelectContainer = document.createElement('div');
            newSelectContainer.classList.add('select-container');

            var newSelect = document.createElement('select');
            newSelect.name = 'homeSubstitutes[]';
            newSelect.required = true;
            newSelect.innerHTML = homePlayerOptions;
            newSelect.addEventListener('change', updateHomeSelectOptions); // Mettre à jour les options lors du changement

            var removeButton = document.createElement('div');
            removeButton.classList.add('remove-button');
            removeButton.classList.add('remove-substitute-home');
            removeButton.innerHTML = `
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                         stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round"
                              d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                    </svg>
            `;

            // Attacher l'événement de suppression au nouveau bouton
            attachHomeRemoveEvent(removeButton);

            newSelectContainer.appendChild(newSelect)
            newDiv.appendChild(newSelectContainer);
            newDiv.appendChild(removeButton);

            container.appendChild(newDiv); // Ajout à la fin du conteneur
            updateHomeSelectOptions(); // Mise à jour après ajout
        }

        // Attacher l'événement de clic au bouton d'ajout de substitut
        document.getElementById('add-substitute-home').addEventListener('click', addHomeSubstitute);

        // Attacher l'événement de changement à tous les selects de positions principales
        document.querySelectorAll('select[name^="homeSubstitutes"], select[name^="pivotHome"], select[name^="leaderHome"], select[name^="wingerHome"], select[name^="strongWingerHome"], select[name^="backHome"]').forEach(select => {
            select.addEventListener('change', updateHomeSelectOptions);
        });

        // Attacher les événements de changement pour Captain et Alternate Captain
        document.getElementById('captainHome').addEventListener('change', updateHomeCaptainSelection);
        document.getElementById('alternateCaptainHome').addEventListener('change', updateHomeCaptainSelection);

        // Initialisation des options
        updateHomeSelectOptions();

        //  ----------------------------------------------------------------------------------------------------------------------
        //  -------------------------JS AWAY TEAM-----------------------------------------
        //  ----------------------------------------------------------------------------------------------------------------------
        let awayPlayerOptions = `<option value="" disabled selected>Sélectionner un joueur</option>`; // Option vide par défaut
        awayPlayerOptions += `<?php
        foreach ($awayPlayers as $player) {
            echo '<option value="' . $player->getId() . '">' . $player->getHuman()->getFirstName() . ' ' . $player->getHuman()->getLastName() . '</option>';
        }
        ?>`;

        let awayCurrentCaptain = "<?= isset($detailedAwayTeamPlayers[TeamPlayer::TITLE_CAPTAIN]) ? $detailedAwayTeamPlayers[TeamPlayer::TITLE_CAPTAIN]->getPlayer()->getId() : '' ?>";
        let awayCurrentAlternateCaptain = "<?= isset($detailedAwayTeamPlayers[TeamPlayer::TITLE_ALTERNATE_CAPTAIN]) ? $detailedAwayTeamPlayers[TeamPlayer::TITLE_ALTERNATE_CAPTAIN]->getPlayer()->getId() : '' ?>";

        // Fonction pour obtenir les joueurs sélectionnés dans les positions principales et les remplaçants
        function getAwaySelectedMainAndSubstitutePlayers() {
            const selectedPlayers = [];

            // Inclure les joueurs des positions principales
            document.querySelectorAll('select[name^="pivotAway"], select[name^="leaderAway"], select[name^="wingerAway"], select[name^="strongWingerAway"], select[name^="backAway"]').forEach(select => {
                if (select.value) {
                    selectedPlayers.push({
                        id: select.value,
                        text: select.options[select.selectedIndex].text
                    });
                }
            });

            // Inclure les joueurs remplaçants
            document.querySelectorAll('select[name="awaySubstitutes[]"]').forEach(select => {
                if (select.value) {
                    selectedPlayers.push({
                        id: select.value,
                        text: select.options[select.selectedIndex].text
                    });
                }
            });

            return selectedPlayers;
        }

        // Fonction pour mettre à jour les options disponibles dans les selects de Capitaine et Capitaine Suppléant de l'équipe Domicile
        function updateAwayCaptainOptions() {
            const selectedPlayers = getAwaySelectedMainAndSubstitutePlayers();
            const captainSelect = document.getElementById('captainAway');
            const alternateCaptainSelect = document.getElementById('alternateCaptainAway');

            // Réinitialiser les options avec l'option par défaut désactivée
            captainSelect.innerHTML = '<option value="" disabled selected>Sélectionner un joueur</option>';
            alternateCaptainSelect.innerHTML = '<option value="" disabled selected>Sélectionner un joueur</option>';

            selectedPlayers.forEach(player => {
                const isSelectedCaptain = player.id === awayCurrentCaptain;
                const isSelectedAlternateCaptain = player.id === awayCurrentAlternateCaptain;

                // Ajouter les options au Capitaine
                captainSelect.innerHTML += `<option value="${player.id}" ${isSelectedCaptain ? 'selected' : ''} ${isSelectedAlternateCaptain ? 'disabled' : ''}>${player.text}</option>`;

                // Ajouter les options au Capitaine Suppléant
                alternateCaptainSelect.innerHTML += `<option value="${player.id}" ${isSelectedAlternateCaptain ? 'selected' : ''} ${isSelectedCaptain ? 'disabled' : ''}>${player.text}</option>`;
            });
        }

        // Fonction pour mettre à jour les sélections de Capitaine et Capitaine Suppléant de l'équipe domicile
        function updateAwayCaptainSelection() {
            const captainSelect = document.getElementById('captainAway');
            const alternateCaptainSelect = document.getElementById('alternateCaptainAway');

            // Désactiver l'option de l'autre select
            captainSelect.querySelectorAll('option').forEach(option => {
                option.disabled = option.value && option.value === alternateCaptainSelect.value;
            });

            alternateCaptainSelect.querySelectorAll('option').forEach(option => {
                option.disabled = option.value && option.value === captainSelect.value;
            });

            // Forcer la désactivation de l'option par défaut après la mise à jour
            captainSelect.querySelector('option[value=""]').disabled = true;
            alternateCaptainSelect.querySelector('option[value=""]').disabled = true;
        }

        // Fonction pour obtenir les joueurs sélectionnés (pour désactivation)
        function $getAwaySelectedPlayers() {
            const selectedPlayers = new Set();
            document.querySelectorAll('select[name^="awaySubstitutes"], select[name^="pivotAway"], select[name^="leaderAway"], select[name^="wingerAway"], select[name^="strongWingerAway"], select[name^="backAway"]').forEach(select => {
                if (select.value) {
                    selectedPlayers.add(select.value);
                }
            });
            return selectedPlayers;
        }

        // Fonction pour désactiver les joueurs déjà sélectionnés dans d'autres sélections
        function updateAwaySelectOptions() {
            const selectedPlayers = $getAwaySelectedPlayers();
            document.querySelectorAll('select[name^="awaySubstitutes"], select[name^="pivotAway"], select[name^="leaderAway"], select[name^="wingerAway"], select[name^="strongWingerAway"], select[name^="backAway"]').forEach(select => {
                const currentSelection = select.value;
                const options = select.querySelectorAll('option');
                options.forEach(option => {
                    option.disabled = selectedPlayers.has(option.value) && option.value !== currentSelection;
                });

                // Forcer la désactivation de l'option par défaut après chaque mise à jour
                select.querySelector('option[value=""]').disabled = true;
            });

            updateAwayCaptainOptions(); // Mettre à jour les options des capitaines
            updateAwayCaptainSelection(); // Mettre à jour la désactivation entre les deux sélecteurs de Capitaine
        }

        // Fonction pour attacher l'événement de suppression
        function attachAwayRemoveEvent(button) {
            button.addEventListener('click', function () {
                let entry = this.closest('.substitute-entry-away');
                entry.parentNode.removeChild(entry);
                updateAwaySelectOptions(); // Mettre à jour les options après suppression
            });
        }

        // Attacher l'événement de suppression aux boutons existants au chargement de la page
        document.querySelectorAll('.remove-substitute-away').forEach(function (button) {
            attachAwayRemoveEvent(button);
        });

        // Fonction pour ajouter un nouveau substitut avec mise à jour des options
        function addAwaySubstitute() {
            var container = document.getElementById('substitutes-container-away');
            var newDiv = document.createElement('div');
            newDiv.classList.add('substitute-entry-away');

            var newSelectContainer = document.createElement('div');
            newSelectContainer.classList.add('select-container');

            var newSelect = document.createElement('select');
            newSelect.name = 'awaySubstitutes[]';
            newSelect.required = true;
            newSelect.innerHTML = awayPlayerOptions;
            newSelect.addEventListener('change', updateAwaySelectOptions); // Mettre à jour les options lors du changement

            var removeButton = document.createElement('div');
            removeButton.classList.add('remove-button');
            removeButton.classList.add('remove-substitute-away');
            removeButton.innerHTML = `
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                         stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round"
                              d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                    </svg>
            `;

            // Attacher l'événement de suppression au nouveau bouton
            attachAwayRemoveEvent(removeButton);

            newSelectContainer.appendChild(newSelect)
            newDiv.appendChild(newSelectContainer);
            newDiv.appendChild(removeButton);

            container.appendChild(newDiv); // Ajout à la fin du conteneur
            updateAwaySelectOptions(); // Mise à jour après ajout
        }

        // Attacher l'événement de clic au bouton d'ajout de substitut
        document.getElementById('add-substitute-away').addEventListener('click', addAwaySubstitute);

        // Attacher l'événement de changement à tous les selects de positions principales
        document.querySelectorAll('select[name^="awaySubstitutes"], select[name^="pivotAway"], select[name^="leaderAway"], select[name^="wingerAway"], select[name^="strongWingerAway"], select[name^="backAway"]').forEach(select => {
            select.addEventListener('change', updateAwaySelectOptions);
        });

        // Attacher les événements de changement pour Captain et Alternate Captain
        document.getElementById('captainAway').addEventListener('change', updateAwayCaptainSelection);
        document.getElementById('alternateCaptainAway').addEventListener('change', updateAwayCaptainSelection);

        // Initialisation des options
        updateAwaySelectOptions();

        //  ----------------------------------------------------------------------------------------------------------------------
        //  -------------------------JS ARBITRATOR  -----------------------------------------
        //  ----------------------------------------------------------------------------------------------------------------------
        // Attacher les événements de changement pour Captain et Alternate Captain
        document.getElementById('captainAway').addEventListener('change', updateAwayCaptainSelection);
        document.getElementById('alternateCaptainAway').addEventListener('change', updateAwayCaptainSelection);


        // Fonction pour obtenir les arbitres sélectionnés (pour désactivation)
        function $getSelectedArbitrators() {
            const selectedArbitrators = new Set();
            document.querySelectorAll('select[name^="arbitratorHolderId"], select[name^="arbitratorAssistant1Id"], select[name^="arbitratorAssistant2Id"]').forEach(select => {
                if (select.value) {
                    selectedArbitrators.add(select.value);
                }
            });
            return selectedArbitrators;
        }

        // Fonction pour désactiver les joueurs déjà sélectionnés dans d'autres sélections
        function updateArbitratorSelectOptions() {
            const selectedArbitrators = $getSelectedArbitrators();
            document.querySelectorAll('select[name^="arbitratorHolderId"], select[name^="arbitratorAssistant1Id"], select[name^="arbitratorAssistant2Id"]').forEach(select => {
                const currentSelection = select.value;
                const options = select.querySelectorAll('option');
                options.forEach(option => {
                    option.disabled = selectedArbitrators.has(option.value) && option.value !== currentSelection;
                });
            });
        }

        // Attacher l'événement de changement à tous les selects de positions principales
        document.querySelectorAll('select[name^="arbitratorHolderId"], select[name^="arbitratorAssistant1Id"], select[name^="arbitratorAssistant2Id"]').forEach(select => {
            select.addEventListener('change', updateArbitratorSelectOptions);
        });

        // Initialisation des options
        updateArbitratorSelectOptions();
    });
</script>


</body>
</html>



