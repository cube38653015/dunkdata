<?php
/**
 * @var array $players
 * @var Player $player
 * @var array $otherPlayers
 * @var Player $otherPlayer
 * @var Franchise $franchise
 * @var Coach $mainCoach
 * @var Coach $assistantCoach
 */
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Liste des joueurs</title>
    <link rel="stylesheet" href="/dunkdata/src/views/admin/styles/admin.css">

</head>
<body>

<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/adminNavbar.php'; ?>
</div>
<div class="content">
    <h1>Modification des joueurs d'une franchise</h1>
    <div class="home-content">
        <div class="card detail-card">
            <div class="card-content">
                <!-- CURRENT PLAYERS -->
                <div class="detail-card-content-part">
                    <h3 class="detail-card-content-part-title">Liste des joueurs</h3>
                    <div class="detail-card-content-part-content content-justify-center">
                        <table class="min-shadow">
                            <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>Sexe</th>
                                <th>Nationalité</th>
                                <th>Date de naissance</th>
                                <th>Numéro maillot</th>
                                <th>Poste principale</th>
                            </tr>
                            </thead>
                            <tbody id="list-players-container">
                            <?php if (!empty($players)): ?>
                                <?php foreach ($players as $player): ?>
                                    <?php if ($player !== null): ?>
                                        <tr class="player-entry">
                                            <td><?= $player->getHuman()->getFirstName() ?></td>
                                            <td><?= $player->getHuman()->getLastName() ?></td>
                                            <td><?= $player->getHuman()->getGender() === Human::GENDER_MAN ? "Homme" : "Femme" ?></td>
                                            <td><?= $player->getHuman()->getNationality() ?></td>
                                            <td><?= (new DateTime($player->getHuman()->getBirthDate()))->format('d/m/Y') ?></td>
                                            <td><?= $player->getJerseyNumber() ?></td>
                                            <td> <?= $player->getTranslatedMainPost() ?></td>
                                            <td>
                                                <form method="post" action="<?= $_SESSION['adminShowUpdatePlayerFormUri'] ?>" style="display:inline;">
                                                    <input type="hidden" name="action" value="edit">
                                                    <input type="hidden" name="playerId" value="<?= $player->getId() ?>">
                                                    <input type="hidden" name="franchiseId" value="<?= $_SESSION['franchiseId'] ?>">
                                                    <button class="table-btn primary" type="submit" name="edit">
                                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                             stroke="currentColor"
                                                             height="20">
                                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                                  d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L6.832 19.82a4.5 4.5 0 0 1-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 0 1 1.13-1.897L16.863 4.487Zm0 0L19.5 7.125"/>
                                                        </svg>
                                                    </button>
                                                </form>
                                                <form method="post" action="<?= $_SESSION['adminDeletePlayerUri'] ?>" style="display:inline;">
                                                    <input type="hidden" name="action" value="delete">
                                                    <input type="hidden" name="playerId" value="<?= $player->getId() ?>">
                                                    <input type="hidden" name="franchiseId" value="<?= $_SESSION['franchiseId'] ?>">
                                                    <button class="table-btn danger" type="submit" name="delete">
                                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                             stroke="currentColor"
                                                             height="20">
                                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                                  d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0"/>
                                                        </svg>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <form method="post" action="<?= $_SESSION['adminUpdateFranchisePlayersUri'] ?>">
                    <input type="hidden" name="franchiseId" value="<?= $_SESSION['franchiseId'] ?>">
                    <!-- CREATE NEW PLAYERS -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Créer un joueurs à ajouter</h3>
                        <div class="detail-card-content-part-content content-justify-center">
                            <table class="min-shadow">
                                <thead>
                                <tr>
                                    <th>Nom</th>
                                    <th>Prénom</th>
                                    <th>Sexe</th>
                                    <th>Nationalité</th>
                                    <th>Date de naissance</th>
                                    <th>Numéro maillot</th>
                                    <th>Poste principale</th>
                                    <th>
                                        <button type="button" id="add-player" class="add-btn-table custom-button orange small" name="create">
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                 stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                      d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                            </svg>
                                        </button>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="new-players-container">
                                <tr class="player-entry">
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <br>

                    <!-- ADD EXISTING PLAYERS -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Ajout de joueurs déjà existants</h3>
                        <div class="detail-card-content-part-content">
                            <button type="button" id="add-existing-player" class="add-btn-table custom-button orange small" name="create">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                     stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                </svg>
                            </button>

                            <br>

                            <div id="selectPlayerSection">
                                <div id="existing-players-container">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-button-container">
                        <button class="custom-button orange small" name="updateFranchisePlayers" type="submit">Sauvegarder</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        // Fonction pour attacher l'événement de suppression
        function attachRemoveEvent(button) {
            button.addEventListener('click', function () {
                var row = this.closest('.player-entry');
                row.parentNode.removeChild(row);
            });
        }

        function attachRemoveSelectEvent(button) {
            button.addEventListener('click', function () {
                var row = this.closest('.player-select');
                row.parentNode.removeChild(row);
                updateSelectOptions();
            });
        }

        // Attacher l'événement de suppression aux boutons existants au chargement de la page
        document.querySelectorAll('.remove-player').forEach(function (button) {
            attachRemoveEvent(button);
        });

        document.querySelectorAll('.remove-player-select').forEach(function (button) {
            attachRemoveSelectEvent(button);
        });

        // Fonction pour ajouter un nouveau joueur
        function addPlayer() {
            var container = document.getElementById('new-players-container');
            var newRow = document.createElement('tr');
            newRow.classList.add('player-entry');

            newRow.innerHTML = `
                <td><input class="min-size" type="text" name="playerFirstName[]" required></td>
                <td><input class="min-size" type="text" name="playerLastName[]" required></td>
                <td>
                    <div class="select-container">
                        <select id="playerGender[]" name="playerGender[]" required>
                            <option value="<?=Human::GENDER_MAN ?>">Homme</option>
                            <option value="<?=Human::GENDER_WOMAN ?>">Femme</option>
                        </select>
                    </div>
                </td>
                <td><input class="min-size" type="text" name="playerNationality[]" required></td>
                <td><input type="date" name="playerBirthDate[]" required></td>
                <td><input class="min-size" type="number" name="playerJerseyNumber[]" required></td>
                <td>
                    <div class="select-container">
                        <select id="playerMainPost[]" name="playerMainPost[]" required>
                            <option value="<?=Player::POST_LEADER?>">Meneur</option>
                            <option value="<?=Player::POST_PIVOT?>">Pivot</option>
                            <option value="<?=Player::POST_WINGER?>">Ailier</option>
                            <option value="<?=Player::POST_STRONG_WINGER?>">Ailier fort</option>
                            <option value="<?=Player::POST_BACK?>">Arrière</option>
                        </select><br>
                    </div>
                </td>
                <td>
                    <div class="remove-button remove-player" id="remove-line">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                             stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                  d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                        </svg>
                    </div>
                </td>
            `;

            // Ajouter la nouvelle ligne au tableau
            container.appendChild(newRow);

            // Attacher l'événement de suppression au nouveau bouton
            attachRemoveEvent(newRow.querySelector('.remove-player'));
        }

        function updateSelectOptions() {
            const selectedPlayers = new Set();
            document.querySelectorAll('select[name^="otherPlayer"]').forEach(select => {
                if (select.value) {
                    selectedPlayers.add(select.value);
                }
            });

            document.querySelectorAll('select[name^="otherPlayer"]').forEach(select => {
                const currentSelection = select.value;
                select.querySelectorAll('option').forEach(option => {
                    if (selectedPlayers.has(option.value) && option.value !== currentSelection) {
                        option.disabled = true;
                    } else {
                        option.disabled = false;
                    }
                });
            });
        }

        document.querySelectorAll('select[name^="otherPlayer"]').forEach(select => {
            select.addEventListener('change', updateSelectOptions);
        });

        function addExistingPlayer() {
            var container = document.getElementById('existing-players-container');
            var newSelect = document.createElement('div');
            newSelect.classList.add('player-select');

            newSelect.innerHTML = `
                <div class="select-container">
                    <select id="otherPlayer" name="otherPlayer[]">
                        <option value=""></option>
                        <?php foreach ($otherPlayers as $otherPlayer): ?>
                            <option value="<?= $otherPlayer->getId() ?>"><?= $otherPlayer->getHuman()->getFirstName() ?> <?= $otherPlayer->getHuman()->getLastName() ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="remove-button remove-player-select"">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                         stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round"
                              d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                    </svg>
                </div>
            `;

            newSelect.querySelector('select').addEventListener('change', updateSelectOptions);
            newSelect.querySelector('.remove-player-select').addEventListener('click', function () {
                newSelect.remove();
                updateSelectOptions();
            });
            container.appendChild(newSelect);
            updateSelectOptions();
        }

        // Attacher l'événement de clic au bouton d'ajout de joueur
        document.getElementById('add-player').addEventListener('click', addPlayer);

        // Attacher l'événement de clic au bouton d'ajout de joueur existant
        document.getElementById('add-existing-player').addEventListener('click', addExistingPlayer);
        updateSelectOptions();
    });
</script>
</body>
</html>
