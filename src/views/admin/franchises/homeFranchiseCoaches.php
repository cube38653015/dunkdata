<?php
/**
 * @var Franchise $franchise
 * @var array $mainCoaches
 * @var Coach $mainCoach
 * @var array $assistantCoaches
 * @var Coach $assistantCoach
 * @var array $coaches
 * @var array $otherCoaches
 * @var Coach $othercoach
 */
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Liste des coachs</title>
    <link rel="stylesheet" href="/dunkdata/src/views/admin/styles/admin.css">
</head>
<body>
<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/adminNavbar.php'; ?>
</div>
<div class="content">
    <h1>Modification des coachs d'une franchise</h1>
    <div class="home-content">
        <div class="card detail-card">
            <div class="card-content">

                <!-- MAIN COACH -->
                <div class="detail-card-content-part">
                    <h3 class="detail-card-content-part-title">Coach principal</h3>
                    <div class="detail-card-content-part-content content-justify-center">
                        <table class="min-shadow">
                            <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>Sexe</th>
                                <th>Nationalité</th>
                                <th>Date de naissance</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($mainCoaches)): ?>
                                <?php foreach ($mainCoaches as $mainCoach): ?>
                                    <?php if ($mainCoach !== null): ?>
                                        <tr>
                                            <td><?= $mainCoach->getHuman()->getFirstName() ?></td>
                                            <td><?= $mainCoach->getHuman()->getLastName() ?></td>
                                            <td><?= $mainCoach->getHuman()->getGender() === Human::GENDER_MAN ? "Homme" : "Femme" ?></td>
                                            <td><?= $mainCoach->getHuman()->getNationality() ?></td>
                                            <td><?= $mainCoach->getHuman()->getBirthDate() ?></td>
                                            <td>
                                                <form method="post" action="<?= $_SESSION['adminShowUpdateCoachFormUri'] ?>" style="display:inline;">
                                                    <input type="hidden" name="action" value="edit">
                                                    <input type="hidden" name="coachId" value="<?= $mainCoach->getId() ?>">
                                                    <input type="hidden" name="franchiseId" value="<?= $_SESSION['franchiseId'] ?>">
                                                    <button class="table-btn primary" type="submit" name="editMainCoach">
                                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                             stroke="currentColor"
                                                             height="20">
                                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                                  d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L6.832 19.82a4.5 4.5 0 0 1-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 0 1 1.13-1.897L16.863 4.487Zm0 0L19.5 7.125"/>
                                                        </svg>
                                                    </button>
                                                </form>
                                                <form method="post" action="<?= $_SESSION['adminDeleteCoachUri'] ?>" style="display:inline;">
                                                    <input type="hidden" name="action" value="delete">
                                                    <input type="hidden" name="coachId" value="<?= $mainCoach->getId() ?>">
                                                    <input type="hidden" name="franchiseId" value="<?= $_SESSION['franchiseId'] ?>">
                                                    <button class="table-btn danger" type="submit" name="deleteMainCoach">
                                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                             stroke="currentColor"
                                                             height="20">
                                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                                  d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0"/>
                                                        </svg>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- SUB COACH -->
                <div class="detail-card-content-part">
                    <h3 class="detail-card-content-part-title">Coach adjoint</h3>
                    <div class="detail-card-content-part-content content-justify-center">
                        <table class="min-shadow">
                            <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>Sexe</th>
                                <th>Nationalité</th>
                                <th>Date de naissance</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($assistantCoaches)): ?>
                                <?php foreach ($assistantCoaches as $assistantCoach): ?>
                                    <?php if ($assistantCoach !== null): ?>
                                        <tr>
                                            <td><?= $assistantCoach->getHuman()->getFirstName() ?></td>
                                            <td><?= $assistantCoach->getHuman()->getLastName() ?></td>
                                            <td><?= $assistantCoach->getHuman()->getGender() === Human::GENDER_MAN ? "Homme" : "Femme" ?></td>
                                            <td><?= $assistantCoach->getHuman()->getNationality() ?></td>
                                            <td><?= $assistantCoach->getHuman()->getBirthDate() ?></td>
                                            <td>
                                                <form method="post" action="<?= $_SESSION['adminShowUpdateCoachFormUri'] ?>" style="display:inline;">
                                                    <input type="hidden" name="action" value="edit">
                                                    <input type="hidden" name="coachId" value="<?= $assistantCoach->getId() ?>">
                                                    <input type="hidden" name="franchiseId" value="<?= $_SESSION['franchiseId'] ?>">
                                                    <button class="table-btn primary" type="submit" name="editMainCoach">
                                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                             stroke="currentColor"
                                                             height="20">
                                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                                  d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L6.832 19.82a4.5 4.5 0 0 1-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 0 1 1.13-1.897L16.863 4.487Zm0 0L19.5 7.125"/>
                                                        </svg>
                                                    </button>
                                                </form>
                                                <form method="post" action="<?= $_SESSION['adminDeleteCoachUri'] ?>" style="display:inline;">
                                                    <input type="hidden" name="action" value="delete">
                                                    <input type="hidden" name="coachId" value="<?= $assistantCoach->getId() ?>">
                                                    <input type="hidden" name="franchiseId" value="<?= $_SESSION['franchiseId'] ?>">
                                                    <button class="table-btn danger" type="submit" name="deleteMainCoach">
                                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                             stroke="currentColor"
                                                             height="20">
                                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                                  d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0"/>
                                                        </svg>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>


                <form method="post" action="<?= $_SESSION['adminUpdateFranchiseCoachesUri'] ?>">
                    <input type="hidden" name="franchiseId" value="<?= $_SESSION['franchiseId'] ?>">

                    <!-- CREATE NEW COACH -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Créer un coach à ajouter</h3>
                        <div class="detail-card-content-part-content content-justify-center">
                            <table class="min-shadow">
                                <thead>
                                <tr>
                                    <th>Nom</th>
                                    <th>Prénom</th>
                                    <th>Sexe</th>
                                    <th>Nationalité</th>
                                    <th>Date de naissance</th>
                                    <th>Titre</th>
                                    <th>
                                        <button type="button" id="add-coach" class="add-btn-table custom-button orange small" name="create">
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                 stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                      d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                            </svg>
                                        </button>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="new-coaches-container">
                                <tr class="coach-entry">
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <br>

                    <!-- ADD EXISTING COACH -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Ajout d'un coach déjà existant</h3>
                        <div class="detail-card-content-part-content">
                            <button type="button" id="add-existing-coach" class="add-btn-table custom-button orange small" name="create">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                     stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                </svg>
                            </button>

                            <br>

                            <div id="selectCoachSection">
                                <div id="existing-coaches-container">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-button-container">
                        <button class="custom-button orange small" name="updateFranchiseCoachs" type="submit">Sauvegarder</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        // Fonction pour attacher l'événement de suppression
        function attachRemoveEvent(button) {
            button.addEventListener('click', function () {
                var row = this.closest('.coach-entry');
                row.parentNode.removeChild(row);
            });
        }

        function attachRemoveSelectEvent(button) {
            button.addEventListener('click', function () {
                var row = this.closest('.coach-select');
                row.parentNode.removeChild(row);
                updateSelectOptions();
            });
        }

        // Attacher l'événement de suppression aux boutons existants au chargement de la page
        document.querySelectorAll('.remove-coach').forEach(function (button) {
            attachRemoveEvent(button);
        });

        document.querySelectorAll('.remove-coach-select').forEach(function (button) {
            attachRemoveSelectEvent(button);
        });

        // Fonction pour ajouter un nouveau joueur
        function addCoach() {
            var container = document.getElementById('new-coaches-container');
            var newRow = document.createElement('tr');
            newRow.classList.add('coach-entry');

            newRow.innerHTML = `
                <td><input class="min-size" type="text" name="coachLastName[]" required></td>
                <td><input class="min-size" type="text" name="coachFirstName[]" required></td>
                <td>
                    <div class="select-container">
                        <select id="coachGender[]" name="coachGender[]" required>
                            <option value="<?=Human::GENDER_MAN ?>">Homme</option>
                            <option value="<?=Human::GENDER_WOMAN ?>">Femme</option>
                        </select>
                    </div>
                </td>
                <td><input class="min-size" type="text" name="coachNationality[]" required></td>
                <td><input type="date" name="coachBirthDate[]" required></td>
                <td>
                    <div class="select-container">
                        <select id="coachTitle[]" name="coachTitle[]" required>
                            <option value="<?=Coach::TITLE_MAIN_COACH?>">Principal</option>
                            <option value="<?=Coach::TITLE_ASSISTANT_COACH?>">Adjoint</option>
                        </select>
                    </div>
                </td>
                <td>
                    <div class="remove-button remove-coach" id="remove-line">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                             stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                  d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                        </svg>
                    </div>
                </td>
            `;

            // Ajouter la nouvelle ligne au tableau
            container.appendChild(newRow);

            // Attacher l'événement de suppression au nouveau bouton
            attachRemoveEvent(newRow.querySelector('.remove-coach'));
        }

        function updateSelectOptions() {
            const selectedCoaches = new Set();
            document.querySelectorAll('select[name^="otherCoach"]').forEach(select => {
                if (select.value) {
                    selectedCoaches.add(select.value);
                }
            });

            document.querySelectorAll('select[name^="otherCoach"]').forEach(select => {
                const currentSelection = select.value;
                select.querySelectorAll('option').forEach(option => {
                    if (selectedCoaches.has(option.value) && option.value !== currentSelection) {
                        option.disabled = true;
                    } else {
                        option.disabled = false;
                    }
                });
            });
        }

        document.querySelectorAll('select[name^="otherCoach"]').forEach(select => {
            select.addEventListener('change', updateSelectOptions);
        });

        function addExistingCoach() {
            var container = document.getElementById('existing-coaches-container');
            var newSelect = document.createElement('div');
            newSelect.classList.add('coach-select');

            newSelect.innerHTML = `
                <div class="select-container">
                    <select id="otherCoach" name="otherCoach[]">
                        <option value=""></option>
                        <?php foreach ($otherCoaches as $otherCoach): ?>
                            <option value="<?= $otherCoach->getId() ?>"><?= $otherCoach->getHuman()->getFirstName() ?> <?= $otherCoach->getHuman()->getLastName() ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="remove-button remove-coach-select"">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                         stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round"
                              d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                    </svg>
                </div>
            `;

            newSelect.querySelector('select').addEventListener('change', updateSelectOptions);
            newSelect.querySelector('.remove-coach-select').addEventListener('click', function () {
                newSelect.remove();
                updateSelectOptions();
            });
            container.appendChild(newSelect);
            updateSelectOptions();
        }

        // Attacher l'événement de clic au bouton d'ajout de joueur
        document.getElementById('add-coach').addEventListener('click', addCoach);

        // Attacher l'événement de clic au bouton d'ajout de joueur existant
        document.getElementById('add-existing-coach').addEventListener('click', addExistingCoach);
        updateSelectOptions();
    });
</script>
</body>
</html>


