<?php
/**
 * @var Player $player
 */
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Modification d'un joueur</title>
</head>
<body>
<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/adminNavbar.php'; ?>
</div>
<div class="content">
<h1>Modification d'un joueur</h1>
    <div>
        <form method="post" action="<?= $_SESSION['adminUpdatePlayerUri'] ?>">
            <input type="hidden" name="playerId" value="<?=$player->getId()?>">
            <button type="submit" name="updatePlayer">Enregistrer</button>
            <h2>Prénom:</h2>
            <input type="text" name="lastName" value="<?=$player->getHuman()->getLastName()?>" required><br>

            <h2>Nom:</h2>
            <input type="text" name="firstName" value="<?=$player->getHuman()->getFirstName()?>" required><br>

            <h2>Sexe:</h2>
            <select id="gender" name="gender" required>
                <option <?=$player->getHuman()->getGender() === Human::GENDER_MAN ? "selected" : ""?> value="<?=Human::GENDER_MAN ?>">Homme</option>
                <option <?=$player->getHuman()->getGender() === Human::GENDER_WOMAN ? "selected" : ""?> value="<?=Human::GENDER_WOMAN ?>">Femme</option>
            </select><br>

            <h2>Nationalité:</h2>
            <input type="text" name="nationality" value="<?=$player->getHuman()->getNationality()?>" required><br>

            <h2>Date de naissance:</h2>
            <input type="date" name="birthDate" value="<?=$player->getHuman()->getBirthDate()?>" required><br>

            <h2>Numéro de maillot:</h2>
            <input type="input" name="jerseyNumber" value="<?=$player->getJerseyNumber()?>" required><br>

            <h2>Poste principal:</h2>
            <select id="playerMainPost[]" name="mainPost" required>
                <option <?=$player->getMainPost() === Player::POST_LEADER ? "selected" : ""?> value="<?=Player::POST_LEADER?>">Meneur</option>
                <option <?=$player->getMainPost() === Player::POST_PIVOT ? "selected" : ""?> value="<?=Player::POST_PIVOT?>">Pivot</option>
                <option <?=$player->getMainPost() === Player::POST_WINGER ? "selected" : ""?> value="<?=Player::POST_WINGER?>">Ailier</option>
                <option <?=$player->getMainPost() === Player::POST_STRONG_WINGER ? "selected" : ""?> value="<?=Player::POST_STRONG_WINGER?>">Ailier fort</option>
                <option <?=$player->getMainPost() === Player::POST_BACK ? "selected" : ""?> value="<?=Player::POST_BACK?>">Arrière</option>
            </select><br>
        </form>
    </div>
</div>
</body>
</html>

