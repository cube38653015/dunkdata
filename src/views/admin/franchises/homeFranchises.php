<?php
/**
 * @var array $franchises
 * @var Franchise $franchise
 */
?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Liste des franchises</title>
    <link rel="stylesheet" href="/dunkdata/src/views/admin/styles/admin.css">
</head>
<body>
<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/adminNavbar.php'; ?>
</div>
<div class="content">
    <h1>Liste des franchises</h1>
    <div class="home-content">
        <table>
            <thead>
            <tr>
                <th>Nom</th>
                <th>Couleur</th>
                <th>Initiales</th>
                <th>Actif</th>
                <th>
                    <form method="post" action="<?= $_SESSION['adminShowNewFranchiseFormUri'] ?>" style="display:inline;">
                        <div class="form-button-container">
                            <button class="add-btn-table custom-button orange small" type="submit" name="create">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                </svg>
                            </button>
                        </div>
                    </form>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($franchises as $franchise): ?>
                <tr>
                    <td><?= $franchise->getName() ?></td>
                    <td><?= $franchise->getColor() ?></td>
                    <td><?= $franchise->getInitials() ?></td>
                    <td><?= $franchise->getActivity() == 1 ? "Actif" : "Inactif" ?></td>
                    <td>
                        <form method="post" action="<?= $_SESSION['adminShowUpdateFranchiseFormUri'] ?>" style="display:inline;">
                            <input type="hidden" name="action" value="edit">
                            <input type="hidden" name="franchiseId" value="<?= $franchise->getId() ?>">
                            <button class="table-btn primary" type="submit" name="edit">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                                     height="20">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L6.832 19.82a4.5 4.5 0 0 1-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 0 1 1.13-1.897L16.863 4.487Zm0 0L19.5 7.125"/>
                                </svg>
                            </button>
                        </form>
                        <form method="post" action="<?= $_SESSION['adminDeleteFranchiseUri'] ?>" style="display:inline;">
                            <input type="hidden" name="action" value="delete">
                            <input type="hidden" name="franchiseId" value="<?= $franchise->getId() ?>">
                            <button class="table-btn danger" type="submit" name="delete" >
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                                     height="20">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M11.412 15.655 9.75 21.75l3.745-4.012M9.257 13.5H3.75l2.659-2.849m2.048-2.194L14.25 2.25 12 10.5h8.25l-4.707 5.043M8.457 8.457 3 3m5.457 5.457 7.086 7.086m0 0L21 21"/>
                                </svg>
                            </button>
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
