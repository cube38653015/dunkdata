<?php
/**
 * @var Franchise $franchise
 */
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Ajout d'une nouvelle franchise</title>
    <link rel="stylesheet" href="/dunkdata/src/views/admin/styles/admin.css">
</head>
<body>
<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/adminNavbar.php'; ?>
</div>
<div class="content">
    <h1>Ajout d'une nouvelle franchise</h1>

    <div class="update-entity-content">
        <form method="post" action="<?= $_SESSION['adminCreateFranchiseUri'] ?>">
            <input type="hidden" name="action" value="create">

            <div class="card detail-card">
                <div class="card-content">
                    <div class="detail-card-content-part">

                        <h3 class="detail-card-content-part-title">Nom</h3>
                        <div class="detail-card-content-part-content">
                            <input type="text" name="name" required>
                        </div>

                        <h3 class="detail-card-content-part-title">Couleur</h3>
                        <div class="detail-card-content-part-content">
                            <input type="text" name="color" required>
                        </div>

                        <h3 class="detail-card-content-part-title">Initiales</h3>
                        <div class="detail-card-content-part-content">
                            <input type="text" name="initials" required>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-button-container">
                <a class="custom-button blue-night small" href="<?= $_SESSION['adminHomeFranchisesUri'] ?>">Annuler</a>
                <button class="custom-button orange small" type="submit">Créer</button>
            </div>
        </form>
    </div>
</div>

</body>
</html>
