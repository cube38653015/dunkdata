<?php
/**
 * @var array $players
 * @var Player $player
 * @var Franchise $franchise
 * @var Coach $mainCoach
 * @var Coach $assistantCoach
 */
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Modification d'une franchise</title>
    <link rel="stylesheet" href="/dunkdata/src/views/admin/styles/admin.css">
</head>
<body>

<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/adminNavbar.php'; ?>
</div>
<div class="content">
    <h1>Modification d'une franchise</h1>
    <div class="update-entity-content">

        <form id="adminHomeFranchisePlayers" method="post" action="<?= $_SESSION['adminHomeFranchisePlayersUri'] ?>"
              style="display:inline;">
            <input type="hidden" name="action" value="players">
            <input type="hidden" name="franchiseId" value="<?= $franchise->getId() ?>">
        </form>

        <form id="adminHomeFranchiseCoaches" method="post" action="<?= $_SESSION['adminHomeFranchiseCoachesUri'] ?>"
              style="display:inline;">
            <input type="hidden" name="action" value="coaches">
            <input type="hidden" name="franchiseId" value="<?= $franchise->getId() ?>">
        </form>

        <form id="adminUpdateFranchise" method="post" action="<?= $_SESSION['adminUpdateFranchiseUri'] ?>">
            <input type="hidden" name="franchiseId" value="<?= $franchise->getId() ?>">
            <input type="hidden" name="isActive" value="<?= $franchise->getActivity() ?>">

            <div class="card detail-card">
                <div class="card-content">
                    <div class="detail-card-content-part">

                        <h3 class="detail-card-content-part-title">Nom</h3>
                        <div class="detail-card-content-part-content">
                            <input type="text" name="name"
                                   value="<?= $franchise->getName() ?>" <?= $franchise->getActivity() ? '' : 'disabled' ?>
                                   required>
                        </div>

                        <h3 class="detail-card-content-part-title">Couleur</h3>
                        <div class="detail-card-content-part-content">
                            <input type="text" name="color"
                                   value="<?= $franchise->getColor() ?>" <?= $franchise->getActivity() ? '' : 'disabled' ?>
                                   required>
                        </div>

                        <h3 class="detail-card-content-part-title">Initiales</h3>
                        <div class="detail-card-content-part-content">
                            <input type="text" name="initials"
                                   value="<?= $franchise->getInitials() ?>" <?= $franchise->getActivity() ? '' : 'disabled' ?>>
                        </div>

                        <br>
                        <br>

                        <div class="detail-card-content-part-content content-justify-center">
                            <div class="form-button-container">
                                <?php if ($franchise->getActivity()): ?>
                                    <button form="adminHomeFranchisePlayers" class="custom-button orange x-small"
                                            type="submit">Gestion des joueurs
                                    </button>

                                    <button form="adminHomeFranchiseCoaches" class="custom-button orange x-small"
                                            type="submit">Gestion des coachs
                                    </button>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="form-button-container">
            <a class="custom-button blue-night small" href="<?= $_SESSION['adminHomeFranchisesUri'] ?>">Annuler</a>
            <button form="adminUpdateFranchise" class="custom-button orange small" type="submit">Sauvegarder</button>
        </div>
    </div>
</div>


</body>
</html>
