<?php
/**
 * @var Coach $coach
 */
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Modification d'un coach</title>
</head>
<body>
<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/adminNavbar.php'; ?>
</div>
<div class="content">
    <h1>Modification d'un coach</h1>
    <div>
        <form method="post" action="<?= $_SESSION['adminUpdateCoachUri'] ?>">
            <input type="hidden" name="coachId" value="<?= $coach->getId() ?>">
            <button type="submit" name="updateCoach">Enregistrer</button>
            <h2>Prénom:</h2>
            <input type="text" name="lastName" value="<?= $coach->getHuman()->getLastName() ?>" required><br>

            <h2>Nom:</h2>
            <input type="text" name="firstName" value="<?= $coach->getHuman()->getFirstName() ?>" required><br>

            <h2>Sexe:</h2>
            <select id="gender" name="gender" required>
                <option <?= $coach->getHuman()->getGender() === Human::GENDER_MAN ? "selected" : "" ?> value="<?= Human::GENDER_MAN ?>">Homme</option>
                <option <?= $coach->getHuman()->getGender() === Human::GENDER_WOMAN ? "selected" : "" ?> value="<?= Human::GENDER_WOMAN ?>">Femme
                </option>
            </select><br>

            <h2>Nationalité:</h2>
            <input type="text" name="nationality" value="<?= $coach->getHuman()->getNationality() ?>" required><br>

            <h2>Date de naissance:</h2>
            <input type="date" name="birthDate" value="<?= $coach->getHuman()->getBirthDate() ?>" required><br>

            <h2>Titre:</h2>
            <select id="title" name="title" required>
                <option <?= $coach->getTitle() === Coach::TITLE_MAIN_COACH ? "selected" : "" ?> value="<?= Coach::TITLE_MAIN_COACH ?>">Principal
                </option>
                <option <?= $coach->getTitle() === Coach::TITLE_ASSISTANT_COACH ? "selected" : "" ?> value="<?= Coach::TITLE_ASSISTANT_COACH ?>">
                    Assistant
                </option>
            </select><br>
        </form>
    </div>
</div>
</body>
</html>

