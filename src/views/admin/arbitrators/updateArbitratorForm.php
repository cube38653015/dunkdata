<?php
/**
 * @var Arbitrator $arbitrator
 */
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Modification d'un arbitre</title>
    <link rel="stylesheet" href="/dunkdata/src/views/admin/styles/admin.css">
</head>
<body>

<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/adminNavbar.php'; ?>
</div>
<div class="content">
    <h1>Modification d'un arbitre</h1>
    <div class="update-entity-content">
        <form method="post" action="<?= $_SESSION['adminUpdateArbitratorUri'] ?>">
            <input type="hidden" name="arbitratorId" value="<?= $arbitrator->getId() ?>">

            <div class="card detail-card">
                <div class="card-content">
                    <div class="detail-card-content-part">

                        <h3 class="detail-card-content-part-title">Nom</h3>
                        <div class="detail-card-content-part-content">
                            <input type="text" name="arbitratorLastName" value="<?= $arbitrator->getHuman()->getLastName() ?>" required>
                        </div>

                        <h3 class="detail-card-content-part-title">Prénom</h3>
                        <div class="detail-card-content-part-content">
                            <input type="text" name="arbitratorFirstName" value="<?= $arbitrator->getHuman()->getFirstName() ?>" required>
                        </div>

                        <h3 class="detail-card-content-part-title">Sexe</h3>
                        <div class="detail-card-content-part-content select-container">
                            <select id="arbitratorGender" name="arbitratorGender" required>
                                <option <?= $arbitrator->getHuman()->getGender() === Human::GENDER_MAN ? "selected" : "" ?>
                                        value="<?= Human::GENDER_MAN ?>">Homme
                                </option>
                                <option <?= $arbitrator->getHuman()->getGender() === Human::GENDER_WOMAN ? "selected" : "" ?>
                                        value="<?= Human::GENDER_WOMAN ?>">Femme
                                </option>
                            </select>
                        </div>

                        <h3 class="detail-card-content-part-title">Nationalité</h3>
                        <div class="detail-card-content-part-content">
                            <input type="text" name="arbitratorNationality" value="<?= $arbitrator->getHuman()->getNationality() ?>" required>
                        </div>

                        <h3 class="detail-card-content-part-title">Date de naissance</h3>
                        <div class="detail-card-content-part-content content-justify-center">
                            <input type="date" name="arbitratorBirthDate" value="<?= $arbitrator->getHuman()->getBirthDate() ?>" required>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-button-container">
                <a class="custom-button blue-night small" href="<?= $_SESSION['adminHomeArbitratorsUri'] ?>">Annuler</a>
                <button class="custom-button orange small" type="submit" name="updateArbitrator">Valider</button>
            </div>
        </form>
    </div>
</div>


</body>
</html>
