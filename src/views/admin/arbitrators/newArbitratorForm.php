<?php
/**
 * @var Arbitrator $arbitrator
 */
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Ajout d'un nouvel arbitre</title>
    <link rel="stylesheet" href="/dunkdata/src/views/admin/styles/admin.css">
</head>
<body>
<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/adminNavbar.php'; ?>
</div>
<div class="content">
    <h1>Ajout d'un nouvel arbitre</h1>
    <div class="update-entity-content">
        <form method="post" action="<?= $_SESSION['adminCreateArbitratorUri'] ?>">
            <input type="hidden" name="action" value="create">

            <div class="card detail-card">
                <div class="card-content">
                    <div class="detail-card-content-part">

                        <h3 class="detail-card-content-part-title">Nom</h3>
                        <div class="detail-card-content-part-content">
                            <input type="text" name="lastName" required>
                        </div>

                        <h3 class="detail-card-content-part-title">Prénom</h3>
                        <div class="detail-card-content-part-content">
                            <input type="text" name="firstName" required>
                        </div>

                        <h3 class="detail-card-content-part-title">Sexe</h3>
                        <div class="detail-card-content-part-content select-container">
                            <select id="gender" name="gender" required>
                                <option value="<?= Human::GENDER_MAN ?>">Homme</option>
                                <option value="<?= Human::GENDER_WOMAN ?>">Femme</option>
                            </select>
                        </div>

                        <h3 class="detail-card-content-part-title">Nationalité</h3>
                        <div class="detail-card-content-part-content">
                            <input type="text" name="nationality" required>
                        </div>

                        <h3 class="detail-card-content-part-title">Date de naissance</h3>
                        <div class="detail-card-content-part-content content-justify-center">
                            <input type="date" name="birthDate" required>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-button-container">
                <a class="custom-button blue-night small" href="<?= $_SESSION['adminHomeArbitratorsUri'] ?>">Annuler</a>
                <button class="custom-button orange small" type="submit">Créer</button>
            </div>
        </form>
    </div>
</div>
