<?php

/**
 * @var User $user
 * @var array $coachesByFranchise
 * @var array $franchises
 * @var int|null $selectedFranchiseId
 * @var int|null $selectedCoachId
 */
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Modification d'un utilisateur</title>
    <link rel="stylesheet" href="/dunkdata/src/views/admin/styles/admin.css">
</head>
<body>

<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/adminNavbar.php'; ?>
</div>
<div class="content">
    <h1>Modification d'un utilisateur</h1>
    <div class="update-entity-content">
        <form method="post" action="<?= $_SESSION['adminUpdateUserUri'] ?>">
            <input type="hidden" name="action" value="update">
            <input type="hidden" name="userId" value="<?= $user->getId() ?>">

            <div class="card detail-card">
                <div class="card-content">
                    <div class="detail-card-content-part">

                        <h3 class="detail-card-content-part-title">Login</h3>
                        <div class="detail-card-content-part-content">
                            <input type="text" name="login" value="<?= htmlspecialchars($user->getLogin()) ?>" required>
                        </div>

                        <h3 class="detail-card-content-part-title">Mot de passe (laisser vide pour garder le mot de passe actuel)</h3>
                        <div class="detail-card-content-part-content">
                            <input type="text" name="password">
                        </div>

                        <h3 class="detail-card-content-part-title">Rôle</h3>
                        <div class="detail-card-content-part-content select-container">
                            <select id="role" name="role" required>
                                <option value="<?= User::ROLE_ADMIN ?>" <?= $user->getRole() === User::ROLE_ADMIN ? 'selected' : '' ?>><?= User::ROLE_ADMIN ?></option>
                                <option value="<?= User::ROLE_MANAGER ?>" <?= $user->getRole() === User::ROLE_MANAGER ? 'selected' : '' ?>><?= User::ROLE_MANAGER ?></option>
                                <option value="<?= User::ROLE_COACH ?>" <?= $user->getRole() === User::ROLE_COACH ? 'selected' : '' ?>><?= User::ROLE_COACH ?></option>
                            </select>
                        </div>

                        <div id="coach-fields">
                            <h3 class="detail-card-content-part-title">Selectionner la franchise</h3>
                            <div class="detail-card-content-part-content select-container">
                                <select id="franchiseId" name="franchiseId" required>
                                    <option value="" disabled <?= is_null($selectedFranchiseId) ? 'selected' : '' ?>>Sélectionner une franchise</option>
                                    <?php foreach ($franchises as $franchise): ?>
                                        <option value="<?= $franchise->getId() ?>" <?= $user->getFranchiseId() === $franchise->getId() ? 'selected' : '' ?>><?= $franchise->getName() ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>

                            <h3 class="detail-card-content-part-title">Selectionner le coach à associer</h3>
                            <div class="detail-card-content-part-content select-container">
                                <select id="coachId" name="coachId" required>
                                    <option value="" disabled <?= is_null($selectedCoachId) ? 'selected' : '' ?>>Sélectionner un coach</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-button-container">
                <a class="custom-button blue-night small" href="<?= $_SESSION['adminHomeUsersUri'] ?>">Annuler</a>
                <button class="custom-button orange small" type="submit" name="update">Valider</button>
            </div>
        </form>
    </div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var roleSelect = document.getElementById('role');
        var coachFields = document.getElementById('coach-fields');
        var coachInputs = coachFields.querySelectorAll('input, select');

        var franchiseSelect = document.getElementById('franchiseId');
        var coachSelect = document.getElementById('coachId');

        var coachesByFranchise = <?= json_encode($coachesByFranchise) ?>;
        var selectedCoachId = <?= json_encode($selectedCoachId) ?>;

        function toggleCoachFields() {
            if (roleSelect.value === '<?= User::ROLE_COACH ?>') {
                coachFields.style.display = 'block';
                coachInputs.forEach(function (input) {
                    input.disabled = false;
                });
                updateCoachOptions();
            } else {
                coachFields.style.display = 'none';
                coachInputs.forEach(function (input) {
                    input.disabled = true;
                });
            }
        }

        function updateCoachOptions() {
            var selectedFranchise = franchiseSelect.value;
            coachSelect.innerHTML = '<option value="" disabled selected>Sélectionner un coach</option>';
            if (selectedFranchise && coachesByFranchise[selectedFranchise]) {
                coachesByFranchise[selectedFranchise].forEach(function (coach) {
                    var option = document.createElement('option');
                    option.value = coach.id;
                    option.textContent = coach.human.firstName + ' ' + coach.human.lastName + ' - ' + coach.titleTrad;
                    if (selectedCoachId && selectedCoachId == coach.id) {
                        option.selected = true;
                    }
                    coachSelect.appendChild(option);
                });
            }
        }

        toggleCoachFields();
        roleSelect.addEventListener('change', toggleCoachFields);
        franchiseSelect.addEventListener('change', updateCoachOptions);


        if (franchiseSelect.value) {
            updateCoachOptions();
        }
    });
</script>
</body>
</html>
