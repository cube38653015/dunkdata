<?php

/**
 * @var array $coachesByFranchise
 * @var array $franchises
 */

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Ajout d'un nouvel utilisateur</title>
    <link rel="stylesheet" href="/dunkdata/src/views/admin/styles/admin.css">
</head>
<body>
<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/adminNavbar.php'; ?>
</div>
<div class="content">
    <h1>Ajout d'un nouvel utilisateur</h1>
    <div class="update-entity-content">
        <form method="post" action="<?= $_SESSION['adminCreateUserUri'] ?>">
            <input type="hidden" name="action" value="create">

            <div class="card detail-card">
                <div class="card-content">
                    <div class="detail-card-content-part">

                        <h3 class="detail-card-content-part-title">Login</h3>
                        <div class="detail-card-content-part-content">
                            <input type="text" name="login" required><br>
                        </div>

                        <h3 class="detail-card-content-part-title">Mot de passe</h3>
                        <div class="detail-card-content-part-content">
                            <input type="text" name="password" required><br>
                        </div>

                        <h3 class="detail-card-content-part-title">Rôle</h3>
                        <div class="detail-card-content-part-content select-container">
                            <select id="role" name="role" required>
                                <option value="<?= User::ROLE_ADMIN ?>"><?= User::ROLE_ADMIN ?></option>
                                <option value="<?= User::ROLE_MANAGER ?>"><?= User::ROLE_MANAGER ?></option>
                                <option value="<?= User::ROLE_COACH ?>"><?= User::ROLE_COACH ?></option>
                            </select>
                        </div>

                        <div id="coach-fields">
                            <h3 class="detail-card-content-part-title">Selectionner la franchise</h3>
                            <div class="detail-card-content-part-content select-container">
                                <select id="franchiseId" name="franchiseId" required>
                                    <option value="" disabled selected>Sélectionner une franchise</option>
                                    <?php foreach ($franchises as $franchise): ?>
                                        <option value="<?= $franchise->getId() ?>"><?= $franchise->getName() ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>

                            <h3 class="detail-card-content-part-title">Selectionner le coach à associer</h3>
                            <div class="detail-card-content-part-content select-container">
                                <select id="coachId" name="coachId" required>
                                    <option value="" disabled selected>Sélectionner un coach</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-button-container">
                <a class="custom-button blue-night small" href="<?= $_SESSION['adminHomeUsersUri'] ?>">Annuler</a>
                <button class="custom-button orange small" type="submit">Créer</button>
            </div>
        </form>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var roleSelect = document.getElementById('role');
        var coachFields = document.getElementById('coach-fields');
        var coachInputs = coachFields.querySelectorAll('input, select');

        var franchiseSelect = document.getElementById('franchiseId');
        var coachSelect = document.getElementById('coachId');

        var coachesByFranchise = <?= json_encode($coachesByFranchise) ?>;
        console.log(coachesByFranchise);

        function toggleCoachFields() {
            if (roleSelect.value === '<?= User::ROLE_COACH ?>') {
                coachFields.style.display = 'block';
                coachInputs.forEach(function (input) {
                    input.disabled = false;
                });
            } else {
                coachFields.style.display = 'none';
                coachInputs.forEach(function (input) {
                    input.disabled = true;
                });
            }
        }

        function updateCoachOptions() {
            var selectedFranchise = franchiseSelect.value;
            coachSelect.innerHTML = '<option value="" disabled selected>Sélectionner un coach</option>';
            if (selectedFranchise && coachesByFranchise[selectedFranchise]) {
                coachesByFranchise[selectedFranchise].forEach(function (coach) {
                    var option = document.createElement('option');
                    option.value = coach.id;
                    option.textContent = coach.human.firstName + ' ' + coach.human.lastName + ' - ' + coach.titleTrad;
                    coachSelect.appendChild(option);
                });
            }
        }

        toggleCoachFields();
        roleSelect.addEventListener('change', toggleCoachFields);
        franchiseSelect.addEventListener('change', updateCoachOptions);
    });
</script>
</body>
</html>
