<?php
/**
 * @var array $detailedGames
 * @var DetailedGame $detailedGame
 * @var int $coachFranchiseId
 * @var array $detailedGames
 * @var array $detailedGames
 */
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Liste des prochains matchs</title>
    <link rel="stylesheet" href="/dunkdata/src/views/coach/styles/coach.css">
</head>
<body>
<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/coachNavbar.php'; ?>
</div>
<div class="content">
    <h1>Matchs en cours</h1>
    <?php if (empty($detailedGames)): ?>
        <div class="no-data-container">
                <span class="no-data-icon">
                                    <svg class="w-6 h-6 text-gray-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                                         width="24" height="24"
                                         fill="none" viewBox="0 0 24 24">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                          d="m15 9-6 6m0-6 6 6m6-3a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                </svg>
                </span>
            <span class="no-data-message">Il n'y a pas de matchs en cours</span>
        </div>
    <?php endif; ?>
    <?php if (!empty($detailedGames)): ?>
        <div class="card-grid">
            <?php foreach ($detailedGames as $detailedGame): ?>
                <div class="card vs-card clickable">
                    <div class="card-content">
                        <div class="card-date-container">
                            <span><?= $detailedGame->getGame()->getBeginDateTime()->format('d-m-Y') ?></span>
                            <span><?= $detailedGame->getGame()->getBeginDateTime()->format('H:i') ?></span>
                        </div>
                        <div class="card-location-container">
                            <span class="card-location-gym"><?= $detailedGame->getGame()->getPlace()->getGym() ?></span>
                            <span class="card-location-address"><?= $detailedGame->getGame()->getPlace()->getAddress() ?></span>
                            <span class="card-location-city"><?= $detailedGame->getGame()->getPlace()->getCity() ?></span>
                        </div>
                        <div class="vs-card-container">
                            <span class="card-home-franchise-logo">
                                <img class="franchise-logo"
                                     src="/../dunkdata/src/assets/logo/franchise_logos/<?= $detailedGame->getTeamByFranchise($coachFranchiseId)->getTeam()->getFranchise()->getName() ?>.png"
                                     alt="Dunk Data Logo">
                            </span>
                            <span class="card-home-franchise-title card-franchise-title"><?= $detailedGame->getTeamByFranchise($coachFranchiseId)->getTeam()->getFranchise()->getName() ?></span>
                            <span class="vs-card-text">VS</span>
                            <span class="card-away-franchise-logo">
                                <img class="franchise-logo"
                                     src="/../dunkdata/src/assets/logo/franchise_logos/<?= $detailedGame->getOpposingTeam($coachFranchiseId)->getTeam()->getFranchise()->getName() ?>.png"
                                     alt="Dunk Data Logo">
                            </span>
                            <span class="card-away-franchise-title card-franchise-title"><?= $detailedGame->getOpposingTeam($coachFranchiseId)->getTeam()->getFranchise()->getName() ?></span>
                        </div>
                    </div>
                    <div class="card-actions one-action">
                        <form method="post" action="<?= $_SESSION['coachShowNewTeamPlayerFormUri'] ?>" style="display:inline;">
                            <input type="hidden" name="action" value="edit">
                            <input type="hidden" name="teamId"
                                   value="<?= $detailedGame->getTeamByFranchise($coachFranchiseId)->getTeam()->getId() ?>">
                            <button class="main-button card-action-button one-of-one-action" type="submit" name="edit">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                                     width="24" height="24">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L6.832 19.82a4.5 4.5 0 0 1-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 0 1 1.13-1.897L16.863 4.487Zm0 0L19.5 7.125"/>
                                </svg>
                            </button>
                        </form>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>
</body>
</html>
