<?php
/**
 * @var array $detailedTeamPlayers
 * @var array $players
 */
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Compose ton équipe !</title>
    <link rel="stylesheet" href="/dunkdata/src/views/coach/styles/coach.css">
</head>
<body>
<div class="navbar">
    <?php include $_SESSION['dirViews'] . '/navbar/coachNavbar.php'; ?>
</div>
<div class="content">
    <h1>Compose ton équipe !</h1>
    <div>
        <form class="making-team-content" method="post" action="<?= $_SESSION['coachCreateTeamPlayerUri'] ?>">
            <!-- MAKING TEAM CARD-->
            <div class="card detail-card">
                <div class="card-content">

                    <!-- PLAYERS -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Titulaires</h3>
                        <div class="detail-card-content-part-content coach-team-players-container">

                            <div class="role-container">
                                <h4>Pivot </h4>
                                <div class="select-container">
                                    <select name="<?= Player::POST_PIVOT ?>" required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($players as $player): ?>
                                            <option <?= array_key_exists(Player::POST_PIVOT, $detailedTeamPlayers) && $detailedTeamPlayers[Player::POST_PIVOT]->getPlayer()->getId() === $player->getId() ? "selected" : "" ?>
                                                    value="<?= $player->getId() ?>">
                                                <?php echo $player->getHuman()->getFirstName() . ' ' . $player->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>


                            <div class="role-container">
                                <h4>Meneur </h4>
                                <div class="select-container">
                                    <select name="<?= Player::POST_LEADER ?>" required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($players as $player): ?>
                                            <option <?= array_key_exists(Player::POST_LEADER, $detailedTeamPlayers) && $detailedTeamPlayers[Player::POST_LEADER]->getPlayer()->getId() === $player->getId() ? "selected" : "" ?>
                                                    value="<?= $player->getId() ?>">
                                                <?php echo $player->getHuman()->getFirstName() . ' ' . $player->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="role-container">
                                <h4>Ailier </h4>
                                <div class="select-container">
                                    <select name="<?= Player::POST_WINGER ?>" required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($players as $player): ?>
                                            <option <?= array_key_exists(Player::POST_WINGER, $detailedTeamPlayers) && $detailedTeamPlayers[Player::POST_WINGER]->getPlayer()->getId() === $player->getId() ? "selected" : "" ?>
                                                    value="<?= $player->getId() ?>">
                                                <?php echo $player->getHuman()->getFirstName() . ' ' . $player->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="role-container">
                                <h4>Ailier fort </h4>
                                <div class="select-container">
                                    <select name="<?= Player::POST_STRONG_WINGER ?>" required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($players as $player): ?>
                                            <option <?= array_key_exists(Player::POST_STRONG_WINGER, $detailedTeamPlayers) && $detailedTeamPlayers[Player::POST_STRONG_WINGER]->getPlayer()->getId() === $player->getId() ? "selected" : "" ?>
                                                    value="<?= $player->getId() ?>">
                                                <?php echo $player->getHuman()->getFirstName() . ' ' . $player->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="role-container">
                                <h4>Arrière </h4>
                                <div class="select-container">
                                    <select name="<?= Player::POST_BACK ?>" required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                        <?php foreach ($players as $player): ?>
                                            <option <?= array_key_exists(Player::POST_BACK, $detailedTeamPlayers) && $detailedTeamPlayers[Player::POST_BACK]->getPlayer()->getId() === $player->getId() ? "selected" : "" ?>
                                                    value="<?= $player->getId() ?>">
                                                <?php echo $player->getHuman()->getFirstName() . ' ' . $player->getHuman()->getLastName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- CAPTAINS -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Capitaines</h3>
                        <div class="detail-card-content-part-content coach-team-captains-container">

                            <div class="role-container">
                                <h4>Capitaine </h4>
                                <div class="select-container">
                                    <select id="<?= TeamPlayer::TITLE_CAPTAIN ?>" name="<?= TeamPlayer::TITLE_CAPTAIN ?>" required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                    </select>
                                </div>
                            </div>

                            <div class="role-container">
                                <h4>Capitaine Suppléant </h4>
                                <div class="select-container">
                                    <select id="<?= TeamPlayer::TITLE_ALTERNATE_CAPTAIN ?>" name="<?= TeamPlayer::TITLE_ALTERNATE_CAPTAIN ?>"
                                            required>
                                        <option value="" disabled selected>Sélectionner un joueur</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- SUBSTITUTES -->
                    <div class="detail-card-content-part">
                        <h3 class="detail-card-content-part-title">Remplaçants</h3>
                        <div class="detail-card-content-part-content">
                            <div class="coach-team-substitutes-container" id="substitutes-container">
                                <?php foreach ($detailedTeamPlayers[TeamPlayer::POST_SUBSTITUTE] as $playerId => $substitute): ?>
                                    <div class="substitute-entry">
                                        <div class="select-container">
                                            <select name="substitutes[]" required>
                                                <option value="" disabled selected>Sélectionner un joueur</option>
                                                <?php foreach ($players as $player): ?>
                                                    <option <?= $playerId === $player->getId() ? "selected" : "" ?> value="<?= $player->getId() ?>">
                                                        <?php echo $player->getHuman()->getFirstName() . ' ' . $player->getHuman()->getLastName(); ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="remove-button remove-substitute">
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                 stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                      d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                            </svg>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="coach-add-substitute-button" id="add-substitute">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                                     class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                </svg>
                                <span>Ajouter un remplacement</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- FORM BUTTONS -->
            <div class="form-button-container">
                <a class="custom-button blue-night small" href="<?= $_SESSION['coachHomeUri'] ?>">Annuler</a>
                <button class="custom-button orange small" type="submit" name="createTeamPlayer">Sauvegarder</button>
            </div>
        </form>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var playerOptions = `<option value="" disabled selected>Sélectionner un joueur</option>`; // Option vide par défaut
        playerOptions += `<?php
        foreach ($players as $player) {
            echo '<option value="' . $player->getId() . '">' . $player->getHuman()->getFirstName() . ' ' . $player->getHuman()->getLastName() . '</option>';
        }
        ?>`;

        var currentCaptain = "<?= isset($detailedTeamPlayers[TeamPlayer::TITLE_CAPTAIN]) ? $detailedTeamPlayers[TeamPlayer::TITLE_CAPTAIN]->getPlayer()->getId() : '' ?>";
        var currentAlternateCaptain = "<?= isset($detailedTeamPlayers[TeamPlayer::TITLE_ALTERNATE_CAPTAIN]) ? $detailedTeamPlayers[TeamPlayer::TITLE_ALTERNATE_CAPTAIN]->getPlayer()->getId() : '' ?>";

        // Fonction pour obtenir les joueurs sélectionnés dans les positions principales et les remplaçants
        function getSelectedMainAndSubstitutePlayers() {
            const selectedPlayers = [];

            // Inclure les joueurs des positions principales
            document.querySelectorAll('select[name^="<?=Player::POST_PIVOT?>"], select[name^="<?=Player::POST_LEADER?>"], select[name^="<?=Player::POST_WINGER?>"], select[name^="<?=Player::POST_STRONG_WINGER?>"], select[name^="<?=Player::POST_BACK?>"]').forEach(select => {
                if (select.value) {
                    selectedPlayers.push({
                        id: select.value,
                        text: select.options[select.selectedIndex].text
                    });
                }
            });

            // Inclure les joueurs remplaçants
            document.querySelectorAll('select[name="substitutes[]"]').forEach(select => {
                if (select.value) {
                    selectedPlayers.push({
                        id: select.value,
                        text: select.options[select.selectedIndex].text
                    });
                }
            });

            return selectedPlayers;
        }

        // Fonction pour mettre à jour les options disponibles dans les selects de Capitaine et Capitaine Suppléant
        function updateCaptainOptions() {
            const selectedPlayers = getSelectedMainAndSubstitutePlayers();
            const captainSelect = document.getElementById('<?=TeamPlayer::TITLE_CAPTAIN?>');
            const alternateCaptainSelect = document.getElementById('<?=TeamPlayer::TITLE_ALTERNATE_CAPTAIN?>');

            // Réinitialiser les options avec l'option par défaut désactivée
            captainSelect.innerHTML = '<option value="" disabled selected>Sélectionner un joueur</option>';
            alternateCaptainSelect.innerHTML = '<option value="" disabled selected>Sélectionner un joueur</option>';

            selectedPlayers.forEach(player => {
                const isSelectedCaptain = player.id === currentCaptain;
                const isSelectedAlternateCaptain = player.id === currentAlternateCaptain;

                // Ajouter les options au Capitaine
                captainSelect.innerHTML += `<option value="${player.id}" ${isSelectedCaptain ? 'selected' : ''} ${isSelectedAlternateCaptain ? 'disabled' : ''}>${player.text}</option>`;

                // Ajouter les options au Capitaine Suppléant
                alternateCaptainSelect.innerHTML += `<option value="${player.id}" ${isSelectedAlternateCaptain ? 'selected' : ''} ${isSelectedCaptain ? 'disabled' : ''}>${player.text}</option>`;
            });
        }

        // Fonction pour mettre à jour les sélections de Capitaine et Capitaine Suppléant
        function updateCaptainSelection() {
            const captainSelect = document.getElementById('<?=TeamPlayer::TITLE_CAPTAIN?>');
            const alternateCaptainSelect = document.getElementById('<?=TeamPlayer::TITLE_ALTERNATE_CAPTAIN?>');

            // Désactiver l'option de l'autre select
            captainSelect.querySelectorAll('option').forEach(option => {
                option.disabled = option.value && option.value === alternateCaptainSelect.value;
            });

            alternateCaptainSelect.querySelectorAll('option').forEach(option => {
                option.disabled = option.value && option.value === captainSelect.value;
            });

            // Forcer la désactivation de l'option par défaut après la mise à jour
            captainSelect.querySelector('option[value=""]').disabled = true;
            alternateCaptainSelect.querySelector('option[value=""]').disabled = true;
        }

        // Fonction pour obtenir les joueurs sélectionnés (pour désactivation)
        function getSelectedPlayers() {
            const selectedPlayers = new Set();
            document.querySelectorAll('select[name^="substitutes"], select[name^="<?=Player::POST_PIVOT?>"], select[name^="<?=Player::POST_LEADER?>"], select[name^="<?=Player::POST_WINGER?>"], select[name^="<?=Player::POST_STRONG_WINGER?>"], select[name^="<?=Player::POST_BACK?>"]').forEach(select => {
                if (select.value) {
                    selectedPlayers.add(select.value);
                }
            });
            return selectedPlayers;
        }

        // Fonction pour désactiver les joueurs déjà sélectionnés dans d'autres sélections
        function updateSelectOptions() {
            const selectedPlayers = getSelectedPlayers();
            document.querySelectorAll('select[name^="substitutes"], select[name^="<?=Player::POST_PIVOT?>"], select[name^="<?=Player::POST_LEADER?>"], select[name^="<?=Player::POST_WINGER?>"], select[name^="<?=Player::POST_STRONG_WINGER?>"], select[name^="<?=Player::POST_BACK?>"]').forEach(select => {
                const currentSelection = select.value;
                const options = select.querySelectorAll('option');
                options.forEach(option => {
                    if (selectedPlayers.has(option.value) && option.value !== currentSelection) {
                        option.disabled = true; // Désactiver les options sélectionnées ailleurs
                    } else {
                        option.disabled = false; // Réactiver les options non sélectionnées
                    }
                });

                // Forcer la désactivation de l'option par défaut après chaque mise à jour
                select.querySelector('option[value=""]').disabled = true;
            });

            updateCaptainOptions(); // Mettre à jour les options des capitaines
            updateCaptainSelection(); // Mettre à jour la désactivation entre les deux sélecteurs de Capitaine
        }

        // Fonction pour attacher l'événement de suppression
        function attachRemoveEvent(button) {
            button.addEventListener('click', function () {
                var entry = this.closest('.substitute-entry');
                entry.parentNode.removeChild(entry);
                updateSelectOptions(); // Mettre à jour les options après suppression
            });
        }

        // Attacher l'événement de suppression aux boutons existants au chargement de la page
        document.querySelectorAll('.remove-substitute').forEach(function (button) {
            attachRemoveEvent(button);
        });

        // Fonction pour ajouter un nouveau substitut avec mise à jour des options
        function addSubstitute() {
            var container = document.getElementById('substitutes-container');
            var newDiv = document.createElement('div');
            newDiv.classList.add('substitute-entry');

            var newSelectContainer = document.createElement('div');
            newSelectContainer.classList.add('select-container');

            var newSelect = document.createElement('select');
            newSelect.name = 'substitutes[]';
            newSelect.required = true;
            newSelect.innerHTML = playerOptions;
            newSelect.addEventListener('change', updateSelectOptions); // Mettre à jour les options lors du changement

            var removeButton = document.createElement('div');
            removeButton.classList.add('remove-button');
            removeButton.classList.add('remove-substitute');
            removeButton.innerHTML = `
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                         stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round"
                              d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                    </svg>
            `;

            // Attacher l'événement de suppression au nouveau bouton
            attachRemoveEvent(removeButton);

            newSelectContainer.appendChild(newSelect)
            newDiv.appendChild(newSelectContainer);
            newDiv.appendChild(removeButton);

            container.appendChild(newDiv); // Ajout à la fin du conteneur
            updateSelectOptions(); // Mise à jour après ajout
        }

        // Attacher l'événement de clic au bouton d'ajout de substitut
        document.getElementById('add-substitute').addEventListener('click', addSubstitute);

        // Attacher l'événement de changement à tous les selects de positions principales
        document.querySelectorAll('select[name^="<?=Player::POST_PIVOT?>"], select[name^="<?=Player::POST_LEADER?>"], select[name^="<?=Player::POST_WINGER?>"], select[name^="<?=Player::POST_STRONG_WINGER?>"], select[name^="<?=Player::POST_BACK?>"]').forEach(select => {
            select.addEventListener('change', updateSelectOptions);
        });

        // Attacher les événements de changement pour Captain et Alternate Captain
        document.getElementById('<?=TeamPlayer::TITLE_CAPTAIN?>').addEventListener('change', updateCaptainSelection);
        document.getElementById('<?=TeamPlayer::TITLE_ALTERNATE_CAPTAIN?>').addEventListener('change', updateCaptainSelection);

        // Initialisation des options
        updateSelectOptions();
    });
</script>
</body>
</html>
