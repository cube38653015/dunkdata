<?php
include_once __DIR__ . '/../../config/connexion-bdd.php';
include_once __DIR__ . '/../services/GameService.php';
include_once __DIR__ . '/../services/PlaceService.php';
include_once __DIR__ . '/../services/FranchiseService.php';
include_once __DIR__ . '/../services/ArbitratorService.php';
include_once __DIR__ . '/../services/PlayerService.php';
include_once __DIR__ . '/../services/CoachService.php';
include_once __DIR__ . '/../services/UserService.php';
include_once __DIR__ . '/../services/CoachService.php';
include_once __DIR__ . '/../services/AdminService.php';
include_once __DIR__ . '/../services/TeamPlayerService.php';

class AdminController
{
    private GameService $gameService;
    private PlaceService $placeService;
    private FranchiseService $franchiseService;
    private ArbitratorService $arbitratorService;
    private PlayerService $playerService;
    private UserService $userService;
    private CoachService $coachService;
    private AdminService $adminService;
    private ManagerController $managerController;
    private TeamPlayerService $teamPlayerService;

    public function __construct() {
        $this->placeService = new PlaceService();
        $this->gameService = new GameService();
        $this->franchiseService = new FranchiseService();
        $this->arbitratorService = new ArbitratorService();
        $this->playerService = new PlayerService();
        $this->userService = new UserService();
        $this->coachService = new CoachService();
        $this->adminService = new AdminService();
        $this->managerController = new ManagerController();
        $this->teamPlayerService = new TeamPlayerService();
    }

    public function showHome(): void
    {
        header('Location: ' . $_SESSION['adminHomeUri']);
    }

    public function historyGames(): void
    {
        $detailedGames = $this->gameService->getDetailedHistoryGames();

        include $_SESSION['dirViews'] . '/admin/games/historyGames.php';
    }

    public function gamesInProgress(): void
    {
        $detailedGames = $this->gameService->getDetailedGamesInProgress();
        include $_SESSION['dirViews'] . '/../views/admin/games/gamesInProgress.php';
    }

    // Users

    public function homeUsers(): void
    {
        $users = $this->userService->getAllUser();
        include $_SESSION['dirViews'] . '/../views/admin/users/homeUsers.php';
    }

    public function showNewUserForm(): void
    {
        $franchises = $this->franchiseService->getAllFranchises();
        $coachesByFranchise = $this->adminService->getCoachesByFranchise();
        include $_SESSION['dirViews'] . '/admin/users/newUserForm.php';
    }

    public function createUser($data): void
    {
        if(!$this->userService->create($data)) {
            echo "Error creating record.";
        }
        header('Location: ' . $_SESSION['adminHomeUsersUri']);
    }

    public function updateUserForm(int $userId): void
    {
        $user = $this->userService->get($userId);
        $franchises = $this->franchiseService->getAllFranchises();
        $coachesByFranchise = $this->adminService->getCoachesByFranchise();
        $selectedFranchiseId = $user->getFranchiseId();
        $selectedCoachId = $user->getCoachId();
        include $_SESSION['dirViews'] . '/admin/users/updateUserForm.php';
    }

    public function updateUser(array $data): void
    {
        $this->userService->update($data);
        header('Location: ' . $_SESSION['adminHomeUsersUri']);
    }

    public function deleteUser(array $data): void
    {
        if (!$this->userService->delete($data)) {
            echo "Error deleting record.";
        }
        header('Location: ' . $_SESSION['adminHomeUsersUri']);
    }

    // Franchises

    public function homeFranchises(): void
    {
        $franchises = $this->franchiseService->getAllFranchises();
        include $_SESSION['dirViews'] . '/../views/admin/franchises/homeFranchises.php';
    }

    public function homeFranchisePlayers($franchiseId): void
    {
        $franchise = $this->franchiseService->getFranchise($franchiseId);
        $players = $this->playerService->getActualPlayersByFranchise($franchiseId);
        $otherPlayers = $this->playerService->getOtherPlayers($franchiseId);
        $_SESSION['franchiseId'] = $franchise->getId();
        include $_SESSION['dirViews'] . '/../views/admin/franchises/homeFranchisePlayers.php';
    }

    public function homeFranchiseCoaches($franchiseId): void
    {
        $franchise = $this->franchiseService->getFranchise($franchiseId);
        $coaches = $this->coachService->getActualCoachesByFranchise($franchiseId);
        $mainCoaches = $coaches['actualMainCoaches'];
        $assistantCoaches = $coaches['actualAssistantCoaches'];
        $otherCoaches = $this->coachService->getOtherCoaches($franchiseId);
        include $_SESSION['dirViews'] . '/../views/admin/franchises/homeFranchiseCoaches.php';
    }

    public function showNewFranchiseForm(): void
    {
        include $_SESSION['dirViews'] . '/admin/franchises/newFranchiseForm.php';
    }

    public function createFranchise($data): void
    {
        if(!$this->franchiseService->create($data)) {
            echo "Error creating record.";
        }
        header('Location: ' . $_SESSION['adminHomeFranchisesUri']);
    }

    public function showUpdateFranchiseForm(int $franchiseId): void
    {
        $franchise = $this->franchiseService->getFranchise($franchiseId);
        $coaches = $this->coachService->getCoachesByFranchise($franchiseId);
        $mainCoach = $coaches[Coach::TITLE_MAIN_COACH];
        $assistantCoach = $coaches[Coach::TITLE_ASSISTANT_COACH];
        $players = $this->playerService->getActualPlayersByFranchise($franchiseId);
        include $_SESSION['dirViews'] . '/admin/franchises/updateFranchiseForm.php';
    }

    public function updateFranchise(array $data): void
    {
        $this->franchiseService->update($data);
        header('Location: ' . $_SESSION['adminHomeFranchisesUri']);
    }

    public function showUpdatePlayerForm(int $playerId): void
    {
        $player = $this->playerService->getPlayer($playerId);
        $franchiseId = $_SESSION['franchiseId'];
        include $_SESSION['dirViews'] . '/admin/franchises/updatePlayerForm.php';
    }

    public function updatePlayer(array $data): void
    {
        $this->playerService->update($data);
        header('Location: ' . $_SESSION['adminHomeFranchisePlayersUri']);
    }

    public function deletePlayer(array $data): void
    {
        $this->playerService->delete($data);
        header('Location: ' . $_SESSION['adminHomeFranchisePlayersUri']);
    }

    public function updateFranchisePlayers(array $data): void
    {
        $this->playerService->updateFranchisePlayers($data);
        header('Location: ' . $_SESSION['adminHomeFranchisePlayersUri']);
    }

    public function showUpdateCoachForm(int $coachId): void
    {
        $coach = $this->coachService->get($coachId);
        $franchiseId = $_SESSION['franchiseId'];
        include $_SESSION['dirViews'] . '/admin/franchises/updateCoachForm.php';
    }

    public function updateCoach(array $data): void
    {
        $this->coachService->update($data);
        header('Location: ' . $_SESSION['adminHomeFranchiseCoachesUri']);
    }

    public function deleteCoach(int $coachId): void
    {
        $this->coachService->delete($coachId);
        header('Location: ' . $_SESSION['adminHomeFranchiseCoachesUri']);
    }

    public function updateFranchiseCoaches(array $data): void
    {
        $this->coachService->updateFranchiseCoaches($data);
        header('Location: ' . $_SESSION['adminHomeFranchiseCoachesUri']);
    }

    public function deleteFranchise(int $franchise): void
    {
        $this->franchiseService->deleteFranchise($franchise);
        header('Location: ' . $_SESSION['adminHomeFranchisesUri']);
    }

    // Arbitrators

    public function homeArbitrators(): void
    {
        $arbitrators = $this->arbitratorService->getAllArbitrators();
        include $_SESSION['dirViews'] . '/../views/admin/arbitrators/homeArbitrators.php';
    }

    public function showNewArbitratorForm(): void
    {
        include $_SESSION['dirViews'] . '/admin/arbitrators/newArbitratorForm.php';
    }

    public function createArbitrator($data): void
    {
        if(!$this->arbitratorService->create($data)) {
            echo "Error creating record.";
        }
        header('Location: ' . $_SESSION['adminHomeArbitratorsUri']);
    }

    public function showUpdateArbitratorForm(int $arbitratorId): void
    {
        $arbitrator = $this->arbitratorService->get($arbitratorId);
        include $_SESSION['dirViews'] . '/admin/arbitrators/updateArbitratorForm.php';
    }

    public function updateArbitrator(array $data): void
    {
        $this->arbitratorService->update($data);
        header('Location: ' . $_SESSION['adminHomeArbitratorsUri']);
    }

    public function deleteArbitrator(array $data): void
    {
        if (!$this->arbitratorService->delete($data)) {
            echo "Error deleting record.";
        }
        header('Location: ' . $_SESSION['adminHomeArbitratorsUri']);
    }

    // Games

    public function homeGames(): void
    {
        include $_SESSION['dirViews'] . '/../views/admin/games/homeGames.php';
    }

    public function showNewGameForm(): void
    {
        $places = $this->placeService->getAllPlaces();
        $franchises = $this->franchiseService->getAllActiveFranchises();
        $arbitrators = $this->arbitratorService->getAllArbitrators();
        include $_SESSION['dirViews'] . '/admin/games/newGameForm.php';
    }

    public function createGame(array $data): void
    {
        $game = $this->gameService->create($data);
        if(!$game) {
            echo "Error creating record.";
        }
        header('Location: ' . $_SESSION['adminGamesInProgressUri']);
    }

    public function updateShowGameInProgressForm(int $gameId): void
    {
        $detailedGame = $this->gameService->getDetailedGame($gameId);
        $places = $this->placeService->getAllPlaces();
        $gameArbitrators = $this->arbitratorService->getAllByGame($gameId);
        $arbitrators = $this->arbitratorService->getAllArbitrators();
        $franchises = $this->franchiseService->getAllFranchises();

        $homeFranchiseId = $detailedGame->getHomeDetailedTeam()->getTeam()->getFranchise()->getId();
        $awayFranchiseId = $detailedGame->getAwayDetailedTeam()->getTeam()->getFranchise()->getId();
        $homeTeamId = $detailedGame->getHomeDetailedTeam()->getTeam()->getId();
        $awayTeamId = $detailedGame->getAwayDetailedTeam()->getTeam()->getId();

        $homePlayers = $this->playerService->getActualPlayersByFranchise($homeFranchiseId);
        $awayPlayers = $this->playerService->getActualPlayersByFranchise($awayFranchiseId);
        $homeTeamPlayers = $this->playerService->getTeamPlayersByTeam($homeTeamId);
        $awayTeamPlayers = $this->playerService->getTeamPlayersByTeam($awayTeamId);

        $detailedHomeTeamPlayers = $this->teamPlayerService->getDetailedTeamPlayer($homeTeamPlayers);
        $detailedAwayTeamPlayers = $this->teamPlayerService->getDetailedTeamPlayer($awayTeamPlayers);

        include $_SESSION['dirViews'] . '/admin/games/updateGameInProgressForm.php';
    }

    public function updateGameInProgress(array $data): bool
    {
        $gameId = $data['gameId'];
        $this->gameService->update($data);
        $this->arbitratorService->deleteByGame($gameId);
        $this->arbitratorService->createForGame($gameId, $data['arbitratorHolderId'], Arbitrator::TITLE_HOLDER);
        $this->arbitratorService->createForGame($gameId, $data['arbitratorAssistant1Id'], Arbitrator::TITLE_ASSISTANT);
        $this->arbitratorService->createForGame($gameId, $data['arbitratorAssistant2Id'], Arbitrator::TITLE_ASSISTANT);
        $this->teamPlayerService->updateTeamPlayers($data);

        header('Location: ' . $_SESSION['adminGamesInProgressUri']);
        return true;
    }

    public function deleteGameInProgress(array $data): bool
    {
        $this->gameService->delete($data['gameId']);
        header('Location: ' . $_SESSION['adminGamesInProgressUri']);
        return true;
    }

    public function updateOrClosingGame(array $data): bool
    {
        $this->gameService->updateOrClosingGame($data);
        header('Location: ' . $_SESSION['adminHistoryGamesUri']);
        return true;
    }

    public function showUpdateHistoryGameForm(int $gameId): void
    {
        $this->gameService->renderGameForm($gameId, '/admin/games/historyGameForm.php');
    }

    public function deleteGameClosed(array $data): bool
    {
        $this->gameService->delete($data['gameId']);
        header('Location: ' . $_SESSION['adminHistoryGamesUri']);
        return true;
    }

    public function downloadGameReport(array $data): void
    {
        $this->gameService->downloadGameReport($data['gameId']);
    }
}
