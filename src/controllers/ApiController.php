<?php
include_once __DIR__ . '/../services/GameService.php';

class ApiController
{
    private GameService $gameService;

    public function __construct() {
        $this->gameService = new GameService();
    }

    public function getHistoryGames(): void
    {
        $detailedGames = $this->gameService->getDetailedHistoryGames();
        echo json_encode($detailedGames);
    }

    public function getGamesInProgress(): void
    {
        $detailedGames = $this->gameService->getDetailedGamesInProgress();
        echo json_encode($detailedGames);
    }
}