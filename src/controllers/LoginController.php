<?php
include_once __DIR__ . '/../services/LoginService.php';
include_once __DIR__ . '/../controllers/CoachController.php';
include_once __DIR__ . '/../controllers/ManagerController.php';
include_once __DIR__ . '/../controllers/AdminController.php';


class LoginController {
    private LoginService $loginService;

    public function __construct() {
        $this->loginService = new LoginService();
    }

    public function showLoginPage(): void
    {
        include $_SESSION['dirViews'] . '/login/login.php';
    }

    public function checkUser(array $data): void
    {
        $login = $data['login'];
        $password = $data['password'];

        if ($this->loginService->checkUser($login, $password)) {
            header('Location: ' . $_SESSION['loginSuccessUri']);
        } else {
            $this->logout();
        }
    }

    public function logout(): void
    {
        $this->resetSession();
        header('Location: ' . $_SESSION['loginUri']);
    }

    /**
     * @return void
     */
    public function resetSession(): void
    {
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }
        session_destroy();
    }
}
