<?php
include_once __DIR__ . '/../services/CoachService.php';
include_once __DIR__ . '/../services/GameService.php';
include_once __DIR__ . '/../services/PlayerService.php';
include_once __DIR__ . '/../services/TeamPlayerService.php';

class CoachController
{
    private GameService $gameService;
    private PlayerService $playerService;
    private TeamPlayerService $teamPlayerService;

    public function __construct() {
        $this->gameService = new GameService();
        $this->playerService = new PlayerService();
        $this->teamPlayerService = new TeamPlayerService();
    }

    public function showGames(): void
    {
        $coach = $_SESSION['coach'];
        $coachFranchiseId = $coach->getFranchise()->getId();
        $detailedGames = $this->gameService->getDetailedGamesInProgressByFranchise($coachFranchiseId);
        include $_SESSION['dirViews'] . '/coach/showGames.php';
    }

    public function newTeamPlayerForm($teamId): void
    {
        $coach = $_SESSION['coach'];
        $coachFranchiseId = $coach->getFranchise()->getId();
        $players = $this->playerService->getActualPlayersByFranchise($coachFranchiseId);
        $teamPlayers = $this->playerService->getTeamPlayersByTeam($teamId);
        $detailedTeamPlayers = $this->teamPlayerService->getDetailedTeamPlayer($teamPlayers);
        include $_SESSION['dirViews'] . '/coach/newTeamPlayerForm.php';
    }

    public function createTeamPlayer(array $data): void
    {
        $teamId = $_SESSION['teamId'];
        if ($this->teamPlayerService->deleteAllTeamPlayerByTeam($teamId) && $this->teamPlayerService->createTeamPlayer($data)) {
            $this->showHome();
        }
    }

    public function showHome(): void
    {
        header('Location: ' . $_SESSION['coachHomeUri']);
    }

}