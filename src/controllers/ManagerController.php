<?php
include_once __DIR__ . '/../../config/connexion-bdd.php';
include_once __DIR__ . '/../services/GameService.php';
include_once __DIR__ . '/../services/PlaceService.php';
include_once __DIR__ . '/../services/FranchiseService.php';
include_once __DIR__ . '/../services/ArbitratorService.php';
include_once __DIR__ . '/../services/CoachService.php';

class ManagerController
{
    private GameService $gameService;
    private PlaceService $placeService;
    private FranchiseService $franchiseService;
    private ArbitratorService $arbitratorService;

    public function __construct() {
        $this->gameService = new GameService();
        $this->placeService = new PlaceService();
        $this->franchiseService = new FranchiseService();
        $this->arbitratorService = new ArbitratorService();
    }

    public function showHome(): void
    {
        header('Location: ' . $_SESSION['managerHomeUri']);
    }

    public function newGameForm(): void
    {
        $places = $this->placeService->getAllPlaces();
        $franchises = $this->franchiseService->getAllActiveFranchises();
        $arbitrators = $this->arbitratorService->getAllArbitrators();
        include $_SESSION['dirViews'] . '/manager/newGameForm.php';
    }

    public function createGame(array $data): void
    {
        $this->gameService->create($data);

        header('Location: ' . $_SESSION['managerGamesInProgressUri']);
    }

    public function gamesInProgress(): void
    {
        $detailedGames = $this->gameService->getDetailedGamesInProgress();

        include $_SESSION['dirViews'] . '/manager/gamesInProgress.php';
    }

    public function closingGameForm(int $gameId): void
    {
        $this->gameService->renderGameForm($gameId, '/manager/closingGameForm.php');
    }

    public function historyGames(): void
    {
        $detailedGames = $this->gameService->getDetailedHistoryGames();

        include $_SESSION['dirViews'] . '/manager/historyGames.php';
    }

    public function historyGameForm(int $gameId): void
    {
        $this->gameService->renderGameForm($gameId, '/manager/historyGameForm.php');
    }

    public function updateOrClosingGame(array $data): void
    {
        $this->gameService->updateOrClosingGame($data);
        $game = $this->gameService->getGame($data['gameId']);
        if ($game->isClosed()) {
            header('Location: ' . $_SESSION['managerGamesInProgressUri']);
        } else {
            header('Location: ' . $_SESSION['managerHistoryGamesUri']);
        }
    }

    public function deleteGameInProgress(array $data): void
    {
        $this->gameService->delete($data['gameId']);
        header('Location: ' . $_SESSION['managerGamesInProgressUri']);
    }

    public function deleteGameClosed(array $data): void
    {
        $this->gameService->delete($data['gameId']);
        header('Location: ' . $_SESSION['managerHistoryGamesUri']);
    }

    public function downloadGameReport(array $data): void
    {
        $this->gameService->downloadGameReport($data['gameId']);
        header('Location: ' . $_SESSION['managerHistoryGamesUri']);
    }
}